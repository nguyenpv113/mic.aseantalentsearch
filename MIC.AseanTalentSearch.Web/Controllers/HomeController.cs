﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.Data;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly AseanTalentSearchContext _context;
        private ConfigTable _configTable;

        public HomeController(ConfigTable configTable,AseanTalentSearchContext context )
        {
            this._configTable = configTable;
            _context = context;
        }

        public IActionResult Index()
        {
            ViewBag.JobCount = _context.PositionJob.Where(x => x.JobStatus.Value).Count();
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewBag.Address = _configTable.GetValue("address").Replace("<p>", "").Replace("</p>", "");
            ViewBag.Email = _configTable.GetValue("email").Replace("<p>", "").Replace("</p>", "");
            ViewBag.Phone = _configTable.GetValue("phone").Replace("<p>", "").Replace("</p>", "");

            return View();
        }

        public IActionResult Policy()
        {
            ViewBag.Policy = _configTable.GetValue("policy");
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
