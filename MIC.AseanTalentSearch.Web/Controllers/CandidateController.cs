using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Infrastructure;
using MIC.AseanTalentSearch.Web.Models;
using MIC.AseanTalentSearch.Web.Models.Candidate;
using MIC.AseanTalentSearch.Web.Services;
using MIC.AseanTalentSearch.Web.Settings;
using Remotion.Linq.Clauses;
using EducationHistory = MIC.AseanTalentSearch.Web.Models.Candidate.EducationHistory;
using EmploymentHistory = MIC.AseanTalentSearch.Web.Models.Candidate.EmploymentHistory;
using Language = MIC.AseanTalentSearch.Web.Models.Candidate.Language;
using Reference = MIC.AseanTalentSearch.Web.Models.Candidate.Reference;
using Sakura.AspNetCore;
using MIC.AseanTalentSearch.Web.Models.ManageViewModels;

namespace MIC.AseanTalentSearch.Web.Controllers
{
    [Authorize(Roles = "Candidate")]
    public class CandidateController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly AseanTalentSearchContext _context;
        private readonly PathUpLoad _pathUpLoad;
        private readonly IImageService _imageService;
        private IHostingEnvironment _env;
        private readonly ILogger _logger;

        public CandidateController(UserManager<ApplicationUser> userManager, AseanTalentSearchContext context, IOptions<PathUpLoad> pathUpload, IImageService imageService, IHostingEnvironment env, SignInManager<ApplicationUser> signInManager, ILoggerFactory loggerFactory)
        {
            _userManager = userManager;
            _context = context;
            _imageService = imageService;
            _env = env;
            _signInManager = signInManager;
            _pathUpLoad = pathUpload.Value;
            _logger = loggerFactory.CreateLogger<ManageController>();
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Profile()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var candidate = _context.Candidate.Include(m => m.IdNavigation).ThenInclude(m => m.GeneralProfile).SingleOrDefault(m => m.Id == user.Id);
            return View(candidate);
        }

        public async Task<IActionResult> ChangePassword()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var model = new ChangePasswordViewModel();
            model.Candidate = _context.Candidate.Include(m => m.IdNavigation).ThenInclude(m => m.GeneralProfile).SingleOrDefault(m => m.Id == user.Id);
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            model.Candidate = _context.Candidate.Include(m => m.IdNavigation).ThenInclude(m => m.GeneralProfile).SingleOrDefault(m => m.Id == user.Id);
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (user != null)
            {
                var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3, "User changed their password successfully.");
                    TempData["message"] = "User changed their password successfully.";
                    return View(model);
                    //return Json(new { success = true, url = Url.Action("ChangePassword", "Candidate") });
                }
                AddErrors(result);
                return View(model);
            }
            return View(model);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        public IActionResult PersonalInformation()
        {
            return ViewComponent("PersonalInformation");
        }

        public async Task<IActionResult> Summary()
        {
            var model = new Sumary();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user != null)
            {
                var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    model.Text = candidate.Summary;
                }
            }
            return PartialView("~/Views/Shared/Components/Candidate/CandidateSumary.cshtml", model);
        }

        public async Task<IActionResult> Skill()
        {
            var model = new List<CandidateSkill>();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user != null)
            {
                var candidate = _context.Candidate.Include(m => m.CandidateSkill).ThenInclude(m => m.Skill).SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    if (candidate.CandidateSkill != null && candidate.CandidateSkill.Any())
                    {
                        foreach (var candidateSkill in candidate.CandidateSkill)
                        {
                            model.Add(candidateSkill);
                        }
                    }
                }
            }
            return PartialView("~/Views/Shared/Components/Candidate/CandidateSkill.cshtml", model);
        }

        public async Task<IActionResult> CandidateLanguage()
        {
            var model = new List<Models.Candidate.Language>();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user != null)
            {
                var candidate = _context.Candidate.Include(m => m.CandidateLanguage).ThenInclude(m => m.Language).SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    if (candidate.CandidateLanguage != null && candidate.CandidateLanguage.Any())
                    {
                        foreach (var candidateLanguage in candidate.CandidateLanguage)
                        {
                            model.Add(new Models.Candidate.Language
                            {
                                Id = candidateLanguage.Id,
                                LanguageName = candidateLanguage.Language.Name,
                                Certification = candidateLanguage.Certification,
                                LanguageId = candidateLanguage.LanguageId,
                                ScoreOrLevel = candidateLanguage.ScoreOrLevel
                            });
                        }
                    }
                }
            }
            return PartialView("~/Views/Shared/Components/Candidate/CandidateLanguage.cshtml", model);
        }

        public IActionResult EducationHistory()
        {

            return ViewComponent("EducationHistory");
        }

        public async Task<IActionResult> EmployerHistory()
        {
            var model = new List<EmploymentHistory>();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user != null)
            {
                var candidate = _context.Candidate.Include(m => m.EmploymentHistory).SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    if (candidate.EmploymentHistory != null && candidate.EmploymentHistory.Any())
                    {
                        foreach (var employmentHistory in candidate.EmploymentHistory)
                        {
                            model.Add(new EmploymentHistory
                            {
                                Company = employmentHistory.CompanyName,
                                Position = employmentHistory.PositionTitle,
                                From = employmentHistory.StartedDate.ToString("MM/yyyy"),
                                To = employmentHistory.EndedDate?.ToString("MM/yyyy"),
                                IsCurrentJob = employmentHistory.IsCurrentJob,
                                Id = employmentHistory.Id,
                                Description = employmentHistory.Description
                            });
                        }
                    }
                }
            }
            return PartialView("~/Views/Shared/Components/Candidate/EmploymentHistory.cshtml", model);
        }

        public IActionResult CandidateReference()
        {
            return ViewComponent("CandidateReference");
        }
        public IActionResult CandidatePreference()
        {
            return ViewComponent("WorkingPreference");
        }

        public IActionResult CandidateSocialNetwork()
        {
            return ViewComponent("CandidateSocialNetwork");
        }
        public async Task<IActionResult> UpdateInfomation()
        {
            var model = new PersonalInformation();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user != null)
            {
                var profile = _context.GeneralProfile.SingleOrDefault(m => m.Id == user.Id);
                var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    model.Email = candidate.Email;
                    model.Phone = candidate.PhoneNumber;
                    model.YearsOfExperience = candidate.YearsOfExperience;
                }

                if (profile != null)
                {
                    model.FirstName = profile.FirstName;
                    model.LastName = profile.LastName;
                    model.DateOfBirth = profile.BirthDate?.ToString("dd/MM/yyyy");
                    model.Nationality = profile.Nationality;
                    model.Gender = profile.Gender;
                    model.MaritalStatusValue = profile.MaritalStatus;
                    model.Country = profile.HomeCountry;
                    model.HomeCity = profile.HomeCity;
                    model.HomeDistrict = profile.HomeDistrict;
                    model.Address = profile.HomeAddress;
                    SetValueForModel(model, profile);
                }
            }

            return PartialView("_UpdateInfomation", model);
        }

        public PersonalInformation SetValueForModel(PersonalInformation model, GeneralProfile profile)
        {
            model.Countries =
                   new SelectList(_context.Location.Where(m => m.Type == 1)
                           .OrderBy(m => m.Name)
                           .Select(m => new { Value = m.Id, Text = m.Name }).ToList(), "Value", "Text");
            if (profile.LocationId != null)
            {
                if (profile.HomeDistrict != null)
                {//location district
                    var districk = _context.Location.Find(profile.LocationId).ParentId;
                    model.HomeDictricts =
                        new SelectList(_context.Location.Where(m => m.ParentId == districk)
                            .OrderBy(m => m.Name)
                            .Select(m => new { Value = m.Id, Text = m.Name }).ToList(), "Value", "Text");
                    var homecity = _context.Location.Find(districk).ParentId;
                    model.HomeCities =
                        new SelectList(_context.Location.Where(m => m.ParentId == homecity)
                            .OrderBy(m => m.Name)
                            .Select(m => new { Value = m.Id, Text = m.Name }).ToList(), "Value", "Text");
                }
                else
                {//location city
                    var cityLocation = _context.Location.Find(profile.LocationId);

                    model.HomeCities =
                        new SelectList(_context.Location.Where(m => m.ParentId == cityLocation.ParentId)
                            .OrderBy(m => m.Name)
                            .Select(m => new { Value = m.Id, Text = m.Name }).ToList(), "Value", "Text");
                    model.HomeDictricts =
                        new SelectList(_context.Location.Where(m => m.ParentId == cityLocation.Id)
                            .OrderBy(m => m.Name)
                            .Select(m => new { Value = m.Id, Text = m.Name }).ToList(), "Value", "Text");


                }

            }
            return model;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateInfomation(PersonalInformation model)
        {
            bool isValid = true;
            if (ModelState.IsValid)
            {
                var user =
                     _context.AspNetUsers.Include(m => m.Candidate)
                        .Include(m => m.GeneralProfile)
                        .SingleOrDefault(m => m.UserName == User.Identity.Name);
                if (user != null)
                {
                    var candidte = user.Candidate;
                    var profile = user.GeneralProfile;
                    if (candidte == null)
                    {
                        candidte = Factory.CreateCandidate();
                    }
                    if (profile == null)
                    {
                        profile = Factory.CreateProfile();
                    }
                    candidte.FullName = $"{model.FirstName} {model.LastName}";
                    candidte.YearsOfExperience = model.YearsOfExperience;
                    candidte.Email = model.Email;
                    candidte.PhoneNumber = model.Phone;
                    profile.BirthDate = DateTime.Parse(model.DateOfBirth.ToString(), new CultureInfo("vi-vn"),
                        DateTimeStyles.AssumeLocal);
                    profile.Nationality = model.Nationality;
                    profile.Gender = model.Gender;
                    profile.MaritalStatus = model.MaritalStatusValue;
                    profile.HomeCountry = model.Country;
                    profile.HomeCity = model.HomeCity;
                    profile.HomeDistrict = model.HomeDistrict;
                    profile.HomeAddress = model.Address;
                    profile.FirstName = model.FirstName;
                    profile.LastName = model.LastName;
                    if (model.HomeDistrict != null)
                    {
                        profile.LocationId = Int32.Parse(model.HomeDistrict);//
                    }
                    else
                    {
                        profile.LocationId = Int32.Parse(model.HomeCity);//
                    }
                    user.GeneralProfile = profile;
                    user.Candidate = candidte;
                    if (model.YearsOfExperience < 0)
                    {
                        isValid = false;
                        ModelState.AddModelError("", "Years Of Experience must be a positive number!");
                    }

                    if (profile.BirthDate > DateTime.Now)
                    {
                        isValid = false;
                        ModelState.AddModelError("DateOfBirth", "Date of birth is not greater than current date!");
                        ModelState.AddModelError("", "Date of birth is not greater than current date!");
                    }

                    if (isValid)
                    {
                        try
                        {
                            await _context.SaveChangesAsync();
                            return Json(new { success = true, url = Url.Action("PersonalInformation", "Candidate") });
                        }
                        catch (Exception e)
                        {
                            ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
                            return Json(new { success = false, url = Url.Action("PersonalInformation", "Candidate") });
                        }
                    }

                }
            }
            else{
                ModelState.AddModelError("","ModelState.");
            }
            model.Countries =
               new SelectList(
                   await _context.Location.Where(m => m.Type == 1)
                       .OrderBy(m => m.Name)
                       .Select(m => new { Value = m.Id, Text = m.Name }).ToListAsync(), "Value", "Text");
            model.HomeCities = new SelectList(string.Empty, "Value", "Text");
            model.HomeDictricts = new SelectList(string.Empty, "Value", "Text");

            if (!String.IsNullOrEmpty(model.Country))
            {
                if (!String.IsNullOrEmpty(model.HomeCity))
                {
                    var countryId = Int32.Parse(model.Country);

                    model.HomeCities =
                        new SelectList(await
                            _context.Location
                                .Where(
                                    m =>
                                        m.ParentId == countryId)
                                .OrderBy(m => m.Name)
                                .Select(m => new { Value = m.Id, Text = m.Name }).ToListAsync(), "Value", "Text");

                    if (!String.IsNullOrEmpty(model.HomeDistrict))
                    {
                        var cityId = Int32.Parse(model.HomeCity);
                        model.HomeDictricts =
                        new SelectList(
                            await _context.Location
                                .Where(
                                    m =>
                                        m.ParentId == cityId)
                                .OrderBy(m => m.Name)
                                .Select(m => new { Value = m.Id, Text = m.Name }).ToListAsync(), "Value", "Text");
                    }
                }
            }
            return PartialView("_UpdateInfomation", model);
        }

        public async Task<IActionResult> UpdateSummary()
        {
            var model = new Sumary();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user != null)
            {
                var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    model.Text = candidate.Summary;
                }
            }
            return PartialView("_UpdateSummary", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateSummary(Sumary model)
        {

            if (ModelState.IsValid)
            {
                var user =
                     _context.AspNetUsers.Include(m => m.Candidate)
                        .SingleOrDefault(m => m.UserName == User.Identity.Name);
                if (user != null)
                {
                    var candidate = user.Candidate;
                    if (candidate == null)
                    {
                        candidate = Factory.CreateCandidate();
                    }
                    candidate.Summary = model.Text;
                    user.Candidate = candidate;

                    try
                    {
                        await _context.SaveChangesAsync();
                        return Json(new { success = true, url = Url.Action("Summary", "Candidate") });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
                    }
                }
            }
            return PartialView("_UpdateSummary", model);
        }

        public async Task<IActionResult> AddSkill()
        {
            var model = new KeySkill();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var lS = _context.Skill.ToList();
            if (user != null)
            {
                var candidate = _context.Candidate.Include(m => m.CandidateSkill).ThenInclude(m => m.Skill).SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    foreach (var item in candidate.CandidateSkill)
                    {
                        lS.Remove(_context.Skill.Find(item.SkillId));
                    }
                }
            }
            model.List = new SelectList(lS, "Id", "Name");
            return PartialView("_AddSkill", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddSkill(KeySkill model)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var lS = _context.Skill.ToList();

            if (user != null && (model.Level >= 0 && model.Level <= 100))
            {
                var candidate =
                    _context.Candidate.Include(m => m.CandidateSkill)
                        .ThenInclude(m => m.Skill)
                        .SingleOrDefault(m => m.Id == user.Id) ?? Factory.CreateCandidate();
                _context.CandidateSkill.Add(new CandidateSkill()
                {
                    SkillId = model.Id.Value,
                    CandidateId = candidate.Id,
                    SkillLevel = model.Level
                });
                try
                {
                    await _context.SaveChangesAsync();
                    return Json(new { success = true, url = Url.Action("Skill", "Candidate") });
                }
                catch
                {
                    ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
                }
            }
            ModelState.AddModelError("Level", "Enter a value between 0 and 100");
            //level value invalid
            var candidate2 = _context.Candidate.Include(m => m.CandidateSkill).ThenInclude(m => m.Skill).SingleOrDefault(m => m.Id == user.Id);
            if (candidate2 != null)
            {
                foreach (var item in candidate2.CandidateSkill)
                {
                    lS.Remove(_context.Skill.Find(item.SkillId));
                }
            }
            model.List = new SelectList(lS, "Id", "Name");
            return PartialView("_AddSkill", model);
        }

        [HttpGet]
        public async Task<IActionResult> EditSkill(int? id)
        {
            var model = new KeySkill();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var lS = _context.Skill.ToList();
            if (user != null)
            {
                var candidate = _context.Candidate.Include(m => m.CandidateSkill).ThenInclude(m => m.Skill).SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    foreach (var candidateSkill in candidate.CandidateSkill)
                    {
                        if (candidateSkill.SkillLevel.HasValue)
                            model.SkillLevel.Add(candidateSkill.SkillLevel.Value);
                        model.SkillId.Add(candidateSkill.SkillId);
                        model.SkillName.Add(candidateSkill.Skill.Name);
                        if (candidateSkill.SkillId != id.Value)
                            lS.Remove(_context.Skill.Find(candidateSkill.SkillId));
                    }
                }
            }
            model = new KeySkill
            {
                Id = id,
                OldId = id,
                Level = _context.CandidateSkill.Where(m => m.CandidateId == user.Id && m.SkillId == id).FirstOrDefault().SkillLevel,
                List = new SelectList(lS, "Id", "Name")
            };
            return PartialView("_EditSkill", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditSkill(KeySkill model, int OldId)
        {


            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var lS = _context.Skill.ToList();
            if (model.Level >= 0 && model.Level <= 100)
            {

                if (user != null)
                {
                    var candidate =
                        _context.Candidate.Include(m => m.CandidateSkill)
                            .ThenInclude(m => m.Skill)
                            .SingleOrDefault(m => m.Id == user.Id) ?? Factory.CreateCandidate();
                    var candidateSkill = candidate.CandidateSkill.ToList().Single(m => m.SkillId == OldId);
                    if (model.Id.Value != candidateSkill.SkillId)
                    {
                        _context.CandidateSkill.Remove(candidateSkill);
                        var nCS = new CandidateSkill
                        {
                            CandidateId = user.Id,
                            SkillId = model.Id.Value,
                            SkillLevel = model.Level
                        };
                        _context.CandidateSkill.Add(nCS);
                    }
                    else
                    {
                        candidateSkill.SkillLevel = model.Level;
                    }
                    try
                    {
                        await _context.SaveChangesAsync();
                        return Json(new { success = true, url = Url.Action("Skill", "Candidate") });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
                    }
                }
            }

            //input level error
            ModelState.AddModelError("Level", "Enter a value between 0 and 100");
            var candidate2 = _context.Candidate.Include(m => m.CandidateSkill).ThenInclude(m => m.Skill).SingleOrDefault(m => m.Id == user.Id);
            if (candidate2 != null)
            {
                foreach (var candidateSkill in candidate2.CandidateSkill)
                {
                    if (candidateSkill.SkillLevel.HasValue)
                        model.SkillLevel.Add(candidateSkill.SkillLevel.Value);
                    model.SkillId.Add(candidateSkill.SkillId);
                    model.SkillName.Add(candidateSkill.Skill.Name);
                    if (candidateSkill.SkillId != OldId)
                        lS.Remove(_context.Skill.Find(candidateSkill.SkillId));
                }
            }
            model.List = new SelectList(lS, "Id", "Name");
            model.OldId = OldId;

            return PartialView("_EditSkill", model);
        }

        public async Task<IActionResult> DeleteSkill(int? id)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            CandidateSkill model = new CandidateSkill();
            var lS = _context.Skill.ToList();
            if (user != null)
            {
                var candidateskill = model = _context.CandidateSkill.Where(m => m.CandidateId == user.Id).SingleOrDefault(m => m.SkillId == id);
                _context.CandidateSkill.Remove(candidateskill);
                try
                {
                    await _context.SaveChangesAsync(); return Json(new { success = true, url = Url.Action("Skill", "Candidate") });
                }
                catch
                {
                    ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
                }
            }
            return PartialView("_EditSkill", model);
        }

        public async Task<IActionResult> UpdateSkill()
        {
            var model = new KeySkill();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user != null)
            {
                var candidate = _context.Candidate.Include(m => m.CandidateSkill).ThenInclude(m => m.Skill).SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    if (candidate.CandidateSkill != null && candidate.CandidateSkill.Any())
                    {
                        foreach (var candidateSkill in candidate.CandidateSkill)
                        {
                            model.SkillId.Add(candidateSkill.SkillId);
                            model.SkillName.Add(candidateSkill.Skill.Name);
                        }
                    }
                }
            }
            model.List = new SelectList(await _context.Skill.ToListAsync(), "Id", "Name");
            return PartialView("_UpdateSkill", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateSkill(KeySkill model, string[] SkillId)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);

            if (user != null)
            {
                var candidate =
                    _context.Candidate.Include(m => m.CandidateSkill)
                        .ThenInclude(m => m.Skill)
                        .SingleOrDefault(m => m.Id == user.Id) ?? Factory.CreateCandidate();

                if (candidate != null)
                {
                    model.SkillName = candidate.CandidateSkill.Select(m => m.Skill.Name).ToList();

                    var selectedSkillIds = model.SkillId ?? new List<int>();
                    var candidateSkill = candidate.CandidateSkill.Select(m => m.SkillId).ToList();

                    var skillsToAdd = selectedSkillIds.Except(candidateSkill);
                    var skillsToDelete = candidateSkill.Except(selectedSkillIds);

                    foreach (var skillId in skillsToAdd)
                    {
                        _context.CandidateSkill.Add(new CandidateSkill()
                        {
                            SkillId = skillId,
                            CandidateId = candidate.Id

                        });
                    }

                    foreach (var skillId in skillsToDelete)
                    {
                        var skill =
                            candidate.CandidateSkill
                                .FirstOrDefault(m => m.CandidateId == candidate.Id && m.SkillId == skillId);

                        if (skill != null)
                        {
                            _context.CandidateSkill.Remove(skill);
                        }

                    }

                    foreach (var skillId in SkillId)
                    {
                        int i;
                        if (!int.TryParse(skillId, out i))
                        {
                            var skill = new Skill { Name = skillId };
                            if (!_context.Skill.Any(m => m.Name.Equals(skillId, StringComparison.OrdinalIgnoreCase)))
                            {
                                _context.CandidateSkill.Add(new CandidateSkill
                                {
                                    Candidate = candidate,
                                    CandidateId = candidate.Id,
                                    SkillId = skill.Id,
                                    Skill = skill
                                });
                            }
                        }
                    }

                    try
                    {
                        await _context.SaveChangesAsync();
                        return Json(new { success = true, url = Url.Action("Skill", "Candidate") });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
                    }

                }
            }
            model.List = new SelectList(await _context.Skill.ToListAsync(), "Id", "Name");
            return PartialView("_UpdateSkill", model);
        }

        public async Task<IActionResult> AddLanguage()
        {
            var model = new Language
            {
                List = new SelectList(await _context.Language.ToListAsync(), "Id", "Name")
            };
            return PartialView("_AddLanguage", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddLanguage(Language model)
        {
            int levelScore = 0;
            bool isValid = true;

            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                if (user != null)
                {
                    var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                    if (candidate != null)
                    {
                        CandidateLanguage languageToAdd = new CandidateLanguage()
                        {
                            CandidateId = candidate.Id,
                            LanguageId = model.LanguageId,
                            Certification = model.Certification,
                            ScoreOrLevel = model.ScoreOrLevel

                        };

                        //var isExist =
                        //    _context.CandidateLanguage.Any(m => m.CandidateId == languageToAdd.CandidateId &&
                        //            m.LanguageId == languageToAdd.LanguageId);
                        //if (!isExist)
                        //{
                        _context.CandidateLanguage.Add(languageToAdd);
                        //}
                        //else
                        //{
                        //    ModelState.AddModelError("", $"The language you have just choosed is exist! Please choose another!");
                        //    model.List = new SelectList(await _context.Language.ToListAsync(), "Id", "Name");
                        //    return PartialView("_AddLanguage", model);
                        //}

                        var isNumber = int.TryParse(model.ScoreOrLevel, out levelScore);

                        if (isNumber)
                        {
                            if (levelScore < 0)
                            {
                                isValid = false;
                                ModelState.AddModelError("", "Score or level must be positive number");
                            }
                        }

                        if (isValid)
                        {
                            try
                            {
                                await _context.SaveChangesAsync();
                                return Json(new { success = true, url = Url.Action("CandidateLanguage", "Candidate") });
                            }
                            catch (Exception e)
                            {
                                ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
                            }
                        }
                    }
                }

            }
            model.List = new SelectList(await _context.Language.ToListAsync(), "Id", "Name");
            return PartialView("_AddLanguage", model);
        }

        public async Task<IActionResult> EditLanguage(int? languageId)
        {
            if (languageId == null)
            {
                return NotFound();
            }

            // var candidate = await _userManager.FindByNameAsync(User.Identity.Name);

            //var candidateLanguage =
            //    _context.CandidateLanguage.Include(m => m.Candidate)
            //        .Include(m => m.Language)
            //        .SingleOrDefault(m => m.CandidateId == candidate.Id && m.LanguageId == languageId);

            var candidateLanguage =
               _context.CandidateLanguage.SingleOrDefault(x => x.Id == languageId);

            if (candidateLanguage == null)
            {
                return NotFound();
            }

            var model = new Language
            {
                Id = candidateLanguage.Id,
                LanguageId = candidateLanguage.LanguageId,
                ScoreOrLevel = candidateLanguage.ScoreOrLevel,
                Certification = candidateLanguage.Certification,
                List = new SelectList(await _context.Language.ToListAsync(), "Id", "Name")

            };
            return PartialView("_EditLanguage", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditLanguage(Language model, int Id)
        {
            int levelScore = 0;
            bool isValid = true;
            if (ModelState.IsValid && model.Id == Id)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                //var languageToUpdate =
                //    _context.CandidateLanguage.SingleOrDefault(
                //        m => m.CandidateId == user.Id && m.LanguageId == model.LanguageId);

                var languageToUpdate = _context.CandidateLanguage.SingleOrDefault(m => m.Id == Id);


                if (languageToUpdate != null)
                {
                    languageToUpdate.LanguageId = model.LanguageId;
                    languageToUpdate.Certification = model.Certification;
                    languageToUpdate.ScoreOrLevel = model.ScoreOrLevel;

                    _context.Update(languageToUpdate);

                    var isNumber = int.TryParse(model.ScoreOrLevel, out levelScore);
                    if (isNumber)
                    {
                        if (levelScore < 0)
                        {
                            isValid = false;
                            ModelState.AddModelError("", "Score or level must be positive number");
                        }
                    }
                    if (isValid)
                    {
                        try
                        {
                            await _context.SaveChangesAsync();
                            return Json(new { success = true, url = Url.Action("CandidateLanguage", "Candidate") });
                        }
                        catch (Exception e)
                        {
                            ModelState.AddModelError("", e.Message);
                        }
                    }

                }

            }
            model.List = new SelectList(await _context.Language.ToListAsync(), "Id", "Name");
            return PartialView("_EditLanguage", model);
        }

        public async Task<IActionResult> EditSocialNetwork()
        {
            var model = new SocialNetwork();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user != null)
            {
                var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    model.Facebook = candidate.Facebook;
                    model.Instagram = candidate.Instagram;
                    model.LinkedIn = candidate.LinkedIn;
                    model.Twitter = candidate.Twitter;
                    model.Youtube = candidate.Youtube;
                }
            }
            return PartialView("_EditSocialNetwork", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditSocialNetwork(SocialNetwork model)
        {

            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                if (user != null)
                {
                    var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                    if (candidate != null)
                    {

                        try
                        {
                            candidate.Facebook = model.Facebook;
                            candidate.Instagram = model.Instagram;
                            candidate.LinkedIn = model.LinkedIn;
                            candidate.Twitter = model.Twitter;
                            candidate.Youtube = model.Youtube;
                            _context.Candidate.Update(candidate);

                            await _context.SaveChangesAsync();
                            return Json(new { success = true, url = Url.Action("CandidateSocialNetwork", "Candidate") });
                        }
                        catch (Exception e)
                        {
                            ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
                        }

                    }
                }

            }

            return PartialView("_EditSocialNetwork", model);
        }

        public async Task<IActionResult> AddEducation()
        {
            var model = new EducationHistory();

            var listUniversity = _context.University.OrderBy(m => m.Name).ToList();
            var listEducationLevel = await _context.EducationLevel.OrderBy(m => m.Name).ToListAsync();

            model.EducationLevels = new SelectList(listEducationLevel, "Id", "Name");
            model.University = new SelectList(listUniversity, "Id", "Name");
            if (listUniversity.Count > 0 && listEducationLevel.Count > 0)
            {
                model.Majors = new SelectList(await _context.Major.Where(m => m.UniversityId == listUniversity.ElementAt(0).Id && m.EducationLevelId == listEducationLevel.ElementAt(0).Id).OrderBy(x => x.Name).ToListAsync(), "Id", "Name");
            }
            return PartialView("_AddEducation", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddEducation(EducationHistory model)
        {
            bool isValid = true;
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                if (user != null)
                {
                    var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                    if (candidate != null)
                    {
                        var university = await _context.University.FirstOrDefaultAsync(m => m.Id == model.UniversityId);
                        if (university == null)
                        {
                            university = new University { Name = model.UniversityName };
                        }

                        var major = await _context.Major.FirstOrDefaultAsync(m => m.Id == model.MajorId);
                        if (major == null)
                        {
                            major = new Major
                            {
                                Name = model.Major,
                                EducationLevelId = model.EducationLevelId,
                                UniversityId = university.Id,
                                University = university
                            };
                        }

                        var educationHistory = new EF.EducationHistory
                        {
                            BeginDate = DateTime.Parse(model.From.ToString(), new CultureInfo("vi-vn"), DateTimeStyles.AssumeLocal),
                            EndDate = DateTime.Parse(model.To.ToString(), new CultureInfo("vi-vn"), DateTimeStyles.AssumeLocal),
                            Major = major,
                            MajorId = major.Id,
                            Achievement = model.Achievements,
                            Candidate = candidate,
                            CandidateId = candidate.Id,
                            Ranking = model.Ranking
                        };

                        if (educationHistory.BeginDate >= educationHistory.EndDate)
                        {
                            isValid = false;
                            ModelState.AddModelError("", "From date must smaller than To date");
                            model.University = new SelectList(_context.University.OrderBy(m => m.Name).ToList(), "Id", "Name");
                            model.Majors = new SelectList(await _context.Major.Include(m => m.University).Where(m => m.UniversityId == major.UniversityId).ToListAsync(), "Id", "Name");
                        }

                        _context.EducationHistory.Add(educationHistory);
                        if (isValid)
                        {
                            try
                            {
                                await _context.SaveChangesAsync();
                                return Json(new { success = true, url = Url.Action("EducationHistory", "Candidate") });
                            }
                            catch (Exception e)
                            {
                                ModelState.AddModelError("", e.Message);
                                //model.University = new SelectList(_context.University.OrderBy(m => m.Name).ToList(), "Id", "Name");
                                //model.Majors = new SelectList(await _context.Major.Include(m => m.University).Where(m => m.UniversityId == major.UniversityId).ToListAsync(), "Id", "Name");
                            }
                        }
                    }
                }
            }
            model.University = new SelectList(await _context.University.OrderBy(m => m.Name).ToListAsync(), "Id", "Name");
            model.EducationLevels = new SelectList(await _context.EducationLevel.OrderBy(m => m.Name).ToListAsync(), "Id", "Name");
            model.Majors = new SelectList(await _context.Major.Where(m => m.UniversityId == model.UniversityId && m.EducationLevelId == model.EducationLevelId).OrderBy(x => x.Name).ToListAsync(), "Id", "Name");

            return PartialView("_AddEducation", model);
        }



        public async Task<IActionResult> EditEducation(int? id)
        {
            var model = new EducationHistory();
            if (id == null)
            {
                return NotFound();
            }

            var educationhistory = _context.EducationHistory.SingleOrDefault(m => m.Id == id);
            if (educationhistory == null)
            {
                return NotFound();
            }
            model.Id = educationhistory.Id;
            model.From = educationhistory.BeginDate.ToString("MM/yyyy");
            model.To = educationhistory.EndDate.ToString("MM/yyyy");
            model.Achievements = educationhistory.Achievement;
            model.Ranking = educationhistory.Ranking;


            var major = _context.Major.Include(m => m.University).SingleOrDefault(m => m.Id == educationhistory.MajorId);

            if (major != null)
            {
                model.MajorId = major.Id;
                model.Majors = new SelectList(await _context.Major.Include(m => m.University).Where(m => m.UniversityId == major.UniversityId).ToListAsync(), "Id", "Name");
                model.EducationLevelId = major.EducationLevelId;
            }

            var university = major?.University;
            if (university != null)
            {
                model.UniversityId = university.Id;
                model.UniversityName = university.Name;
            }
            var listUniversity = _context.University;
            model.University = new SelectList(listUniversity.OrderBy(m => m.Name).ToList(), "Id", "Name");

            model.EducationLevels = new SelectList(await _context.EducationLevel.OrderBy(m => m.Name).ToListAsync(), "Id", "Name");
            return PartialView("_EditEducation", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditEducation(EducationHistory model)
        {
            bool isValid = true;
            if (ModelState.IsValid)
            {
                if (model.Id == null)
                {
                    return NotFound();
                }

                var educationHistory = _context.EducationHistory.SingleOrDefault(m => m.Id == model.Id);

                if (educationHistory == null)
                {
                    return NotFound();
                }

                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                if (user != null)
                {
                    var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                    if (candidate != null)
                    {
                        var isUpdate = candidate.Id == educationHistory.CandidateId;
                        if (isUpdate)
                        {
                            var university =
                                await _context.University.FirstOrDefaultAsync(m => m.Id == (model.UniversityId)) ??
                                new University { Id = model.UniversityId.Value };

                            var major = await _context.Major.FirstOrDefaultAsync(m => m.Id == model.MajorId) ??
                                        new Major();
                            major.EducationLevelId = model.EducationLevelId;
                            major.UniversityId = university.Id;
                            major.University = university;

                            educationHistory.BeginDate = DateTime.Parse(model.From.ToString(), new CultureInfo("vi-vn"),
                                DateTimeStyles.AssumeLocal);
                            educationHistory.EndDate = DateTime.Parse(model.To.ToString(), new CultureInfo("vi-vn"),
                                DateTimeStyles.AssumeLocal);
                            educationHistory.Major = major;
                            educationHistory.MajorId = major.Id;
                            educationHistory.Achievement = model.Achievements;
                            educationHistory.Candidate = candidate;
                            educationHistory.CandidateId = candidate.Id;
                            educationHistory.Ranking = model.Ranking;

                            if (educationHistory.BeginDate >= educationHistory.EndDate)
                            {
                                isValid = false;
                                ModelState.AddModelError("", "From date must smaller than To date");
                                model.University = new SelectList(_context.University.OrderBy(m => m.Name).ToList(), "Id", "Name");
                                model.Majors = new SelectList(await _context.Major.Include(m => m.University).Where(m => m.UniversityId == major.UniversityId).ToListAsync(), "Id", "Name");
                            }
                            _context.Update(educationHistory);

                            if (isValid)
                            {
                                try
                                {
                                    await _context.SaveChangesAsync();
                                    return Json(new { success = true, url = Url.Action("EducationHistory", "Candidate") });
                                }
                                catch (Exception e)
                                {
                                    ModelState.AddModelError("", e.Message);
                                    //model.University = new SelectList(_context.University.OrderBy(m => m.Name).ToList(), "Id", "Name");
                                    //model.Majors = new SelectList(await _context.Major.Include(m => m.University).Where(m => m.UniversityId == major.UniversityId).ToListAsync(), "Id", "Name");
                                }
                            }
                        }
                        else
                        {
                            return Unauthorized();
                        }
                    }
                }
            }
            else
            {

            }
            model.University = new SelectList(await _context.University.OrderBy(m => m.Name).ToListAsync(), "Id", "Name");
            model.EducationLevels = new SelectList(await _context.EducationLevel.OrderBy(m => m.Name).ToListAsync(), "Id", "Name");
            model.Majors = new SelectList(await _context.Major.Where(m => m.UniversityId == model.UniversityId && m.EducationLevelId == model.EducationLevelId).OrderBy(x => x.Name).ToListAsync(), "Id", "Name");

            return PartialView("_EditEducation", model);
        }

        public IActionResult AddEmployerHistory()
        {
            var model = new EmploymentHistory();
            return PartialView("_AddEmployerHistory", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddEmployerHistory(EmploymentHistory model)
        {
            bool isValid = true;
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                if (user != null)
                {
                    var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                    if (candidate != null)
                    {
                        var employerHistory = new EF.EmploymentHistory();
                        employerHistory.Candidate = candidate;
                        employerHistory.CompanyName = model.Company;
                        employerHistory.Description = model.Description;
                        employerHistory.StartedDate = DateTime.Parse(model.From, new CultureInfo("vi-vn"),
                            DateTimeStyles.AssumeLocal);
                        if (model.To != null)
                        {
                            employerHistory.EndedDate = DateTime.Parse(model.To, new CultureInfo("vi-vn"),
                                                       DateTimeStyles.AssumeLocal);
                        }
                        else
                        {
                            employerHistory.EndedDate = null;
                        }
                        employerHistory.PositionTitle = model.Position;

                        if (employerHistory != null && employerHistory.StartedDate >= employerHistory.EndedDate)
                        {
                            isValid = false;
                            ModelState.AddModelError("", "From date must smaller than To date");
                        }
                        _context.EmploymentHistory.Add(employerHistory);
                        if (isValid)
                        {
                            try
                            {
                                await _context.SaveChangesAsync();
                                return Json(new { success = true, url = Url.Action("EmployerHistory", "Candidate") });
                            }
                            catch (Exception e)
                            {
                                ModelState.AddModelError("", e.Message);
                            }
                        }
                    }
                }
            }
            return PartialView("_AddEmployerHistory", model);
        }

        public IActionResult EditEmployerHistory(int? id)
        {
            var model = new EmploymentHistory();
            if (id == null)
            {
                return NotFound();
            }

            var employerHistory = _context.EmploymentHistory.SingleOrDefault(m => m.Id == id);
            if (employerHistory == null)
            {
                return NotFound();
            }
            model.Id = employerHistory.Id;
            model.From = employerHistory.StartedDate.ToString("MM/yyyy");
            model.To = employerHistory.EndedDate?.ToString("MM/yyyy");
            model.Description = employerHistory.Description;
            model.Company = employerHistory.CompanyName;
            model.Position = employerHistory.PositionTitle;

            return PartialView("_EditEmployerHistory", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditEmployerHistory(int? id, EmploymentHistory model)
        {
            bool isValid = true;
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    return NotFound();
                }

                var employerHistorty = _context.EmploymentHistory.SingleOrDefault(m => m.Id == id);

                if (employerHistorty == null)
                {
                    return NotFound();
                }

                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                if (user != null)
                {
                    var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                    if (candidate != null)
                    {
                        var isUpdate = candidate.Id == employerHistorty.CandidateId;
                        if (isUpdate)
                        {
                            employerHistorty.CompanyName = model.Company;
                            employerHistorty.PositionTitle = model.Position;
                            employerHistorty.StartedDate = DateTime.Parse(model.From, new CultureInfo("vi-vn"),
                            DateTimeStyles.AssumeLocal);
                            if (model.To != null)
                            {
                                employerHistorty.EndedDate = DateTime.Parse(model.To, new CultureInfo("vi-vn"),
                                                           DateTimeStyles.AssumeLocal);
                            }
                            else
                            {
                                employerHistorty.EndedDate = null;
                            }

                            employerHistorty.Description = model.Description;

                            if (employerHistorty.EndedDate != null && employerHistorty.StartedDate >= employerHistorty.EndedDate)
                            {
                                isValid = false;
                                ModelState.AddModelError("", "From date must smaller than To date");
                            }

                            _context.Update(employerHistorty);

                            if (isValid)
                            {
                                try
                                {
                                    await _context.SaveChangesAsync();
                                    return Json(new { success = true, url = Url.Action("EmployerHistory", "Candidate") });
                                }
                                catch (Exception e)
                                {
                                    ModelState.AddModelError("", e.Message);
                                }
                            }


                        }
                        else
                        {
                            return Unauthorized();
                        }

                    }
                }
            }
            return PartialView("_EditEmployerHistory", model);
        }

        public IActionResult AddReference()
        {
            return PartialView("_AddReference");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddReference(Reference model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                if (user != null)
                {
                    var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                    if (candidate != null)
                    {
                        var reference = new EF.Reference();
                        reference.FullName = model.FullName;
                        reference.Position = model.Position;
                        reference.Company = model.Company;
                        reference.Email = model.Email;
                        reference.PhoneNumber = model.Phone;
                        reference.CandidateId = candidate.Id;

                        _context.Reference.Add(reference);

                        try
                        {
                            await _context.SaveChangesAsync();
                            return Json(new { success = true, url = Url.Action("CandidateReference", "Candidate") });
                        }
                        catch (Exception e)
                        {
                            ModelState.AddModelError("", e.Message);
                        }
                    }

                }
            }
            return PartialView("_AddReference", model);
        }

        public IActionResult EditReference(int? id)
        {
            Reference model = new Reference();

            if (id == null)
            {
                return NotFound();
            }

            var reference = _context.Reference.SingleOrDefault(m => m.Id == id);

            if (reference == null)
            {
                return NotFound();
            }


            model.Position = reference.Position;
            model.Company = reference.Company;
            model.Email = reference.Email;
            model.FullName = reference.FullName;
            model.Phone = reference.PhoneNumber;
            model.Id = reference.Id;


            return PartialView("_EditReference", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditReference(int? id, Reference model)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reference = _context.Reference.SingleOrDefault(m => m.Id == id);

            if (reference == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);

            if (candidate.Id == reference.CandidateId)
            {
                reference.FullName = model.FullName;
                reference.Position = model.Position;
                reference.Company = model.Company;
                reference.Email = model.Email;
                reference.PhoneNumber = model.Phone;

                _context.Update(reference);

                try
                {
                    await _context.SaveChangesAsync();
                    return Json(new { success = true, url = Url.Action("CandidateReference", "Candidate") });
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }

            return PartialView("_EditReference", model);
        }

        public async Task<IActionResult> AddPreference()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user == null)
            {
                return Unauthorized();
            }

            var candidate = _context.Candidate.Include(m => m.CandidateBenefit).Include(m => m.CandidateCategory)
                .Include(m => m.CandidateLocation).SingleOrDefault(m => m.Id == user.Id);

            if (candidate == null)
            {
                return Unauthorized();
            }



            var model = new WorkingPreference
            {
                Locations = new SelectList(await _context.Location.Where(m => m.Type == 2).OrderBy(m => m.Name).ToListAsync(), "Id", "Name"),
                ExpectedCategory = new SelectList(await _context.Category.ToListAsync(), "Id", "Name"),
                JobLevels = new SelectList(await _context.JobLevel.OrderBy(m => m.JobLevelName).ToListAsync(), "JobLevelId", "JobLevelName"),
            };

            model.CategoryId = candidate.CandidateCategory.Select(m => m.CategoryId).ToList();
            model.LocationId = candidate.CandidateLocation.Select(m => m.LocationId).ToList();
            model.JobLevelId = candidate.ExpectedJobLevelId;
            model.Salary = candidate.ExpectedSalary;

            var benefits = await _context.Benefit.ToListAsync();
            var candidateBenefits = new HashSet<int>(candidate.CandidateBenefit.Select(m => m.DesiredBenefitId));
            model.SelectedBenefits = new List<AssignedBenefitData>();

            foreach (var benefit in benefits)
            {
                model.SelectedBenefits.Add(new AssignedBenefitData
                {
                    BenefitId = benefit.Id,
                    BenefitName = benefit.Name,
                    Assigned = candidateBenefits.Contains(benefit.Id)
                });
            }
            return PartialView("_AddPreference", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPreference(WorkingPreference model)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user == null)
            {
                return Unauthorized();
            }

            var candidate = _context.Candidate.Include(m => m.CandidateBenefit).Include(m => m.CandidateCategory).Include(m => m.CandidateLocation).SingleOrDefault(m => m.Id == user.Id);

            if (candidate == null)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                //Update candidate category
                var selectedCategories = model.CategoryId;
                var candidateCategories = candidate.CandidateCategory.Select(m => m.CategoryId).ToList();

                var categoriesToAdd = selectedCategories.Except(candidateCategories);
                var categoriesToDelete = candidateCategories.Except(selectedCategories);

                foreach (var categoryId in categoriesToAdd)
                {
                    _context.CandidateCategory.Add(new CandidateCategory()
                    {
                        CategoryId = categoryId,
                        CandidateId = candidate.Id
                    });
                }

                foreach (var categoryId in categoriesToDelete)
                {
                    var category =
                        candidate.CandidateCategory
                            .FirstOrDefault(m => m.CandidateId == candidate.Id && m.CategoryId == categoryId);

                    if (category != null)
                    {
                        _context.CandidateCategory.Remove(category);
                    }

                }


                //Update candidate language
                var selectedLocations = model.LocationId;
                var candidateLocations = candidate.CandidateLocation.Select(m => m.LocationId).ToList();

                var locationsToAdd = selectedLocations.Except(candidateLocations);
                var locationsToDelete = candidateLocations.Except(selectedLocations);

                foreach (var locationId in locationsToAdd)
                {
                    _context.CandidateLocation.Add(new CandidateLocation()
                    {
                        LocationId = locationId,
                        CandidateId = candidate.Id
                    });
                }

                foreach (var locationId in locationsToDelete)
                {
                    var location =
                        candidate.CandidateLocation
                            .FirstOrDefault(m => m.CandidateId == candidate.Id && m.LocationId == locationId);

                    if (location != null)
                    {
                        _context.CandidateLocation.Remove(location);
                    }
                }

                //Update candidate language
                var selectedBenefics = model.BeneficIds;
                var candidateBenefics = candidate.CandidateBenefit.Select(m => m.DesiredBenefitId).ToList();

                var beneficsToAdd = selectedBenefics.Except(candidateBenefics);
                var beneficsToDelete = candidateBenefics.Except(selectedBenefics);

                foreach (var beneficId in beneficsToAdd)
                {
                    _context.CandidateBenefit.Add(new CandidateBenefit()
                    {
                        DesiredBenefitId = beneficId,
                        CandidateId = candidate.Id
                    });
                }

                foreach (var beneficId in beneficsToDelete)
                {
                    var benefic =
                        candidate.CandidateBenefit
                            .FirstOrDefault(m => m.CandidateId == candidate.Id && m.DesiredBenefitId == beneficId);

                    if (benefic != null)
                    {
                        _context.CandidateBenefit.Remove(benefic);
                    }

                }

                IList<CandidateBenefit> benefitsToAdd = new List<CandidateBenefit>();
                foreach (var beneficId in model.BeneficIds)
                {
                    benefitsToAdd.Add(new CandidateBenefit
                    {
                        CandidateId = candidate.Id,
                        DesiredBenefitId = beneficId
                    });
                }

                //Set value candidate to add
                candidate.ExpectedJobLevelId = model.JobLevelId;
                candidate.ExpectedSalary = model.Salary;

                _context.Update(candidate);

                try
                {
                    await _context.SaveChangesAsync();
                    return Json(new { success = true, url = Url.Action("CandidatePreference", "Candidate") });
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
                }
            }

            model.Locations = new SelectList(await _context.Location.Where(m => m.Type == 2).OrderBy(m => m.Name).ToListAsync(), "Id", "Name");
            model.ExpectedCategory = new SelectList(await _context.Category.ToListAsync(), "Id", "Name");
            model.JobLevels = new SelectList(await _context.JobLevel.OrderBy(m => m.JobLevelName).ToListAsync(), "JobLevelId", "JobLevelName");
            var benefits = await _context.Benefit.ToListAsync();
            var candidateBenefits = new HashSet<int>(candidate.CandidateBenefit.Select(m => m.DesiredBenefitId));
            model.SelectedBenefits = new List<AssignedBenefitData>();

            foreach (var benefit in benefits)
            {
                model.SelectedBenefits.Add(new AssignedBenefitData
                {
                    BenefitId = benefit.Id,
                    BenefitName = benefit.Name,
                    Assigned = candidateBenefits.Contains(benefit.Id)
                });
            }
            return PartialView("_AddPreference", model);
        }

        [HttpPost]
        public async Task<IActionResult> RemoveLanguage(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user == null)
            {
                return Unauthorized();
            }

            //var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
            //if (candidate == null)
            //{
            //    return Unauthorized();
            //}

            // var candidateLanguage = _context.CandidateLanguage.SingleOrDefault(m => m.LanguageId == id && m.CandidateId == candidate.Id);
            var candidateLanguage = _context.CandidateLanguage.Find(id);
            if (candidateLanguage == null)
            {
                return NotFound();
            }

            try
            {
                _context.CandidateLanguage.Remove(candidateLanguage);
                await _context.SaveChangesAsync();
                return Json(new { success = true, url = Url.Action("CandidateLanguage", "Candidate") });
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
            }

            return Json(new { success = false, url = Url.Action("CandidateLanguage", "Candidate") });
        }

        [HttpPost]
        public async Task<IActionResult> RemoveReference(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user == null)
            {
                return Unauthorized();
            }

            var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
            if (candidate == null)
            {
                return Unauthorized();
            }

            var candidateReference = _context.Reference.SingleOrDefault(m => m.Id == id);
            if (candidateReference == null)
            {
                return NotFound();
            }

            try
            {
                _context.Reference.Remove(candidateReference);
                await _context.SaveChangesAsync();
                return Json(new { success = true, url = Url.Action("CandidateReference", "Candidate") });
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
            }

            return Json(new { success = false, url = Url.Action("CandidateReference", "Candidate") });
        }

        [HttpPost]
        public async Task<IActionResult> RemoveEducation(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user == null)
            {
                return Unauthorized();
            }

            var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
            if (candidate == null)
            {
                return Unauthorized();
            }

            var candidateEducation = _context.EducationHistory.SingleOrDefault(m => m.Id == id);
            if (candidateEducation == null)
            {
                return NotFound();
            }

            try
            {
                _context.EducationHistory.Remove(candidateEducation);
                await _context.SaveChangesAsync();
                return Json(new { success = true, url = Url.Action("EducationHistory", "Candidate") });
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
            }

            return Json(new { success = false, url = Url.Action("EducationHistory", "Candidate") });
        }

        [HttpPost]
        public async Task<IActionResult> RemoveEmployment(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user == null)
            {
                return Unauthorized();
            }

            var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
            if (candidate == null)
            {
                return Unauthorized();
            }

            var candidateEmployment = _context.EmploymentHistory.SingleOrDefault(m => m.Id == id);
            if (candidateEmployment == null)
            {
                return NotFound();
            }

            try
            {
                _context.EmploymentHistory.Remove(candidateEmployment);
                await _context.SaveChangesAsync();
                return Json(new { success = true, url = Url.Action("EmployerHistory", "Candidate") });
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
            }

            return Json(new { success = false, url = Url.Action("EmployerHistory", "Candidate") });
        }
        [AllowAnonymous]
        [HttpGet]
        public IActionResult FindStateByCountryName(int id)
        {
            List<SelectListItem> states = new List<SelectListItem>();
            if (!String.IsNullOrEmpty(id.ToString()))
            {
                //int countryId = _context.Location.SingleOrDefault(m => m.Name == name).Id;
                int countryId = Int32.Parse(id.ToString());
                foreach (var item in _context.Location.Where(m => m.ParentId == countryId).OrderBy(z => z.Name))
                {
                    states.Add(new SelectListItem { Text = item.Name, Value = item.Id.ToString() });
                }
            }
            return Json(states);
        }
        [AllowAnonymous]
        [HttpGet]
        public IActionResult FindStateByStateName(int id)
        {
            List<SelectListItem> district = new List<SelectListItem>();
            if (!String.IsNullOrEmpty(id.ToString()))
            {
                //int stateId = _context.Location.SingleOrDefault(m => m.Name == name).Id;
                int stateId = id;
                foreach (var item in _context.Location.Where(m => m.ParentId == stateId).OrderBy(z => z.Name))
                {
                    district.Add(new SelectListItem { Text = item.Name, Value = item.Id.ToString() });
                }

            }
            return Json(district);
        }


        [HttpGet]
        public async Task<IActionResult> ListUniversity()
        {
            var universities = await _context.University.Select(m => new { id = m.Id, name = m.Name }).ToListAsync();
            return Json(universities);
        }

        [HttpGet]
        public async Task<IActionResult> ListMajor()
        {
            var major = await _context.Major.Select(m => new { id = m.Id, name = m.Name }).ToListAsync();
            return Json(major);
        }

        [HttpPost]
        public async Task<int> InsertMajor(int universityId, string name, int educationLevel)
        {
            var _user = _userManager.FindByNameAsync(User.Identity.Name);
            var nMajor = new Major
            {
                Name = name,
                UniversityId = universityId,
                EducationLevelId = educationLevel,
                CreatedDate = DateTime.Now,
                CreatedBy = _user.Id
            };

            if (string.IsNullOrEmpty(name))
                return 0;
            else
            {
                if (!await _context.Major.Where(x => x.UniversityId == universityId && x.EducationLevelId == educationLevel && x.Name.Equals(name, StringComparison.OrdinalIgnoreCase)).AnyAsync())
                {
                    _context.Major.Add(nMajor);
                    await _context.SaveChangesAsync();

                    return nMajor.Id;
                }
                else
                {
                    return -1;
                }
            }
        }

        //***
        [HttpGet]
        public async Task<int> GetMajorById(string name)
        {
            var lMajor = _context.Major.Where(m => m.Name.ToLower().CompareTo(name.ToLower()) == 0).FirstOrDefault();
            if (lMajor != null)
                return lMajor.Id;
            else
                return await _context.Major.CountAsync() - 1;
        }

        [HttpGet]
        public async Task<IActionResult> ListSkill()
        {
            var skills = await _context.Skill.Select(m => new { id = m.Id, name = m.Name }).ToListAsync();
            return Json(skills);
        }

        [HttpGet]
        public async Task<IActionResult> ListCompany()
        {
            var companies = await _context.Company.Select(m => new { id = m.Id, name = m.Name }).ToListAsync();
            return Json(companies);
        }

        [HttpGet]
        public async Task<IActionResult> ListPostion()
        {
            var position = await _context.PositionJob.Select(m => new { id = m.Id, name = m.PositionTitle }).ToListAsync();
            return Json(position);
        }

        [HttpPost]
        public async Task<IActionResult> UploadFilesAjax()
        {
            string imagePath = String.Empty;
            var file = Request.Form.Files[0];
            if (file != null)
            {
                if (_imageService.Validate(file))
                {
                    try
                    {
                        imagePath = _pathUpLoad.FrontImage + await _imageService.SaveAsync(file, _pathUpLoad.FrontImage);
                    }
                    catch (Exception e)
                    {
                        TempData["error"] = "Sorry an error occurred saving to the database, please try again";
                        ModelState.AddModelError("",
                            "Sorry an error occurred saving to the database, please try again");
                    }
                }
                else
                {
                    TempData["error"] = "The file must be gif, png, jpeg or jpg and less than 2MB in size";
                    ModelState.AddModelError("", "The file must be gif, png, jpeg or jpg and less than 2MB in size");
                }
            }
            else
            {
                TempData["error"] = "Please choose a file";
                ModelState.AddModelError("", "Please choose a file");
            }

            if (ModelState.IsValid)
            {
                var user = _context.AspNetUsers.Include(m => m.Candidate).Include(m => m.GeneralProfile).SingleOrDefault(m => m.UserName == User.Identity.Name);
                if (user != null)
                {
                    user.Candidate.Image = imagePath;
                    user.GeneralProfile.Image = imagePath;

                    _context.Update(user);

                    try
                    {
                        await _context.SaveChangesAsync();
                        TempData["message"] = "Success!";
                    }
                    catch (Exception e)
                    {
                        TempData["error"] = "Sorry an error occurred saving to the database, please try again";
                        ModelState.AddModelError("",
                            "Sorry an error occurred saving to the database, please try again");
                    }
                }
            }
            return Json(new { success = true });
        }

        public async Task<IActionResult> ApplyCV(int? positionJobId)
        {
            if (positionJobId == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var candidate =
                _context.Candidate.Include(m => m.IdNavigation)
                    .ThenInclude(m => m.GeneralProfile)
                    .SingleOrDefault(m => m.Id == user.Id);
            var position = _context.PositionJob.SingleOrDefault(m => m.Id == positionJobId);

            ViewData["Position"] = position;
            ViewData["Candidate"] = candidate;
            return PartialView();
        }

        [HttpPost]
        public async Task<IActionResult> ApplyCV(JobApplication jobApplication, IFormFile file)
        {
            string fileName = String.Empty;
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var candidate =
                _context.Candidate.Include(m => m.IdNavigation)
                    .ThenInclude(m => m.GeneralProfile)
                    .SingleOrDefault(m => m.Id == user.Id);
            var position = _context.PositionJob.SingleOrDefault(m => m.Id == jobApplication.PositionJobId);

            if (file != null)
            {
                if (Validate(file))
                {
                    try
                    {
                        var uploads = Path.Combine(_env.WebRootPath, "Uploads", "CV");
                        var extension = Path.GetExtension(file.FileName);
                        fileName = Path.GetFileNameWithoutExtension(file.FileName.ToLowerInvariant()) + Guid.NewGuid() + extension;
                        using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);
                            fileStream.Flush();
                        }
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("",
                            "Sorry an error occurred saving to the database, please try again");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The file must be doc, docx, pdf and less than 2MB in size");
                }
            }
            else
            {
                ModelState.AddModelError("", "You have to select a file!");
            }

            if (ModelState.IsValid)
            {
                if (_context.JobApplication.Where(m => m.PositionJobId == jobApplication.PositionJobId && m.CandidateId == candidate.Id).Count() > 0)
                {
                    ModelState.AddModelError("", "You have applied for this position!");
                    ViewData["Position"] = position;
                    ViewData["Candidate"] = candidate;
                    return PartialView(jobApplication);
                    //return Json(new { success = false });
                }
                else
                {
                    //candidate.Cvlink = fileName;
                    jobApplication.Linkfile = fileName;
                    jobApplication.JobStatus = false;

                    _context.Add(jobApplication);
                    await _context.SaveChangesAsync();
                    return Json(new { success = true });

                }
            }

            ViewData["Position"] = position;
            ViewData["Candidate"] = candidate;
            return PartialView(jobApplication);
        }

        public async Task<IActionResult> ActiveLayout(string layoutName)
        {
            if (layoutName == null)
            {
                return NotFound();
            }
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var candidate = _context.Candidate.Include(m => m.IdNavigation).Include(m => m.Layout).SingleOrDefault(m => m.Id == user.Id);
            var layout = await _context.Layout.SingleOrDefaultAsync(m => m.Title == layoutName);

            if (layout == null)
            {
                return NotFound();
            }
            try
            {
                candidate.LayoutId = layout.Id;
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return RedirectToAction("ListCV", "CurriculumVitae",
                    new { errorr = "Sorry an error occurred saving to the database, please try again" });
            }
            return RedirectToAction("ListCV", "CurriculumVitae");
        }
        public async Task<IActionResult> ActiveYourCV()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
            if (candidate.Cvlink != null)
            {
                candidate.LayoutId = null;
               await _context.SaveChangesAsync();
            }
            return RedirectToAction("ListCV", "CurriculumVitae");
        }



        public async  Task<IActionResult> AddYourCV()
        {
            var user =await  _userManager.FindByNameAsync(User.Identity.Name);
            var candidate = _context.Candidate.Find(user.Id);
            ViewBag.CVLink = candidate.Cvlink;
            return PartialView();
        }
        [HttpPost]
        public async Task<IActionResult> AddYourCV(IFormFile cvFile,bool isActive)
        {
            string fileName = String.Empty;
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var candidate = _context.Candidate.Find(user.Id);

            if (cvFile != null)
            {
                if (Validate(cvFile))
                {
                    try
                    {
                        var uploads = Path.Combine(_env.WebRootPath, "Uploads", "CV");
                        var extension = Path.GetExtension(cvFile.FileName);
                        fileName = Guid.NewGuid() + extension;
                        using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                        {
                            await cvFile.CopyToAsync(fileStream);
                            fileStream.Flush();
                        }
                        //delete old cv file
                        if (candidate.Cvlink != null)
                        {
                            if(System.IO.File.Exists(Path.Combine(uploads, candidate.Cvlink))){
                                System.IO.File.Delete(Path.Combine(uploads, candidate.Cvlink));
                            }
                        }
                        candidate.Cvlink = fileName;
                        if (isActive)
                        {
                            candidate.LayoutId = null;
                        }
                        _context.SaveChanges();
                        return Json(new { success = true });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("","Sorry an error occurred saving to the database, please try again");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The file must be doc, docx, pdf and less than 2MB in size");
                }
            }
            else
            {
                ModelState.AddModelError("", "You have to select a file!");
            }
            ViewBag.CVLink = candidate.Cvlink;
            return PartialView();
        }

        [HttpPost]
        public async Task<IActionResult> TogglePublish(bool isPublish)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var candidate = _context.Candidate.Include(m => m.IdNavigation).Include(m => m.Layout).SingleOrDefault(m => m.Id == user.Id);
            candidate.IsPublish = isPublish;
            await _context.SaveChangesAsync();
            return Json(new { success = true });

        }
        //[HttpPost]
        //public async Task<IActionResult> UploadFilesAjax()
        //{
        //    Candidate candidate = new Candidate();
        //    string imagePath = String.Empty;
        //    var file = Request.Form.Files[0];
        //    if (file != null)
        //    {
        //        if (_imageService.Validate(file))
        //        {
        //            try
        //            {
        //                imagePath = await _imageService.SaveAsync(file, _pathUpLoad.FrontImage);
        //            }
        //            catch (Exception e)
        //            {
        //                ModelState.AddModelError("",
        //                    "Sorry an error occurred saving the file to disk, please try again");
        //            }
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", "The file must be gif, png, jpge or jpg and less than 2MB in size");
        //        }
        //    }
        //    else
        //    {
        //        ModelState.AddModelError("", "Please choose a file");
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        var user = _context.AspNetUsers.SingleOrDefault(m => m.UserName == User.Identity.Name);
        //        if (user != null)
        //        {
        //            var isExist = _context.Company.Any(m => m.Id == user.Id);
        //            candidate = isExist ? _context.Candidate.SingleOrDefault(m => m.Id == user.Id) : new Candidate();
        //            candidate.Image = imagePath;
        //            company.IdNavigation = user;
        //            if (isExist)
        //            {
        //                _context.Update(company);
        //            }
        //            else
        //            {
        //                await _context.AddAsync(company);
        //            }

        //            try
        //            {
        //                await _context.SaveChangesAsync();
        //                return Json(new { success = true, path = imagePath });
        //            }
        //            catch (Exception e)
        //            {
        //                ModelState.AddModelError("",
        //                    "Sorry an error occurred saving to the database, please try again");
        //            }
        //        }
        //    }
        //    return PartialView("_UploadFilesAjax", company);
        //}
        [HttpPost]
        public async Task<IActionResult> SaveJob(int id)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var job = _context.JobSave.SingleOrDefault(m => m.PositionJobId == id && m.UserId == user.Id);

            if (job == null)
            {
                job = new JobSave
                {
                    PositionJobId = id,
                    UserId = user.Id
                };
                _context.JobSave.Add(job);

            }
            else
            {
                _context.JobSave.Remove(job);
            }

            try
            {
                await _context.SaveChangesAsync();
                TempData["message"] = "Success!";
            }
            catch (Exception e)
            {
                TempData["error"] = "An error occur, may be you have already saved this job!";
            }

            return RedirectToAction("Details", "Recruiment", new { id = id });
        }

        [HttpPost]
        public async Task<IActionResult> RemoveSavedJob([FromForm]IList<int> position)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (position != null && position.Any())
            {
                foreach (var id in position)
                {
                    var itemToDelete =
                        await _context.JobSave.FirstOrDefaultAsync(m => m.UserId == user.Id && m.PositionJobId == id);
                    if (itemToDelete != null)
                    {
                        _context.JobSave.Remove(itemToDelete);
                    }
                }
                try
                {
                    await _context.SaveChangesAsync();
                    TempData["message"] = "Delete success!";
                }
                catch (Exception e)
                {
                    TempData["error"] = "A error occur. Try again!";
                }
            }
            else
            {
                TempData["error"] = "Please choose job which you want to delete!";
            }

            return RedirectToAction("ListSavedJob");
        }
        public async Task<ActionResult> ListSavedJob(int? page = 1)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var candidate = _context.Candidate.Include(m => m.IdNavigation).ThenInclude(m => m.GeneralProfile).SingleOrDefault(m => m.Id == user.Id);
            return View(candidate);
        }

        public async Task<IActionResult> ListAppliedJob(int? page = 1)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var candidate = _context.Candidate.Include(m => m.IdNavigation).ThenInclude(m => m.GeneralProfile).SingleOrDefault(m => m.Id == user.Id);
            return View(candidate);
        }

        public async Task<IActionResult> RemoveApplyJob([FromForm]IList<int> position)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (position != null && position.Any())
            {
                foreach (var id in position)
                {
                    var itemToDelete =
                        await _context.JobApplication.FirstOrDefaultAsync(m => m.CandidateId == user.Id && m.PositionJobId == id);
                    if (itemToDelete != null)
                    {
                        _context.JobApplication.Remove(itemToDelete);
                    }
                }
                try
                {
                    await _context.SaveChangesAsync();
                    TempData["message"] = "Delete success!";
                }
                catch (Exception e)
                {
                    TempData["error"] = "A error occur. Try again!";
                }
            }
            else
            {
                TempData["error"] = "Please choose job which you want to delete!";
            }

            return RedirectToAction("ListAppliedJob");
        }

        public async Task<IActionResult> InterviewInvitation(int? page = 1)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var candidate = _context.Candidate.Include(m => m.IdNavigation).ThenInclude(m => m.GeneralProfile).SingleOrDefault(m => m.Id == user.Id);
            return View(candidate);
        }

        public async Task<IActionResult> RemoveInterviewInvitation([FromForm]IList<int> invitation)
        {
            foreach (int invite in invitation)
            {
                var interviewinvitation = _context.InterviewInvitation.Where(x => x.Id == invite).FirstOrDefault();
                if (interviewinvitation != null)
                {
                    interviewinvitation.CandidateDisable = true;
                    await _context.SaveChangesAsync();
                }
            }

            return RedirectToAction("InterviewInvitation");
        }


        #region helper
        public bool Validate(IFormFile file)
        {
            string fileExtentison = System.IO.Path.GetExtension(file.FileName).ToLower();
            string[] allowedFileType = { ".doc", ".docx", ".pdf" };
            if (file.Length > 0 && file.Length < 2097152 && allowedFileType.Contains(fileExtentison))
            {
                return true;
            }
            return false;
        }


        #endregion

        #region Network
        public async Task<IActionResult> MyFriends()
        {

            return View();
        }

        #endregion

    }

    public enum ManageMessageId
    {
        AddPhoneSuccess,
        AddLoginSuccess,
        ChangePasswordSuccess,
        SetTwoFactorSuccess,
        SetPasswordSuccess,
        RemoveLoginSuccess,
        RemovePhoneSuccess,
        Error
    }
}