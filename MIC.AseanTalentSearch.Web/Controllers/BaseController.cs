using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Http;
using MIC.AseanTalentSearch.Web.EF.Logical;

namespace MIC.AseanTalentSearch.Web.Controllers
{
    public class BaseController : Controller
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var url = HttpContext.Request.Query["lang"].ToString();
            if (url != null && url.Length > 0)
            {
                Response.Cookies.Append("Language", url, new CookieOptions());
                LanguageExtension.CurentLanguage = url;
            }
            base.OnActionExecuting(context);
        }
    }
}