using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF.Logical;
using Sakura.AspNetCore;
using MIC.AseanTalentSearch.Web.Services;

namespace MIC.AseanTalentSearch.Web.Controllers
{
    public class NewsController : BaseController
    {
        private readonly INewsManager _newsManager;
        public NewsController(INewsManager newsManager)
        {
            _newsManager = newsManager;
        }
        public IActionResult Index(string stringSearch = null)
        {
            return View();
        }

        public async Task<IActionResult> SearchNews(int? page, string stringSearch = null)
        {
            var listnews = _newsManager.GetAllNews(true);
            if (stringSearch != null)
                listnews = listnews.Where(x => x.Title.NonUnicode().Contains(stringSearch.NonUnicode()) || 
                x.Description.NonUnicode().Contains(stringSearch.NonUnicode()) || x.Content.NonUnicode().Contains(stringSearch.NonUnicode()));
            int pageSize = 10;
            int pageIndex = page ?? 1;
            var pagedData = await listnews.ToPagedListAsync(pageSize, pageIndex);

            var model = pagedData;

            return View("SearchResult", model);
        }

        public IActionResult Detail(int id)
        {
            var model = _newsManager.GetNewsShowById(id, true);
            if(model == null)
                {
                    Response.StatusCode = 404;
                    return View("Error");
                }
            return View(model);
        }
    }
}