using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MIC.AseanTalentSearch.Web.Controllers
{
    public class EmployerViewController : Controller
    {
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public EmployerViewController(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var candidate = _context.Candidate.Include(m => m.IdNavigation).ThenInclude(m => m.GeneralProfile).SingleOrDefault(m => m.Id == user.Id);
            return View(candidate);
        }
    }
}