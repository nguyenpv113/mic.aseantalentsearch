﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Infrastructure;
using MIC.AseanTalentSearch.Web.Models;
using MIC.AseanTalentSearch.Web.Models.Common;
using MIC.AseanTalentSearch.Web.Services;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Controllers {
    public class RecruimentController : BaseController {
        private readonly AseanTalentSearchContext _context;

        public RecruimentController (AseanTalentSearchContext context) {
            _context = context;
        }
        public IActionResult Index (int? categoryId, int? locationId, string jobName, int? jobLevelId) {
            ViewData["CategoryId"] = categoryId;
            ViewData["locationId"] = locationId;
            ViewData["JobName"] = jobName?.StripHtml ();
            //
            ViewData["JobLevelId"] = jobLevelId;
            return View ();
        }

        public async Task<IActionResult> RecruitSearch (int? page, List<int> categoryId, List<int> locationId, string jobName, IList<int> skillId, IList<int?> jobLevelId, string salaryLevel, IList<string> skillNames) {
            var model = new JobSearchViewModel {
                //CategoryId = categoryId,
                //LocationId = locationId,
                JobName = jobName?.StripHtml (),
                SkillId = skillId,
                JobLevelId = jobLevelId,
                SkillNames = skillNames
            };

            int pageSize = 10;
            int pageIndex = page ?? 1;
            var positionJobs =
                _context.PositionJob.Include (m => m.JobCategory)
                .ThenInclude (m => m.Category)
                .Include (m => m.Company)
                .Include (m => m.JobType)
                .Include (m => m.JobLocation)
                .ThenInclude (m => m.Location)
                .Include (m => m.JobSkill)
                .ThenInclude (m => m.KeySkill)
                .Include (m => m.JobLevel)
                .Where (m => m.JobStatus == true)
                .OrderByDescending (m => m.ClosedDate).AsQueryable ();

            //check company status (null|false then not show job)
            positionJobs = positionJobs.Where (m => m.Company.ActiveStatus == true);

            if (!String.IsNullOrEmpty (model.JobName)) {
                positionJobs =
                    positionJobs.Where (
                        m => m.PositionTitle.NonUnicode ().ToLower ().Contains (model.JobName.NonUnicode ().ToLower ()) ||
                        m.JobSkill.Any (s => s.KeySkill.Name.NonUnicode ().ToLower ().Contains (model.JobName.NonUnicode ().ToLower ())));
            }

            if (categoryId != null && categoryId.Any ()) {
                //positionJobs = positionJobs.Where(m => m.JobCategory.Any(c => c.CategoryId == model.CategoryId));
                positionJobs = positionJobs.Where (m => m.JobCategory.Any (c => categoryId.Contains (c.CategoryId)));
            }

            if (locationId != null && locationId.Any ()) {
                //positionJobs = positionJobs.Where(m => m.JobCategory.Any(c => c.CategoryId == model.CategoryId));
                positionJobs = positionJobs.Where (m => m.JobLocation.Any (c => locationId.Contains (c.LocationId)));
            }

            if (model.SkillId != null && model.SkillId.Any ()) {
                positionJobs = positionJobs.Where (m => m.JobSkill.Any (c => model.SkillId.Contains (c.KeySkillId)));
            }

            if (model.JobLevelId != null && model.JobLevelId.Any ()) {
                positionJobs = positionJobs.Where (m => model.JobLevelId.Contains (m.JobLevelId));
            }

            if (!string.IsNullOrEmpty (salaryLevel)) {
                switch (salaryLevel) {
                    case "<$500":

                        positionJobs = positionJobs.Where (m => (m.MinPay >= 0 && m.MinPay < 500) || (m.MaxPay >= 0 && m.MaxPay < 500));
                        break;
                    case "$500-$1000":
                        positionJobs = positionJobs.Where (m => (m.MinPay >= 500 && m.MinPay <= 1000) || (m.MaxPay >= 500 && m.MaxPay <= 1000));
                        break;
                    case "$1000-$1500":
                        positionJobs = positionJobs.Where (m => (m.MinPay >= 1000 && m.MinPay <= 1500) || (m.MaxPay >= 1000 && m.MaxPay <= 1500));
                        break;
                    case "$1500-$2000":
                        positionJobs = positionJobs.Where (m => (m.MinPay >= 1500 && m.MinPay <= 2000) || (m.MaxPay >= 1500 && m.MaxPay <= 2000));
                        break;
                    case ">$2000":
                        positionJobs = positionJobs.Where (m => m.MinPay > 2000 || m.MaxPay > 2000);
                        break;
                    default:
                        break;
                }
            }

            var pagedData = await positionJobs.ToPagedListAsync (pageSize, pageIndex);

            model.Result = pagedData;

            return View ("JobSearch", model);
        }

        public async Task<IActionResult> Details (int? id) {
            if (id == null) {
                return NotFound ();
            }

            var user = _context.Candidate.Include (m => m.IdNavigation)
                .SingleOrDefault (m => m.IdNavigation.UserName == User.Identity.Name);

            var possitionJob =
                await _context.PositionJob.Include (m => m.Company)
                .Include (m => m.JobSave)
                .Include (m => m.EducationLevel)
                .Include (m => m.JobCategory)
                .Include (m => m.JobType)
                .Include (m => m.JobLanguage)
                .ThenInclude (m => m.Language)
                .Include (m => m.JobLocation).
            ThenInclude (m => m.Location)
                .Include (m => m.JobSkill)
                .ThenInclude (m => m.KeySkill)
                .Include (m => m.JobCategory)
                .ThenInclude (m => m.Category)
                .Include (m => m.JobLevel)
                .OrderByDescending (m => m.ClosedDate)
                .Where (m => m.JobStatus == true)
                .SingleOrDefaultAsync (m => m.Id == id);
            if (possitionJob == null) {
                Response.StatusCode = 404;
                return View("Error");
            }
            if (user != null) {
                var jobSave = _context.JobSave.SingleOrDefault (m => m.PositionJobId == id && m.UserId == user.Id);
                ViewBag.Saved = jobSave;
                //check user applied 
                ViewBag.Applied = _context.JobApplication.Where (m => m.PositionJobId == id && m.CandidateId == user.Id).Count () > 0;
            }

            return View (possitionJob);
        }
        ///// <summary>
        ///// Hỗ trợ tìm kiếm công việc theo :
        ///// - Danh mục công việc
        ///// - Tiêu đề
        ///// - Vị trí : Name / EnlishName
        ///// </summary>
        ///// <param name="page"></param>
        ///// <param name="model"></param>
        ///// <returns>View nội dung công việc (được đính kèm theo sau Shared/Components/JobSerchForm)</returns>
        //public async Task<IActionResult> JobSearch([FromQuery] int? page, JobSearchForm model)
        //{
        //    // TODO : đang cố định pageSize = 5
        //    int pageSize = 5;
        //    int pageIndex = page ?? 1;
        //    if (model.CategoryId == null && model.LocationId == null && string.IsNullOrEmpty(model.JobName))
        //    {
        //        return Ok(string.Empty);
        //    }
        //    var jobs = await _context.PositionJob
        //        .Include(m => m.Company).ThenInclude(r => r.Location)
        //        .Include(m => m.EducationLevel)
        //        .Include(m => m.JobLevel)
        //        .Include(m => m.JobType)
        //        .Include(m => m.JobCategory)
        //        .Include(m => m.JobLocation).ThenInclude(r => r.Location)
        //        .Where(m => model.CategoryId == null || m.JobCategory.Any(r => r.CategoryId == model.CategoryId))
        //        .Where(m => model.JobName == null || m.PositionTitle.ToUpper().Contains(model.JobName.ToUpper()))
        //        .Where(m => model.LocationId == null || m.JobLocation.Any(r => r.LocationId == model.LocationId))
        //        .ToListAsync();
        //    var pagedData = jobs.ToPagedList(pageSize, pageIndex);
        //    return View(pagedData);
        //}
    }
}