﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models.Candidate;
using Microsoft.AspNetCore.Identity;
using MIC.AseanTalentSearch.Web.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MIC.AseanTalentSearch.Web.Controllers
{
    public class CommentController : Controller
    {
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public CommentController(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: /<controller>/
        public async Task<IActionResult> Index(string candidateId)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var candidate = _context.Candidate.Include(m => m.IdNavigation).ThenInclude(m => m.GeneralProfile).SingleOrDefault(m => m.Id == user.Id);
            return View(candidate);
            //candidateId=cvID
            //if (string.IsNullOrEmpty(candidateId))
            //{
            //    return NotFound();
            //}
            //var candidate = await _context.Candidate.SingleOrDefaultAsync(x => x.Cvid.ToString().Equals(candidateId));
            //if (candidate == null)
            //{
            //    return NotFound();
            //}

            //var data = await _context.CandidateComment.Where(m => m.UniversityId != null)
            //    .Include(m => m.UserComment)
            //    .ThenInclude(m => m.GeneralProfile)
            //    .Include(m => m.UserComment).ThenInclude(m => m.Instructor).ThenInclude(m => m.University)
            //    .Where(m => m.CandidateId == candidate.Id)
            //    .OrderByDescending(m => m.CreatedDate)
            //    .ToListAsync();


            //if (data == null || data.Count == 0)
            //{
            //    ViewData["response"] = "No comment for this candidate";
            //}
            //else
            //{
            //    ViewData["response"] = String.Format("Have {0} comments", data.Count);
            //}
            //List<CandidateCommentViewModel> model = new List<CandidateCommentViewModel>();
            //foreach (var item in data)
            //{
            //    string _University = item.UniversityId.HasValue ? _context.University.Find(item.UniversityId.Value).Name : "Please update University";
            //    var i = new CandidateCommentViewModel()
            //    {
            //        CondidateComment = item,
            //        _OldUniversity = _University
            //    };
            //    model.Add(i);
            //}
            //ViewBag.CurrentUser = GetNameCurrentUser(candidateId);
            //return View(model);
        }

        public string GetNameCurrentUser(string Cvid)
        {
            var userId = _context.Candidate.Where(m => m.Cvid.ToString() == Cvid).FirstOrDefault().Id;
            return _context.AspNetUsers.Find(userId).UserName;
        }

        public async Task<JsonResult> SetValueStatus(int Id)
        {
            try
            {
                var cm = await _context.CandidateComment.FindAsync(Id);
                var cvid = _context.Candidate.Find(cm.CandidateId).Cvid;
                cm.CommentStatus = cm.CommentStatus == null ? true : !cm.CommentStatus;
                _context.SaveChanges();
                return Json(new { success = true, url = Url.Action("Index", "Comment", new { candidateId = cvid.ToString() }) });
            }
            catch
            {
                return Json(null);
            }
        }
    }
}