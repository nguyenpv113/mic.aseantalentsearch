﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using Sakura.AspNetCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MIC.AseanTalentSearch.Web.Controllers {
    public class CompanyController : BaseController {
        private AseanTalentSearchContext _context;

        public CompanyController (AseanTalentSearchContext context) {
            _context = context;
        }
        // GET: /<controller>/

        public async Task<IActionResult> Details (Guid? id) {
            if (id == null) {
                return NotFound ();
            }
            var company = await _context.Company.Include (m => m.Location).Include (m => m.CompanyCategory).ThenInclude (m => m.Category)
                .Include (m => m.CompanyBenefit).ThenInclude (m => m.Benefit).Include (m => m.PositionJob).Where (m => m.ActiveStatus == true)
                .SingleOrDefaultAsync (m => m.Guid == id);

            if (company == null) {
                Response.StatusCode = 404;
                return View ("Error");
            }

            return View (company);
        }

        public async Task<IActionResult> List (int? page) {
            var data = await _context.Company.Include (m => m.PositionJob).Include (m => m.Location)
                .Include (x => x.CompanyCategory).Where (m => m.ActiveStatus == true).OrderByDescending (m => m.ModifiedDate).Select (
                    m => new HosttestCompanyViewModel {
                        Company = m,
                            Location = m.Location,
                            Vacancies = m.PositionJob.Count
                    }).ToListAsync ();

            int pageSize = 10;
            int pageIndex = page ?? 1;
            var dataPager = data.ToPagedList (pageSize, pageIndex);
            return View ("Hottest", dataPager);
        }
    }
}