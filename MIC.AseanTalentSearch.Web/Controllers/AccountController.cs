﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Infrastructure;
using MIC.AseanTalentSearch.Web.Models;
using MIC.AseanTalentSearch.Web.Models.AccountViewModels;
using MIC.AseanTalentSearch.Web.Services;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace MIC.AseanTalentSearch.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;
        private readonly ILogger _logger;
        private readonly string _externalCookieScheme;
        private readonly AseanTalentSearchContext _context;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IOptions<IdentityCookieOptions> identityCookieOptions,
            IEmailSender emailSender,
            ISmsSender smsSender,
            ILoggerFactory loggerFactory, RoleManager<ApplicationRole> roleManager, AseanTalentSearchContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _externalCookieScheme = identityCookieOptions.Value.ExternalCookieAuthenticationScheme;
            _emailSender = emailSender;
            _smsSender = smsSender;
            _roleManager = roleManager;
            _context = context;
            _logger = loggerFactory.CreateLogger<AccountController>();
        }


        //using for modal login(new layout)yh
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginAndRegister(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.Authentication.SignOutAsync(_externalCookieScheme);

            ViewData["ReturnUrl"] = returnUrl;
            return PartialView();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginAndRegister(LoginAndRegisterViewModel lrModel, string returnUrl = null)
        {
            if (!lrModel.IsRegister)//Login
            {
                #region Login
                ModelState["RegisterEmail"].ValidationState = Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Valid;
                ModelState["RegisterPassword"].ValidationState = Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Valid;
                ModelState["Role"].ValidationState = Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Valid;

                //check captcha 
                //if (!await CaptchaAuthenticate.AuthenticationAsync(Request, new HttpClient()))
                //{
                //    ModelState.AddModelError("recaptchaValidation", "Please authenticate captcha");
                //    return PartialView(lrModel);
                //}
                if (ModelState.IsValid)
                {
                    LoginViewModel model = new LoginViewModel
                    {
                        Email = lrModel.Email,
                        Password = lrModel.Password,
                        RememberMe = lrModel.RememberMe
                    };

                    ViewData["ReturnUrl"] = returnUrl;
                    // Require the user to have a confirmed email before they can log on.
                    var user = await _userManager.FindByNameAsync(model.Email);
                    if (user != null)
                    {
                        if (!await _userManager.IsEmailConfirmedAsync(user))
                        {
                            ModelState.AddModelError(string.Empty,
                                          "You must have a confirmed email to log in.");
                            return PartialView(lrModel);
                        }
                    }
                    // This doesn't count login failures towards account lockout
                    // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                    var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                    if (result.Succeeded)
                    {
                        //var candidate = _context.Candidate.Include(m => m.IdNavigation)
                        //    .SingleOrDefault(m => m.IdNavigation.UserName.Equals(model.Email, StringComparison.OrdinalIgnoreCase));

                        _logger.LogInformation(1, "User logged in.");
                        //return RedirectToLocal(returnUrl);
                        return Json(new { success = true });
                    }
                    if (result.RequiresTwoFactor)
                    {
                        return RedirectToAction(nameof(SendCode), new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                    }
                    if (result.IsLockedOut)
                    {
                        _logger.LogWarning(2, "User account locked out.");
                        return View("Lockout");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                        return PartialView(lrModel);
                    }

                }
                #endregion
            }
            else//Register
            {
                #region Register
                ModelState["Email"].ValidationState = Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Valid;
                ModelState["Password"].ValidationState = Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Valid;
                //check captcha 
                //if (!await CaptchaAuthenticate.AuthenticationAsync(Request, new HttpClient()))
                //{
                //    ModelState.AddModelError("recaptchaValidationRegister", "Please authenticate captcha");
                //    return PartialView(lrModel);
                //}
                if (ModelState.IsValid)
                {
                    ViewData["ReturnUrl"] = returnUrl;

                    RegisterViewModel model = new RegisterViewModel
                    {
                        Email = lrModel.RegisterEmail,
                        Password = lrModel.RegisterPassword,
                        ConfirmPassword = lrModel.RegisterConfirmPassword,
                        Role = lrModel.Role
                    };
                    try
                    {
                        var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                        var result = await _userManager.CreateAsync(user, model.Password);
                        if (result.Succeeded)
                        {
                            if (!String.IsNullOrEmpty(model.Role))
                            {
                                IList<string> acceptedRoles = new List<string>
                                {
                                    "Candidate",
                                    "Employer",
                                    "Instructor"
                                };

                                if (acceptedRoles.Contains(model.Role))
                                {
                                    //Set IsConfig = true => disable send mail
                                    user.EmailConfirmed = true;
                                    IdentityResult roleResult = await _userManager.AddToRoleAsync(user, model.Role);
                                    if (roleResult.Succeeded)
                                    {
                                        var result1 = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, lockoutOnFailure: false);
                                        if (model.Role.Equals("Candidate"))
                                        {
                                            Candidate candidate = Factory.CreateCandidate();
                                            GeneralProfile profile = Factory.CreateProfile();
                                            AspNetUsers aspNetUser =
                                                _context.AspNetUsers.SingleOrDefault(m => m.UserName == user.UserName);
                                            aspNetUser.Candidate = candidate;
                                            aspNetUser.GeneralProfile = profile;
                                            //Set IsConfig = true => disable send mail
                                            aspNetUser.EmailConfirmed = true;
                                            await _context.SaveChangesAsync();
                                        }
                                        //Nhat-do disable send email
                                        //// For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=532713
                                        //// Send an email with this link
                                        //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                                        //var callbackUrl = Url.Action(nameof(ConfirmEmail), "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);

                                        ////
                                        //string mailContent = "<div style=\"background:#f3f3f3; text-align: center; padding:0; margin:0;\">" +
                                        // "<div style=\"text-align: center; background:white;display:inline-block; min-width: 800px; max-width: 100%;\">" +
                                        // "<div style=\"display: inline-block; text-align: left\">" +
                                        // "<center><h1 style =\"color:#14b1bb\">Welcome to Contact Asean </h1></center>" +
                                        // $"<p>Dear <b>{user.UserName}</b></p>" +
                                        // "<p>Congratulations on successful registration of your account on Contact Asean!</p>" +
                                        // "<p>Your account information is as follows:</p>" +
                                        // $"<p>Email : {user.UserName}</p>" +
                                        // "<p>To start looking for effective work with Contact Asean, you need: </p>" +
                                        // $"Please confirm your account by clicking this link: <a href = '{callbackUrl}' > <button style=\"background:#5bc0de; color:white; border-radius:4px; padding:5px 10px; cursor:pointer;\"> Click here </button></a>" +
                                        // "<p><ul><li>Completing your resume is a way to show your serious attitude to your career.</li>" +
                                        // "<li>Completing the profile is the way you invest for your own future.</li>" +
                                        // "<li>Complete the application to apply for free and at least 20 business contacts after 24h.</li></ul></p>" +
                                        // "	" +
                                        // "Please contact with Contact Asean if you need a help : " +
                                        // "<ul><li>Room 221 - Duy Tan University - 03 Quang Trung , Hai Chau district , Da Nang city </li>" +
                                        // "<li>Phone number : (0236)-3-827111 (Ext. 221)</li>" +
                                        // "</div></div></div>";



                                        /////

                                        //await _emailSender.SendEmailAsync(model.Email, "Confirm your account",
                                        //    mailContent);
                                        ////await _signInManager.SignInAsync(user, isPersistent: false);
                                        //_logger.LogInformation(3, "User created a new account with password.");
                                        //TempData["message"] = $"Please confirm your account by click the link in email have sent to you!";

                                        return Json(new { success = true, role = model.Role });
                                    }
                                }
                                else
                                {
                                    foreach (IdentityError error in result.Errors)
                                    {
                                        ModelState.AddModelError("", $"Error code: {error.Code}, Description: {error.Description}");
                                    }
                                    return View(lrModel);
                                }
                            }

                        }
                        AddErrors(result);


                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e.Message);
                    }
                }
                #endregion
            }
            return PartialView(lrModel);
        }



        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.Authentication.SignOutAsync(_externalCookieScheme);

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            //check captcha 
            //if (!await CaptchaAuthenticate.AuthenticationAsync(Request, new HttpClient()))
            //{
            //    ModelState.AddModelError("recaptchaValidation", "Please authenticate captcha");
            //    return View(model);
            //}

            // ModelState["ConfirmPassword"].ValidationState = Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Valid;

            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // Require the user to have a confirmed email before they can log on.
                var user = await _userManager.FindByNameAsync(model.Email);
                if (user != null)
                {
                    if (!await _userManager.IsEmailConfirmedAsync(user))
                    {
                        ModelState.AddModelError(string.Empty,
                                      "You must have a confirmed email to log in.");
                        return View(model);
                    }
                }
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    var candidate = _context.Candidate.Include(m => m.IdNavigation)
                        .SingleOrDefault(m => m.IdNavigation.UserName.Equals(model.Email, StringComparison.OrdinalIgnoreCase));

                    _logger.LogInformation(1, "User logged in.");
                    return RedirectToLocal(returnUrl);
                }
                if (result.RequiresTwoFactor)
                {
                    return RedirectToAction(nameof(SendCode), new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                }
                if (result.IsLockedOut)
                {
                    _logger.LogWarning(2, "User account locked out.");
                    return View("Lockout");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Register
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            // check captcha
            if (!await CaptchaAuthenticate.AuthenticationAsync(Request, new HttpClient()))
            {
                ModelState.AddModelError("recaptchaValidation", "Please authenticate captcha");
                return View(model);
            }

            ViewData["ReturnUrl"] = returnUrl;

            try
            {


                if (ModelState.IsValid)
                {
                    var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        if (!String.IsNullOrEmpty(model.Role))
                        {
                            IList<string> acceptedRoles = new List<string>
                        {
                            "Candidate",
                            "Employer",
                            "Instructor"
                        };

                            if (acceptedRoles.Contains(model.Role))
                            {
                                //Set IsConfig = true => disable send mail
                                user.EmailConfirmed = true;
                                IdentityResult roleResult = await _userManager.AddToRoleAsync(user, model.Role);
                                if (roleResult.Succeeded)
                                {
                                    if (model.Role.Equals("Candidate"))
                                    {
                                        Candidate candidate = Factory.CreateCandidate();
                                        GeneralProfile profile = Factory.CreateProfile();
                                        AspNetUsers aspNetUser =
                                            _context.AspNetUsers.SingleOrDefault(m => m.UserName == user.UserName);
                                        aspNetUser.Candidate = candidate;
                                        aspNetUser.GeneralProfile = profile;
                                        //astive email
                                        aspNetUser.EmailConfirmed = true;
                                        await _context.SaveChangesAsync();
                                    }

                                    //Nhat-do disable send email
                                    //// For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=532713
                                    //// Send an email with this link
                                    //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                                    //var callbackUrl = Url.Action(nameof(ConfirmEmail), "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);

                                    ////
                                    //string mailContent = "<div style=\"background:#f3f3f3; text-align: center; padding:0; margin:0;\">" +
                                    // "<div style=\"text-align: center; background:white;display:inline-block; min-width: 800px; max-width: 100%;\">" +
                                    // "<div style=\"display: inline-block; text-align: left\">" +
                                    // "<center><h1 style =\"color:#14b1bb\">Welcome to Contact Asean </h1></center>" +
                                    // $"<p>Dear <b>{user.UserName}</b></p>" +
                                    // "<p>Congratulations on successful registration of your account on Contact Asean!</p>" +
                                    // "<p>Your account information is as follows:</p>" +
                                    // $"<p>Email : {user.UserName}</p>" +
                                    // "<p>To start looking for effective work with Contact Asean, you need: </p>" +
                                    // $"Please confirm your account by clicking this link: <a href = '{callbackUrl}' > <button style=\"background:#5bc0de; color:white; border-radius:4px; padding:5px 10px; cursor:pointer;\"> Click here </button></a>" +
                                    // "<p><ul><li>Completing your resume is a way to show your serious attitude to your career.</li>" +
                                    // "<li>Completing the profile is the way you invest for your own future.</li>" +
                                    // "<li>Complete the application to apply for free and at least 20 business contacts after 24h.</li></ul></p>" +
                                    // "	" +
                                    // "Please contact with Contact Asean if you need a help : " +
                                    // "<ul><li>Room 221 - Duy Tan University - 03 Quang Trung , Hai Chau district , Da Nang city </li>" +
                                    // "<li>Phone number : (0236)-3-827111 (Ext. 221)</li>" +
                                    // "</div></div></div>";



                                    /////




                                    //await _emailSender.SendEmailAsync(model.Email, "Confirm your account",
                                    //    mailContent);
                                    ////await _signInManager.SignInAsync(user, isPersistent: false);
                                    //_logger.LogInformation(3, "User created a new account with password.");
                                    //TempData["message"] = $"Please confirm your account by click the link in email have sent to you!";
                                    //return RedirectToLocal(returnUrl);
                                    return RedirectToAction("Login");
                                }
                            }
                            else
                            {
                                foreach (IdentityError error in result.Errors)
                                {
                                    ModelState.AddModelError("", $"Error code: {error.Code}, Description: {error.Description}");
                                }
                                return View(model);
                            }
                        }

                    }
                    AddErrors(result);
                }

            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }


            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public async Task<IActionResult> LogOff()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation(4, "User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        //
        // POST: /Account/Logout

        public async Task<IActionResult> Logout()
        {

            await _signInManager.SignOutAsync();
            _logger.LogInformation(4, "User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            var redirectUrl = Url.Action(nameof(ExternalLoginCallback), "Account", new { ReturnUrl = returnUrl });
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return Challenge(properties, provider);
        }

        //
        // GET: /Account/ExternalLoginCallback
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            if (remoteError != null)
            {
                ModelState.AddModelError(string.Empty, $"Error from external provider: {remoteError}");
                return View(nameof(Login));
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return RedirectToAction(nameof(Login));
            }

            // Sign in the user with this external login provider if the user already has a login.
            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false);
            if (result.Succeeded)
            {
                _logger.LogInformation(5, "User logged in with {Name} provider.", info.LoginProvider);
                return RedirectToLocal(returnUrl);
            }
            if (result.RequiresTwoFactor)
            {
                return RedirectToAction(nameof(SendCode), new { ReturnUrl = returnUrl });
            }
            if (result.IsLockedOut)
            {
                return View("Lockout");
            }
            else
            {
                // If the user does not have an account, then ask the user to create an account.
                ViewData["ReturnUrl"] = returnUrl;
                ViewData["LoginProvider"] = info.LoginProvider;
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                //check role
                IList<string> acceptedRoles = new List<string>
                        {
                            "Candidate",
                            "Employer",
                            "Instructor"
                        };
                if (model.Role == null || !acceptedRoles.Contains(model.Role))
                {
                    ModelState.AddModelError("", "Please select type of account(Candidate|Employer|Instructor)!");
                    return View(model);
                }

                // Get the information about the user from the external login provider
                var info = await _signInManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, EmailConfirmed = true };
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userManager.AddLoginAsync(user, info);
                    if (result.Succeeded)
                    {


                        IdentityResult roleResult = await _userManager.AddToRoleAsync(user, model.Role);
                        if (roleResult.Succeeded)
                        {
                            if (model.Role.Equals("Candidate"))
                            {

                                Candidate candidate = Factory.CreateCandidate();
                                GeneralProfile profile = Factory.CreateProfile();
                                AspNetUsers aspNetUser =
                                    _context.AspNetUsers.SingleOrDefault(m => m.UserName == user.UserName);
                                aspNetUser.Candidate = candidate;
                                aspNetUser.GeneralProfile = profile;
                                await _context.SaveChangesAsync();
                            }
                        }
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation(6, "User created an account using {Name} provider.", info.LoginProvider);

                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(model);
        }

        // GET: /Account/ConfirmEmail
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return View("Error");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=532713
                // Send an email with this link
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.Action(nameof(ResetPassword), "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                await _emailSender.SendEmailAsync(model.Email, "Reset Password",
                   $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");
                return View("ForgotPasswordConfirmation");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(AccountController.ResetPasswordConfirmation), "Account");
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(AccountController.ResetPasswordConfirmation), "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/SendCode
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl = null, bool rememberMe = false)
        {
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            var userFactors = await _userManager.GetValidTwoFactorProvidersAsync(user);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                return View("Error");
            }

            // Generate the token and send it
            var code = await _userManager.GenerateTwoFactorTokenAsync(user, model.SelectedProvider);
            if (string.IsNullOrWhiteSpace(code))
            {
                return View("Error");
            }

            var message = "Your security code is: " + code;
            if (model.SelectedProvider == "Email")
            {
                await _emailSender.SendEmailAsync(await _userManager.GetEmailAsync(user), "Security Code", message);
            }
            else if (model.SelectedProvider == "Phone")
            {
                await _smsSender.SendSmsAsync(await _userManager.GetPhoneNumberAsync(user), message);
            }

            return RedirectToAction(nameof(VerifyCode), new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/VerifyCode
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> VerifyCode(string provider, bool rememberMe, string returnUrl = null)
        {
            // Require that the user has already logged in via username/password or external login
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes.
            // If a user enters incorrect codes for a specified amount of time then the user account
            // will be locked out for a specified amount of time.
            var result = await _signInManager.TwoFactorSignInAsync(model.Provider, model.Code, model.RememberMe, model.RememberBrowser);
            if (result.Succeeded)
            {
                return RedirectToLocal(model.ReturnUrl);
            }
            if (result.IsLockedOut)
            {
                _logger.LogWarning(7, "User account locked out.");
                return View("Lockout");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid code.");
                return View(model);
            }
        }

        //
        // GET /Account/AccessDenied
        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
