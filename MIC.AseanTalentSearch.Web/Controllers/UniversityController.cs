using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Controllers
{
    public class UniversityController : BaseController
    {
        private AseanTalentSearchContext _context;

        public UniversityController(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult GetMajorByUniversity(int? uid, int ? elid)
        {
            if (!uid.HasValue|| !elid.HasValue)
                return Json(null);
            else
            {
                var model = _context.Major.Where(e => e.UniversityId == uid.Value&&e.EducationLevelId==elid).OrderBy(x=>x.Name);
                return Json(model);
            }
        }

        public IActionResult Detail(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var university = _context.University.Include(m=>m.Location).FirstOrDefault(m=>m.Id == id);
            ViewBag.StudentCount = _context.EducationHistory.Include(m => m.Candidate).ThenInclude(m => m.IdNavigation)
                .ThenInclude(m => m.AspNetUserRoles).ThenInclude(m => m.Role).Include(m => m.Major)
                .ThenInclude(m => m.University)
                .Where(m => m.Major.University.Id == id &&
                            m.Candidate.IdNavigation.AspNetUserRoles.Any(u => u.Role.Name == "Candidate"))
                .Select(m => m.CandidateId)
                .Distinct().Count();
            if (university == null)
            {
                return NotFound();
            }
            return View(university);
        }

        public async Task<IActionResult> List(int? page)
        {
            var data = await _context.University.Include(m => m.Location).OrderByDescending(m => m.ModifiedDate).Where(a => a.ActiveStatus == true).Select(
                 m => new HottestUniversityViewModel
                 {
                     University = m,
                     Location = m.Location
                 }).ToListAsync();
            foreach (var item in data)
            {
                item.StudentCount = GetNumberCandidate(item.University.Id);
            }
            int pageSize = 10;
            int pageIndex = page ?? 1;
            var dataPager = data.ToPagedList(pageSize, pageIndex);
            return View("Hottest", dataPager);
        }


        public int? GetNumberCandidate(int UId)
        {
            var rs = from major in _context.Major
                     join eh in _context.EducationHistory on major.Id equals eh.MajorId
                     join can in _context.Candidate on eh.CandidateId equals can.Id
                     where major.UniversityId == UId
                     select new
                     {
                         id = eh.Candidate.Id,
                         name = eh.Candidate.FullName
                     };
            return rs.GroupBy(m => m.id).Count();
        }
    }
}