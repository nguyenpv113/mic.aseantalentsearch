﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MIC.AseanTalentSearch.Web.Controllers
{
    public class LocationController : Controller
    {
        private readonly AseanTalentSearchContext _context;

        public LocationController(AseanTalentSearchContext context)
        {
            _context = context;
        }

        // GET: /<controller>/
        public async Task<IActionResult> GetJobLocation(string search)
        {
            var data = await _context.Location.Where(m => m.Type == 2)
                .Where(m => m.Name.Contains(search)).OrderBy(m => m.Name).Select(
                    m => new
                    {
                        Id = m.Id,
                        Text = m.Name
                    }).ToListAsync();
            return Json(data);
        }
    }
}
