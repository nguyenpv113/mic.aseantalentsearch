using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using System.Text.RegularExpressions;
using System.Security.Claims;
using MIC.AseanTalentSearch.Web.EF.Logical;
using Microsoft.AspNetCore.Http;
using MIC.AseanTalentSearch.Web.Models.Candidate;

namespace MIC.AseanTalentSearch.Web.Controllers
{
    public class CurriculumVitaeController : BaseController
    {
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public CurriculumVitaeController(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index(string name, Guid? id)
        {
            if (string.IsNullOrEmpty(name))
                name = "Theme-1";
            if (string.IsNullOrEmpty(name) || id == null)
            {
                return NotFound();
            }

            string path = $"~/wwwroot/Themes/{name}/Index.cshtml";
            var layout = await _context.Layout.SingleOrDefaultAsync(m => m.Title == name);
            var data = await
               _context.Candidate.Include(m => m.CandidateCategory)
                    .ThenInclude(m => m.Category)
                    .Include(m => m.ExpectedJobLevel)
                    .Include(m => m.IdNavigation)
                    .ThenInclude(m => m.GeneralProfile)
                    .Include(m => m.CandidateSkill)
                    .ThenInclude(m => m.Skill)
                    .Include(m => m.EducationHistory)
                    .ThenInclude(m => m.Major)
                    .ThenInclude(m => m.University)
                    .Include(m => m.EmploymentHistory)
                    .Include(m => m.Layout)
                    .FirstOrDefaultAsync(m => m.Cvid == id);

            if (data == null || layout == null)
            {
                return NotFound();
            }

            ViewBag.ThemeName = name;
            ViewBag.Title = data.FullName;

           //concat address
            string fullAddress = data.IdNavigation.GeneralProfile.HomeAddress??"";
            if (data.IdNavigation.GeneralProfile.HomeDistrict != null)
            {
                fullAddress += ", " + _context.Location.Find(int.Parse(data.IdNavigation.GeneralProfile.HomeDistrict)).Name;
            }
            if (data.IdNavigation.GeneralProfile.HomeCity != null)
            {
                fullAddress += ", " + _context.Location.Find(int.Parse(data.IdNavigation.GeneralProfile.HomeCity)).Name;
            }
            if (data.IdNavigation.GeneralProfile.HomeCountry != null)
            {
                fullAddress += ", " + _context.Location.Find(int.Parse(data.IdNavigation.GeneralProfile.HomeCountry)).Name;
            }
            var regex = new Regex("^, ");
            data.Address = regex.Replace(fullAddress, "").Trim();

            var data2 = await _context.CandidateComment.Where(m => m.UniversityId != null)
                .Include(m => m.UserComment)
                .ThenInclude(m => m.GeneralProfile)
                .Include(m => m.UserComment).ThenInclude(m => m.Instructor).ThenInclude(m => m.University)
                .Where(m => m.CandidateId == data.Id)
                .OrderByDescending(m => m.CreatedDate)
                .ToListAsync();
            List<CandidateCommentViewModel> model = new List<CandidateCommentViewModel>();
            foreach (var item in data2)
            {
                string _University = item.UniversityId.HasValue ? _context.University.Find(item.UniversityId.Value).Name : "Please update University";
                var i = new CandidateCommentViewModel()
                {
                    CondidateComment = item,
                    _OldUniversity = _University
                };
                model.Add(i);
            }

            ViewBag.ListCommnet = model;

            return View(path, data);
        }

        public async Task<IActionResult> ShowCv(Guid? id)
        {
            var candidate =
                _context.Candidate
                   .Include(m => m.Layout)
                    .FirstOrDefault(m => m.Cvid == id);
            if (candidate.IsPublish != null && candidate.IsPublish.Value)
            {
                //view your cv
                if (candidate.Cvlink != null && candidate.LayoutId == null)
                {
                    ViewBag.CVName = candidate.FullName;
                    ViewBag.CVLink = candidate.Cvlink;
                    return View("PreviewCV");
                }

                //cv from website
                var themeName = _context.Layout.FirstOrDefault().Title;
                if (candidate.Layout != null)
                {
                    themeName = (candidate.Layout.Title ?? _context.Layout.FirstOrDefault().Title);
                }

                return await Index(themeName, candidate.Cvid);
            }
            return NotFound();
        }

        [Authorize(Roles = "Candidate")]
        public async Task<IActionResult> ListCV()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var candidate = _context.Candidate.Include(m => m.IdNavigation).Include(m => m.Layout).SingleOrDefault(m => m.Id == user.Id);
            var layout = await _context.Layout.ToListAsync();
            ViewBag.Candidate = candidate;
            return View(layout);
        }

        //add view
        [HttpPost]
        public async Task<JsonResult> AddView(Guid? id)
        {
            var candidate =
                _context.Candidate
                   .Include(m => m.Layout)
                    .FirstOrDefault(m => m.Cvid == id);
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId != null)
            {
                long? companyId = long.Parse(userId);
                if (companyId == null)
                {
                    return Json(false);
                }
                if (await _context.Company.FindAsync(companyId) != null)
                {
                    var view = new ViewCV
                    {
                        CandidateId = candidate.Id,
                        CompanyId = companyId.Value
                    };
                    view.CreatedDate = DateTime.Now;
                    _context.ViewCV.Add(view);
                    _context.SaveChanges();
                    return Json(true);
                }
            }
            return Json(false);
        }

        //your cv

        public IActionResult PreviewCV(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var candidate = _context.Candidate.FirstOrDefault(m => m.Cvid == id);
            if (candidate == null)
            {
                return NotFound();
            }
            ViewBag.CVName = candidate.FullName;
            ViewBag.CVLink = candidate.Cvlink;
            return View();
        }
    }
}