﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Settings
{
    public class SmtpConfig
    {
        public string Server { get; set; }
        public string UserName { get; set; }
        public string MailLabel { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public string Email { get; set; }
    }
}
