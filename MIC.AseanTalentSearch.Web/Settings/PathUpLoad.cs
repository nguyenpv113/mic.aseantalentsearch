﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Settings
{
    public class PathUpLoad
    {
        public string FrontImage { get; set; }
        public string FrontThumbnails { get; set; }
        public string BackImage { get; set; }
        public string BackThumbnails { get; set; }
    }
}
