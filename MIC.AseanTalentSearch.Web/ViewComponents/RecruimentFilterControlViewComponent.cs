﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using MIC.AseanTalentSearch.Web.Models.Common;

namespace MIC.AseanTalentSearch.Web.ViewComponents
{
    public class RecruimentFilterControlViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;

        public RecruimentFilterControlViewComponent(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = new FilterControl
            {
                JobLevels = await _context.JobLevel.Include(m => m.PositionJob).Where(m => m.PositionJob.Any())
                    .OrderBy(m => m.JobLevelName).ToListAsync(),
                Skills = await _context.Skill.Include(m => m.JobSkill).ThenInclude(m => m.PositionJob)
                    .Where(m => m.JobSkill.Any(p => p.PositionJob != null)).OrderBy(m => m.Name).ToListAsync(),
                Categories = await _context.Category.Include(m => m.JobCategory).ThenInclude(m => m.PositionJob)
                    .Where(m => m.JobCategory.Any(p => p.PositionJob != null)).OrderBy(m => m.Name).ToListAsync(),
                Locations = await _context.Location.Include(m => m.JobLocation).ThenInclude(m => m.PositionJob).Where(m=>m.Type == 2)
                    .Where(m => m.JobLocation.Any(p => p.PositionJob != null)).OrderBy(m => m.Name).ToListAsync()
            };
            model.SalaryLevel.Add("<$500");
            model.SalaryLevel.Add("$500-$1000");
            model.SalaryLevel.Add("$1000-$1500");
            model.SalaryLevel.Add("$1500-$2000");
            model.SalaryLevel.Add(">$2000");
            return View(model);
        }
    }
}
