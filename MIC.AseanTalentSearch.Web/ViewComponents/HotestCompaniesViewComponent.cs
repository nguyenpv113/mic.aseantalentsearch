﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.ViewComponents
{
    public class HotestCompaniesViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;

        public HotestCompaniesViewComponent(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
           var companies = await _context.Company.Where(m=>m.ActiveStatus == true).OrderBy(m=>m.CompanySize).Take(8).ToListAsync();
            return View(companies);
        }
    }
}
