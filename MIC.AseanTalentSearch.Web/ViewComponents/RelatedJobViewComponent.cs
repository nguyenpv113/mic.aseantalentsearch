﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;

namespace MIC.AseanTalentSearch.Web.ViewComponents
{
    public class RelatedJobViewComponent : ViewComponent
    {
    
        private readonly AseanTalentSearchContext _context;

        public RelatedJobViewComponent(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(int? id)
        {
            List<PositionJob> result = new List<PositionJob>();
            if (id != null)
            {
                var conn = _context.Database.GetDbConnection();
                try
                {
                    await conn.OpenAsync();
                    using (var command = conn.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        SqlParameter parameter = new SqlParameter("@PositionJob", id);
                        command.CommandText = "PeopleAlsoViewed";
                        command.Parameters.Add(parameter);
                        DbDataReader reader = await command.ExecuteReaderAsync();

                        if (reader.HasRows)
                        {
                            while (await reader.ReadAsync())
                            {
                                //var row = new PositionJob() { EnrollmentDate = reader.GetDateTime(0), StudentCount = reader.GetInt32(1) };
                                var row =
                                    await _context.PositionJob.Include(x => x.JobLocation).SingleOrDefaultAsync(m => m.Id == reader.GetInt32(0) && m.Id != id);
                                if (row != null)
                                {
                                    result.Add(row);
                                }
                            }
                        }
                        reader.Dispose();
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
            return View(result.Where(m=>m.JobStatus == true).OrderByDescending(m=>m.ClosedDate));
        }
    }
}
