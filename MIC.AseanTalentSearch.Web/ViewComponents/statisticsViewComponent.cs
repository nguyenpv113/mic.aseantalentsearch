﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;

namespace MIC.AseanTalentSearch.Web.ViewComponents
{
    public class StatisticsViewComponent : ViewComponent
    {
        private AseanTalentSearchContext _context;
        public StatisticsViewComponent(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            StatisticsViewModel model = new StatisticsViewModel();
            model.CandidateCount = await _context.Candidate.CountAsync();
            model.JobCount = await _context.PositionJob.CountAsync();
            model.CompanyCount = await _context.Company.CountAsync(x=>x.ActiveStatus==true);
            model.UniversityCount = await _context.University.CountAsync(x=>x.ActiveStatus==true);
            model.siteConfig = _context.SiteConfig;
            return View(model);
        }
    }
}
