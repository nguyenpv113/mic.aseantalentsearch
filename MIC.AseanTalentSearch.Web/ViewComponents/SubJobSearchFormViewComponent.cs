using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.AspNetCore.Mvc.Rendering;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.EntityFrameworkCore;

namespace MIC.AseanTalentSearch.Web.ViewComponents
{
    public class SubJobSearchFormViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;

        public SubJobSearchFormViewComponent(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(string bgUrl = "~/images/bgchitietcty.png")
        {
            var model = new JobSearchViewModel();
            model.BackgroundUrl = bgUrl;
            model.CategoryIdSelectList = new SelectList(await _context.Category.OrderBy(m => m.Name).ToListAsync(), "Id", "Name");
            var locationGroup = await GetLocationGroupAsync();

            var listLocation = new List<SelectListItem>();
            foreach (var item in await _context.Location.OrderBy(m => m.Name).Where(m => m.Type == 2).ToListAsync())
            {
                var selectItem = new SelectListItem
                {
                    Value = item.Id.ToString(),
                    Text = item.Name,
                    Group = locationGroup[item.ParentId]
                };
                listLocation.Add(selectItem);
            }
            model.LocationIdSelectList = listLocation;
            return View(model);
        }

        private async Task<Dictionary<int?, SelectListGroup>> GetLocationGroupAsync()
        {
            var locationGroup = new Dictionary<int?, SelectListGroup>();
            var countries = await _context.Location.OrderBy(m => m.Name).Where(m => m.Type == 1).ToListAsync();
            foreach (var country in countries)
            {
                locationGroup.Add(country.Id, new SelectListGroup { Name = country.Name });
            }
            return locationGroup;
        }
    }
}