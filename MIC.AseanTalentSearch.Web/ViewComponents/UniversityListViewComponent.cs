﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;

namespace MIC.AseanTalentSearch.Web.ViewComponents
{
    public class UniversityListViewComponent : ViewComponent
    {
        private AseanTalentSearchContext _context;
        public UniversityListViewComponent(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var universities = await _context.University.Where(m => m.ActiveStatus.Value).ToListAsync();
            List<HottestUniversityViewModel> lHU = new List<HottestUniversityViewModel>();
            foreach (var item in universities)
            {
                var cHU = new HottestUniversityViewModel
                {
                    University = item,
                    StudentCount = GetNumberCandidate(item.Id)
                };
                lHU.Add(cHU);
            }
            return View(lHU.OrderBy(m => m.University.Name).OrderByDescending(m => m.StudentCount).Take(8).ToList());
        }

        public int? GetNumberCandidate(int UId)
        {
            var rs = from major in _context.Major
                     join eh in _context.EducationHistory on major.Id equals eh.MajorId
                     join can in _context.Candidate on eh.CandidateId equals can.Id
                     where major.UniversityId == UId
                     select new
                     {
                         id = eh.Candidate.Id,
                         name = eh.Candidate.FullName
                     };
            return rs.GroupBy(m => m.id).Count();
        }
    }
}
