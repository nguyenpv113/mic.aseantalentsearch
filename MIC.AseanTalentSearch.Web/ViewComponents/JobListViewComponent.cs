﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.ViewComponents
{
    public class JobListViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;

        public JobListViewComponent(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(int takeItem = 10, int skip = 0, bool isHeightest = false)
        {
            var jobPositions = _context.PositionJob.Include(m => m.JobCategory)
                .ThenInclude(m => m.Category)
                .Include(m => m.Company)
                .Include(m => m.JobType)
                .Include(m => m.JobLocation)
                .ThenInclude(m => m.Location)
                .Include(m => m.JobSkill)
                .Include(m => m.JobLevel).Where(m => m.JobStatus == true && m.Company.ActiveStatus == true && m.ClosedDate >= DateTime.Now);

            if (isHeightest == true)
            {
                //jobPositions = jobPositions.Where(m => m.MinPay >= 1000);
                jobPositions = jobPositions.OrderByDescending(x => x.MaxPay);
                jobPositions = OrderList(jobPositions.ToList()).AsQueryable();
            }
            var model = jobPositions.ToList();
            ViewBag.JobCount = model.Count>takeItem?takeItem:model.Count;
            //add check company status
            return View(model.Skip(skip).Take(takeItem));
        }

        private List<PositionJob> OrderList(List<PositionJob> list)
        {
            var n = list.Count;
            do
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].MaxPay == null)
                    {
                        if (i - 1 >= 0)
                        {
                            if (list[i].MinPay > list[i - 1].MaxPay)
                            {
                                var temp = list[i];
                                list[i] = list[i - 1];
                                list[i - 1] = temp;
                            }
                        }
                    }
                }
                n--;
            }
            while (n > 0);
            
            return list;
        }
    }
}
