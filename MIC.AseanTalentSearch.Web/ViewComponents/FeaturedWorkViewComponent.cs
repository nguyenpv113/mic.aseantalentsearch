﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.ViewComponents
{
    public class FeaturedWorkViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;

        public FeaturedWorkViewComponent(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
           var jobPositions = await _context.PositionJob.Include(m => m.Company)
               .Include(m => m.EducationLevel)
               .Include(m => m.JobCategory)
               .Include(m => m.JobType)
               .Include(m => m.JobLanguage)
               .ThenInclude(m => m.Language)
               .Include(m => m.JobLocation).
               ThenInclude(m => m.Location)
               .Include(m => m.JobSkill)
               .ThenInclude(m => m.KeySkill)
               .Include(m => m.JobCategory)
               .ThenInclude(m => m.Category)
               .Where(m => m.JobStatus == true).Take(6).ToListAsync();
            return View(jobPositions);
        }
    }
}
