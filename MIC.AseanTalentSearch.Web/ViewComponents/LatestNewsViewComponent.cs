﻿using MIC.AseanTalentSearch.Web.EF.Logical;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.ViewComponents
{
    public class LatestNewsViewComponent : ViewComponent
    {
        private readonly INewsManager _newsManager;

        public LatestNewsViewComponent(INewsManager newsManager)
        {
            _newsManager = newsManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = _newsManager.GetAllNews(true).OrderByDescending(x => x.CreatedDate).Take(3);
            return View(model);
        }
    }
}
