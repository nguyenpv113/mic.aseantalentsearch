﻿using MIC.AseanTalentSearch.Web.EF.Logical;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.ViewComponents
{
    public class RecentNewsViewComponent : ViewComponent
    {
        private readonly INewsManager _newsManager;

        public RecentNewsViewComponent(INewsManager newsManager)
        {
            _newsManager = newsManager;
        }

        public async Task<IViewComponentResult> InvokeAsync(bool type)
        {
            var model = _newsManager.GetAllNews(type).OrderByDescending(x => x.CreatedDate).Take(5).ToList();
            return View(model);
        }
    }
}
