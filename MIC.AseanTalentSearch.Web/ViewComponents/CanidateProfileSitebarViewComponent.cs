﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;

namespace MIC.AseanTalentSearch.Web.ViewComponents
{
    public class CanidateProfileSitebarViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AseanTalentSearchContext _context;

        public CanidateProfileSitebarViewComponent(UserManager<ApplicationUser> userManager, AseanTalentSearchContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {

            ProfileViewModel profile = new ProfileViewModel();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user != null)
            {
                var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                var generalProfile = _context.GeneralProfile.SingleOrDefault(m => m.Id == user.Id);

                profile.Image = generalProfile?.Image;
                profile.FullName = candidate?.FullName;
                profile.Id = candidate?.Id;
                profile.CvId = candidate.Cvid.ToString();
                profile.InterviewInvitationCount = _context.InterviewInvitation.Where(x => x.CandidateId == candidate.Id && x.CandidateDisable.Value != true).Count();
                profile.IsPublishCV = candidate.IsPublish;
            }
            return View(profile);
        }
    }
}
