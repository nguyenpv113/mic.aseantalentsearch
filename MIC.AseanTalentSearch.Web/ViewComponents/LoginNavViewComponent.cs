﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using MIC.AseanTalentSearch.Web.Models.Employer;

namespace MIC.AseanTalentSearch.Web.ViewComponents
{
    public class LoginNavViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AseanTalentSearchContext _context;

        public LoginNavViewComponent(UserManager<ApplicationUser> userManager, AseanTalentSearchContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var profile = new Models.ProfileViewModel();
            if (User.Identity.IsAuthenticated)
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                if (user != null)
                {
                    if (User.IsInRole("Employer"))
                    {
                        Company company = _context.Company.Find(user.Id);
                        if (company != null)
                        {
                            profile.Image = company.Logo;
                        }
                    }
                    else//candidate or admin or instructor
                    {
                        var generalProfile = _context.GeneralProfile.SingleOrDefault(m => m.Id == user.Id);

                        profile.Image = generalProfile?.Image;
                        profile.Id = user?.Id;
                    }
                    
                }
            }
            return View("~/Views/Shared/_LoginPartial.cshtml", profile);
        }
    }
}
