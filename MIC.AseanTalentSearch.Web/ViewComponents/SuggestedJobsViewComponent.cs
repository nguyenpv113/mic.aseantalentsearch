﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;

namespace MIC.AseanTalentSearch.Web.ViewComponents
{
    public class SuggestedJobsViewComponent : ViewComponent
    {
    
        private readonly AseanTalentSearchContext _context;

        public SuggestedJobsViewComponent(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var username = User.Identity.Name;
            var candidate = await 
                _context.Candidate.Include(m => m.IdNavigation)
                    .SingleOrDefaultAsync(m => m.IdNavigation.UserName == username);
            

            List<PositionJob> result = new List<PositionJob>();
            if (candidate != null)
            {
                var conn = _context.Database.GetDbConnection();
                try
                {
                    await conn.OpenAsync();
                    using (var command = conn.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        SqlParameter parameter = new SqlParameter("@CandidateID",candidate.Id);
                        command.CommandText = "findPositionJob";
                        command.Parameters.Add(parameter);
                        DbDataReader reader = await command.ExecuteReaderAsync();

                        if (reader.HasRows)
                        {
                            while (await reader.ReadAsync())
                            {
                                //var row = new PositionJob() { EnrollmentDate = reader.GetDateTime(0), StudentCount = reader.GetInt32(1) };
                                //add check company status
                                var row =await _context.PositionJob.Include(m=>m.Company).Include(x=>x.JobLocation).SingleOrDefaultAsync(m => m.Id == reader.GetInt32(0)&&m.Company.ActiveStatus==true && m.JobStatus.Value);
                                if (row != null)
                                {
                                    result.Add(row);
                                }
                            }
                        }
                        reader.Dispose();
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
            ViewBag.JobCount = result.Count>24?24:result.Count;
            return View(result.OrderByDescending(m=>m.ClosedDate).Take(24));
        }
    }
}
