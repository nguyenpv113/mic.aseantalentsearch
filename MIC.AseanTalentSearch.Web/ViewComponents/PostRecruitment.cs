﻿using System;
using System.ComponentModel.DataAnnotations;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.ViewComponents
{
    public class PostRecruitment
    {
        public PostRecruitment(Company company)
        {
            CompanyId = company.Id;
            ContactName = company.ContactPerson;
            ContactPhone = company.ContactPhone;
            ContactEmail = company.Email;
        }

        public PostRecruitment()
        {
        }

        [Display(Name = "Vị trí cần tuyển"), Required, MaxLength(250)]
        public string PositionTitle { get; set; }

        [Display(Name = "Giới tính")]
        public bool? Sex { get; set; }

        [Display(Name = "Trình độ học vấn")]
        public int? EducationLevelId { get; set; }

        [Display(Name = "Cấp bậc")]
        public int? JobLevelId { get; set; }

        [Display(Name = "Loại hình công việc")]
        public int JobTypeId { get; set; }

        [Display(Name = "Mức lương cao nhất")]
        public decimal? MaxPay { get; set; }

        [Display(Name = "Mức lương thấp nhất")]
        public decimal? MinPay { get; set; }

        [Display(Name = "Đơn vị tiền tệ")]
        public string Currency { get; set; }

        
        [Display(Name = "Kinh nghiệm")]
        public double? YearsOfExperienceRequirement { get; set; }


        [Display(Name = "Ngành nghề")]
        public int CategoryId { get; set; }

//        public SelectList CategoryIdSelectList { get; set; }


        [Display(Name = "Địa điểm làm việc")]
        public int? LocationId { get; set; }

//        public SelectList LocationIdSelectList { get; set; }

        [Display(Name = "Mô tả công việc")]
        public string JobDescription { get; set; }

        [Display(Name = "Yêu cầu kỹ năng")]
        public string SkillRequirement { get; set; }

        [Display(Name = "Yêu cầu công việc")]
        public string Responsibilities { get; set; }

        // TODO : truong quyen loi dược huong ??
        // TODO : Field - yêu cầu hồ sơ ??

        [Display(Name = "Số lượng cần tuyển")]
        public int? Quantity { get; set; }

        [Display(Name = "Yêu cầu công tác xa")]
        public bool? TravelRequired { get; set; }

        [DataType(DataType.Date), Display(Name = "Ngày tuyển")]
        public DateTime? OpenDate { get; set; }

        [DataType(DataType.Date), Display(Name = "Ngày hết hạn")]
        public DateTime? ClosedDate { get; set; }

        //--- Thông tin liên hệ
        public long CompanyId { get; set; }
        [Display(Name = "Tên người liên hệ")]
        public string ContactName { get; set; }
        [Display(Name = "Số điện thoại")]
        public string ContactPhone { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string ContactEmail { get; set; }
//
//
//        public bool? PayStatus { get; set; }
//
//        public bool? PayStatusView { get; set; }
//
//
//
//        
//        public bool? JobStatus { get; set; }
//        public int? TopNumber { get; set; }
    }
}