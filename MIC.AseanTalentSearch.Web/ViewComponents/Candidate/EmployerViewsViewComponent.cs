﻿using MIC.AseanTalentSearch.Web.Areas.Employer.Helpers;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sakura.AspNetCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.ViewComponents.Candidate
{
    public class EmployerViewsViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public EmployerViewsViewComponent(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync(int? page = 1)
        {
            int pageSize = 21;
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var employersaved = _context.SaveCandidate.Include(x=>x.Company).Where(x => x.CandidateId == user.Id);
            ViewBag.ListSaved = employersaved.ToList();
            var candidate = await _context.ViewCV.Where(x => x.CandidateId == user.Id).Include(x => x.Company).Include(x => x.Candidate).ToListAsync();
            var listRs = employersaved.Select(x => x.CompanyId).Union(candidate.Select(x => x.CompanyId));
            var rs = (from v in listRs
                      select new ResultViewSave()
                      {
                          CompanyId = v,
                          SaveDetail = candidate.Where(x => x.CompanyId == v).Select(x => x.Company) ?? candidate.Where(x => x.CompanyId == v).Select(x => x.Company),
                          SavedDate = candidate.Where(x => x.CompanyId == v).LastOrDefault().CreatedDate,
                          Company = _context.Company.Where(x=>x.Id==v).FirstOrDefault()
                      }).ToList();
            return View("~/Views/Shared/Components/Candidate/EmployerView.cshtml", rs.ToPagedList(pageSize, page.Value));
        }
    }
}
