﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using MIC.AseanTalentSearch.Web.Models.Candidate;
using Language = MIC.AseanTalentSearch.Web.Models.Candidate.Language;
using Reference = MIC.AseanTalentSearch.Web.Models.Candidate.Reference;

namespace MIC.AseanTalentSearch.Web.ViewComponents.Candidate
{
    public class WorkingPreferenceViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public WorkingPreferenceViewComponent(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = new WorkingPreference();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user!= null)
            {
                var candidate = _context.Candidate.Include(m=>m.CandidateLocation).ThenInclude(m=>m.Location).Include(m=>m.ExpectedJobLevel).Include(m=>m.CandidateCategory).ThenInclude(m=>m.Category).Include(m=>m.CandidateBenefit).ThenInclude(m=>m.DesiredBenefit).SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    var locations = candidate.CandidateLocation.Select(m => m.Location);
                    foreach (var location in locations)
                    {
                        model.LocationName.Add(location.Name);
                    }
                    
                    var expectedJobCategorys = candidate.CandidateCategory.Select(m => m.Category);

                    foreach (var expectedJobCategory in expectedJobCategorys)
                    {
                       model.CategoryName.Add(expectedJobCategory.Name);
                    }

                    var jobLevel = candidate.ExpectedJobLevel;
                    if (jobLevel != null)
                    {
                        model.JobLevelName = jobLevel.JobLevelName;
                    }

                    var benefits = await _context.Benefit.Where(x=>x.CandidateBenefit.Any(y=>y.CandidateId==user.Id)).ToListAsync();
                    var candidateBenefits = new HashSet<int>(candidate.CandidateBenefit.Select(m=>m.DesiredBenefitId));
                    model.SelectedBenefits = new List<AssignedBenefitData>();

                    foreach (var benefit in benefits)
                    {
                        model.SelectedBenefits.Add( new AssignedBenefitData
                        {
                            BenefitId = benefit.Id,
                            BenefitName = benefit.Name,
                            Assigned = candidateBenefits.Contains(benefit.Id)
                        });
                    }
                    model.Salary = candidate.ExpectedSalary;
                    model.Benefits = benefits;
                }
            }           
            return View("~/Views/Shared/Components/Candidate/WorkingPreferences.cshtml", model);
        }
    }
}
