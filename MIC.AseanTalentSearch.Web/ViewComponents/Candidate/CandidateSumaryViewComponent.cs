﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using MIC.AseanTalentSearch.Web.Models.Candidate;

namespace MIC.AseanTalentSearch.Web.ViewComponents.Candidate
{
    public class CandidateSumaryViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public CandidateSumaryViewComponent(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = new Sumary();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user != null)
            {
                var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    model.Text = candidate.Summary;
                }       
            }
            return View("~/Views/Shared/Components/Candidate/CandidateSumary.cshtml", model);
        }
    }
}
