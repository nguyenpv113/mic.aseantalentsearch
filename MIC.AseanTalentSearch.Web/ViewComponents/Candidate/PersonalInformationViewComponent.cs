﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using MIC.AseanTalentSearch.Web.Models.Candidate;

namespace MIC.AseanTalentSearch.Web.ViewComponents.Candidate
{
    public class PersonalInformationViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public PersonalInformationViewComponent(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = new PersonalInformation();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user != null)
            {
                var profile = _context.GeneralProfile.SingleOrDefault(m => m.Id == user.Id);
                var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    model.Email = candidate.Email;
                    model.Phone = candidate.PhoneNumber;
                    model.YearsOfExperience = candidate.YearsOfExperience;
                }

                if (profile != null)
                {
                    model.FirstName = profile.FirstName;
                    model.LastName = profile.LastName;
                    model.DateOfBirth = profile.BirthDate?.ToString("dd/MM/yyyy");
                    model.Nationality = profile.Nationality;
                    model.Gender = profile.Gender;
                    model.MaritalStatusValue = profile.MaritalStatus;
                    if (!String.IsNullOrEmpty(profile.HomeCountry))
                        model.Country = _context.Location.Find(Int32.Parse(profile.HomeCountry)).Name;
                    if (!String.IsNullOrEmpty(profile.HomeCity))
                        model.HomeCity = _context.Location.Find(Int32.Parse(profile.HomeCity)).Name;
                    if (!String.IsNullOrEmpty(profile.HomeDistrict))
                        model.HomeDistrict = _context.Location.Find(Int32.Parse(profile.HomeDistrict)).Name;
                    model.Address = profile.HomeAddress;
                }
            }
            return View("~/Views/Shared/Components/Candidate/PersonalInformation.cshtml", model);
        }
    }
}
