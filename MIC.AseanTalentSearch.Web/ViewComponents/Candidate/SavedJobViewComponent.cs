﻿using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sakura.AspNetCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.ViewComponents.Candidate
{
    public class SavedJobViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AseanTalentSearchContext _context;
        public SavedJobViewComponent(UserManager<ApplicationUser> userManager, AseanTalentSearchContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(int? page = 1)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            //var data = await _context.Candidate.Include(m => m.IdNavigation).ThenInclude(m => m.JobSave)
            //    .ThenInclude(m => m.PositionJob).ThenInclude(m => m.Company).Where(m => m.IdNavigation.JobSave.Any(p=>p.PositionJob.JobStatus == true))/*.Where(m=>m.PositionJob.ClosedDate > DateTime.Now)*/
            //    .OrderByDescending(m => m.IdNavigation.JobSave.OrderByDescending(c=>c.CreatedDate).FirstOrDefault().CreatedDate).SingleOrDefaultAsync(m=>m.Id == user.Id);
            //return View(data);
            var jobSave = await _context.JobSave.Where(m => m.UserId == user.Id)
                .Select(m => m.PositionJobId).ToListAsync();
            var position = await _context.PositionJob.Include(m => m.Company).Include(m => m.JobLocation)
                .ThenInclude(m => m.Location).Where(m => m.JobStatus == true)
                .Where(m => jobSave.Contains(m.Id)).ToListAsync();
            var model = position.ToPagedList(5, page.Value);
            ViewBag.IsPublish = _context.Candidate.Find(user.Id).IsPublish;
            return View("~/Views/Shared/Components/Candidate/SavedJob.cshtml", model);
        }
    }
}
