﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using MIC.AseanTalentSearch.Web.Models.Candidate;
using EmploymentHistory = MIC.AseanTalentSearch.Web.Models.Candidate.EmploymentHistory;

namespace MIC.AseanTalentSearch.Web.ViewComponents.Candidate
{
    public class EmploymentHistoryViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public EmploymentHistoryViewComponent(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = new List<EmploymentHistory>();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user!= null)
            {
                var candidate = _context.Candidate.Include(m=>m.EmploymentHistory).SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    if (candidate.EmploymentHistory != null && candidate.EmploymentHistory.Any())
                    {
                        var datas = candidate.EmploymentHistory.OrderBy(m => m.StartedDate);
                        foreach (var employmentHistory in datas)
                        {
                            model.Add(new EmploymentHistory
                            {
                                Company = employmentHistory.CompanyName,
                                Position = employmentHistory.PositionTitle,
                                From = employmentHistory.StartedDate.ToString("MM/yyyy"),
                                To = employmentHistory.EndedDate?.ToString("MM/yyyy"),
                                IsCurrentJob = employmentHistory.IsCurrentJob,
                                Id = employmentHistory.Id,
                                Description = employmentHistory.Description
                            });
                        }
                    } 
                }
            }
            return View("~/Views/Shared/Components/Candidate/EmploymentHistory.cshtml", model);
        }
    }
}
