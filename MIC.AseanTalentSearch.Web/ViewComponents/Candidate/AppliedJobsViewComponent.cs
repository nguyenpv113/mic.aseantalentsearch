﻿using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sakura.AspNetCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.ViewComponents.Candidate
{
    public class AppliedJobsViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AseanTalentSearchContext _context;
        public AppliedJobsViewComponent(UserManager<ApplicationUser> userManager, AseanTalentSearchContext context)
        {
            _userManager = userManager;
            _context = context;
        }
        public async Task<IViewComponentResult> InvokeAsync(int? page = 1)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var jobApply = await _context.JobApplication.Where(m => m.CandidateId == user.Id)
                .Select(m => m.PositionJobId).ToListAsync();
            var position = await _context.PositionJob.Include(m => m.Company).Include(m => m.JobLocation)
                .ThenInclude(m => m.Location).Where(m => m.JobStatus == true)
                .Where(m => jobApply.Contains(m.Id)).ToListAsync();
            var model = position.ToPagedList(5, page.Value);
            ViewBag.IsPublish = _context.Candidate.Find(user.Id).IsPublish;
            return View("~/Views/Shared/Components/Candidate/AppliedJobs.cshtml", model);
        }
    }
}
