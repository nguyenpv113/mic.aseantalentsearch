﻿using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models.Candidate;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.ViewComponents.Candidate
{
    public class CommentViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;

        public CommentViewComponent(AseanTalentSearchContext context)
        {
            _context = context;
        }
        public async Task<IViewComponentResult> InvokeAsync(string candidateId)
        {
            var candidate = await _context.Candidate.SingleOrDefaultAsync(x => x.Cvid.ToString().Equals(candidateId));

            var data = await _context.CandidateComment.Where(m => m.UniversityId != null)
                .Include(m => m.UserComment)
                .ThenInclude(m => m.GeneralProfile)
                .Include(m => m.UserComment).ThenInclude(m => m.Instructor).ThenInclude(m => m.University)
                .Where(m => m.CandidateId == candidate.Id)
                .OrderByDescending(m => m.CreatedDate)
                .ToListAsync();


            if (data == null || data.Count == 0)
            {
                ViewData["response"] = "No comment for this candidate";
            }
            else
            {
                ViewData["response"] = String.Format("Have {0} comments", data.Count);
            }
            List<CandidateCommentViewModel> model = new List<CandidateCommentViewModel>();
            foreach (var item in data)
            {
                string _University = item.UniversityId.HasValue ? _context.University.Find(item.UniversityId.Value).Name : "Please update University";
                var i = new CandidateCommentViewModel()
                {
                    CondidateComment = item,
                    _OldUniversity = _University
                };
                model.Add(i);
            }
            ViewBag.CurrentUser = GetNameCurrentUser(candidateId);
            return View("~/Views/Shared/Components/Candidate/Comment.cshtml", model);
        }

        public string GetNameCurrentUser(string Cvid)
        {
            var userId = _context.Candidate.Where(m => m.Cvid.ToString() == Cvid).FirstOrDefault().Id;
            return _context.AspNetUsers.Find(userId).UserName;
        }
    }
}
