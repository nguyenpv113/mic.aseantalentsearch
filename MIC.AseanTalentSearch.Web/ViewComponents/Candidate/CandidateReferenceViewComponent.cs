﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using MIC.AseanTalentSearch.Web.Models.Candidate;
using Language = MIC.AseanTalentSearch.Web.Models.Candidate.Language;
using Reference = MIC.AseanTalentSearch.Web.Models.Candidate.Reference;

namespace MIC.AseanTalentSearch.Web.ViewComponents.Candidate
{
    public class CandidateReferenceViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public CandidateReferenceViewComponent(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = new List<Reference>();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user!= null)
            {
                var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    model =
                        await _context.Reference.Where(m => m.CandidateId == candidate.Id).Select(m => new Reference
                        {
                            Position = m.Position,
                            Company = m.Company,
                            Phone = m.PhoneNumber,
                            Email = m.Email,
                            FullName = m.FullName,
                            Id = m.Id
                        }).ToListAsync();

                }
            }           
            return View("~/Views/Shared/Components/Candidate/CandidateReference.cshtml", model);
        }
    }
}
