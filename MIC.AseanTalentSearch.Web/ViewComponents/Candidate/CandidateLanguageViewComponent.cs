﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using MIC.AseanTalentSearch.Web.Models.Candidate;
using Language = MIC.AseanTalentSearch.Web.Models.Candidate.Language;

namespace MIC.AseanTalentSearch.Web.ViewComponents.Candidate
{
    public class CandidateLanguageViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public CandidateLanguageViewComponent(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = new List<Language>();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user!= null)
            {
                var candidate = _context.Candidate.Include(m=>m.CandidateLanguage).ThenInclude(m=>m.Language).SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    if (candidate.CandidateLanguage != null && candidate.CandidateLanguage.Any())
                    {
                        foreach (var candidateLanguage in candidate.CandidateLanguage)
                        {
                            model.Add(new Language
                            {
                                Id= candidateLanguage.Id,
                                LanguageName = candidateLanguage.Language.Name,
                                Certification = candidateLanguage.Certification,
                                LanguageId = candidateLanguage.LanguageId,
                                ScoreOrLevel = candidateLanguage.ScoreOrLevel
                            });
                        }
                    } 
                }
            }
            return View("~/Views/Shared/Components/Candidate/CandidateLanguage.cshtml", model);
        }
    }
}
