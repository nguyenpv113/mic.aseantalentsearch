using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.AspNetCore.Identity;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.EntityFrameworkCore;

namespace MIC.AseanTalentSearch.Web.ViewComponents.Candidate
{
    public class CurriculumVitaeViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AseanTalentSearchContext _context;
        public CurriculumVitaeViewComponent(UserManager<ApplicationUser> userManager, AseanTalentSearchContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var candidate = _context.Candidate.Include(m => m.IdNavigation).Include(m => m.Layout).SingleOrDefault(m => m.Id == user.Id);
            var layout = await _context.Layout.ToListAsync();
            ViewBag.Candidate = candidate;
            return View(layout);
        }
    }
}