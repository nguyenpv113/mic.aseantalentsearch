﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using MIC.AseanTalentSearch.Web.Models.Candidate;
using EducationHistory = MIC.AseanTalentSearch.Web.Models.Candidate.EducationHistory;

namespace MIC.AseanTalentSearch.Web.ViewComponents.Candidate
{
    public class EducationHistoryViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public EducationHistoryViewComponent(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = new List<EducationHistory>();
            
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user!= null)
            {
                var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    var educationHistories =  await _context.EducationHistory.Include(m=>m.Major).Where(m => m.CandidateId == candidate.Id).OrderBy(m=>m.BeginDate).ToListAsync();
                    if (educationHistories != null)
                    {
                        foreach (var educationHistory in educationHistories)
                        {
                            var item = new EducationHistory();
                            item.Id = educationHistory.Id;
                            item.From = educationHistory.BeginDate.ToString("MM/yyyy");
                            item.To = educationHistory.EndDate.ToString("MM/yyyy");
                            item.Achievements = educationHistory.Achievement;
                            item.Ranking = educationHistory.Ranking;
                            if (educationHistory.Major != null)
                            {
                                var major =
                                    _context.Major.Include(m => m.EducationLevel)
                                        .Include(m => m.University)
                                        .SingleOrDefault(m => m.Id == educationHistory.MajorId);

                                if (major != null)
                                {
                                    item.Major = major.Name;

                                    var university = major.University;
                                    if (university != null)
                                    {
                                        item.UniversityName = university.Name;
                                        item.UniversityId = university.Id;
                                    }

                                    var educationLevel = major.EducationLevel;
                                    if (educationLevel != null)
                                    {
                                        item.EducationLevelName = educationLevel.Name;
                                        item.EducationLevelId = educationLevel.Id;
                                    }
                                }
                            }
                            model.Add(item);
                        }

                    }                  
                }
            }
            return View("~/Views/Shared/Components/Candidate/EducationHistory.cshtml", model);
        }
    }
}
