﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using MIC.AseanTalentSearch.Web.Models.Candidate;

namespace MIC.AseanTalentSearch.Web.ViewComponents.Candidate
{
    public class CandidateSkillViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public CandidateSkillViewComponent(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = new List<CandidateSkill>();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user!= null)
            {
                var candidate = _context.Candidate.Include(m=>m.CandidateSkill).ThenInclude(m=>m.Skill).SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    if (candidate.CandidateSkill != null && candidate.CandidateSkill.Any())
                    {
                        foreach (var candidateSkill in candidate.CandidateSkill)
                        {
                            model.Add(candidateSkill);
                        }
                        //model.List = new SelectList(candidate.CandidateSkill, "Id", "Name");
                    } 
                }
            }
            //model.List = new SelectList(await _context.Skill.ToListAsync(), "Id", "Name");
            return View("~/Views/Shared/Components/Candidate/CandidateSkill.cshtml", model);
        }
    }
}
