﻿using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using MIC.AseanTalentSearch.Web.Models.Candidate;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.ViewComponents.Candidate
{
   
    public class CandidateSocialNetworkViewComponent : ViewComponent
    {
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public CandidateSocialNetworkViewComponent(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }



        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = new SocialNetwork();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user != null)
            {
                var candidate = _context.Candidate.SingleOrDefault(m => m.Id == user.Id);
                if (candidate != null)
                {
                    model.Facebook = candidate.Facebook;
                    model.Instagram = candidate.Instagram;
                    model.LinkedIn = candidate.LinkedIn;
                    model.Twitter = candidate.Twitter;
                    model.Youtube = candidate.Youtube;
                }
            }            
            return View("~/Views/Shared/Components/Candidate/CandidateSocialNetwork.cshtml", model);
        }

    }
}
