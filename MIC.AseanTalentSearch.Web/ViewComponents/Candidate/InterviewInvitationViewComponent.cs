﻿using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sakura.AspNetCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.ViewComponents.Candidate
{
    public class InterviewInvitationViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AseanTalentSearchContext _context;
        public InterviewInvitationViewComponent(UserManager<ApplicationUser> userManager, AseanTalentSearchContext context)
        {
            _userManager = userManager;
            _context = context;
        }
        public async Task<IViewComponentResult> InvokeAsync(int? page = 1)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var interviewInvitation = _context.InterviewInvitation.Where(x => x.CandidateId == user.Id && x.CandidateDisable.Value != true)
                .Include(x => x.Company).ThenInclude(x => x.Location).Include(x => x.PositionJob).OrderByDescending(x => x.Id);
            var model = interviewInvitation.ToPagedList(10, page.Value);
            ViewBag.IsPublish = _context.Candidate.Find(user.Id).IsPublish;
            return View("~/Views/Shared/Components/Candidate/InterviewInvitation.cshtml", model);
        }
    }
}
