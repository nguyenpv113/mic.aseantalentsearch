﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.Data;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;

namespace MIC.AseanTalentSearch.Web.ViewComponents
{
    public class SocialFooterViewComponent : ViewComponent
    {
        private readonly ConfigTable _configTable;

        public SocialFooterViewComponent(ConfigTable configTable)
        {
            _configTable = configTable;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            ViewBag.Facebook = _configTable.GetValue("facebook")??"#";
            ViewBag.GooglePlus = _configTable.GetValue("googleplus") ?? "#";
            ViewBag.Twitter = _configTable.GetValue("twitter") ?? "#";
            ViewBag.Youtube = _configTable.GetValue("youtube") ?? "#";

            return View();
        }
    }
}
