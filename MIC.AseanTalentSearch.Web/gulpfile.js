/// <binding BeforeBuild='clean, min' />
"use strict";

var gulp = require("gulp"),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify");
    //browserSync = require('browser-sync').create();

var webroot = "./wwwroot/";

var paths = {
    js: webroot + "js/**/*.js",
    minJs: webroot + "js/**/*.min.js",
    css: webroot + "css/**/*.css",
    minCss: webroot + "css/**/*.min.css",
    concatJsDest: webroot + "js/site.min.js",
    concatCssDest: webroot + "css/site.min.css",
    adminJs: webroot + "areas/admin/js/**/*.js",
    adminMinJs: webroot + "areas/admin/js/**/*.min.js",
    adminCss: webroot + "areas/admin/css/**/*.css",
    adminMinCss: webroot + "areas/admin/css/**/*.min.css",
    adminConcatJsDest: webroot + "areas/admin/js/site.min.js",
    adminConcatCssDest: webroot + "areas/admin/css/site.min.css",

    employerJs: webroot + "areas/employer/js/**/*.min.js",
    employerMinJs: webroot + "areas/employer/js/**/*.min.js",
    
    employerCss: webroot + "areas/employer/css/**/*.css",
    employerMinCss: webroot + "areas/employer/css/**/*.min.css",
};

gulp.task("clean:js", function (cb) {
    rimraf(paths.concatJsDest, cb);
});

gulp.task("clean:adminJs", function (cb) {
    rimraf(paths.adminConcatJsDest, cb);
});

gulp.task("clean:css", function (cb) {
    rimraf(paths.concatCssDest, cb);
});

gulp.task("clean:adminCss", function (cb) {
    rimraf(paths.adminConcatCssDest, cb);
});

gulp.task("clean", ["clean:js", "clean:css"]);
gulp.task("adminClean", ["clean:adminJs", "clean:adminCss"]);

gulp.task("min:js", function () {
    return gulp.src([paths.js, "!" + paths.minJs], { base: "." })
        .pipe(concat(paths.concatJsDest))
        .pipe(uglify())
        .pipe(gulp.dest("."));
});

gulp.task("min:adminJs", function () {
    return gulp.src([paths.adminJs, "!" + paths.adminMinJs], { base: "." })
        .pipe(concat(paths.adminConcatJsDest))
        .pipe(uglify())
        .pipe(gulp.dest("."));
});

gulp.task("min:css", function () {
    return gulp.src([paths.css, "!" + paths.minCss, ! + webroot + "css/login.css"])
        .pipe(concat(paths.concatCssDest))
        .pipe(cssmin())
        .pipe(gulp.dest("."));
});

gulp.task("min:adminCss", function () {
    return gulp.src([paths.adminCss, "!" + paths.adminMinCss])
        .pipe(concat(paths.adminConcatCssDest))
        .pipe(cssmin())
        .pipe(gulp.dest("."));
});

gulp.task("min", ["min:js", "min:css"]);
gulp.task("adminMin", ["min:adminJs", "min:adminCss"]);

gulp.task("min:employerJs", function () {
    return gulp.src([paths.employerJs, "!" + paths.employerMinJs], { base: "." })
        .pipe(uglify())
        .pipe(gulp.dest("."));
});
gulp.task("min:employerCss", function () {
    return gulp.src([paths.employerJs, "!" + paths.employerMinJs], { base: "." })
        .pipe(uglify())
        .pipe(gulp.dest("."));
});
gulp.task("employerMin", ["min:employerJs", "min:employerCss"]);
//gulp.task('browserSync', function () {
//    browserSync.init({
//        //server: {
//        //    baseDir: webroot
//        //}
//        ////proxy: "localhost:5000"
//        //server: {
//        //    baseDir: webroot
//        //},
//        ////logPrefix: 'Your Project',
//        ////host: 'site1.domain.dev',
//        //port: 5000
//        ////open: false,
//        ////notify: false,
//        ////ghost: false

//        proxy: 'http://localhost:5000/'
//        //host: 'localhost',
//        //port:5000,
//        //open: 'external'

//    });
//});

gulp.task('watch', ['clean', 'min', 'adminClean','adminMin'], function () {
    gulp.watch([paths.js, paths.css], ['clean', 'min']);
    gulp.watch([paths.adminJs, paths.adminCss], ['adminClean', 'adminMin']);
});