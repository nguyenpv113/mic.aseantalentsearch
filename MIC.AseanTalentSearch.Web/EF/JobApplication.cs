﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class JobApplication
    {
        public JobApplication()
        {
            Review = new HashSet<Review>();
        }

        public int Id { get; set; }
        public string JobApplicationName { get; set; }
        public int PositionJobId { get; set; }
        public long CandidateId { get; set; }
        public string CoverLetter { get; set; }
        public string CreateBy { get; set; }
        public bool JobStatus { get; set; }
        public string Linkfile { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<Review> Review { get; set; }
        public virtual Candidate Candidate { get; set; }
        public virtual PositionJob PositionJob { get; set; }
    }
}
