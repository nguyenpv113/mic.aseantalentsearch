﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class ComboPost
    {
        public ComboPost()
        {
            ComboService = new HashSet<ComboService>();
            Order = new HashSet<Order>();
        }

        public int Id { get; set; }
        public string ComboCode { get; set; }
        public string ComboPostName { get; set; }
        public string ComboPostEnglishName { get; set; }
        public decimal? DiscountPrice { get; set; }
        public double? Discount { get; set; }
        public bool? ActiveStatus { get; set; }
        public DateTime? ActiveDate { get; set; }
        public string ActiveBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<ComboService> ComboService { get; set; }
        public virtual ICollection<Order> Order { get; set; }
    }
}
