﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.EF
{
    public class SaveCandidate
    {
        public long CompanyId { get; set; }
        public long CandidateId { get; set; }
        public DateTime CreatedDate { get; set; }
        
        public virtual Candidate Candidate { get; set; }
        public virtual Company Company { get; set; }
    }
}
