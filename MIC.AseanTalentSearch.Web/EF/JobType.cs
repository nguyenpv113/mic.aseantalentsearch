﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class JobType
    {
        public JobType()
        {
            PositionJob = new HashSet<PositionJob>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<PositionJob> PositionJob { get; set; }
    }
}
