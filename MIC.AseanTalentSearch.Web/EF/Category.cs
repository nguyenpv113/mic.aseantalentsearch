﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Category
    {
        public Category()
        {
            CandidateCategory = new HashSet<CandidateCategory>();
            CompanyCategory = new HashSet<CompanyCategory>();
            JobCategory = new HashSet<JobCategory>();
        }

        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<CandidateCategory> CandidateCategory { get; set; }
        public virtual ICollection<CompanyCategory> CompanyCategory { get; set; }
        public virtual ICollection<JobCategory> JobCategory { get; set; }
        public virtual Category Parent { get; set; }
        public virtual ICollection<Category> InverseParent { get; set; }
    }
}
