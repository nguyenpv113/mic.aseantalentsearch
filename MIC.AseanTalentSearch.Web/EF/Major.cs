﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Major
    {
        public Major()
        {
            EducationHistory = new HashSet<EducationHistory>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public int? ParentCode { get; set; }
        public int UniversityId { get; set; }
        public int EducationLevelId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ActiveDate { get; set; }
        public long? ActiveBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<EducationHistory> EducationHistory { get; set; }
        public virtual EducationLevel EducationLevel { get; set; }
        public virtual University University { get; set; }
    }
}
