﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Benefit
    {
        public Benefit()
        {
            CandidateBenefit = new HashSet<CandidateBenefit>();
            CompanyBenefit = new HashSet<CompanyBenefit>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }

        public virtual ICollection<CandidateBenefit> CandidateBenefit { get; set; }
        public virtual ICollection<CompanyBenefit> CompanyBenefit { get; set; }
    }
}
