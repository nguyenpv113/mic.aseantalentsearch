﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class CompanyCategory
    {
        public long CompanyId { get; set; }
        public int CategoryId { get; set; }
        public bool? ActiveStatus { get; set; }
        public DateTime? ActiveDate { get; set; }
        public DateTime? ClosedDate { get; set; }
        public string ActiveBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual Category Category { get; set; }
        public virtual Company Company { get; set; }
    }
}
