﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class EducationHistory
    {
        public long Id { get; set; }
        public int? MajorId { get; set; }
        public long? CandidateId { get; set; }
        public int? GraduatedYear { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Ranking { get; set; }
        public string ModeOfStudy { get; set; }
        public string Achievement { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ActiveDate { get; set; }
        public long? ActiveBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public long? InstructorId { get; set; }

        public virtual Candidate Candidate { get; set; }
        public virtual Instructor Instructor { get; set; }
        public virtual Major Major { get; set; }
    }
}
