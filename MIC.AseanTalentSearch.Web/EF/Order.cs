﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Order
    {
        public Order()
        {
            OrderDetail = new HashSet<OrderDetail>();
        }

        public int Id { get; set; }
        public long CompanytId { get; set; }
        public string OrderDate { get; set; }
        public decimal Total { get; set; }
        public int? ComboPostId { get; set; }
        public DateTime? ActiveDate { get; set; }
        public string ActiveBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<OrderDetail> OrderDetail { get; set; }
        public virtual ComboPost ComboPost { get; set; }
        public virtual Company Companyt { get; set; }
    }
}
