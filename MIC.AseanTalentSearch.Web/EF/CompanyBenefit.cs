﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class CompanyBenefit
    {
        public long CompanyId { get; set; }
        public int BenefitId { get; set; }
        public bool? ActiveStatus { get; set; }
        public DateTime? ActiveDate { get; set; }
        public DateTime? ActiveClosed { get; set; }
        public string ActiveBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual Benefit Benefit { get; set; }
        public virtual Company Company { get; set; }
    }
}
