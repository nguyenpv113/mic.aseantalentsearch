﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class CandidateComment
    {
        public int Id { get; set; }
        public long UserCommentId { get; set; }
        public long CandidateId { get; set; }
        public string Comment { get; set; }
        public int? StarRating { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
            
        public int? UniversityId { get; set; }
        public bool? CommentStatus { get; set; }


        public virtual Candidate Candidate { get; set; }
        public virtual AspNetUsers UserComment { get; set; }
    }
}
