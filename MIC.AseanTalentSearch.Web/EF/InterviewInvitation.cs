﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class InterviewInvitation
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string LetterContent { get; set; }
        public long CandidateId { get; set; }
        public long CompanyId { get; set; }
        public int PositionJobId { get; set; }
        public bool SendToEmailCandidate { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPersonName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ActiveDate { get; set; }
        public long? ActiveBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? CandidateDisable { get; set; }

        public virtual Candidate Candidate { get; set; }
        public virtual Company Company { get; set; }
        public virtual PositionJob PositionJob { get; set; }
    }
}
