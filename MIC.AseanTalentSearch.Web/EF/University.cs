﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class University
    {
        public University()
        {
            Instructor = new HashSet<Instructor>();
            Major = new HashSet<Major>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public string Address { get; set; }
        public int? LocationId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ActiveDate { get; set; }
        public long? ActiveBy { get; set; }
        public bool? ActiveStatus { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public string Logo { get; set; }
        public string Introduction { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string LinkedIn { get; set; }
        public string Twitter { get; set; }
        public int? Position { get; set; }

        public virtual ICollection<Instructor> Instructor { get; set; }
        public virtual ICollection<Major> Major { get; set; }
        public virtual Location Location { get; set; }
    }
}
