﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Instructor
    {
        public Instructor()
        {
            EducationHistory = new HashSet<EducationHistory>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public bool HighestQualification { get; set; }
        public bool? Status { get; set; }
        public string Comment { get; set; }
        public int UniversityId { get; set; }    
        
        public string Department{ get; set; }
        public string Position { get; set; }

        public virtual ICollection<EducationHistory> EducationHistory { get; set; }
        public virtual AspNetUsers IdNavigation { get; set; }
        public virtual University University { get; set; }
    }
}
