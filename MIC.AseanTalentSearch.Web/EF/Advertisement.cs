﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Advertisement
    {
        public int Id { get; set; }
        public long? CompanytId { get; set; }
        public int? AdvertisementTypeId { get; set; }
        public double? Length { get; set; }
        public double? Width { get; set; }
        public double? Depth { get; set; }
        public decimal Price { get; set; }
        public int? Duration { get; set; }
        public string TimeUnit { get; set; }
        public string LinkImage { get; set; }
        public DateTime? StartAdvertisementDate { get; set; }
        public DateTime? CloseAdvertisementDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual AdvertisementType AdvertisementType { get; set; }
        public virtual Company Companyt { get; set; }
    }
}
