﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class JobSkill
    {
        public int PositionJobId { get; set; }
        public int KeySkillId { get; set; }
        public double? YearsOfExperienceRequirement { get; set; }
        public string Comment { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual Skill KeySkill { get; set; }
        public virtual PositionJob PositionJob { get; set; }
    }
}
