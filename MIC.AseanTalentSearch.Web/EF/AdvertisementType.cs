﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class AdvertisementType
    {
        public AdvertisementType()
        {
            Advertisement = new HashSet<Advertisement>();
        }

        public int Id { get; set; }
        public string AdvertisementPlace { get; set; }
        public bool? Type { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<Advertisement> Advertisement { get; set; }
    }
}
