﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Feature
    {
        public Feature()
        {
            RoleFeature = new HashSet<RoleFeature>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public int? ParentId { get; set; }
        public string Version { get; set; }
        public string FeatureUrl { get; set; }
        public string Icon { get; set; }
        public int? OrderNumber { get; set; }
        public bool IsSystemFeature { get; set; }
        public string Comment { get; set; }

        public virtual ICollection<RoleFeature> RoleFeature { get; set; }
        public virtual Feature Parent { get; set; }
        public virtual ICollection<Feature> InverseParent { get; set; }
    }
}
