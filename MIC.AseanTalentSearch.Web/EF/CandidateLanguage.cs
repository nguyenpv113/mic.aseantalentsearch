﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class CandidateLanguage
    {
        public int Id { get; set; }
        public long CandidateId { get; set; }
        public int LanguageId { get; set; }
        public string Certification { get; set; }
        public string ScoreOrLevel { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual Candidate Candidate { get; set; }
        public virtual Language Language { get; set; }
    }
}
