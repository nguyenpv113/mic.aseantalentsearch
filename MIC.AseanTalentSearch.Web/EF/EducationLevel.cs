﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class EducationLevel
    {
        public EducationLevel()
        {
            Major = new HashSet<Major>();
            PositionJob = new HashSet<PositionJob>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Major> Major { get; set; }
        public virtual ICollection<PositionJob> PositionJob { get; set; }
    }
}
