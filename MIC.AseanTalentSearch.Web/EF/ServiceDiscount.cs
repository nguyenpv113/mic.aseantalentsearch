﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class ServiceDiscount
    {
        public int Id { get; set; }
        public int ServiceId { get; set; }
        public int? Duration { get; set; }
        public string TimeUnit { get; set; }
        public double Discount { get; set; }
        public bool ActiveStatus { get; set; }
        public DateTime? StartDiscountDate { get; set; }
        public DateTime? EndDiscountDate { get; set; }
        public string ActiveBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual Service Service { get; set; }
    }
}
