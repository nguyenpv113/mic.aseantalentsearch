﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class JobSave
    {
        public long UserId { get; set; }
        public int PositionJobId { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual PositionJob PositionJob { get; set; }
        public virtual AspNetUsers User { get; set; }
    }
}
