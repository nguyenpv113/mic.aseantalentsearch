﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class CandidateBenefit
    {
        public long CandidateId { get; set; }
        public int DesiredBenefitId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Candidate Candidate { get; set; }
        public virtual Benefit DesiredBenefit { get; set; }
    }
}
