﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class GeneralProfile
    {
        public long Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public bool? Gender { get; set; }
        public string MaritalStatus { get; set; }
        public int? LocationId { get; set; }
        public DateTime? BirthDate { get; set; }
        public int? Zipcode { get; set; }
        public string Telephone { get; set; }
        public string EmailAddress { get; set; }
        public string EmailAddressCompany { get; set; }
        public string Image { get; set; }
        public string HomeAddress { get; set; }
        public string HomeWard { get; set; }
        public string HomeDistrict { get; set; }
        public string HomeCity { get; set; }
        public string HomeCountry { get; set; }
        public string CurrentAddress { get; set; }
        public string CurrentWard { get; set; }
        public string CurrentDistrict { get; set; }
        public string CurrentCity { get; set; }
        public string CurrentCountry { get; set; }
        public string Nationality { get; set; }
        public string PeopleIdNumber { get; set; }
        public DateTime? PeopleIdIssueDate { get; set; }
        public string PeopleIdIssuePlace { get; set; }
        public string PassportNumber { get; set; }
        public string PassportIssuePlace { get; set; }
        public DateTime? PassportIssueDate { get; set; }
        public DateTime? PassportExpirationDate { get; set; }
        public int? ReligionId { get; set; }
        public string ContactLastName { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactMiddleName { get; set; }
        public int? ContactRelationshipId { get; set; }
        public string ContactAddress { get; set; }
        public string ContactWard { get; set; }
        public string ContactDistrict { get; set; }
        public string ContactCity { get; set; }
        public string ContactCountry { get; set; }
        public string ContactEmailAddress { get; set; }
        public string ContactTelephone { get; set; }
        public string Comment { get; set; }
        public DateTime? Createdate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual AspNetUsers IdNavigation { get; set; }
        public virtual Location Location { get; set; }
    }
}
