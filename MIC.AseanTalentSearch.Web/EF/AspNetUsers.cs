﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class AspNetUsers
    {
        public AspNetUsers()
        {
            AspNetUserClaims = new HashSet<AspNetUserClaims>();
            AspNetUserLogins = new HashSet<AspNetUserLogins>();
            AspNetUserRoles = new HashSet<AspNetUserRoles>();
            CandidateComment = new HashSet<CandidateComment>();
            JobSave = new HashSet<JobSave>();
            NotificationUserIdFromNavigation = new HashSet<Notification>();
            NotificationUserIdToNavigation = new HashSet<Notification>();
            Review = new HashSet<Review>();
            UserLog = new HashSet<UserLog>();
        }

        public long Id { get; set; }
        public int AccessFailedCount { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool LockoutEnabled { get; set; }
        public DateTimeOffset? LockoutEnd { get; set; }
        public string NormalizedEmail { get; set; }
        public string NormalizedUserName { get; set; }
        public string PasswordHash { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public string SecurityStamp { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public string UserName { get; set; }

        public virtual ICollection<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual ICollection<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual ICollection<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual Candidate Candidate { get; set; }
        public virtual ICollection<CandidateComment> CandidateComment { get; set; }
        public virtual Company Company { get; set; }
        public virtual GeneralProfile GeneralProfile { get; set; }
        public virtual Instructor Instructor { get; set; }
        public virtual ICollection<JobSave> JobSave { get; set; }
        public virtual ICollection<Notification> NotificationUserIdFromNavigation { get; set; }
        public virtual ICollection<Notification> NotificationUserIdToNavigation { get; set; }
        public virtual ICollection<Review> Review { get; set; }
        public virtual ICollection<UserLog> UserLog { get; set; }
    }
}
