﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class JobLevel
    {
        public JobLevel()
        {
            Candidate = new HashSet<Candidate>();
            EmploymentHistory = new HashSet<EmploymentHistory>();
            PositionJob = new HashSet<PositionJob>();
        }

        public int JobLevelId { get; set; }
        public string JobLevelName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<Candidate> Candidate { get; set; }
        public virtual ICollection<EmploymentHistory> EmploymentHistory { get; set; }
        public virtual ICollection<PositionJob> PositionJob { get; set; }
    }
}
