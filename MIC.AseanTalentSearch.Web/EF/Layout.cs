﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Layout
    {
        public Layout()
        {
            Candidate = new HashSet<Candidate>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Path { get; set; }

        public virtual ICollection<Candidate> Candidate { get; set; }
    }
}
