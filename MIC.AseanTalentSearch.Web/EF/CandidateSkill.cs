﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class CandidateSkill
    {
        public long CandidateId { get; set; }
        public int SkillId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? SkillLevel { get; set; }

        public virtual Candidate Candidate { get; set; }
        public virtual Skill Skill { get; set; }
    }
}
