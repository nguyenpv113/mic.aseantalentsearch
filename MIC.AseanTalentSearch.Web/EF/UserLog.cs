﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class UserLog
    {
        public int Id { get; set; }
        public long UserId { get; set; }
        public string Action { get; set; }
        public DateTime? CreatedDate { get; set; }

        public virtual AspNetUsers User { get; set; }
    }
}
