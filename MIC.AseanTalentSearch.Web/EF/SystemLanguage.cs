﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class SystemLanguage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool ActiveStatus { get; set; }
    }
}
