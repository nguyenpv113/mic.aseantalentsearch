﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Notification
    {
        public int Id { get; set; }
        public long UserIdFrom { get; set; }
        public long UserIdTo { get; set; }
        public DateTime? SendDate { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public bool IsRead { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ActiveDate { get; set; }
        public long? ActiveBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual AspNetUsers UserIdFromNavigation { get; set; }
        public virtual AspNetUsers UserIdToNavigation { get; set; }
    }
}
