﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class CandidateReference
    {
        public long CandidateId { get; set; }
        public int ReferenceId { get; set; }
        public string Position { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int IsCurrent { get; set; }

        public virtual Candidate Candidate { get; set; }
        public virtual Reference Reference { get; set; }
    }
}
