﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Location
    {
        public Location()
        {
            CandidateLocation = new HashSet<CandidateLocation>();
            Company = new HashSet<Company>();
            GeneralProfile = new HashSet<GeneralProfile>();
            JobLocation = new HashSet<JobLocation>();
            University = new HashSet<University>();
        }

        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public int? Zipcode { get; set; }
        public int? Population { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int Type { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string Comment { get; set; }

        public virtual ICollection<CandidateLocation> CandidateLocation { get; set; }
        public virtual ICollection<Company> Company { get; set; }
        public virtual ICollection<GeneralProfile> GeneralProfile { get; set; }
        public virtual ICollection<JobLocation> JobLocation { get; set; }
        public virtual ICollection<University> University { get; set; }
        public virtual Location Parent { get; set; }
        public virtual ICollection<Location> InverseParent { get; set; }
    }
}
