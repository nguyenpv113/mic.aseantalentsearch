﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Reference
    {
        public int Id { get; set; }
        public long CandidateId { get; set; }
        public string FullName { get; set; }
        public string Position { get; set; }
        public string Company { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public virtual Candidate Candidate { get; set; }
    }
}
