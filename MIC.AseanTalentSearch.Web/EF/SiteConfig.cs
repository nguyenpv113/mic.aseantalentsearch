﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class SiteConfig
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
