﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Skill
    {
        public Skill()
        {
            CandidateSkill = new HashSet<CandidateSkill>();
            JobSkill = new HashSet<JobSkill>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Comment { get; set; }

        public virtual ICollection<CandidateSkill> CandidateSkill { get; set; }
        public virtual ICollection<JobSkill> JobSkill { get; set; }
    }
}
