﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Language
    {
        public Language()
        {
            CandidateLanguage = new HashSet<CandidateLanguage>();
            JobLanguage = new HashSet<JobLanguage>();
            LocaleStringResource = new HashSet<LocaleStringResource>();
        }

        public int Id { get; set; }
        public string EnglishName { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string LanguageCulture { get; set; }
        public bool IsCurrentLanguage { get; set; }
        public bool Status { get; set; }
        public virtual ICollection<CandidateLanguage> CandidateLanguage { get; set; }
        public virtual ICollection<JobLanguage> JobLanguage { get; set; }
        public virtual ICollection<LocaleStringResource> LocaleStringResource { get; set; }
    }
}
