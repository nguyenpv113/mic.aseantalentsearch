﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Religion
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public string Comment { get; set; }
    }
}
