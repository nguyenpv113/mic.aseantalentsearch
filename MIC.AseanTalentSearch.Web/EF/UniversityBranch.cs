﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class UniversityBranch
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int? Zipcode { get; set; }
        public int? Telephone { get; set; }
        public byte[] Image { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string Comment { get; set; }
        public int? LocationId { get; set; }
    }
}
