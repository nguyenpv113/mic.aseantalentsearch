﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Department
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string Type { get; set; }
        public int? OrderNumber { get; set; }
        public string Comment { get; set; }

        public virtual Department Parent { get; set; }
        public virtual ICollection<Department> InverseParent { get; set; }
    }
}
