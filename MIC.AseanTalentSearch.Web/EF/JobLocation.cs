﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class JobLocation
    {
        public int PositionJobId { get; set; }
        public int LocationId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual Location Location { get; set; }
        public virtual PositionJob PositionJob { get; set; }
    }
}
