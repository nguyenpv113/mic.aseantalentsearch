﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class EmploymentHistory
    {
        public long Id { get; set; }
        public long? CandidateId { get; set; }
        public string PositionTitle { get; set; }
        public string CompanyName { get; set; }
        public bool? IsCurrentJob { get; set; }
        public DateTime StartedDate { get; set; }
        public DateTime? EndedDate { get; set; }
        public string Description { get; set; }
        public int? JobLevelId { get; set; }

        public virtual Candidate Candidate { get; set; }
        public virtual JobLevel JobLevel { get; set; }
    }
}
