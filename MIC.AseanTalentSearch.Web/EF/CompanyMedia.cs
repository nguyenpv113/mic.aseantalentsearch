﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class CompanyMedia
    {
        public int Id { get; set; }
        public long CompanyId { get; set; }
        public string Link { get; set; }
        public bool? Type { get; set; }
        public bool? ActiveStatus { get; set; }
        public DateTime? ActiveDate { get; set; }
        public DateTime? ClosedDate { get; set; }
        public string ActiveBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual Company Company { get; set; }
    }
}
