﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Review
    {
        public int Id { get; set; }
        public int JobApplicationId { get; set; }
        public long UserId { get; set; }
        public string Assessment { get; set; }
        public int StarRating { get; set; }
        public int? ReviewNumber { get; set; }
        public DateTime? ReviewDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual JobApplication JobApplication { get; set; }
        public virtual AspNetUsers User { get; set; }
    }
}
