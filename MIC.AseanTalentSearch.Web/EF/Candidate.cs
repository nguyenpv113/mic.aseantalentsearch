﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Candidate
    {
        public Candidate()
        {
            CandidateBenefit = new HashSet<CandidateBenefit>();
            CandidateCategory = new HashSet<CandidateCategory>();
            CandidateComment = new HashSet<CandidateComment>();
            CandidateLanguage = new HashSet<CandidateLanguage>();
            CandidateLocation = new HashSet<CandidateLocation>();
            CandidateSkill = new HashSet<CandidateSkill>();
            EducationHistory = new HashSet<EducationHistory>();
            EmploymentHistory = new HashSet<EmploymentHistory>();
            InterviewInvitation = new HashSet<InterviewInvitation>();
            JobApplication = new HashSet<JobApplication>();
            Reference = new HashSet<Reference>();
            ViewCV = new HashSet<ViewCV>();
            SaveCandidate = new HashSet<SaveCandidate>();
        }

        public long Id { get; set; }
        public int? LayoutId { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string LinkedIn { get; set; }
        public string Twitter { get; set; }
        public string Youtube { get; set; }
        public string Image { get; set; }
        public string Summary { get; set; }
        public int? ExpectedCategory { get; set; }
        public decimal? ExpectedSalary { get; set; }
        public int? ExpectedJobLevelId { get; set; }
        public bool? WillingToRelocate { get; set; }
        public double? YearsOfExperience { get; set; }
        public bool? Dtustudent { get; set; }
        public int? DtugraduatedYear { get; set; }
        public string DtustudentCode { get; set; }
        public string Cvlink { get; set; }
        public bool? IsPublish { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? ActiveStatus { get; set; }
        public DateTime? ActiveDate { get; set; }
        public long? ActiveBy { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? Cvid { get; set; }

        public virtual ICollection<CandidateBenefit> CandidateBenefit { get; set; }
        public virtual ICollection<CandidateCategory> CandidateCategory { get; set; }
        public virtual ICollection<CandidateComment> CandidateComment { get; set; }
        public virtual ICollection<CandidateLanguage> CandidateLanguage { get; set; }
        public virtual ICollection<CandidateLocation> CandidateLocation { get; set; }
        public virtual ICollection<CandidateSkill> CandidateSkill { get; set; }
        public virtual ICollection<EducationHistory> EducationHistory { get; set; }
        public virtual ICollection<EmploymentHistory> EmploymentHistory { get; set; }
        public virtual ICollection<InterviewInvitation> InterviewInvitation { get; set; }
        public virtual ICollection<JobApplication> JobApplication { get; set; }
        public virtual ICollection<Reference> Reference { get; set; }
        public virtual ICollection<ViewCV> ViewCV { get; set; }
        public virtual ICollection<SaveCandidate> SaveCandidate { get; set; }
        public virtual JobLevel ExpectedJobLevel { get; set; }
        public virtual AspNetUsers IdNavigation { get; set; }
        public virtual Layout Layout { get; set; }
    }
}
