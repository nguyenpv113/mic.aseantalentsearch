﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class ComboService
    {
        public int ComboPostId { get; set; }
        public int ServiceId { get; set; }
        public bool? ActiveStatus { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Createby { get; set; }
        public DateTime? ActiveDate { get; set; }
        public DateTime? ActiveClosed { get; set; }
        public string ActiveBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ComboPost ComboPost { get; set; }
        public virtual Service Service { get; set; }
    }
}
