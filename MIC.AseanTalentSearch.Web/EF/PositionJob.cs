﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class PositionJob
    {
        public PositionJob()
        {
            InterviewInvitation = new HashSet<InterviewInvitation>();
            JobApplication = new HashSet<JobApplication>();
            JobCategory = new HashSet<JobCategory>();
            JobLanguage = new HashSet<JobLanguage>();
            JobLocation = new HashSet<JobLocation>();
            JobSave = new HashSet<JobSave>();
            JobSkill = new HashSet<JobSkill>();
        }

        public int Id { get; set; }
        public long? CompanyId { get; set; }
        public int? JobTypeId { get; set; }
        public string PositionTitle { get; set; }
        public string Code { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? OpenDate { get; set; }
        public DateTime? ClosedDate { get; set; }
        public long? CreatedBy { get; set; }
        public int? EducationLevelId { get; set; }
        public int? JobLevelId { get; set; }
        public bool? PayStatus { get; set; }
        public bool? PayStatusView { get; set; }
        public decimal? MaxPay { get; set; }
        public decimal? MinPay { get; set; }
        public string Currency { get; set; }
        public string JobDescription { get; set; }
        public double? YearsOfExperienceRequirement { get; set; }
        public string SkillRequirement { get; set; }
        public string Responsibilities { get; set; }
        public bool? JobStatus { get; set; }
        public bool? TravelRequired { get; set; }
        public int? TopNumber { get; set; }
        public int? Quantity { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? Viewed { get; set; }

        public virtual ICollection<InterviewInvitation> InterviewInvitation { get; set; }
        public virtual ICollection<JobApplication> JobApplication { get; set; }
        public virtual ICollection<JobCategory> JobCategory { get; set; }
        public virtual ICollection<JobLanguage> JobLanguage { get; set; }
        public virtual ICollection<JobLocation> JobLocation { get; set; }
        public virtual ICollection<JobSave> JobSave { get; set; }
        public virtual ICollection<JobSkill> JobSkill { get; set; }
        public virtual Company Company { get; set; }
        public virtual EducationLevel EducationLevel { get; set; }
        public virtual JobLevel JobLevel { get; set; }
        public virtual JobType JobType { get; set; }
    }
}
