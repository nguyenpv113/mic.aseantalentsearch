﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class OrderDetail
    {
        public int OrdersId { get; set; }
        public int ServiceId { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Vat { get; set; }
        public decimal Total { get; set; }
        public bool ActiveStatus { get; set; }
        public DateTime? ActiveDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string ActiveBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual Order Orders { get; set; }
        public virtual Service Service { get; set; }
    }
}
