﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Service
    {
        public Service()
        {
            ComboService = new HashSet<ComboService>();
            OrderDetail = new HashSet<OrderDetail>();
            ServiceDiscount = new HashSet<ServiceDiscount>();
        }

        public int Id { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceName { get; set; }
        public string ServiceEnglishName { get; set; }
        public string Type { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }
        public bool ActiveStatus { get; set; }
        public int? Duration { get; set; }
        public string TimeUnit { get; set; }
        public int? TopNumber { get; set; }
        public DateTime? ActiveDate { get; set; }
        public DateTime? ClosedDate { get; set; }
        public string ActiveBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<ComboService> ComboService { get; set; }
        public virtual ICollection<OrderDetail> OrderDetail { get; set; }
        public virtual ICollection<ServiceDiscount> ServiceDiscount { get; set; }
    }
}
