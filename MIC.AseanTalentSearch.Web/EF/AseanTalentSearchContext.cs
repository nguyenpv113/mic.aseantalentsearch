﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Linq;
using System.Reflection;
using System.Security.Claims;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class AseanTalentSearchContext : DbContext
    {
        public virtual DbSet<Advertisement> Advertisement { get; set; }
        public virtual DbSet<AdvertisementType> AdvertisementType { get; set; }
        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<Benefit> Benefit { get; set; }
        public virtual DbSet<Candidate> Candidate { get; set; }
        public virtual DbSet<CandidateBenefit> CandidateBenefit { get; set; }
        public virtual DbSet<CandidateCategory> CandidateCategory { get; set; }
        public virtual DbSet<CandidateComment> CandidateComment { get; set; }
        public virtual DbSet<CandidateLanguage> CandidateLanguage { get; set; }
        public virtual DbSet<CandidateLocation> CandidateLocation { get; set; }
        public virtual DbSet<CandidateSkill> CandidateSkill { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<ComboPost> ComboPost { get; set; }
        public virtual DbSet<ComboService> ComboService { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<CompanyBenefit> CompanyBenefit { get; set; }
        public virtual DbSet<CompanyCategory> CompanyCategory { get; set; }
        public virtual DbSet<CompanyMedia> CompanyMedia { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<EducationHistory> EducationHistory { get; set; }
        public virtual DbSet<EducationLevel> EducationLevel { get; set; }
        public virtual DbSet<EmploymentHistory> EmploymentHistory { get; set; }
        public virtual DbSet<Feature> Feature { get; set; }
        public virtual DbSet<GeneralProfile> GeneralProfile { get; set; }
        public virtual DbSet<Instructor> Instructor { get; set; }
        public virtual DbSet<InterviewInvitation> InterviewInvitation { get; set; }
        public virtual DbSet<JobApplication> JobApplication { get; set; }
        public virtual DbSet<JobCategory> JobCategory { get; set; }
        public virtual DbSet<JobLanguage> JobLanguage { get; set; }
        public virtual DbSet<JobLevel> JobLevel { get; set; }
        public virtual DbSet<JobLocation> JobLocation { get; set; }
        public virtual DbSet<JobSave> JobSave { get; set; }
        public virtual DbSet<JobSkill> JobSkill { get; set; }
        public virtual DbSet<JobType> JobType { get; set; }
        public virtual DbSet<Language> Language { get; set; }
        public virtual DbSet<Layout> Layout { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<Major> Major { get; set; }
        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<OrderDetail> OrderDetail { get; set; }
        public virtual DbSet<PositionJob> PositionJob { get; set; }
        public virtual DbSet<Reference> Reference { get; set; }
        public virtual DbSet<Religion> Religion { get; set; }
        public virtual DbSet<Review> Review { get; set; }
        public virtual DbSet<RoleFeature> RoleFeature { get; set; }
        public virtual DbSet<Service> Service { get; set; }
        public virtual DbSet<ServiceDiscount> ServiceDiscount { get; set; }
        public virtual DbSet<SiteConfig> SiteConfig { get; set; }
        public virtual DbSet<Skill> Skill { get; set; }
        public virtual DbSet<SystemLanguage> SystemLanguage { get; set; }
        public virtual DbSet<University> University { get; set; }
        public virtual DbSet<UniversityBranch> UniversityBranch { get; set; }
        public virtual DbSet<UserLog> UserLog { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<LocaleStringResource> LocaleStringResource { get; set; }
        public virtual DbSet<ViewCV> ViewCV { get; set; }
        public virtual DbSet<SaveCandidate> SaveCandidate { get; set; }

        // Unable to generate entity type for table 'dbo.AspNetUserTokens'. Please see the warning messages.

        private readonly IHttpContextAccessor _httpContext;
        public AseanTalentSearchContext(DbContextOptions<AseanTalentSearchContext> options, IHttpContextAccessor httpContext)
            : base(options)
        {
            _httpContext = httpContext;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<News>();

            modelBuilder.Entity<Advertisement>(entity =>
            {
                entity.Property(e => e.CloseAdvertisementDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasColumnType("varchar(50)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Depth).HasColumnName("depth");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Price).HasColumnType("numeric");

                entity.Property(e => e.StartAdvertisementDate).HasColumnType("datetime");

                entity.Property(e => e.TimeUnit).HasColumnType("varchar(10)");

                entity.HasOne(d => d.AdvertisementType)
                    .WithMany(p => p.Advertisement)
                    .HasForeignKey(d => d.AdvertisementTypeId)
                    .HasConstraintName("FK_Advertisement_Advertisement_Type");

                entity.HasOne(d => d.Companyt)
                    .WithMany(p => p.Advertisement)
                    .HasForeignKey(d => d.CompanytId)
                    .HasConstraintName("FK_Advertisement_Recruitment");
            });

            modelBuilder.Entity<LocaleStringResource>(entity =>
            {
                entity.HasOne(d => d.Language)
                .WithMany(p => p.LocaleStringResource)
                .HasForeignKey(d => d.LanguageId)
                .HasConstraintName("FK_LocaleStringSource_Language");
            });

            modelBuilder.Entity<ViewCV>(entity=> {
                entity.HasOne(e => e.Company)
                .WithMany(c => c.ViewCV);
                entity.HasOne(e => e.Candidate)
                .WithMany(c => c.ViewCV);
            });

            modelBuilder.Entity<SaveCandidate>(entity=> {
                entity.HasKey(c => new { c.CandidateId, c.CompanyId });
            });

            modelBuilder.Entity<AdvertisementType>(entity =>
            {
                entity.Property(e => e.AdvertisementPlace).HasMaxLength(300);

                entity.Property(e => e.CreatedBy).HasColumnType("varchar(50)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey })
                    .HasName("PK_AspNetUserLogins");

                entity.Property(e => e.LoginProvider).HasMaxLength(450);

                entity.Property(e => e.ProviderKey).HasMaxLength(450);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId })
                    .HasName("PK_AspNetUserRoles_1");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<Benefit>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<Candidate>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.Address).HasMaxLength(250);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Cvid)
                    .HasColumnName("CVID")
                    .HasDefaultValueSql("newsequentialid()");

                entity.Property(e => e.Cvlink).HasColumnName("CVlink");

                entity.Property(e => e.DtugraduatedYear).HasColumnName("DTUGraduatedYear");

                entity.Property(e => e.Dtustudent).HasColumnName("DTUStudent");

                entity.Property(e => e.DtustudentCode)
                    .HasColumnName("DTUStudentCode")
                    .HasMaxLength(50);

                entity.Property(e => e.Email).HasColumnType("varchar(100)");

                entity.Property(e => e.ExpectedSalary).HasColumnType("numeric");

                entity.Property(e => e.FullName).HasMaxLength(250);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PhoneNumber).HasMaxLength(50);

                entity.HasOne(d => d.ExpectedJobLevel)
                    .WithMany(p => p.Candidate)
                    .HasForeignKey(d => d.ExpectedJobLevelId)
                    .HasConstraintName("FK_Candidate_JobLevel");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.Candidate)
                    .HasForeignKey<Candidate>(d => d.Id)
                    .HasConstraintName("FK_Candidate_AspNetUsers");

                entity.HasOne(d => d.Layout)
                    .WithMany(p => p.Candidate)
                    .HasForeignKey(d => d.LayoutId)
                    .HasConstraintName("FK_Candidate_Layout");
            });

            modelBuilder.Entity<CandidateBenefit>(entity =>
            {
                entity.HasKey(e => new { e.CandidateId, e.DesiredBenefitId })
                    .HasName("PK__Candidat__15DE5E08B38B17A9");

                entity.ToTable("Candidate_Benefit");

                entity.Property(e => e.DesiredBenefitId).HasColumnName("Desired_BenefitId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Candidate)
                    .WithMany(p => p.CandidateBenefit)
                    .HasForeignKey(d => d.CandidateId)
                    .HasConstraintName("FK_Application_Benefit_Application");

                entity.HasOne(d => d.DesiredBenefit)
                    .WithMany(p => p.CandidateBenefit)
                    .HasForeignKey(d => d.DesiredBenefitId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Application_Benefit_Benefit");
            });

            modelBuilder.Entity<CandidateCategory>(entity =>
            {
                entity.HasKey(e => new { e.CandidateId, e.CategoryId })
                    .HasName("PK__Candidat__7EC3085E76D54FD4");

                entity.ToTable("Candidate_Category");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Candidate)
                    .WithMany(p => p.CandidateCategory)
                    .HasForeignKey(d => d.CandidateId)
                    .HasConstraintName("FK_Application_Categories_Application");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.CandidateCategory)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Application_Categories_Categories");
            });

            modelBuilder.Entity<CandidateComment>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Candidate)
                    .WithMany(p => p.CandidateComment)
                    .HasForeignKey(d => d.CandidateId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Application_Comment_Application");

                entity.HasOne(d => d.UserComment)
                    .WithMany(p => p.CandidateComment)
                    .HasForeignKey(d => d.UserCommentId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_CandidateComment_AspNetUsers");
            });

            modelBuilder.Entity<CandidateLanguage>(entity =>
            {
                entity.ToTable("Candidate_Language");

                entity.Property(e => e.Certification).HasMaxLength(250);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ScoreOrLevel)
                    .HasColumnName("Score_or_Level")
                    .HasMaxLength(250);

                entity.HasOne(d => d.Candidate)
                    .WithMany(p => p.CandidateLanguage)
                    .HasForeignKey(d => d.CandidateId)
                    .HasConstraintName("FK_Candidate_Language_Candidate");

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.CandidateLanguage)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Candidate_Language_Language");
            });

            modelBuilder.Entity<CandidateLocation>(entity =>
            {
                entity.HasKey(e => new { e.CandidateId, e.LocationId })
                    .HasName("PK__Candidat__A07EBA55D69A4C0A");

                entity.ToTable("Candidate_Location");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Candidate)
                    .WithMany(p => p.CandidateLocation)
                    .HasForeignKey(d => d.CandidateId)
                    .HasConstraintName("FK_Application_Location_Application");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.CandidateLocation)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Application_Location_Location");
            });

            modelBuilder.Entity<CandidateSkill>(entity =>
            {
                entity.HasKey(e => new { e.CandidateId, e.SkillId })
                    .HasName("PK__Candidat__B2A992E2409F62B5");

                entity.ToTable("Candidate_Skill");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Candidate)
                    .WithMany(p => p.CandidateSkill)
                    .HasForeignKey(d => d.CandidateId)
                    .HasConstraintName("FK_Application_Skill_Application");

                entity.HasOne(d => d.Skill)
                    .WithMany(p => p.CandidateSkill)
                    .HasForeignKey(d => d.SkillId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Application_Skill_Key_Skills");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Type).HasMaxLength(200);

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_Category_Category");
            });

            modelBuilder.Entity<ComboPost>(entity =>
            {
                entity.Property(e => e.ActiveBy).HasColumnType("varchar(50)");

                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.ComboCode).HasColumnType("varchar(50)");

                entity.Property(e => e.ComboPostEnglishName).HasColumnType("varchar(300)");

                entity.Property(e => e.ComboPostName).HasMaxLength(250);

                entity.Property(e => e.CreateBy).HasColumnType("varchar(50)");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DiscountPrice).HasColumnType("numeric");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ComboService>(entity =>
            {
                entity.HasKey(e => new { e.ComboPostId, e.ServiceId })
                    .HasName("PK__ComboSer__023D50440DA7CA35");

                entity.Property(e => e.ComboPostId).HasColumnName("ComboPostID");

                entity.Property(e => e.ServiceId).HasColumnName("ServiceID");

                entity.Property(e => e.ActiveBy).HasColumnType("varchar(50)");

                entity.Property(e => e.ActiveClosed).HasColumnType("datetime");

                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Createby).HasColumnType("varchar(50)");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.ComboPost)
                    .WithMany(p => p.ComboService)
                    .HasForeignKey(d => d.ComboPostId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ComboService_ComboPost");

                entity.HasOne(d => d.Service)
                    .WithMany(p => p.ComboService)
                    .HasForeignKey(d => d.ServiceId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ComboService_Services");
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.CompanySize).HasMaxLength(50);

                entity.Property(e => e.ContactPerson).HasMaxLength(250);

                entity.Property(e => e.ContactPhone).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Owner).HasMaxLength(250);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.Property(e => e.Taxcode).HasMaxLength(50);

                entity.Property(e => e.Website).HasMaxLength(250);

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.Company)
                    .HasForeignKey<Company>(d => d.Id)
                    .HasConstraintName("FK_Company_AspNetUsers");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Company)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("FK_Company_Location");
            });

            modelBuilder.Entity<CompanyBenefit>(entity =>
            {
                entity.HasKey(e => new { e.CompanyId, e.BenefitId })
                    .HasName("PK__Company___98E2501F74CFA3E8");

                entity.ToTable("Company_Benefit");

                entity.Property(e => e.ActiveBy).HasColumnType("varchar(50)");

                entity.Property(e => e.ActiveClosed).HasColumnType("datetime");

                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Benefit)
                    .WithMany(p => p.CompanyBenefit)
                    .HasForeignKey(d => d.BenefitId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Company_Benefit_Benefit");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.CompanyBenefit)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_Company_Benefit_Company");
            });

            modelBuilder.Entity<CompanyCategory>(entity =>
            {
                entity.HasKey(e => new { e.CompanyId, e.CategoryId })
                    .HasName("PK__Company___0C077C2E07923686");

                entity.ToTable("Company_Category");

                entity.Property(e => e.ActiveBy).HasColumnType("varchar(50)");

                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.ClosedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.CompanyCategory)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Company_Category_Category");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.CompanyCategory)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_Company_Category_Company");
            });

            modelBuilder.Entity<CompanyMedia>(entity =>
            {
                entity.Property(e => e.ActiveBy).HasColumnType("varchar(50)");

                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.ClosedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.CompanyMedia)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Company_Media_Company");
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.Property(e => e.Address).HasMaxLength(250);

                entity.Property(e => e.Code).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Telephone).HasMaxLength(150);

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_Department_Department");
            });

            modelBuilder.Entity<EducationHistory>(entity =>
            {
                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.BeginDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ModeOfStudy).HasMaxLength(100);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Ranking).HasMaxLength(50);

                entity.HasOne(d => d.Candidate)
                    .WithMany(p => p.EducationHistory)
                    .HasForeignKey(d => d.CandidateId)
                    .HasConstraintName("FK_EducationHistory_Candidate");

                entity.HasOne(d => d.Instructor)
                    .WithMany(p => p.EducationHistory)
                    .HasForeignKey(d => d.InstructorId)
                    .HasConstraintName("FK_EducationHistory_Instructor");

                entity.HasOne(d => d.Major)
                    .WithMany(p => p.EducationHistory)
                    .HasForeignKey(d => d.MajorId)
                    .HasConstraintName("FK_Education_History_Major");
            });

            modelBuilder.Entity<EducationLevel>(entity =>
            {
                entity.Property(e => e.Code).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<EmploymentHistory>(entity =>
            {
                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.EndedDate).HasColumnType("date");

                entity.Property(e => e.PositionTitle)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.StartedDate).HasColumnType("date");

                entity.HasOne(d => d.Candidate)
                    .WithMany(p => p.EmploymentHistory)
                    .HasForeignKey(d => d.CandidateId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Employment_History_Candidate");

                entity.HasOne(d => d.JobLevel)
                    .WithMany(p => p.EmploymentHistory)
                    .HasForeignKey(d => d.JobLevelId)
                    .HasConstraintName("FK_EmploymentHistory_JobLevel");
            });

            modelBuilder.Entity<Feature>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FeatureUrl)
                    .HasColumnName("FeatureURL")
                    .HasMaxLength(250);

                entity.Property(e => e.Icon).HasMaxLength(250);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Version).HasColumnType("varchar(10)");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_Feature_Feature");
            });

            modelBuilder.Entity<GeneralProfile>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.BirthDate).HasColumnType("date");

                entity.Property(e => e.ContactAddress).HasMaxLength(250);

                entity.Property(e => e.ContactCity).HasMaxLength(250);

                entity.Property(e => e.ContactCountry).HasMaxLength(250);

                entity.Property(e => e.ContactDistrict).HasMaxLength(250);

                entity.Property(e => e.ContactEmailAddress).HasColumnType("varchar(50)");

                entity.Property(e => e.ContactFirstName).HasMaxLength(50);

                entity.Property(e => e.ContactLastName).HasMaxLength(50);

                entity.Property(e => e.ContactMiddleName).HasMaxLength(50);

                entity.Property(e => e.ContactTelephone).HasMaxLength(150);

                entity.Property(e => e.ContactWard).HasMaxLength(250);

                entity.Property(e => e.Createdate).HasColumnType("datetime");

                entity.Property(e => e.CurrentAddress).HasMaxLength(250);

                entity.Property(e => e.CurrentCity).HasMaxLength(250);

                entity.Property(e => e.CurrentCountry).HasMaxLength(250);

                entity.Property(e => e.CurrentDistrict).HasMaxLength(250);

                entity.Property(e => e.CurrentWard).HasMaxLength(250);

                entity.Property(e => e.EmailAddress).HasColumnType("varchar(150)");

                entity.Property(e => e.EmailAddressCompany).HasColumnType("varchar(150)");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.HomeAddress).HasMaxLength(250);

                entity.Property(e => e.HomeCity).HasMaxLength(250);

                entity.Property(e => e.HomeCountry).HasMaxLength(250);

                entity.Property(e => e.HomeDistrict).HasMaxLength(250);

                entity.Property(e => e.HomeWard).HasMaxLength(250);

                entity.Property(e => e.Image).HasColumnType("varchar(255)");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.MaritalStatus).HasMaxLength(50);

                entity.Property(e => e.MiddleName).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Nationality).HasMaxLength(150);

                entity.Property(e => e.PassportExpirationDate).HasColumnType("datetime");

                entity.Property(e => e.PassportIssueDate).HasColumnType("datetime");

                entity.Property(e => e.PassportIssuePlace).HasMaxLength(250);

                entity.Property(e => e.PassportNumber).HasMaxLength(50);

                entity.Property(e => e.PeopleIdIssueDate).HasColumnType("datetime");

                entity.Property(e => e.PeopleIdIssuePlace).HasMaxLength(250);

                entity.Property(e => e.PeopleIdNumber).HasMaxLength(50);

                entity.Property(e => e.Telephone).HasMaxLength(150);

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.GeneralProfile)
                    .HasForeignKey<GeneralProfile>(d => d.Id)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_GeneralProfile_AspNetUsers");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.GeneralProfile)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("FK_GeneralProfile_Location");
            });

            modelBuilder.Entity<Instructor>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("nvarchar(150)");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.Instructor)
                    .HasForeignKey<Instructor>(d => d.Id)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Instructor_AspNetUsers");

                entity.HasOne(d => d.University)
                    .WithMany(p => p.Instructor)
                    .HasForeignKey(d => d.UniversityId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Instructor_University");
            });

            modelBuilder.Entity<InterviewInvitation>(entity =>
            {
                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.ContactEmail).HasColumnType("varchar(100)");

                entity.Property(e => e.ContactPersonName).HasMaxLength(250);

                entity.Property(e => e.ContactPhone).HasColumnType("varchar(50)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.LetterContent).IsRequired();

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.HasOne(d => d.Candidate)
                    .WithMany(p => p.InterviewInvitation)
                    .HasForeignKey(d => d.CandidateId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_InterviewInvitation_Application");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.InterviewInvitation)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_InterviewInvitation_Recruitment");

                entity.HasOne(d => d.PositionJob)
                    .WithMany(p => p.InterviewInvitation)
                    .HasForeignKey(d => d.PositionJobId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_InterviewInvitation_PositionJob");
            });

            modelBuilder.Entity<JobApplication>(entity =>
            {
                entity.Property(e => e.CoverLetter).HasColumnName("Cover_letter");

                entity.Property(e => e.CreateBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.JobApplicationName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Candidate)
                    .WithMany(p => p.JobApplication)
                    .HasForeignKey(d => d.CandidateId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Job_Application_Application");

                entity.HasOne(d => d.PositionJob)
                    .WithMany(p => p.JobApplication)
                    .HasForeignKey(d => d.PositionJobId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Job_Application_PositionJob");
            });

            modelBuilder.Entity<JobCategory>(entity =>
            {
                entity.HasKey(e => new { e.PositionJobId, e.CategoryId })
                    .HasName("PK__Job_Cate__2521B71FE7D4A668");

                entity.ToTable("Job_Category");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.JobCategory)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Job_Categories_Categories1");

                entity.HasOne(d => d.PositionJob)
                    .WithMany(p => p.JobCategory)
                    .HasForeignKey(d => d.PositionJobId)
                    .HasConstraintName("FK_Job_Categories_PositionJob");
            });

            modelBuilder.Entity<JobLanguage>(entity =>
            {
                entity.HasKey(e => new { e.PositionJobId, e.LanguageId })
                    .HasName("PK__Job_Lang__2F22A1E582FFAA3B");

                entity.ToTable("Job_Language");

                entity.Property(e => e.Certification).HasMaxLength(250);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ScoreOrLevel)
                    .HasColumnName("Score_or_Level")
                    .HasMaxLength(250);

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.JobLanguage)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Job_Language_Language");

                entity.HasOne(d => d.PositionJob)
                    .WithMany(p => p.JobLanguage)
                    .HasForeignKey(d => d.PositionJobId)
                    .HasConstraintName("FK_Job_Language_PositionJob");
            });

            modelBuilder.Entity<JobLevel>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.JobLevelName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<JobLocation>(entity =>
            {
                entity.HasKey(e => new { e.PositionJobId, e.LocationId })
                    .HasName("PK__Job_Loca__EACECEF69F3FAC7A");

                entity.ToTable("Job_Location");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.JobLocation)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Job_Location_Location");

                entity.HasOne(d => d.PositionJob)
                    .WithMany(p => p.JobLocation)
                    .HasForeignKey(d => d.PositionJobId)
                    .HasConstraintName("FK_Job_Location_PositionJob");
            });

            modelBuilder.Entity<JobSave>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.PositionJobId })
                    .HasName("PK_JobSave");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.PositionJob)
                    .WithMany(p => p.JobSave)
                    .HasForeignKey(d => d.PositionJobId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_JobSave_PositionJob");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.JobSave)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_JobSave_AspNetUsers");
            });

            modelBuilder.Entity<JobSkill>(entity =>
            {
                entity.HasKey(e => new { e.PositionJobId, e.KeySkillId })
                    .HasName("PK__Job_Skil__3644AB05512A4E6E");

                entity.ToTable("Job_Skill");

                entity.Property(e => e.KeySkillId).HasColumnName("Key_SkillId");

                entity.Property(e => e.Comment).HasMaxLength(200);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.YearsOfExperienceRequirement).HasColumnName("Years_of_experience_requirement");

                entity.HasOne(d => d.KeySkill)
                    .WithMany(p => p.JobSkill)
                    .HasForeignKey(d => d.KeySkillId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Job_Skill_Key_Skills");

                entity.HasOne(d => d.PositionJob)
                    .WithMany(p => p.JobSkill)
                    .HasForeignKey(d => d.PositionJobId)
                    .HasConstraintName("FK_Job_Skill_PositionJob");
            });

            modelBuilder.Entity<JobType>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<Language>(entity =>
            {
                entity.Property(e => e.EnglishName).HasColumnType("varchar(300)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<Layout>(entity =>
            {
                entity.Property(e => e.Path).HasMaxLength(250);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.Property(e => e.EnglishName).HasColumnType("varchar(300)");

                entity.Property(e => e.Lat).HasMaxLength(15);

                entity.Property(e => e.Long).HasMaxLength(15);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_Location_Location");
            });

            modelBuilder.Entity<Major>(entity =>
            {
                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.Code).HasColumnType("varchar(50)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EnglishName).HasColumnType("varchar(250)");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.HasOne(d => d.EducationLevel)
                    .WithMany(p => p.Major)
                    .HasForeignKey(d => d.EducationLevelId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Major_EducationLevel");

                entity.HasOne(d => d.University)
                    .WithMany(p => p.Major)
                    .HasForeignKey(d => d.UniversityId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Major_University");
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.SendDate).HasColumnType("datetime");

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.UserIdFrom).HasColumnName("UserId_From");

                entity.Property(e => e.UserIdTo).HasColumnName("UserId_To");

                entity.HasOne(d => d.UserIdFromNavigation)
                    .WithMany(p => p.NotificationUserIdFromNavigation)
                    .HasForeignKey(d => d.UserIdFrom)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Notification_AspNetUsers");

                entity.HasOne(d => d.UserIdToNavigation)
                    .WithMany(p => p.NotificationUserIdToNavigation)
                    .HasForeignKey(d => d.UserIdTo)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Notification_AspNetUsers1");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.Property(e => e.ActiveBy).HasColumnType("varchar(50)");

                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.OrderDate)
                    .IsRequired()
                    .HasColumnType("nchar(10)");

                entity.Property(e => e.Total).HasColumnType("numeric");

                entity.HasOne(d => d.ComboPost)
                    .WithMany(p => p.Order)
                    .HasForeignKey(d => d.ComboPostId)
                    .HasConstraintName("FK_Order_ComboPost");

                entity.HasOne(d => d.Companyt)
                    .WithMany(p => p.Order)
                    .HasForeignKey(d => d.CompanytId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Order_Company");
            });

            modelBuilder.Entity<OrderDetail>(entity =>
            {
                entity.HasKey(e => new { e.OrdersId, e.ServiceId })
                    .HasName("PK__OrderDet__DF5A22762B40EE62");

                entity.Property(e => e.ActiveBy).HasColumnType("varchar(50)");

                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.Discount).HasColumnType("numeric");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Price).HasColumnType("numeric");

                entity.Property(e => e.Total).HasColumnType("numeric");

                entity.Property(e => e.Vat)
                    .HasColumnName("VAT")
                    .HasColumnType("numeric");

                entity.HasOne(d => d.Orders)
                    .WithMany(p => p.OrderDetail)
                    .HasForeignKey(d => d.OrdersId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Orders_Detail_Order");

                entity.HasOne(d => d.Service)
                    .WithMany(p => p.OrderDetail)
                    .HasForeignKey(d => d.ServiceId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Orders_Detail_Services");
            });

            modelBuilder.Entity<PositionJob>(entity =>
            {
                entity.Property(e => e.ClosedDate).HasColumnType("date");

                entity.Property(e => e.Code).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Currency).HasColumnType("varchar(3)");

                entity.Property(e => e.JobStatus).HasDefaultValueSql("0");

                entity.Property(e => e.MaxPay).HasColumnType("numeric");

                entity.Property(e => e.MinPay).HasColumnType("numeric");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.OpenDate).HasColumnType("date");

                entity.Property(e => e.PositionTitle).HasMaxLength(250);

                entity.Property(e => e.SkillRequirement).HasColumnName("Skill_Requirement");

                entity.Property(e => e.TravelRequired).HasColumnName("Travel_Required");

                entity.Property(e => e.YearsOfExperienceRequirement).HasColumnName("Years_of_experience_requirement");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.PositionJob)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_PositionJob_Recruitment");

                entity.HasOne(d => d.EducationLevel)
                    .WithMany(p => p.PositionJob)
                    .HasForeignKey(d => d.EducationLevelId)
                    .HasConstraintName("FK_PositionJob_LevelDegree");

                entity.HasOne(d => d.JobLevel)
                    .WithMany(p => p.PositionJob)
                    .HasForeignKey(d => d.JobLevelId)
                    .HasConstraintName("FK_PositionJob_JobLevel");

                entity.HasOne(d => d.JobType)
                    .WithMany(p => p.PositionJob)
                    .HasForeignKey(d => d.JobTypeId)
                    .HasConstraintName("FK_PositionJob_JobType");
            });

            modelBuilder.Entity<Reference>(entity =>
            {
                entity.Property(e => e.Company)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.PhoneNumber).HasMaxLength(50);

                entity.Property(e => e.Position).HasMaxLength(250);

                entity.HasOne(d => d.Candidate)
                    .WithMany(p => p.Reference)
                    .HasForeignKey(d => d.CandidateId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Reference_Candidate");
            });

            modelBuilder.Entity<Religion>(entity =>
            {
                entity.Property(e => e.EnglishName).HasColumnType("varchar(250)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<Review>(entity =>
            {
                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ReviewDate).HasColumnType("datetime");

                entity.HasOne(d => d.JobApplication)
                    .WithMany(p => p.Review)
                    .HasForeignKey(d => d.JobApplicationId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Review_Job_Application");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Review)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Review_AspNetUsers");
            });

            modelBuilder.Entity<RoleFeature>(entity =>
            {
                entity.HasKey(e => new { e.RoleId, e.FeatureId })
                    .HasName("PK_Role_Feature");

                entity.ToTable("Role_Feature");

                entity.HasOne(d => d.Feature)
                    .WithMany(p => p.RoleFeature)
                    .HasForeignKey(d => d.FeatureId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Role_Feature_Feature");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RoleFeature)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Role_Feature_AspNetRoles");
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.Property(e => e.ActiveBy).HasColumnType("varchar(50)");

                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.ClosedDate).HasColumnType("datetime");

                entity.Property(e => e.Currency).HasColumnType("varchar(3)");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Price).HasColumnType("numeric");

                entity.Property(e => e.ServiceCode)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ServiceEnglishName).HasColumnType("varchar(300)");

                entity.Property(e => e.ServiceName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.TimeUnit)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Type).HasMaxLength(50);
            });

            modelBuilder.Entity<ServiceDiscount>(entity =>
            {
                entity.Property(e => e.ActiveBy).HasColumnType("varchar(50)");

                entity.Property(e => e.EndDiscountDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.StartDiscountDate).HasColumnType("datetime");

                entity.Property(e => e.TimeUnit).HasMaxLength(10);

                entity.HasOne(d => d.Service)
                    .WithMany(p => p.ServiceDiscount)
                    .HasForeignKey(d => d.ServiceId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Service_Discount_Services");
            });

            modelBuilder.Entity<SiteConfig>(entity =>
            {
                entity.HasKey(e => e.Key)
                    .HasName("PK_SiteConfig");

                entity.Property(e => e.Key)
                    .HasColumnName("key")
                    .HasMaxLength(50);

                entity.Property(e => e.Value).HasColumnName("value");
            });

            modelBuilder.Entity<Skill>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<SystemLanguage>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<University>(entity =>
            {
                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.Address).HasMaxLength(400);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasColumnType("varchar(100)");

                entity.Property(e => e.EnglishName).HasColumnType("varchar(300)");

                entity.Property(e => e.Logo).HasMaxLength(250);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.Property(e => e.Website).HasMaxLength(250);

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.University)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("FK_University_Location");
            });

            modelBuilder.Entity<UniversityBranch>(entity =>
            {
                entity.Property(e => e.Address).HasMaxLength(250);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<UserLog>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserLog)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UserLog_AspNetUsers");
            });
        }

        #region Helpers

        public override int SaveChanges()
        {
            BaseStamps();
            return base.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            BaseStamps();
            return await base.SaveChangesAsync();
        }

        public void BaseStamps()
        {
            var entities = ChangeTracker.Entries().Where(x =>
                x.Entity.GetType().GetProperty("CreatedDate") != null
                || x.Entity.GetType().GetProperty("ModifiedDate") != null
                || x.Entity.GetType().GetProperty("CreatedBy") != null
                || x.Entity.GetType().GetProperty("ModifiedBy") != null
                && (x.State == EntityState.Added || x.State == EntityState.Modified)).ToList();

            if (entities.Any())
            {
                foreach (var entry in entities)
                {
                    if (entry.Entity.GetType().GetProperty("CreatedDate") != null)
                    {
                        if (entry.State == EntityState.Added)
                        {
                            entry.Property("CreatedDate").CurrentValue = DateTime.Now;
                        }
                        else if (entry.State == EntityState.Modified)
                        {
                            entry.Property("CreatedDate").IsModified = false;
                        }
                    }
                    if (entry.Entity.GetType().GetProperty("ModifiedDate") != null)
                    {
                        entry.Property("ModifiedDate").CurrentValue = DateTime.Now;
                    }
                    long userid;
                    if (entry.Entity.GetType().GetProperty("CreatedBy") != null)
                    {
                        if (entry.State == EntityState.Added)
                        {
                            if (entry.Property("CreatedBy").Metadata.PropertyInfo.PropertyType == typeof(long?))
                            {
                                entry.Property("CreatedBy").CurrentValue = long.TryParse(_httpContext.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier), out userid) ? (object)userid : null;
                            }
                            else
                            {
                                entry.Property("CreatedBy").CurrentValue = _httpContext.HttpContext.User.FindFirstValue(ClaimTypes.Name);
                            }

                        }
                        else if (entry.State == EntityState.Modified)
                        {
                            entry.Property("CreatedBy").IsModified = false;
                        }
                    }
                    if (entry.Entity.GetType().GetProperty("ModifiedBy") != null)
                    {
                        if (entry.Property("ModifiedBy").Metadata.PropertyInfo.PropertyType == typeof(long?))
                        {
                            entry.Property("ModifiedBy").CurrentValue = long.TryParse(_httpContext.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier), out userid) ? (object)userid : null;
                        }
                        else
                        {
                            entry.Property("ModifiedBy").CurrentValue = _httpContext.HttpContext.User.FindFirstValue(ClaimTypes.Name);
                        }
                    }
                }
            }
        }

        #endregion
    }
}