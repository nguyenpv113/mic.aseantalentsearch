﻿using System;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.EF
{
    public partial class Company
    {
        public Company()
        {
            Advertisement = new HashSet<Advertisement>();
            CompanyBenefit = new HashSet<CompanyBenefit>();
            CompanyCategory = new HashSet<CompanyCategory>();
            CompanyMedia = new HashSet<CompanyMedia>();
            InterviewInvitation = new HashSet<InterviewInvitation>();
            Order = new HashSet<Order>();
            PositionJob = new HashSet<PositionJob>();
            ViewCV = new HashSet<ViewCV>();
            SaveCandidate = new HashSet<SaveCandidate>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public int? LocationId { get; set; }
        public string Taxcode { get; set; }
        public string Phone { get; set; }
        public string ContactPhone { get; set; }
        public string ContactPerson { get; set; }
        public bool? ContactPersonGender { get; set; }
        public string Owner { get; set; }
        public string CompanySize { get; set; }
        public string Logo { get; set; }
        public string Introduction { get; set; }
        public string Summary { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string LinkedIn { get; set; }
        public string Twitter { get; set; }
        public string AttractivePolicies { get; set; }
        public bool? ActiveStatus { get; set; }
        public DateTime? ActiveDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ActiveBy { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? Position { get; set; }
        public float? Long { get; set; }
        public float? Lat { get; set; }
        public Guid? Guid { get; set; }


        public virtual ICollection<Advertisement> Advertisement { get; set; }
        public virtual ICollection<CompanyBenefit> CompanyBenefit { get; set; }
        public virtual ICollection<CompanyCategory> CompanyCategory { get; set; }
        public virtual ICollection<CompanyMedia> CompanyMedia { get; set; }
        public virtual ICollection<InterviewInvitation> InterviewInvitation { get; set; }
        public virtual ICollection<Order> Order { get; set; }
        public virtual ICollection<PositionJob> PositionJob { get; set; }
        public virtual ICollection<ViewCV> ViewCV { get; set; }
        public virtual ICollection<SaveCandidate> SaveCandidate { get; set; }
        public virtual AspNetUsers IdNavigation { get; set; }
        public virtual Location Location { get; set; }
    }
}
