﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.EF.Logical
{
    public class LanguageExtension
    {
        public static string CurentLanguage { get; set; }

        public static IEnumerable<Claim> CurrentClaim { get; set; }

        public static string CurrentCookie { get; set; }

        public static string CurrentSession { get; set; }
    }
}
