﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.EF.Logical
{
    public class MultipleLanguage
    {
        
            private readonly AseanTalentSearchContext _context;
            private Language Lang;

            public MultipleLanguage(AseanTalentSearchContext context)
            {
                _context = context;
            }

            public Language language
            {
                get
                {
                    if (Lang != null)
                        return Lang;
                    if (LanguageExtension.CurentLanguage != null)
                        return Lang = GetLanguage(LanguageExtension.CurentLanguage);
                    else
                    {
                        Lang = _context.Language.Where(x => x.IsCurrentLanguage).FirstOrDefault();
                        if (Lang == null)
                            Lang = _context.Language.FirstOrDefault();
                        LanguageExtension.CurentLanguage = Lang.LanguageCulture;
                    }
                    return Lang;
                }
                set
                {
                    Lang = value;
                }
            }

            public Language GetLanguage(string Language)
            {
                try
                {
                    return _context.Language.Where(x => x.LanguageCulture == Language).FirstOrDefault();
                }
                catch
                {
                    return _context.Language.FirstOrDefault();
                }
            }

            public string GetResource(string resourceKey)
            {
                try
                {
                    var reResource = _context.LocaleStringResource.Where(x => x.LanguageId == language.Id
                    && x.ResourceName == resourceKey).First().ResourceValue;
                    return reResource;
                }
                catch
                {
                    return resourceKey;
                }
            }

            Localizer Localizer;
            public Localizer T
            {
                get
                {
                    if (Localizer == null)
                    {
                        Localizer = (format, args) =>
                        {
                            var resFormat = GetResource(format);
                            if (string.IsNullOrEmpty(resFormat))
                            {
                                return format;
                            }
                            return
                                ((args == null || args.Length == 0)
                                                        ? resFormat
                                                        : string.Format(resFormat, args));
                        };
                    }
                    return Localizer;
                }
            }

            public List<Language> listLanguage
            {
                get
                {
                    var gL = _context.Language.Where(x => x.Status).ToList();
                    return gL;
                }
            }
        
        }
        //public class ListLanguage
        //{
        //    public static List<Language> CurrnetList(DuyTanTravelDbContext _context)
        //    {
        //        return _context.Language.Where(x => x.Status.Value).ToList();
        //    }
        //}
        public delegate List<Language> ListLanguage(List<Language> list);

        public delegate String Localizer(string format, params object[] args);
    
}
