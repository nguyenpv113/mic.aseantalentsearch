﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.EF.Logical
{
    public interface INewsManager
    {
        News GetNewsById(int id, bool type);
        News GetNewsShowById(int id, bool type);
        News InsertNews(News model);
        int UpdateNews(News model);
        int DeleteNewsById(int id);
        List<News> GetAllNews();
        IQueryable<News> GetAllNews(bool type);
    }
}
