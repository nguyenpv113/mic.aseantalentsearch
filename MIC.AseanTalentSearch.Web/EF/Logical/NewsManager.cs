﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.EF.Logical
{
    public class NewsManager : INewsManager
    {
        private readonly AseanTalentSearchContext _context;

        public NewsManager(AseanTalentSearchContext context)
        {
            _context = context;
        }
        public int DeleteNewsById(int id)
        {
            _context.News.Remove(_context.News.Find(id));
            return _context.SaveChanges();
        }

        public List<News> GetAllNews()
        {
            return _context.News.OrderByDescending(x => x.CreatedDate).ToList();
        }

        public IQueryable<News> GetAllNews(bool type)
        {
            return _context.News.Where(x => x.Type == type && x.Status).OrderByDescending(x => x.CreatedDate);
        }

        public News GetNewsById(int id, bool type)
        {
            try
            {
                return _context.News.Where(x => x.Type == type && x.NewsId == id).FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }

        public News GetNewsShowById(int id, bool type)
        {
            try
            {
                return _context.News.Where(x => x.Type == type && x.NewsId == id && x.Status).FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }

        public News InsertNews(News model)
        {
            try
            {
                _context.News.Add(model);
                _context.SaveChanges();
                return model;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public int UpdateNews(News model)
        {
            try
            {
                _context.News.Update(model);
                return _context.SaveChanges();
            }
            catch(Exception ex)
            {
                return new int();
            }
        }
    }
}
