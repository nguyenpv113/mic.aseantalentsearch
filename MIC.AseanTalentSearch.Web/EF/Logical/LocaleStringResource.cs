﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.EF.Logical
{
    public class LocaleStringResourceManager : ILocaleStringResource
    {
        private readonly AseanTalentSearchContext _context;
        public LocaleStringResourceManager(AseanTalentSearchContext context)
        {
            _context = context;
        }
        public int Delete(LocaleStringResource model)
        {
            _context.LocaleStringResource.Remove(model);
            return _context.SaveChanges();
        }

        public int DeleteById(int Id)
        {
            try
            {
                var model = _context.LocaleStringResource.Find(Id);
                if (model != null)
                {
                    _context.LocaleStringResource.Remove(model);
                    return _context.SaveChanges();
                }
                return new int();
            }
            catch
            {
                return new int();
            }
        }

        public List<LocaleStringResource> GetAllByLanguage(int LangId)
        {
            return _context.LocaleStringResource.Where(x => x.LanguageId == LangId).OrderBy(x => x.ResourceName).ToList();
        }

        public List<LocaleStringResource> GetAllResource()
        {
            return _context.LocaleStringResource.ToList();
        }

        public LocaleStringResource GetLocaleString(int Id)
        {
            try
            {
                return _context.LocaleStringResource.Find(Id);
            }
            catch
            {
                return null;
            }
        }

        public LocaleStringResource GetLocaleString(int LangId, string ResourceName)
        {
            try
            {
                return _context.LocaleStringResource.Where(x => x.LanguageId == LangId && x.ResourceName == ResourceName).First();
            }
            catch
            {
                return null;
            }
        }

        public int Insert(LocaleStringResource model)
        {
            var cSL = GetLocaleString(model.LanguageId, model.ResourceName);
            if (cSL != null)
                return Update(model);
            else
                _context.LocaleStringResource.Add(model);
            return _context.SaveChanges();
        }

        public int Update(LocaleStringResource model)
        {
            _context.LocaleStringResource.Update(model);
            return _context.SaveChanges();
        }
    }
}
