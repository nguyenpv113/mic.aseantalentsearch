﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.EF.Logical
{
    public partial interface ILocaleStringResource
    {
        int Insert(LocaleStringResource model);
        int Delete(LocaleStringResource model);
        int DeleteById(int Id);
        int Update(LocaleStringResource model);
        List<LocaleStringResource> GetAllResource();
        List<LocaleStringResource> GetAllByLanguage(int LangId);
        LocaleStringResource GetLocaleString(int Id);
        LocaleStringResource GetLocaleString(int LangId, string ResourceName);
    }
}
