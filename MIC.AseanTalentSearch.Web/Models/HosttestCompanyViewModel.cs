﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Models
{
    public class HosttestCompanyViewModel
    {
        public Company Company { get; set; }
        public int? Vacancies { get; set; }
        public Location Location { get; set; }
    }
}
