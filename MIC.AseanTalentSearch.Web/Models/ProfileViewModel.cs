﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Models
{
    public class ProfileViewModel
    {
        public string Image { get; set; }
        public string  FullName { get; set; }
        public long? Id { get; set; }

        public string CvId { get; set; }

        public int InterviewInvitationCount { get; set; }

        public bool? IsPublishCV { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        [RegularExpression(@"^[A-Za-z0-9\[\]/!$%^&*()\-_+{};:'£@#.?]*$", ErrorMessage = "Password do not allow space")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        [RegularExpression(@"^[A-Za-z0-9\[\]/!$%^&*()\-_+{};:'£@#.?]*$", ErrorMessage = "Password do not allow space")]
        public string ConfirmPassword { get; set; }
    }
}
