﻿using MIC.AseanTalentSearch.Web.EF;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Models
{
    public class StatisticsViewModel
    {
        public int? CandidateCount { get; set; }
        public int? UniversityCount { get; set; }
        public int?  CompanyCount { get; set; }
        public int? JobCount { get; set; }
        public DbSet<SiteConfig> siteConfig { get; set; }
    }
}
