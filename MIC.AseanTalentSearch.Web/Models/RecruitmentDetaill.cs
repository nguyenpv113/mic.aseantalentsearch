﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Models
{
    public class RecruitmentDetaill
    {
        public Company Company { get; set; }
        public PositionJob PositionJob { get; set; }
    }
}
