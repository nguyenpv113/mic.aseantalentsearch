﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using MIC.AseanTalentSearch.Web.EF;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Models
{
    public class JobSearchViewModel
    {
        public JobSearchViewModel()
        {
            SkillId = new List<int>();
            JobLevelId = new List<int?>();
            SalaryLevel = new List<string>();
            SkillNames = new List<string>();
        }

        public int? CategoryId { get; set; }
        public int? LocationId { get; set; }
        public string JobName { get; set; }
        public IList<int> SkillId { get; set; }
        public IList<string> SkillNames { get; set; }
        public IList<int?> JobLevelId { get; set; }
        public IList<string> SalaryLevel { get; set; }
        public IPagedList<PositionJob> Result { get; set; }
        public SelectList CategoryIdSelectList { get; set; }
        public List<SelectListItem> LocationIdSelectList { get; set; }
        public string BackgroundUrl { get; set; }
    }
}
