﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Models
{
    public class SuggestedJob
    {
        public int? PositionJobID { get; set; }
        public int? CandidateID { get; set; }
    }
}
