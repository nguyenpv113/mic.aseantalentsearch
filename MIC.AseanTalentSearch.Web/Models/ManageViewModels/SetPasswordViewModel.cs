﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Models.ManageViewModels
{
    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        [RegularExpression(@"^[A-Za-z0-9\[\]/!$%^&*()\-_+{};:'£@#.?]*$", ErrorMessage = "Password do not allow space")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        [RegularExpression(@"^[A-Za-z0-9\[\]/!$%^&*()\-_+{};:'£@#.?]*$", ErrorMessage = "Password do not allow space")]
        public string ConfirmPassword { get; set; }
    }
}
