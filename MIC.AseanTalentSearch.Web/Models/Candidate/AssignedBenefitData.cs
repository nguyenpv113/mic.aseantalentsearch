﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Models.Candidate
{
    public class AssignedBenefitData
    {
        public int BenefitId { get; set; }
        public string BenefitName { get; set; }
        public bool Assigned { get; set; }
    }
}
