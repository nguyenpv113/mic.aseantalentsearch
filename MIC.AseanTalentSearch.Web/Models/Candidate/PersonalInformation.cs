﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using MIC.AseanTalentSearch.Web.EF;
using System.ComponentModel.DataAnnotations;
namespace MIC.AseanTalentSearch.Web.Models.Candidate
{
    public class PersonalInformation
    {
        // [RegularExpression(".*<.*?>.*", ErrorMessage = "Html tags are not allowed")]
        [Required]
        [Display(Name = "Years Of Experience")]
        [Range(0,30,ErrorMessage = "Years Of Experience from 0 to 30")]
        public double? YearsOfExperience { get; set; }
        [Display(Name = "First name")]
        //[RegularExpression("^(?!.*<[^>]+>).*", ErrorMessage = "Html tags are not allowed")]
        public string FirstName { get; set; }
        [Display(Name = "Last name")]
        //[RegularExpression("^(?!.*<[^>]+>).*", ErrorMessage = "Html tags are not allowed")]
        public string LastName { get; set; }
        [Display(Name = "Email")]
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Please enter a valid Email address")]
        public string Email { get; set; }
        [Display(Name = "Phone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("^(?!.*<[^>]+>).*", ErrorMessage = "Html tags are not allowed")]
        public string Phone { get; set; }
        [Display(Name = "Date of Birth ")]
        [Required(ErrorMessage = "Date field is required")]
        public String DateOfBirth { get; set; }
        [Display(Name = "Nationality")]
        public string Nationality { get; set; }

        public IList<SelectListItem> Genders = new List<SelectListItem>
        {
            new SelectListItem() {Value = "", Text = "Unknown"},
            new SelectListItem() {Value = true.ToString(), Text = "Male"},
            new SelectListItem() {Value = false.ToString(), Text = "Female"}
        };
        [Display(Name = "Gender")]
        public bool? Gender { get; set; }

        public IList<SelectListItem> MaritalStatus = new List<SelectListItem>
        {
            new SelectListItem() {Value = "", Text = "Please select"},
            new SelectListItem() {Value = "Single", Text = "Single"},
            new SelectListItem() {Value = "Married", Text = "Married"}
        };
        [Display(Name = "Marital status")]
        [Required]
        public string MaritalStatusValue { get; set; }
        [Display(Name = "Country")]
        [Required]
        public string Country { get; set; }
        public SelectList Countries { get; set; }
        [Display(Name = "City")]
        [Required]
        public string HomeCity { get; set; }

        public SelectList HomeCities { get; set; }
        [Display(Name = "District")]
        //[Required]
        public string HomeDistrict { get; set; }
        public SelectList HomeDictricts { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
    }
}
