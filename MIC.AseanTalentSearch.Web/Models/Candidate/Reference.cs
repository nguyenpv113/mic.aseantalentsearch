﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Models.Candidate
{
    public class Reference
    {
        [Display(Name = "Fullname")]
        [Required]
        public string FullName { get; set; }
        [Display(Name = "Position")]
        [Required]
        public string Position { get; set; }
        [Display(Name = "Company")]
        [Required]
        public string Company{ get; set; }
        [Display(Name = "Email")]
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Please enter a valid Email address")]
        public string Email { get; set; }
        [Display(Name = "Phone")]
        public string Phone { get; set; }
        public int Id { get; set; }
    }
}
