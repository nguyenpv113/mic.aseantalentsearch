﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace MIC.AseanTalentSearch.Web.Models.Candidate
{
    public class EducationHistory
    {
        public long? Id { get; set; }
        [Required]
        [Display(Name = "From date")]
        public string From { get; set; }
        [Required]
        [Display(Name = "To date")]
        public string To { get; set; }
        public string Major { get; set; }
        [Required]
        [Display(Name = "Major")]
        public int? MajorId { get; set; }
        public SelectList EducationLevels { get; set; }
        [Required]
        [Display(Name = "Education Level")]
        public int EducationLevelId { get; set; }
        public string EducationLevelName { get; set; }
        [Display(Name = "Achievements")]
        public string Achievements { get; set; }
        public string UniversityName { get; set; }

        [Required]
        [Display(Name = "University")]
        public int? UniversityId { get; set; }
        [Display(Name = "Ranking")]
        [Required]
        public string Ranking { get; set; }

        public SelectList University { get; set; }
        public SelectList Majors { get; set; }
    }
}
