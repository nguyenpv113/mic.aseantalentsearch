﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace MIC.AseanTalentSearch.Web.Models.Candidate
{
    public class Language
    {
        public int Id { get; set; }
        public SelectList List { get; set; }
        [Display(Name = "Language")]
        public int LanguageId { get; set; }
        [Display(Name = "Candidate")]
        public long? CandidateId { get; set; }
        [Display(Name = "Certification")]
        public string Certification { get; set; }
        [Display(Name = "Score or level")]
        public string ScoreOrLevel { get; set; }
        [Display(Name = "Language")]
        public string LanguageName { get; set; }
    }
}
