﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace MIC.AseanTalentSearch.Web.Models.Candidate
{
    public class KeySkill
    {
        public KeySkill()
        {
            SkillId = new List<int>();
            SkillName = new List<string>();
            SkillLevel = new List<int>();
        }
        public SelectList List { get; set; }
        [Display(Name = "Skill")]
        public IList<int> SkillId { get; set; }
        public IList<string> SkillName { get; set; }
        public IList<int> SkillLevel { get; set; }
        public int? Id { get; set; }

        public int? OldId { get; set; }

        [Display(Name = "Skill")]
        [Required]
        public string Skill { get; set; }
        
        [Display(Name ="Level")]
        [Range(0,100,ErrorMessage = "Enter a value between 0 and 100")]
        public int? Level { get; set; }
    }
}
