﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using MIC.AseanTalentSearch.Web.EF;
using System.ComponentModel.DataAnnotations;
namespace MIC.AseanTalentSearch.Web.Models.Candidate
{
    public class WorkingPreference
    {

        public WorkingPreference()
        {
            LocationId = new List<int>();
            LocationName = new List<string>();
            BeneficIds = new List<int>();
            Benefits = new List<Benefit>();
            CategoryId = new List<int>();
            CategoryName = new List<string>();
            SelectedBenefits = new List<AssignedBenefitData>();
        }
        [Display(Name = "Preferred Working Location")]
        [Required]
        public IList<int> LocationId { get; set; }
        public IList<string> LocationName { get; set; }
        public SelectList Locations { get; set; }
        [Display(Name = "Expected Job Category")]
        [Required]
        public IList<int> CategoryId { get; set; }
        public IList<string> CategoryName { get; set; }
        public SelectList ExpectedCategory { get; set; }
        [Display(Name = "Expected Job Level")]
        [Required]
        public int? JobLevelId { get; set; }
        public string JobLevelName { get; set; }
        public SelectList JobLevels { get; set; }
        [Range(0,999999999.999,ErrorMessage = "Please enter salary from 0 to 1 billion dollar")]
        public decimal? Salary { get; set; }
        public IList<Benefit> Benefits { get; set; }
        public IList<AssignedBenefitData> SelectedBenefits { get; set; }
        public IList<int> BeneficIds { get; set; }
    }
}
