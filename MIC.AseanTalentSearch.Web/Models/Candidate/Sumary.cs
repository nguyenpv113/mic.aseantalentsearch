﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Models.Candidate
{
    public class Sumary
    {
        [Display(Name = "Summary")]
        public string Text { get; set; }
    }
}
