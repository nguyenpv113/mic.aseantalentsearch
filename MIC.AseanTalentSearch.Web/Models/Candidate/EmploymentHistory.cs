﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Models.Candidate
{
    public class EmploymentHistory
    {
        public long Id { get; set; }
        [Required]
        [Display(Name = "From month")]
        public string From { get; set; }
   
        [Display(Name = "To month")]
        public string To { get; set; }
        [Display(Name = "Position")]
        [Required]
        public string Position { get; set; }
        [Display(Name = "Company")]
        [Required]
        public string Company { get; set; }
        public string Description { get; set; }
        public bool? IsCurrentJob { get; set; }
    }
}
