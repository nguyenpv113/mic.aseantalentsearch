﻿using MIC.AseanTalentSearch.Web.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Models.Candidate
{
    public class CandidateCommentViewModel
    {
        public CandidateComment CondidateComment { get; set; }
        public string _OldUniversity { get; set; }
    }
}
