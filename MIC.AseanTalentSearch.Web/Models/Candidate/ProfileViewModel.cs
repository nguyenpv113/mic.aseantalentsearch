﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Models.Candidate
{
    public class ProfileViewModel
    {
        public ProfileViewModel()
        {
            PersonalInformation = new PersonalInformation();
            DesiredJob = new WorkingPreference();
            Degree = new List<EducationHistory>();
            JobHistory = new List<EmploymentHistory>();
        }
        public PersonalInformation PersonalInformation { get; set; }
        public WorkingPreference DesiredJob { get; set; }
        public IList<EducationHistory> Degree { get; set; }
        public IList<EmploymentHistory> JobHistory { get; set; }
    }
}
