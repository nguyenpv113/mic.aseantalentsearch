﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Models
{
    public class HottestUniversityViewModel
    {
        public University University { get; set; }
        public Location Location { get; set; }
        public int? StudentCount { get; set; }
    }
}
