﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Models.Employer
{
    public class ProfileViewModel
    {
        [Display(Name = "Logo")]
        public string Image { get; set; }
        [Display(Name = "Company name")]
        [Required]
        public string CompanyName { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone number")]
        public string CompanyPhone { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string CompanyEmail { get; set; }
        [Display(Name = "Company size")]
        public string CompanySize { get; set; }
        [Display(Name = "Website")]
        public string Website { get; set; }
        [Display(Name = "Adress")]
        public string Address { get; set; }
        [Display(Name = "Introduction")]
        public string Introduction { get; set; }
        [Display(Name = "Account")]
        public string Account { get; set; }
    }
}
