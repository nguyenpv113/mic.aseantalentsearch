﻿using MIC.AseanTalentSearch.Web.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Models
{
    public class LanguageViewModel
    {
        public Language Language { get; set; }
        public LocaleStringResource StringResource { get; set; }
    }
}
