﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Models.Common
{
    /// <summary>
    /// ViewModel : Khai báo thông tin tìm kiếm công việc
    /// </summary>
    public class JobSearchForm
    {
        public int? CategoryId { get; set; }
        public SelectList CategoryIdSelectList { get; set; }
        public int? LocationId { get; set; }
        public SelectList LocationIdSelectList { get; set; }
        public string JobName { get; set; }
        public string BackgroundUrl { get; set; }
    }
}
