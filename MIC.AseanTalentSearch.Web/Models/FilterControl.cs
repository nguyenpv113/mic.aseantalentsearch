﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Models
{
    public class FilterControl
    {
        public FilterControl()
        {
            Skills = new List<Skill>();
            JobLevels = new List<JobLevel>();
            SalaryLevel = new List<string>();
            Locations = new List<Location>();
        }
        public IList<Skill> Skills { get; set; }
        public IList<JobLevel> JobLevels { get; set; }
        public IList<string> SalaryLevel { get; set; }
        public int? LocationId { get; set; }
        public IList<Location> Locations { get; set; }
        public int? CategoryId { get; set; }
        public IList<Category> Categories { get; set; }
        public string JobName { get; set; }
    }
}
