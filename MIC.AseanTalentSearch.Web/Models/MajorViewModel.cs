﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Models
{
    public class MajorViewModel
    {
        public int? universityId { get; set; }
        public int? educationlevelId { get; set; }

        public SelectList UniversityList { get; set; }
        public SelectList EducationLevelList { get; set; }
        public IList<Major> Major { get; set; }
    }
}
