﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Models
{
    public class CaptchaAuthenticate
    {
        public static async Task<bool> AuthenticationAsync(Microsoft.AspNetCore.Http.HttpRequest request, System.Net.Http.HttpClient client)
        {
            var response = request.Form["g-recaptcha-response"];
            string secretKey = "6LcoZ28UAAAAAJrYfhJ95aV0TcZNjgk7XWeMGgcM";
            var resultCaptcha = await client.GetStringAsync(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(resultCaptcha);

            return captchaResponse.Success;
        }

        class CaptchaResponse
        {
            public bool Success { get; set; }
        }


        //var response = Request.Form["g-recaptcha-response"];
        //string secretKey = "6LdoTyYUAAAAANBjO_2CZEQXIDggsrVWLJ0wgriD";
        //var client = new HttpClient();
        //var resultCaptcha = await client.GetStringAsync(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
        //var captchaResponse = JsonConvert.DeserializeObject<JSonTest>(resultCaptcha);

        //    if (!captchaResponse.Success)
        //    {
        //        ModelState.AddModelError("recaptchaValidation", "You must authenticate  captcha");
        //        return View(model);
        //}

    }
}
