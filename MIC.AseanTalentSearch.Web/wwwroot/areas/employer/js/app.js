﻿jQuery.fn.extend({
    beforeSubmitForm: function () {
        $('.mask-money', this).mask('000.000.000.000.000,00', {
            reverse: true,
            placeholder: "10.000,00"
        });
        $('.mask-vat', this).mask('000,00', {
            reverse: true,
            placeholder: "10,00"
        });
        $('.mask-quantity', this).mask('000.000.000.000.000', {
            reverse: true,
            placeholder: "1.000"
        });
        $('.mask-factor', this).mask('000,00', {
            reverse: true,
            placeholder: "10,00"
        });
        this.on('submit',
            function (e) {
                $('.mask-money', this)
                    .each(function () {
                        var money = $(this).val().replace(/[^0-9,]/g, '');
                        $(this).val(money);
                    });
                $('.mask-vat', this)
                    .each(function () {
                        var vat = $(this).val().replace(/[^0-9,]/g, '');
                        $(this).val(vat);
                    });
                $('.mask-quantity', this)
                    .each(function () {
                        var factor = $(this).val().replace(/[^0-9,]/g, '');
                        $(this).val(factor);
                    });
                $('.mask-factor', this)
                    .each(function () {
                        var factor = $(this).val().replace(/[^0-9,]/g, '');
                        $(this).val(factor);
                    });
            });
    },
    displayValidation: function (xhr) {
        $("span.field-validation-valid", this).each(function () {
            $(this).text('');
        });
        $("div.alert").remove();
        // Thong diep sau request
        var responses = xhr.responseJSON;
        // Tao div thong báo
        var container = $('<div class="alert bg-warning text-danger alert-dismissible animated fadeInRight">' +
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
            '<h4><i class="icon fa fa-warning"></i> Alert!</h4>' +
            '</div>');
        var ul = document.createElement("ul");
        if (responses && xhr.readyState == 4 && xhr.status == 400) {
            for (var key in responses) {
                if (responses.hasOwnProperty(key)) {
                    var elements = responses[key];
                    if (elements) {
                        var li = document.createElement("li");
                        for (var i in elements) {
                            $("span[data-valmsg-for='" + key + "']").text(elements[i]);
                            li.innerHTML = elements[i];
                            ul.append(li);
                        }
                    }
                }
            }
            container.append(ul);
            this.prepend(container);
        }
    },
    displayLoading: function () {
        var container = $('<div class="overlay"><i class="fa fa-refresh fa-spin text-info"></i></div>');
        this.closest('div.box').append(container);
    },
    removeLoading: function () {
        $("div.overlay").remove();
    }
});

function notifyStatusCode(xhr) {
    var msm = xhr.responseText === "" ? xhr.statusText : xhr.responseText;
    switch (xhr.status) {
        case 200:
            showNotify('Info', 'Action successful!');
            break;
        case 201:
            showNotify('Info', 'Created successful!');
            break;
        case 204:
            showNotify('Info', 'Updated successful!');
            break;
        case 205:
            showNotify('Info', msm);
            break;
        case 400:
            showNotify('Notice', xhr.statusText);
            break;
        case 401:
            showNotify('Notice', xhr.statusText);
            break;
        case 409:
            showNotify('Notice', xhr.statusText);
            break;
        case 413:
            showNotify('Notice', xhr.statusText);
            break;
        case 415:
            showNotify('Notice', xhr.statusText);
            break;
        case 500:
            showNotify('Notice', xhr.statusText);
            break;
        default:
            showNotify('Notice', xhr.statusText);
    }
}

function showNotify(type, msm) {
    var opts = {
        styling: "bootstrap3",
        title: "Title",
        text: msm,
        delay: 4000,
        animate: {
            animate: true,
            in_class: 'fadeInRight',
            out_class: 'slideOutUp'
        },
        mobile: {
            swipe_dismiss: false
        },
        history: {
            history: false
        }
    };
    switch (type) {
        case 'Info':
            opts.title = "Info";
            opts.icon = "fa fa-info-circle";
            opts.type = "info";
            break;
        case 'Notice':
            opts.title = "Notice";
            opts.icon = "fa fa-exclamation-triangle";
            break;
        default:
            break;
    }
    new PNotify(opts);
}