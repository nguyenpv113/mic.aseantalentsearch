$(document).ready(function() {
    // Header Scroll
    $(window).on('scroll', function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 50) {
            $('.header-fixed').addClass('fixed');
        } else {
            $('.header-fixed').removeClass('fixed');
        }
    });

    // Page Scroll
    var sections = $('section')
    nav = $('nav[role="navigation"]');

    $(window).on('scroll', function() {
        var cur_pos = $(this).scrollTop();
        sections.each(function() {
            var top = $(this).offset().top - 76
            bottom = top + $(this).outerHeight();
            if (cur_pos >= top && cur_pos <= bottom) {
                nav.find('a').removeClass('active');
                nav.find('a[href="#' + $(this).attr('id') + '"]').addClass('active');
            }
        });
    });
    nav.find('a').on('click', function() {
        var $el = $(this)
        id = $el.attr('href');
        $('html, body').animate({
            scrollTop: $(id).offset().top - 75
        }, 500);
        return false;
    });

    $('.nav-toggle').on('click', function() {
        $(this).toggleClass('close-nav');
        nav.toggleClass('open');
        return false;
    });
    nav.find('a').on('click', function() {
        $('.nav-toggle').toggleClass('close-nav');
        nav.toggleClass('open');
    });



});
$(document).ready(function() {
    $('.advance-select').select2({
        theme: "bootstrap",
        placeholder: "All categories",
        allowClear: true
    });
    $('#locations').select2({
        theme: "bootstrap",
        placeholder: "All locations",
        allowClear: true
    });
});
$(document).ready(function() {
    $('#intro').owlCarousel({
        animateOut: 'zoomOut',
        animateIn: 'zoomIn',
        items: 1,
        nav: false,
        autoHeight: true,
        dots: true,
        loop: true,
        autoplay: true,
        mouseDrag: false,
        autoplayTimeout: 5000,
    });
    $('#sub-intro').owlCarousel({
        animateOut: 'zoomOut',
        animateIn: 'zoomIn',
        items: 1,
        nav: false,
        autoHeight: true,
        dots: true,
        loop: true,
        autoplay: true,
        mouseDrag: false,
        autoplayTimeout: 5000,
    });
    var x= $('.img-step').owlCarousel({
        animateOut: 'zoomOut',
        animateIn: 'zoomIn',
        items: 1,
        nav: true,
        navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        autoHeight: true,
        dots: true,
        dotsContainer: '#carousel-custom-dots',
        loop: true,
        autoplay: true,
        mouseDrag: false,
        autoplayTimeout: 5000,
    });
    $('.owl-dot').click(function (e) { e.preventDefault(); x.trigger('to.owl.carousel', [$(this).index(), 300]); });
    //$('.owl-dot').click(function () { alert($(this).index());});

    $('#hottest-img').owlCarousel({
        items: 1,
        autoHeight: true,
        dots: true,
        loop: true,
        autoplay: true,
        autoplayTimeout: 100,
    });
    $('#universites-tabs').owlCarousel({
        items: 1,
        autoHeight: true,
        dots: true,
        loop: true,
        autoplay: true,
        autoplayTimeout: 100,
    });
    $('.content-company .for-card').owlCarousel({
        animateOut: 'zoomOut',
        animateIn: 'zoomIn',
        items: 1,
        nav: true,
        navText: ['<i class="fa fa-arrow-left" aria-hidden="true"></i>', '<i class="fa fa-arrow-right" aria-hidden="true"></i>'],
        autoHeight: true,
        dots: false,
        loop: true,
        autoplay: true,
        mouseDrag: false,
        autoplayTimeout: 4000,
    });
});