﻿$(document).ready(function () {
    $(".btn-select").each(function (e) {
        var value = $(this).find("ul li.selected").html();
        if (value != undefined) {
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
    });
});

$(document).on('click', '.btn-select', function (e) {
    e.preventDefault();
    var ul = $(this).find("ul");
    if ($(this).hasClass("active")) {
        if (ul.find("li").is(e.target)) {
            var target = $(e.target);
            target.addClass("selected").siblings().removeClass("selected");
            var value = target.html();
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
        ul.hide();
        $(this).removeClass("active");
    }
    else {
        $('.btn-select').not(this).each(function () {
            $(this).removeClass("active").find("ul").hide();
        });
        ul.slideDown(300);
        $(this).addClass("active");
    }
});

$(document).on('click', function (e) {
    var target = $(e.target).closest(".btn-select");
    if (!target.length) {
        $(".btn-select").removeClass("active").find("ul").hide();
    }
});

function OnSuccessRequest(result) {
    if (result.success) {
        window.location = result.url;
    } else {
        $("#mymodel .modal-content").html(result);
    }
}


function OnSuccessRegister(result) {
    if (result.success) {
        toastr.success('Yeeaahh! You are now registered. Please check your mail and confirm to finish!');
        $('#mymodel').modal('hide');
    } else {
        $("#mymodel .modal-content").html(result);
    }
}


function OnSuccessRecoveryPassword(result) {
    if (result.success) {
        toastr.success('Please check your email to reset your password.');
        $('#mymodel').modal('hide');
    } else {
        $("#mymodel .modal-content").html(result);
    }
}

function OnSuccessUpload(result) {
    if (result.success) {
        toastr.success('Your image is uploaded');
        $("#employerLogo").attr("src", "~/Uploads/Front/Images" + result.path);
    } else {
        $("#menu-infor .box-heading").html(result);
    }
}

function OnUpdateSuccess(result) {
    var blockToUpdate = $(this).data("updated-block");
    if (result.success) {
        toastr.success('Update success!!');
        if (blockToUpdate) {
            $(blockToUpdate).load(result.url).hide().delay(400).fadeIn();
        }
        $('#modal-default').modal('hide');
        $('#modal-edit').modal('hide');
    } else {
        $("#modal-default .modal-content").html(result);
    }
}


function OnInsertCVSuccess(result) {
    if (result.success) {
        toastr.success('Add success!!');
        location.reload();      
    } else {      
        $("#modal-default .modal-content").html(result);
    }
}

function OnFailure(jqXhr, textStatus, errorThrown) {
    if (jqXhr.status === 401) {
        window.location.href = "/Account/login";
    } else {
        window.location.href = "/Error";
    };
}

function OnUpdateSummarySuccess(result) {
    if (result.success) {
        toastr.success('Success! Your summary is updated');
        $("#personal-sumary").load(result.url).hide().delay(400).fadeIn();
        $('#update-model').modal('hide');
    } else {
        $("#update-model .modal-content").html(result);
    }
}

function OnUpdateSkillSuccess(result) {
    if (result.success) {
        toastr.success('Success! Your skill is updated');
        $("#key-skill").load(result.url).hide().delay(400).fadeIn();
        $('#update-model').modal('hide');
    } else {
        $("#update-model .modal-content").html(result);
    }
}

function OnUpdateModalLoadSuccess(result) {
    if (result.success) {
        window.location = result.url;
    } else {
        $("#update-model .modal-content").html(result);
    }
}

//function OnAddLanguageSuccess(result) {
//    if (result.success) {
//        toastr.success('Success! Your language is added');
//        $("#candidate-languages").load(result.url).hide().delay(400).fadeIn();
//        $('#update-model').modal('hide');
//    } else {
//        $("#update-model .modal-content").html(result);
//    }
//}
function OnAddLanguageSuccess(result) {

    var blockToUpdate = $(this).data("updated-block");
    if (result.success) {
        toastr.success('Success! Your language is added');
        if (blockToUpdate) {
            $(blockToUpdate).load(result.url).hide().delay(400).fadeIn();
        }
        $('#modal-default').modal('hide');
    } else {
        $("#modal-default .modal-content").html(result);
    }

    ////var blockToUpdate = $(this).data("updated-block");
    //if (result.success) {
    //    toastr.success('Success! Your language is added');
    //    //if (blockToUpdate) {
    //        $(blockToUpdate).load(result.url).hide().delay(400).fadeIn();
    //  //  }
    //    $('#modal -default').modal('hide');
    //} else {
    //    $("#modal-default .modal-content").html(result);
    //}
}



function OnUpdateLanguageSuccess(result) {
    if (result.success) {
        toastr.success('Success! Your language is updated');
        $("#candidate-languages").load(result.url).hide().delay(400).fadeIn();
        $('#update-model').modal('hide');
    } else {
        $("#update-model .modal-content").html(result);
    }
}


function OnAddEducationHistorySuccess(result) {
    if (result.success) {
        toastr.success('Success! Your history is updated');
        $("#education-history").load(result.url).hide().delay(400).fadeIn();
        $('#update-model').modal('hide');
    } else {
        $("#update-model .modal-content").html(result);
    }
}

function OnUpdateEmployerHistorySuccess(result) {
    if (result.success) {
        toastr.success('Success! Your history is updated');
        $("#employment-history").load(result.url).hide().delay(400).fadeIn();
        $('#update-model').modal('hide');
    } else {
        $("#update-model .modal-content").html(result);
    }
}

function OnUpdateReferenceSuccess(result) {
    if (result.success) {
        toastr.success('Success! Your reference is updated');
        $("#candidate-reference").load(result.url).hide().delay(400).fadeIn();
        $('#update-model').modal('hide');
    } else {
        $("#update-model .modal-content").html(result);
    }
}


function OnUpdatePreferenceSuccess(result) {
    if (result.success) {
        toastr.success('Success! Your preference is updated');
        $("#working-preferences").load(result.url).hide().delay(400).fadeIn();
        $('#update-model').modal('hide');
    } else {
        $("#update-model .modal-content").html(result);
    }
}
function deleteSkill(replaceTarget, data, url) {
    var bool = confirm('Do you want to delete your skill?');
    if (bool == true) {
        data = { id: data };
        $.post(url, data, function (result, status, xhr) {
            if (result.success) {
                toastr.success('Success! Your language is deleted');
                $(replaceTarget).load(result.url).hide().delay(400).fadeIn();
                $('#modal-edit').modal('hide');
            } else {
                toastr.warning('Warning! An error is occurred, please try again!');
                $(replaceTarget).load(result.url).hide().delay(400).fadeIn();
                $('#modal-edit').modal('hide');
            }
        });
    }
}

function deleteLanguage(replaceTarget, data, url) {
    var bool = confirm('Do you want to delete your language?');
    if (bool == true) {
        data = { id: data };
        $.post(url, data, function (result, status, xhr) {
            if (result.success) {
                toastr.success('Success! Your language is deleted');
                $(replaceTarget).load(result.url).hide().delay(400).fadeIn();
                $('#modal-edit').modal('hide');
            } else {
                toastr.warning('Warning! An error is occurred, please try again!');
                $(replaceTarget).load(result.url).hide().delay(400).fadeIn();
                $('#modal-edit').modal('hide');
            }
        });
    }
}
function deleteReference(replaceTarget, data, url) {
    data = { id: data };
    $.post(url, data, function (result, status, xhr) {
        if (result.success) {
            toastr.success('Success! Your reference is deleted');
            $(replaceTarget).load(result.url).hide().delay(400).fadeIn();
            $('#modal-edit').modal('hide');
        } else {
            toastr.warning('Warning! An error is occurred, please try again!');
            $(replaceTarget).load(result.url).hide().delay(400).fadeIn();
            $('#modal-edit').modal('hide');
        }
    });
}

function deleteEducation(replaceTarget, data, url) {
    var bool = confirm('Do you want to delete your education history?');
    if (bool == true) {
        data = { id: data };
        $.post(url, data, function (result, status, xhr) {
            if (result.success) {
                toastr.success('Success! Your education history is deleted');
                $(replaceTarget).load(result.url).hide().delay(400).fadeIn();
                $('#modal-edit').modal('hide');
            } else {
                toastr.warning('Warning! An error is occurred, please try again!');
                $(replaceTarget).load(result.url).hide().delay(400).fadeIn();
                $('#modal-edit').modal('hide');
            }
        });
    }
}

function deleteEmployment(replaceTarget, data, url) {
    data = { id: data };
    $.post(url, data, function (result, status, xhr) {
        if (result.success) {
            toastr.success('Success! Your employment history is deleted');
            $(replaceTarget).load(result.url).hide().delay(400).fadeIn();
            $('#modal-edit').modal('hide');
        } else {
            toastr.warning('Warning! An error is occurred, please try again!');
            $(replaceTarget).load(result.url).hide().delay(400).fadeIn();
            $('#modal-edit').modal('hide');
        }
    });
}


//validation form
$(document).ready(function () {
    //$('.text_without_html').mask('ZZZZ', {
    //    translation: {
    //        'Z': {
    //            pattern: /[a-z]/gm, optional: false
    //        }
    //    }
    //});
    //$('.text_without_html').inputmask({
    //    mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
    //    greedy: false,
    //    onBeforePaste: function (pastedValue, opts) {
    //        pastedValue = pastedValue.toLowerCase();
    //        return pastedValue.replace("mailto:", "");
    //    },
    //    definitions: {
    //        '*': {
    //            validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
    //            cardinality: 1,
    //            casing: "lower"
    //        }
    //    }
    //});
});