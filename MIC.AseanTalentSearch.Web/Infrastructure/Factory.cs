﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Infrastructure
{
    public class Factory
    {
        public static Candidate CreateCandidate()
        {
            Candidate candidate = new Candidate();
            
            return candidate;
        }

        public static GeneralProfile CreateProfile()
        {
            GeneralProfile profile = new GeneralProfile
            {
                FirstName = "<No name>",
                LastName = "<No name>"
            };
            return profile;
        }
    }
}
