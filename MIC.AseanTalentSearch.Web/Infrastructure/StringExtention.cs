﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Infrastructure
{
    public static class StringExtention
    {
        public static string StripHtml(this string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);           
            //return "";
        }
    }
}
