﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace MIC.AseanTalentSearch.Web
{
    public class Program
    {
        public static void Main(string[] args)
         {
            //var config = new ConfigurationBuilder()
            //    .AddCommandLine(args)
            //    .AddEnvironmentVariables(prefix: "ASPNETCORE_")
            //    .SetBasePath(Directory.GetCurrentDirectory())
            //    .Build();

            var host = new WebHostBuilder()
                //.UseConfiguration(config)
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseApplicationInsights()
                .Build();

            host.Run();
        }
    }
}
