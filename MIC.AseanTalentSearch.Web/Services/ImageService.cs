﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.CodeAnalysis.Options;
using Microsoft.Extensions.Options;
using MIC.AseanTalentSearch.Web.Settings;
using Microsoft.AspNetCore.Hosting;

namespace MIC.AseanTalentSearch.Web.Services
{
    public interface IImageService
    {
        bool Validate(IFormFile file);
        Task<string> SaveAsync(IFormFile file, string directory);
    }

    public class ImageService : IImageService
    {
        private readonly IHostingEnvironment _env;

        public ImageService(IHostingEnvironment env)
        {
            _env = env;
        }

        public bool Validate(IFormFile file)
        {
            string fileExtentison = System.IO.Path.GetExtension(file.FileName).ToLower();
            string[] allowedFileType =  {".gif", ".png", ".jpeg", ".jpg"};
            if (file.Length > 0 && file.Length < 2097152 && allowedFileType.Contains(fileExtentison) && file.ContentType.Contains("image"))
            {
                return true;
            }
            return false;
        }

        public async Task<string> SaveAsync(IFormFile file, string directory)
        {           
            var extension = Path.GetExtension(file.FileName);
            var fileName = Path.GetFileNameWithoutExtension(file.FileName.ToLowerInvariant()) + Guid.NewGuid() + extension;
            var uploadPath = MapPath(directory, fileName);
            using (var fileStream = new FileStream(uploadPath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
                fileStream.Flush();
            }
            return fileName;
        }

        public string MapPath(string path, string fileName)
        {
            var result = path ?? string.Empty;
            if (IsPathMapped(path) == false)
            {
                var wwwroot = _env.WebRootPath;
                if (result.StartsWith("~", StringComparison.Ordinal))
                {
                    result = result.Substring(1);
                }
                if (result.StartsWith("/", StringComparison.Ordinal))
                {
                    result = result.Substring(1);
                }

               string folderupload= Path.Combine(wwwroot, result.Replace('/', '\\'));
                if (!Directory.Exists(folderupload))
                {
                    Directory.CreateDirectory(folderupload);
                }

                result = Path.Combine(folderupload, fileName);
            }

            return result;
        }

        private bool IsPathMapped(string path)
        {
            var result = path ?? string.Empty;
            return result.StartsWith(_env.WebRootPath,
                StringComparison.Ordinal);
        }
    }
}
