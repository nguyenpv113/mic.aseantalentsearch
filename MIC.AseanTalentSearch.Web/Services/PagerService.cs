﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Dynamic.Core;
//using System.Linq.Expressions;
//using System.Threading.Tasks;
//using Microsoft.EntityFrameworkCore;
//using MIC.AseanTalentSearch.Web.EF;
//using Sakura.AspNetCore;

//namespace MIC.AseanTalentSearch.Web.Services
//{
//    public class PagerService<T> where T : class
//    {
//        private AseanTalentSearchContext _context;

//        public PagerService(AseanTalentSearchContext context)
//        {
//            _context = context;
//        }

//        public DataPager<T> GetData(string search, string sort, string sortdir, int pageSize, int pageNumber, string includeProperties = "", Expression<Func<T, bool>> filter = null)

//        {
//            IQueryable<T> query = _context.Set<T>();
//            foreach (var includeProperty in includeProperties.Split
//                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
//            {
//                query = query.Include(includeProperty);
//            }
//            if (filter != null && !String.IsNullOrEmpty(search))
//            {
//                query = query.Where(filter);
//            }
//            if (advfilter != null)
//            {
//                query = query.Where(advfilter);
//            }
//            query = query.OrderBy(sort + " " + sortdir);
//            DataPager<T> data = new DataPager<T>
//            {
//                PagedData = query.ToPagedList(pageSize, pageNumber),
//                Options = new DataPagerOptions
//                {
//                    Search = search,
//                    Sort = sort,
//                    Sortdir = sortdir,
//                    Page = pageNumber,
//                    PageSize = pageSize
//                }
//            };
//            return data;
//        }
//    }
//}
