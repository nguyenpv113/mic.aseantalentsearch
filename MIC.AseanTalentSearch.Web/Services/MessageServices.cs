﻿using System;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using MIC.AseanTalentSearch.Web.Settings;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace MIC.AseanTalentSearch.Web.Services
{
    // This class is used by the application to send Email and SMS
    // when you turn on two-factor authentication in ASP.NET Identity.
    // For more details see this link https://go.microsoft.com/fwlink/?LinkID=532713
    public class AuthMessageSender : IEmailSender, ISmsSender
    {
        public SmtpConfig SmtpConfig { get; }
        public AuthMessageSender(IOptions<SmtpConfig> smtpConfig)
        {
            SmtpConfig = smtpConfig.Value;
        }
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            // Plug in your email service here to send an email.
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(SmtpConfig.MailLabel, SmtpConfig.Email));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(TextFormat.Html) { Text = message };
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(SmtpConfig.UserName, SmtpConfig.Password);
            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(
                    SmtpConfig.Server,
                    SmtpConfig.Port,
                   SecureSocketOptions.Auto)
                    .ConfigureAwait(false);
                var mechanisms = string.Join(", ", client.AuthenticationMechanisms);
                Console.WriteLine("The SMTP server supports the following SASL mechanisms: {0}", mechanisms);
                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                // Note: only needed if the SMTP server requires authentication

                client.Authenticate(credentials, System.Threading.CancellationToken.None);
                await client.SendAsync(emailMessage).ConfigureAwait(false);
                await client.DisconnectAsync(true).ConfigureAwait(false);
            }
        }

        public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }

        // The following method is invoked by the RemoteCertificateValidationDelegate.
        public static bool ValidateServerCertificate(
              object sender,
              X509Certificate certificate,
              X509Chain chain,
              SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;

            Console.WriteLine("Certificate error: {0}", sslPolicyErrors);

            // Do not allow this client to communicate with unauthenticated servers.
            return false;
        }
    }
}
