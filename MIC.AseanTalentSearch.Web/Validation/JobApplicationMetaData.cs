﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.Validation;

namespace MIC.AseanTalentSearch.Web.Validation
{
    public class JobApplicationMetadata
    {
        [Display(Name = "Job application name")]
        [Required]
        public string JobApplicationName { get; set; }
        public int PositionJobId { get; set; }
        public long CandidateId { get; set; }
        [Display(Name = "Cover Letter")]
        public string CoverLetter { get; set; }
        public string CreateBy { get; set; }
        public bool JobStatus { get; set; }
        public string Linkfile { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}

namespace MIC.AseanTalentSearch.Web.EF
{
    [ModelMetadataType(typeof(JobApplicationMetadata))]
    public partial class JobApplication
    {
    }
}