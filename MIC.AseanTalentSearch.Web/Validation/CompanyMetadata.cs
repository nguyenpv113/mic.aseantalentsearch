﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.Validation;

namespace MIC.AseanTalentSearch.Web.Validation
{
    public class CompanyMetadata
    {
        [Required, MaxLength(250)]
        public string Name { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        [DataType(DataType.EmailAddress), Required, MaxLength(250)]
        public string Email { get; set; }
        [Display(Name = "Location")]
        public int? LocationId { get; set; }
        [MaxLength(50)]
        [Display(Name = "Tax Code")]
        public string Taxcode { get; set; }
        [DataType(DataType.PhoneNumber), Required, MaxLength(50)]
        public string Phone { get; set; }
        [MaxLength(50)]
        [Display(Name = "Contact Phone")]
        public string ContactPhone { get; set; }
        [Display(Name = "Contact Person")]
        public string ContactPerson { get; set; }
        [Display(Name = "Gender")]
        public bool? ContactPersonGender { get; set; }
        public string Owner { get; set; }
        [Display(Name = "Attractive Policies")]
        public string AttractivePolicies { get; set; }
        public bool? ActiveStatus { get; set; }
        [Display(Name = "Actived Date")]
        public DateTime? ActiveDate { get; set; }
        [Display(Name = "Company Size")]
        public string CompanySize { get; set; }
    }

}

namespace MIC.AseanTalentSearch.Web.EF
{
    [ModelMetadataType(typeof(CompanyMetadata))]
    public partial class Company
    {
    }
}