﻿using System;
using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.Validation;

namespace MIC.AseanTalentSearch.Web.Validation
{
    public class CompanyMediaMetadata
    {
        [Required]
        public string Link { get; set; }
    }

}

namespace MIC.AseanTalentSearch.Web.EF
{
    [ModelMetadataType(typeof(CompanyMediaMetadata))]
    public partial class CompanyMedia
    {
    }
}