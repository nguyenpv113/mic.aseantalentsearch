﻿using MIC.AseanTalentSearch.Web.Validation;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Validation
{
    public class LocationMetadata
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string EnglishName { get; set; }
        [Display(Name = "Latitude")]
        public string Lat { get; set; }
        [Display(Name = "Longitude")]
        public string Long { get; set; }
    }
}
namespace MIC.AseanTalentSearch.Web.EF
{
    [ModelMetadataType(typeof(LocationMetadata))]
    public partial class Location
    {

    }
}
