﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.Validation;

namespace MIC.AseanTalentSearch.Web.Validation
{
    public class PositionJobMetadata
    {
        [Display(Name = "Job Type")]
        public int? JobTypeId { get; set; }

        [Required, MaxLength(250)]
        [Display(Name = "Title")]
        public string PositionTitle { get; set; }

        [MaxLength(250)]
        public string Code { get; set; }

        [DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        [Display(Name = "Open Date")]

        [Required]
        public DateTime? OpenDate { get; set; }

        [DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        [Display(Name = "Create Date")]
        public DateTime CreatedDate { get; set; }

        [DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        [Display(Name = "Close Date")]
        [Required]
        public DateTime? ClosedDate { get; set; }
        
        [Display(Name = "Education Level")]
        public int? EducationLevelId { get; set; }

        [Display(Name = "Job Level")]
        public int? JobLevelId { get; set; }

        [Display(Name = "Max Salary")]
        [DataType(DataType.Currency), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n0}")]
        public decimal? MaxPay { get; set; }

        [Display(Name = "Min Salary")]
        [DataType(DataType.Currency), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n0}")]
        public decimal? MinPay { get; set; }
        
        [Required]
        [Display(Name = "Job Description")]
        public string JobDescription { get; set; }

        [Display(Name = "Exp.Years Requirement")]
        public double? YearsOfExperienceRequirement { get; set; }

        [Display(Name = "Skill Requirement")]
        public string SkillRequirement { get; set; }

        [Display(Name = "Responsibilities")]
        public string Responsibilities { get; set; }

        [Display(Name = "Travel Requirement")]
        public bool? TravelRequired { get; set; }

        [Display(Name = "Quantity")]
        [Required]
        public int? Quantity { get; set; }
        [DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        [Display(Name = "Modified Date")]
        public DateTime ModifiedDate { get; set; }

    }
}

namespace MIC.AseanTalentSearch.Web.EF
{
    [ModelMetadataType(typeof(PositionJobMetadata))]
    public partial class PositionJob
    {
    }
}