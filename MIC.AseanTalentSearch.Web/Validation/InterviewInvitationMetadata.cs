﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.Validation;

namespace MIC.AseanTalentSearch.Web.Validation
{
    public class InterviewInvitationMetadata
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required, Display(Name = "Letter Content")]
        public string LetterContent { get; set; }

        [Display(Name = "Candidate")]
        public long CandidateId { get; set; }

        [Display(Name = "Company")]
        public long CompanyId { get; set; }

        [Display(Name = "Position Job")]
        public int PositionJobId { get; set; }

        [Display(Name = "Send Email")]
        public bool SendToEmailCandidate { get; set; }

        [Phone]
        public string ContactPhone { get; set; }
        
        [EmailAddress]
        public string ContactEmail { get; set; }
        
        [MaxLength(250)]
        public string ContactPersonName { get; set; }
    }
}

namespace MIC.AseanTalentSearch.Web.EF
{
    [ModelMetadataType(typeof(InterviewInvitationMetadata))]
    public partial class InterviewInvitation
    {
    }
}