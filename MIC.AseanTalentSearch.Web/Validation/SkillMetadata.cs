﻿using MIC.AseanTalentSearch.Web.Validation;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Validation
{
    public class SkillMetadata
    {
        [Required]
        public string Name { get; set; }

        public string Comment { get; set; }
    }
}
namespace MIC.AseanTalentSearch.Web.EF
{
    [ModelMetadataType(typeof(SkillMetadata))]
    public partial class Skill
    {

    }
}
