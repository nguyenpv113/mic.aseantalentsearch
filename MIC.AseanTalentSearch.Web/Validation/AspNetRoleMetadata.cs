﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.Validation;

namespace MIC.AseanTalentSearch.Web.Validation
{
    public class AspNetRoleMetadata
    {
        public long Id { get; set; }
        public string ConcurrencyStamp { get; set; }
        [Required]
        public string Name { get; set; }
        public string NormalizedName { get; set; }
    }
}

namespace MIC.AseanTalentSearch.Web.EF
{
    [ModelMetadataType(typeof(AspNetRoleMetadata))]
    public partial class AspNetRoles
    {
    }
}