﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.ViewModels
{
    public class CandidateSearchViewModel
    {
        [Display(Name = "Job Categories")]
        public IEnumerable<CategoryFilter> CategoryFilters { get; set; }

        public int[] categoryFilter { get; set; }
        public int[] skillFilter { get; set; }
        public int[] joblevelFilter { get; set; }
        public int[] languageFilter { get; set; }


        [Display(Name = "Location")]
        public int? NationId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        [Display(Name = "Skills")]
        public IEnumerable<SkillFilter> SkillFilters { get; set; }
        [Display(Name = "Job Level")]
        public IEnumerable<JobLevelFilter> JobLevelFilters { get; set; }
        public int? MinExperienceYear { get; set; }
        [Display(Name = "Langagues")]
        public IEnumerable<LanguageFilter> LanguageFilters { get; set; }
        [Display(Name = "Salary")]
        public decimal? MinPay { get; set; }
        [Display(Name = "Salary")]
        public decimal? MaxPay { get; set; }
    }
    public class CategoryFilter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }
    public class SkillFilter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }
    public class JobLevelFilter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }

    public class LanguageFilter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }
    
}
