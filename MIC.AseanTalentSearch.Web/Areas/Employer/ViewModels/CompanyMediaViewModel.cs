﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.ViewModels
{
    public class CompanyMediaViewModel
    {
        public long CompanyId { get; set; }

        [Required, MaxLength(250)]
        public string Link { get; set; }

        public bool Type { get; set; }

        [Display(Name = "Active Status")]
        public bool ActiveStatus { get; set; }

        [Display(Name = "Active Date"), DataType(DataType.Date),DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime? ActiveDate { get; set; }

        [Display(Name = "Close Date"), DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime? ClosedDate { get; set; }

        public string ActiveBy { get; set; }

        [DataType(DataType.Date),DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime? ModifiesDate { get; set; }

        public string ModifiesBy { get; set; }

        public virtual Company Company { get; set; }    
    }
}