﻿// ***********************************************************************
// Assembly         : 
// Author           : Employeristrator
// Created          : 05-06-2017
//
// Last Modified By : Employeristrator
// Last Modified On : 05-06-2017
// ***********************************************************************
// <copyright file="CompaniesController.cs" company="MIC.AseanTalentSearch.Web">
//     Copyright (c) Microsoft. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Settings;
using MIC.AseanTalentSearch.Web.Services;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.Controllers
{
    /// <summary>
    /// Class CompaniesController.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Area("Employer")]
    [Authorize(Roles = "Employer")]
    public class CompaniesController : Controller
    {
        /// <summary>
        /// The context
        /// </summary>
        private readonly AseanTalentSearchContext _context;
        private readonly PathUpLoad _pathUpLoad;
        private readonly IHostingEnvironment _env;
        private readonly IImageService _imageService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CompaniesController"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public CompaniesController(AseanTalentSearchContext context, IOptions<PathUpLoad> pathUpload, IHostingEnvironment env, IImageService imageService)
        {
            _context = context;
            _env = env;
            _pathUpLoad = pathUpload.Value;
            _imageService = imageService;
        }

        // GET: Employer/Companies
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns>Task&lt;IActionResult&gt;.</returns>
        public async Task<IActionResult> Index()
        {
            var aseanTalentSearchContext = _context.Company.Include(c => c.IdNavigation).Include(c => c.Location);
            return View(await aseanTalentSearchContext.ToListAsync());
        }

        // GET: Employer/Companies/Details/5
        /// <summary>
        /// Detailses the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;IActionResult&gt;.</returns>
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var company = await _context.Company
                .Include(c => c.IdNavigation)
                .Include(c => c.Location)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (company == null)
            {
                return NotFound();
            }

            return View(company);
        }

        // GET: Employer/Companies/Create
        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns>IActionResult.</returns>
        public async Task<IActionResult> Create()
        {
            var locations = await _context.Location.Where(m => m.Type == 1 || m.Type == 2).OrderBy(m => m.Name).ThenBy(m => m.EnglishName).Select(m => new
            {
                m.Id,
                m.Name,
                m.EnglishName,
                m.ParentId,
                m.Type
            }).OrderBy(m => m.Name).ToListAsync();
            ViewData["Locations"] = locations;
            ViewData["NationId"] = new SelectList(locations.Where(m => m.Type == 1).OrderBy(m => m.Name).ToList(), "Id", "Name");
            return View();
        }

        // POST: Employer/Companies/Create
        /// <summary>
        /// Creates the specified company.
        /// </summary>
        /// <param name="company">The company.</param>
        /// <returns>Task&lt;IActionResult&gt;.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Company company)
        {
            if (ModelState.IsValid)
            {
                company.Address = string.IsNullOrEmpty(company.Address) ? "" : company.Address;
                company.Guid = Guid.NewGuid();
                _context.Company.Add(company);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Home", new { @id = company.Id });
            }
            ViewData["LocationId"] = new SelectList(_context.Location, "Id", "Name", company.LocationId);
            return View(company);
        }

        // GET: Employer/Companies/Edit/5
        /// <summary>
        /// Edits the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;IActionResult&gt;.</returns>
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var company = await _context.Company.FindAsync(id);

            if (company == null)
            {
                return NotFound();
            }
            var locations = await _context.Location.Where(m => m.Type == 1 || m.Type == 2).OrderBy(m => m.Name).ThenBy(m => m.EnglishName).Select(m => new
            {
                m.Id,
                m.Name,
                m.EnglishName,
                m.ParentId,
                m.Type
            }).OrderBy(m => m.Name).ToListAsync();
            ViewData["Locations"] = locations;
            ViewData["NationId"] = new SelectList(locations.Where(m => m.Type == 1).OrderBy(m => m.Name).ToList(), "Id", "Name");
            var parentId = _context.Location.Where(m => m.Id == company.LocationId).FirstOrDefault().ParentId;
            ViewData["Country"] = new SelectList(_context.Location.Where(m => m.Type == 1), "Id", "Name", FindLocationByChildId(parentId.Value));
            return View(company);
        }

        public Location FindLocationByChildId(int childId)
        {
            return _context.Location.Where(m => m.Id == childId).FirstOrDefault();
        }

        // POST: Employer/Companies/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, Company company)
        {
            if (id != company.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    var companyOld = await _context.Company.FindAsync(id);
                    _context.Entry(companyOld).State = EntityState.Detached;
                    
                    company.ActiveStatus = companyOld.ActiveStatus;
                    company.Guid = companyOld.Guid;
                    _context.Update(company);
                    await _context.SaveChangesAsync();
                    ModelState.AddModelError("Success", "Updated successful !");
                    return Ok(ModelState);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompanyExists(company.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        ModelState.AddModelError("Error", "Internal server error ..!");
                    }
                }
            }
            return BadRequest(ModelState);
        }

        // GET: Employer/Companies/Delete/5
        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;IActionResult&gt;.</returns>
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var company = await _context.Company
                .Include(c => c.IdNavigation)
                .Include(c => c.Location)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (company == null)
            {
                return NotFound();
            }

            return View(company);
        }

        // POST: Employer/Companies/Delete/5
        /// <summary>
        /// Deletes the confirmed.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;IActionResult&gt;.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var company = await _context.Company.SingleOrDefaultAsync(m => m.Id == id);
            _context.Company.Remove(company);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Companies the exists.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        private bool CompanyExists(long id)
        {
            return _context.Company.Any(e => e.Id == id);
        }

        public bool Validate(IFormFile file)
        {
            string fileExtentison = System.IO.Path.GetExtension(file.FileName).ToLower();
            string[] allowedFileType = {".gif", ".png", ".jpeg", ".jpg"};
            if (file.Length > 0 && file.Length < 2097152 && allowedFileType.Contains(fileExtentison) &&
                file.ContentType.Contains("image"))
            {
                return true;
            }
            return false;
        }
//        public bool UpdateLogo(long? id)
//        {
//            string fileExtentison = System.IO.Path.GetExtension(file.FileName).ToLower();
//            string[] allowedFileType = { ".gif", ".png", ".jpeg", ".jpg" };
//            if (file.Length > 0 && file.Length < 2097152 && allowedFileType.Contains(fileExtentison) &&
//                file.ContentType.Contains("image"))
//            {
//                return true;
//            }
//            return false;
//        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateLogo(long? id, IFormFile logoUploader)
        {
            if (id == null)
            {
                return NotFound();
            }

            var company = await _context.Company.FindAsync(id);
            if (company == null)
            {
                return NotFound();
            }

            string oldLogo = company.Logo ?? string.Empty;

            if (logoUploader != null && logoUploader.Length > 0)
            {
                if (Validate(logoUploader))
                {
                    try
                    {
                        var uploads = _env.WebRootPath.Replace("\\", "/") + _pathUpLoad.FrontImage;
                        if (!System.IO.Directory.Exists(uploads)) { System.IO.Directory.CreateDirectory(uploads); }
                        var extension = Path.GetExtension(logoUploader.FileName);
                        var fileName = Path.GetFileNameWithoutExtension(logoUploader.FileName.ToLowerInvariant()) + Guid.NewGuid() + extension;
                        using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                        {
                            await logoUploader.CopyToAsync(fileStream);
                            fileStream.Flush();
                        }
                        company.Logo = Path.Combine(_pathUpLoad.FrontImage, fileName);
                        _context.Company.Update(company);
                        await _context.SaveChangesAsync();
                        ModelState.AddModelError("Success", "Updated successful");
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("Error", "Sorry an error occurred saving to the database, please try again");
                    }
                    if (!string.IsNullOrEmpty(oldLogo) && System.IO.File.Exists(_env.WebRootPath.Replace("\\", "/") + oldLogo))
                    {
                        try
                        {
                            System.IO.File.Delete(Path.Combine(_env.WebRootPath.Replace("\\", "/") + oldLogo));
                        }
                        catch (Exception)
                        {
                            ModelState.AddModelError("Error", "Your old logo not remain..");
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("Error", "The file must be gif, png, jpeg or jpg and less than 2MB in size");
                }
            }
            else
            {
                ModelState.AddModelError("Error", "Please choose a file");
            }
            return View("Partial/_CompanyLogoPartial", company);
        }

        [HttpPost]
        public async Task<IActionResult> UploadFilesAjax(long? id)
        {
            var company = await _context.Company.FindAsync(id);
            if (company == null)
            {
                return NotFound();
            }

            string oldLogo = company.Logo ?? string.Empty;
            string imagePath = String.Empty;
            var file = Request.Form.Files[0];
            if (file != null)
            {
                if (_imageService.Validate(file))
                {
                    try
                    {
                        imagePath = _pathUpLoad.FrontImage + await _imageService.SaveAsync(file, _pathUpLoad.FrontImage);
                    }
                    catch (Exception e)
                    {
                        TempData["error"] = "Sorry an error occurred saving to the database, please try again";
                        ModelState.AddModelError("",
                            "Sorry an error occurred saving to the database, please try again");
                    }
                }
                else
                {
                    TempData["error"] = "The file must be gif, png, jpeg or jpg and less than 2MB in size";
                    ModelState.AddModelError("", "The file must be gif, png, jpeg or jpg and less than 2MB in size");
                }
            }
            else
            {
                TempData["error"] = "Please choose a file";
                ModelState.AddModelError("", "Please choose a file");
            }

            if (ModelState.IsValid)
            {
                
                    company.Logo = imagePath;
                    _context.Update(company);

                try
                {
                    await _context.SaveChangesAsync();
                    ModelState.AddModelError("Success", "Updated successful");
                }
                catch (Exception e)
                {
                    TempData["error"] = "Sorry an error occurred saving to the database, please try again";
                    ModelState.AddModelError("",
                        "Sorry an error occurred saving to the database, please try again");
                }
            }
            return Json(new { success = true });
        }
    }
}