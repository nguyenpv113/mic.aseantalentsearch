using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.Controllers
{
    [Area("Employer")]
    [Authorize(Roles = "Employer")]
    public class JobApplicationsController : Controller
    {
        private readonly AseanTalentSearchContext _context;

        public JobApplicationsController(AseanTalentSearchContext context)
        {
            _context = context;    
        }

        // GET: Employer/JobApplications
        public async Task<IActionResult> Index()
        {
            var aseanTalentSearchContext = _context.JobApplication.Include(j => j.Candidate).Include(j => j.PositionJob);
            return View(await aseanTalentSearchContext.ToListAsync());
        }

        // GET: Employer/JobApplications/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var jobApplication = await _context.JobApplication
                .Include(j => j.Candidate)
                .Include(j => j.PositionJob)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (jobApplication == null)
            {
                return NotFound();
            }

            return View(jobApplication);
        }

        // GET: Employer/JobApplications/Create
        public IActionResult Create()
        {
            ViewData["CandidateId"] = new SelectList(_context.Candidate, "Id", "Id");
            ViewData["PositionJobId"] = new SelectList(_context.PositionJob, "Id", "JobDescription");
            return View();
        }

        // POST: Employer/JobApplications/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(JobApplication jobApplication)
        {
            if (ModelState.IsValid)
            {
                _context.Add(jobApplication);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["CandidateId"] = new SelectList(_context.Candidate, "Id", "Id", jobApplication.CandidateId);
            ViewData["PositionJobId"] = new SelectList(_context.PositionJob, "Id", "JobDescription", jobApplication.PositionJobId);
            return View(jobApplication);
        }

        // GET: Employer/JobApplications/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var jobApplication = await _context.JobApplication.SingleOrDefaultAsync(m => m.Id == id);
            if (jobApplication == null)
            {
                return NotFound();
            }
            ViewData["CandidateId"] = new SelectList(_context.Candidate, "Id", "Id", jobApplication.CandidateId);
            ViewData["PositionJobId"] = new SelectList(_context.PositionJob, "Id", "JobDescription", jobApplication.PositionJobId);
            return View(jobApplication);
        }

        // POST: Employer/JobApplications/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,  JobApplication jobApplication)
        {
            if (id != jobApplication.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(jobApplication);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!JobApplicationExists(jobApplication.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["CandidateId"] = new SelectList(_context.Candidate, "Id", "Id", jobApplication.CandidateId);
            ViewData["PositionJobId"] = new SelectList(_context.PositionJob, "Id", "JobDescription", jobApplication.PositionJobId);
            return View(jobApplication);
        }

        // GET: Employer/JobApplications/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var jobApplication = await _context.JobApplication
                .Include(j => j.Candidate)
                .Include(j => j.PositionJob)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (jobApplication == null)
            {
                return NotFound();
            }

            return View(jobApplication);
        }

        // POST: Employer/JobApplications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var jobApplication = await _context.JobApplication.SingleOrDefaultAsync(m => m.Id == id);
            _context.JobApplication.Remove(jobApplication);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool JobApplicationExists(int id)
        {
            return _context.JobApplication.Any(e => e.Id == id);
        }
    }
}
