using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Areas.Employer.ViewModels;
using Sakura.AspNetCore;
using System.Collections.Generic;
using System.Security.Claims;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.Controllers
{
    [Area("Employer")]
    [Authorize(Roles = "Employer")]
    public class CandidatesController : Controller
    {
        private readonly AseanTalentSearchContext _context;
        public static List<Candidate> _currentList;
        public CandidatesController(AseanTalentSearchContext context)
        {
            _context = context;
        }

        // GET: Employer/Candidates
        /*public async Task<IActionResult> Index()
        {
            var aseanTalentSearchContext = _context.Candidate.Include(c => c.ExpectedJobLevel).Include(c => c.IdNavigation).Include(c => c.Layout);
            return View(await aseanTalentSearchContext.ToListAsync());
        }

        // GET: Employer/Candidates/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidate = await _context.Candidate
                .Include(c => c.ExpectedJobLevel)
                .Include(c => c.IdNavigation)
                .Include(c => c.Layout)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (candidate == null)
            {
                return NotFound();
            }

            return View(candidate);
        }

        // GET: Employer/Candidates/Create
        public IActionResult Create()
        {
            ViewData["ExpectedJobLevelId"] = new SelectList(_context.JobLevel, "JobLevelId", "JobLevelName");
            ViewData["Id"] = new SelectList(_context.AspNetUsers, "Id", "Id");
            ViewData["LayoutId"] = new SelectList(_context.Layout, "Id", "Title");
            return View();
        }

        // POST: Employer/Candidates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,LayoutId,FullName,Address,Email,PhoneNumber,Facebook,Instagram,LinkedIn,Twitter,Image,Summary,ExpectedCategory,ExpectedSalary,ExpectedJobLevelId,WillingToRelocate,YearsOfExperience,Dtustudent,DtugraduatedYear,DtustudentCode,Cvlink,IsPublish,CreatedDate,ActiveStatus,ActiveDate,ActiveBy,ModifiedBy,ModifiedDate")] Candidate candidate)
        {
            if (ModelState.IsValid)
            {
                _context.Add(candidate);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["ExpectedJobLevelId"] = new SelectList(_context.JobLevel, "JobLevelId", "JobLevelName", candidate.ExpectedJobLevelId);
            ViewData["Id"] = new SelectList(_context.AspNetUsers, "Id", "Id", candidate.Id);
            ViewData["LayoutId"] = new SelectList(_context.Layout, "Id", "Title", candidate.LayoutId);
            return View(candidate);
        }

        // GET: Employer/Candidates/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidate = await _context.Candidate.SingleOrDefaultAsync(m => m.Id == id);
            if (candidate == null)
            {
                return NotFound();
            }
            ViewData["ExpectedJobLevelId"] = new SelectList(_context.JobLevel, "JobLevelId", "JobLevelName", candidate.ExpectedJobLevelId);
            ViewData["Id"] = new SelectList(_context.AspNetUsers, "Id", "Id", candidate.Id);
            ViewData["LayoutId"] = new SelectList(_context.Layout, "Id", "Title", candidate.LayoutId);
            return View(candidate);
        }

        // POST: Employer/Candidates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,LayoutId,FullName,Address,Email,PhoneNumber,Facebook,Instagram,LinkedIn,Twitter,Image,Summary,ExpectedCategory,ExpectedSalary,ExpectedJobLevelId,WillingToRelocate,YearsOfExperience,Dtustudent,DtugraduatedYear,DtustudentCode,Cvlink,IsPublish,CreatedDate,ActiveStatus,ActiveDate,ActiveBy,ModifiedBy,ModifiedDate")] Candidate candidate)
        {
            if (id != candidate.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(candidate);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CandidateExists(candidate.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["ExpectedJobLevelId"] = new SelectList(_context.JobLevel, "JobLevelId", "JobLevelName", candidate.ExpectedJobLevelId);
            ViewData["Id"] = new SelectList(_context.AspNetUsers, "Id", "Id", candidate.Id);
            ViewData["LayoutId"] = new SelectList(_context.Layout, "Id", "Title", candidate.LayoutId);
            return View(candidate);
        }

        // GET: Employer/Candidates/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidate = await _context.Candidate
                .Include(c => c.ExpectedJobLevel)
                .Include(c => c.IdNavigation)
                .Include(c => c.Layout)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (candidate == null)
            {
                return NotFound();
            }

            return View(candidate);
        }

        // POST: Employer/Candidates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var candidate = await _context.Candidate.SingleOrDefaultAsync(m => m.Id == id);
            _context.Candidate.Remove(candidate);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool CandidateExists(long id)
        {
            return _context.Candidate.Any(e => e.Id == id);
        }

          

*/


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Filter(CandidateSearchViewModel model, int[] CategoryFilters, int[] SkillFilters, 
            int[] JobLevelFilters, int[] LanguageFilters, int? page = 1)
        {
            int pageSize = 21;
            var query = _context.Candidate
                .Where(m => (m.IsPublish ?? false) && (m.ActiveStatus ?? false))
                .Include(m => m.ExpectedJobLevel)
                .Include(m => m.CandidateCategory).ThenInclude(r => r.Category)
                .Include(m => m.CandidateSkill).ThenInclude(r => r.Skill)
                .Include(m => m.CandidateLanguage).ThenInclude(r => r.Language)
                .Include(m => m.CandidateLocation).ThenInclude(r => r.Location)
                .AsQueryable();
            // Filter by job category
            if (CategoryFilters.Length>0)
            {
                var categoryIds = CategoryFilters;
                if (categoryIds.Any())
                {
                    query = query.Where(m => categoryIds.Any(r => r == m.ExpectedCategory) || m.CandidateCategory.Any(u => categoryIds.Contains(u.CategoryId)));
                }
            }
            // Filter by skill
            var skillIds = SkillFilters;
            if (skillIds.Any())
            {
                query = query.Where(m => m.CandidateSkill.Any(u => skillIds.Contains(u.SkillId)));
            }
            // Filter by job level
            var jobLevelIds = JobLevelFilters;
            if (jobLevelIds.Any())
            {
                query = query.Where(m => jobLevelIds.Any(r => r == m.ExpectedJobLevelId));
            }
            // Filter by langauge
            var languageIds = LanguageFilters;
            if (languageIds.Any())
            {
                query = query.Where(m => m.CandidateLanguage.Any(u => languageIds.Contains(u.LanguageId)));
            }
            // Filter by salary
            if (model.MaxPay != null)
            {
                query = query.Where(m => m.ExpectedSalary <= model.MaxPay);
            }
            if (model.MinPay != null)
            {
                query = query.Where(m => m.ExpectedSalary >= model.MinPay);
            }

            // Filter by experience years
            if (model.MinExperienceYear != null)
            {
                query = query.Where(m => m.YearsOfExperience >= model.MinExperienceYear);
            }
            if (model.MinPay != null)
            {
                query = query.Where(m => m.ExpectedSalary >= model.MinPay);
            }

            // Filter by location
            //Nullable<int> locationId = model.DistrictId ?? model.ProvinceId ?? model.NationId ?? null;

            Nullable<int> locationId = model.ProvinceId ?? model.NationId ?? null;
            if (model.ProvinceId == null)
            {//filter by nation

                query = query.Where(m => locationId == null ||m.CandidateLocation.Any(x=>x.Location.ParentId==locationId));
            }
            else
            {              
                //filter by province
                query = query.Where(m => locationId == null || m.CandidateLocation.Any(r => r.LocationId == locationId));
            }




            var candidates = await query.ToListAsync();
            _currentList = candidates;
            ViewBag.Count = candidates.Count;
            var modelresual = candidates.OrderBy(m => m.FullName).ToPagedList(pageSize, page.Value);
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            long? companyId = long.Parse(userId);
            if (companyId == null)
            {
                return NotFound();
            }
            if (await _context.Company.FindAsync(companyId) == null)
            {
                return NotFound();
            }
            ViewBag.ListSaved = (from savedcandidate in _context.SaveCandidate
                                where savedcandidate.CompanyId == companyId
                                select savedcandidate.CandidateId).ToList();
            return View("FilterIndex", modelresual);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> FilterMore(CandidateSearchViewModel model, int? page = 1)
        {
            int pageSize = 21;
            var query = _context.Candidate
                .Where(m => (m.IsPublish ?? false) && (m.ActiveStatus ?? false))
                .Include(m => m.ExpectedJobLevel)
                .Include(m => m.CandidateCategory).ThenInclude(r => r.Category)
                .Include(m => m.CandidateSkill).ThenInclude(r => r.Skill)
                .Include(m => m.CandidateLanguage).ThenInclude(r => r.Language)
                .Include(m => m.CandidateLocation).ThenInclude(r => r.Location)
                .AsQueryable();
            // Filter by job category
            var categoryIds = model.CategoryFilters.Where(m => m.Selected).Select(m => m.Id).ToList();
            if (categoryIds.Any())
            {
                query = query.Where(m => categoryIds.Any(r => r == m.ExpectedCategory) || m.CandidateCategory.Any(u => categoryIds.Contains(u.CategoryId)));
            }
            // Filter by skill
            var skillIds = model.SkillFilters.Where(m => m.Selected).Select(m => m.Id).ToList();
            if (skillIds.Any())
            {
                query = query.Where(m => m.CandidateSkill.Any(u => skillIds.Contains(u.SkillId)));
            }
            // Filter by job level
            var jobLevelIds = model.JobLevelFilters.Where(m => m.Selected).Select(m => m.Id).ToList();
            if (jobLevelIds.Any())
            {
                query = query.Where(m => jobLevelIds.Any(r => r == m.ExpectedJobLevelId));
            }
            // Filter by langauge
            var languageIds = model.LanguageFilters.Where(m => m.Selected).Select(m => m.Id).ToList();
            if (languageIds.Any())
            {
                query = query.Where(m => m.CandidateLanguage.Any(u => languageIds.Contains(u.LanguageId)));
            }
            // Filter by salary
            if (model.MaxPay != null)
            {
                query = query.Where(m => m.ExpectedSalary <= model.MaxPay);
            }
            if (model.MinPay != null)
            {
                query = query.Where(m => m.ExpectedSalary >= model.MinPay);
            }

            // Filter by experience years
            if (model.MinExperienceYear != null)
            {
                query = query.Where(m => m.YearsOfExperience >= model.MinExperienceYear);
            }
            if (model.MinPay != null)
            {
                query = query.Where(m => m.ExpectedSalary >= model.MinPay);
            }

            // Filter by location
            //Nullable<int> locationId = model.DistrictId ?? model.ProvinceId ?? model.NationId ?? null;
            //query = query.Where(m => locationId == null || m.CandidateLocation.Any(r => r.LocationId == locationId));
            Nullable<int> locationId = model.ProvinceId ?? model.NationId ?? null;
            if (model.ProvinceId == null)
            {//filter by nation

                query = query.Where(m => locationId == null || m.CandidateLocation.Any(x => x.Location.ParentId == locationId));
            }
            else
            {
                //filter by province
                query = query.Where(m => locationId == null || m.CandidateLocation.Any(r => r.LocationId == locationId));
            }
            

            var candidates = await query.ToListAsync();
            _currentList = candidates;
            ViewBag.Count = candidates.Count;
            var modelresual = candidates.OrderBy(m => m.FullName).ToPagedList(pageSize, page.Value);
            var modelSaveCandidate = _context.SaveCandidate.Include(x => x.Candidate).ToList();
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            long? companyId = long.Parse(userId);
            if (companyId == null)
            {
                return NotFound();
            }
            if (await _context.Company.FindAsync(companyId) == null)
            {
                return NotFound();
            }
            ViewBag.ListSaved = from savedcandidate in _context.SaveCandidate
                                where savedcandidate.CompanyId == companyId
                                select savedcandidate.CandidateId;
            return View("FilterIndex", modelresual);
        }

        [HttpPost]
        public async Task<JsonResult> SaveCandidate(long id)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            long? companyId = long.Parse(userId);
            if (companyId == null)
            {
                return Json(false);
            }
            if (await _context.Company.FindAsync(companyId) == null)
            {
                return Json(false);
            }

            _context.SaveCandidate.Add(new EF.SaveCandidate
            {
                CandidateId = id,
                CompanyId = companyId.Value,
                CreatedDate = DateTime.Now
            });
            await _context.SaveChangesAsync();

            return Json(true);
        }

        public async Task<IActionResult> DeleteSaved(long id)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            long? companyId = long.Parse(userId);
            if (companyId == null)
            {
                return NotFound();
            }
            if (await _context.Company.FindAsync(companyId) == null)
            {
                return NotFound();
            }
            var saved = _context.SaveCandidate.Where(x => x.CandidateId == id && x.CompanyId == companyId).FirstOrDefault();
            if (saved != null)
            {
                _context.SaveCandidate.Remove(saved);
                _context.SaveChanges();
            }
            return RedirectToAction("CandidateSaved", "Home");
        }
    }
}
