// ***********************************************************************
// Assembly         : 
// Author           : Employeristrator
// Created          : 05-06-2017
//
// Last Modified By : Employeristrator
// Last Modified On : 05-06-2017
// ***********************************************************************
// <copyright file="CompaniesController.cs" CompanyMediaViewModel="MIC.AseanTalentSearch.Web">
//     Copyright (c) Microsoft. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.Controllers
{
    /// <summary>
    /// Class CompaniesController.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Area("Employer")]
    [Authorize(Roles = "Employer")]
    public class CompanyMediasController : Controller
    {
        private readonly AseanTalentSearchContext _context;

        public CompanyMediasController(AseanTalentSearchContext context)
        {
            _context = context;
        }

        // GET: Employer/CompanieMedias
        public async Task<IActionResult> Index(long companyId)
        {
            if (await _context.Company.FindAsync(companyId) == null)
            {
                return NotFound();
            }
            ViewData["CompanyId"] = companyId;
            return View(await _context.CompanyMedia.Where(m => m.CompanyId == companyId).OrderBy(m => m.Link).ToListAsync());
        }

        // GET: Employer/CompanieMedias/Create
        public async Task<IActionResult> Create(long companyId)
        {
            if (await _context.Company.FindAsync(companyId) == null)
            {
                return NotFound();
            }
            return View(new CompanyMedia() {CompanyId = companyId});
        }

        // POST: Employer/CompanieMedias/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CompanyMedia companyMedia)
        {
            if (ModelState.IsValid)
            {
                if (companyMedia.ActiveDate != null && companyMedia.ClosedDate != null &&
                    companyMedia.ClosedDate < companyMedia.ActiveDate)
                {
                    ModelState.TryAddModelError("ClosedDate", "ClosedDate is small than ActiveDate !");
                }
                else
                {
                    _context.Add(companyMedia);
                    await _context.SaveChangesAsync();

                    ModelState.AddModelError("Success", "Created successful !");
                    ViewData["CompanyId"] = companyMedia.CompanyId;
                    return View("Index", await _context.CompanyMedia.Where(m => m.CompanyId == companyMedia.CompanyId).ToListAsync());
                }
            }
            return BadRequest(ModelState);
        }

        // GET: Employer/CompanieMedias/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyMedia = await _context.CompanyMedia.FindAsync(id);

            if (companyMedia == null)
            {
                return NotFound();
            }
            return View(companyMedia);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, CompanyMedia companyMedia)
        {
            if (id != companyMedia.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (companyMedia.ActiveDate != null && companyMedia.ClosedDate != null && companyMedia.ClosedDate < companyMedia.ActiveDate)
                    {
                        ModelState.TryAddModelError("ClosedDate", "ClosedDate is small than ActiveDate !");
                    }
                    else
                    {
                        _context.Update(companyMedia);
                        await _context.SaveChangesAsync();

                        ModelState.AddModelError("Success", "Updated successful !");
                        return Ok(ModelState);
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompanyMediaExists(companyMedia.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        ModelState.AddModelError("Error", "Internal server error ..!");
                    }
                }
            }
            return BadRequest(ModelState);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyMedia = await _context.CompanyMedia.FindAsync(id);

            if (companyMedia == null)
            {
                return NotFound();
            }
            return View(companyMedia);

        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var companyMedia = await _context.CompanyMedia.SingleOrDefaultAsync(m => m.Id == id);
            _context.CompanyMedia.Remove(companyMedia);
            await _context.SaveChangesAsync();

            ModelState.AddModelError("Success", "Deleted successful !");
            ViewData["CompanyId"] = companyMedia.CompanyId;
            return View("Index", await _context.CompanyMedia.Where(m => m.CompanyId == companyMedia.CompanyId).ToListAsync());
        }

        private bool CompanyMediaExists(int id)
        {
            return _context.CompanyMedia.Any(e => e.Id == id);
        }
    }
}