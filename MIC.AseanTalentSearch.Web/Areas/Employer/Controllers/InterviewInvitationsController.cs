using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Services;
using Microsoft.AspNetCore.Identity;
using MIC.AseanTalentSearch.Web.Models;
using System.Security.Claims;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.Controllers
{
    [Area("Employer")]
    [Authorize(Roles = "Employer")]
    public class InterviewInvitationsController : Controller
    {
        private readonly AseanTalentSearchContext _context;
        private readonly IEmailSender _emailSender;

        public InterviewInvitationsController(AseanTalentSearchContext context, IEmailSender emailSender)
        {
            _context = context;
            _emailSender = emailSender;
        }

        // GET: Employer/InterviewInvitations
        public async Task<IActionResult> Index(long? companyId)
        {
            if (await _context.Company.FindAsync(companyId) == null)
            {
                return NotFound();
            }
            var letters = await _context.InterviewInvitation
                .Where(m => m.CompanyId == companyId)
                .Include(m => m.Candidate)
                .OrderByDescending(m => m.ActiveDate)
                .ThenByDescending(m => m.ModifiedDate)
                .AsNoTracking()
                .ToListAsync();
            return View("~/Areas/Employer/Views/Shared/Components/_InterviewInvitationLetter.cshtml", letters);
        }

        // GET: Employer/InterviewInvitations/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var interviewInvitation = await _context.InterviewInvitation
                .Include(i => i.Candidate)
                .Include(i => i.Company)
                .Include(i => i.PositionJob)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (interviewInvitation == null)
            {
                return NotFound();
            }

            return View(interviewInvitation);
        }

        // GET: Employer/InterviewInvitations/Create/5
        public async Task<IActionResult> Create(long? candidateId, long? companyId, int? positionJobId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var applyJob = _context.JobApplication.Include(x => x.PositionJob).Where(x => x.CandidateId == candidateId && x.PositionJobId == positionJobId && x.PositionJob.CompanyId == companyId).FirstOrDefault();

            if (applyJob == null || long.Parse(userId) != companyId)
            {
                return NotFound();
            }

            var candidate = await _context.Candidate.FindAsync(candidateId);
            var company = await _context.Company.FindAsync(companyId);
            var positionJob = await _context.PositionJob.FindAsync(positionJobId);

            if (candidate == null || company == null || positionJob == null)
            {
                return NotFound();
            }
            var invitation = new InterviewInvitation();
            invitation.CompanyId = company.Id;
            invitation.Company = company;
            invitation.CandidateId = candidate.Id;
            invitation.Candidate = candidate;
            invitation.PositionJobId = positionJob.Id;
            invitation.PositionJob = positionJob;
            invitation.ContactPhone = company.ContactPhone;
            invitation.ContactEmail = company.Email;
            invitation.ContactPersonName = company.ContactPerson;

            return View(invitation);
        }

        // POST: Employer/InterviewInvitations/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(InterviewInvitation interviewInvitation, [FromServices] IEmailSender emailSender)
        {
            if (ModelState.IsValid)
            {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var applyJob = _context.JobApplication.Include(x => x.PositionJob).Where(x => x.CandidateId == interviewInvitation.CandidateId && x.PositionJobId == interviewInvitation.PositionJobId && x.PositionJob.CompanyId == interviewInvitation.CompanyId).FirstOrDefault();

                if (applyJob == null || long.Parse(userId) != interviewInvitation.CompanyId)
                {
                    return NotFound();
                }

                if (interviewInvitation.SendToEmailCandidate)
                    {

                        string SenderName = Request.Form["SenderName"].FirstOrDefault();
                        string RecieverEmail = Request.Form["RecieverEmail"].FirstOrDefault();

                        var body = "<h2></h2>";
                        body += "<h4>" + interviewInvitation.Title + "</h4>";
                        body += "<p>" + interviewInvitation.LetterContent + "</p>";
                        body += "<p>Contact Information :</p>";
                        body += "<b>" + interviewInvitation.ContactPersonName + "</b><br/>";
                        body += "<b>" + interviewInvitation.ContactPhone + "</b><br/>";
                        body += "<b>" + interviewInvitation.ContactEmail + "</b><br/>";

                        if (!string.IsNullOrEmpty(RecieverEmail))
                        {
                            try
                            {
                                await emailSender.SendEmailAsync(RecieverEmail, interviewInvitation.Title, body);
                            }
                            catch (System.Exception)
                            {
                                ModelState.TryAddModelError("Error", "Can not sent email ..!");
                            }
                        }
                    }
                _context.Add(interviewInvitation);
                await _context.SaveChangesAsync();
                return RedirectToAction("CandidateSubmit", "Home", new
                {
                    companyId = interviewInvitation.CompanyId
                });
            }
            return View(interviewInvitation);
        }

        // GET: Employer/InterviewInvitations/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);


            var interviewInvitation = await _context.InterviewInvitation
                .Include(m => m.Candidate)
                .Include(m => m.Company)
                .Include(m => m.PositionJob)
                .SingleOrDefaultAsync(m => m.Id == id && m.CompanyId == int.Parse(userId));
            if (interviewInvitation == null)
            {
                return NotFound();
            }
            return View(interviewInvitation);
        }

        // POST: Employer/InterviewInvitations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, InterviewInvitation interviewInvitation)
        {
            if (id != interviewInvitation.Id)
            {
                return NotFound();
            }



            if (ModelState.IsValid)
            {
                try
                {
                    var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                    var interviewUpdate = _context.InterviewInvitation.Where(x => x.Id == interviewInvitation.Id && x.CompanyId == int.Parse(userId)).FirstOrDefault();
                    if (interviewUpdate == null)
                    {
                        return NotFound();
                    }
                    interviewUpdate.Title = interviewInvitation.Title;
                    interviewUpdate.ContactPhone = interviewInvitation.ContactPhone;
                    interviewUpdate.ContactEmail = interviewInvitation.ContactEmail;
                    interviewUpdate.ContactPersonName = interviewInvitation.ContactPersonName;
                    interviewUpdate.LetterContent = interviewInvitation.LetterContent;

                    //_context.Update(interviewUpdate);
                    await _context.SaveChangesAsync();
                    ModelState.AddModelError("Success", "Updated successful !");
                    return Ok(ModelState);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InterviewInvitationExists(interviewInvitation.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        ModelState.AddModelError("Error", "Internal server error ..!");
                    }
                }
            }
            return BadRequest(ModelState);
        }

        // GET: Employer/InterviewInvitations/Delete/5
        public async Task<IActionResult> Delete(long? companyId)
        {

            if (companyId == null)
            {
                return NotFound();
            }

            var interviewInvitation = await _context.InterviewInvitation
                .Include(i => i.Candidate)
                .Include(i => i.Company)
                .Include(i => i.PositionJob)
                .SingleOrDefaultAsync(m => m.Id == companyId);
            if (interviewInvitation == null)
            {
                return NotFound();
            }

            return View(interviewInvitation);
        }

        // POST: Employer/InterviewInvitations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long? companyId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (companyId == null || companyId != long.Parse(userId))
            {
                return NotFound();
            }

            var company = await _context.Company.FindAsync(companyId);
            if (company == null)
            {
                return NotFound();
            }
            var interInvitationIds = Request.Form["InterviewInvitation"].ToList();

            foreach (var item in interInvitationIds)
            {
                var letter = await _context.InterviewInvitation.SingleOrDefaultAsync(m => m.Id == long.Parse(item));
                if (letter != null)
                {
                    _context.InterviewInvitation.Remove(letter);
                }
            }
            await _context.SaveChangesAsync();
            var letters = await _context.InterviewInvitation
                .Where(m => m.CompanyId == companyId)
                .Include(m => m.Candidate)
                .OrderByDescending(m => m.ActiveDate)
                .ThenByDescending(m => m.ModifiedDate)
                .AsNoTracking()
                .ToListAsync();
            return View("~/Areas/Employer/Views/Shared/Components/_InterviewInvitationLetter.cshtml", letters);
        }

        private bool InterviewInvitationExists(int id)
        {
            return _context.InterviewInvitation.Any(e => e.Id == id);
        }
    }
}