using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.Areas.Employer.ViewModels;
using MIC.AseanTalentSearch.Web.EF;
using Sakura.AspNetCore;
using System.Security.Claims;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.Controllers {
    [Area("Employer")]
    [Authorize(Roles = "Employer")]
    public class PositionJobsController: Controller {
        private readonly AseanTalentSearchContext _context;

        public PositionJobsController(AseanTalentSearchContext context) {
            _context = context;
        }

        // GET: Employer/PositionJobs
        public async Task <IActionResult> Index(int?page=1) {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            /*if (await _context.Company.FindAsync(long.Parse(userId)) == null) {
                return NotFound();
            }*/
            var company = await _context.Company.FindAsync(long.Parse(userId));
            ViewData["CompanyId"] = userId;
            var positionJobs = await _context.PositionJob
                .Where(p => p.CompanyId == long.Parse(userId))
                .Include(p => p.EducationLevel)
                .Include(p => p.JobApplication)
                .Include(p => p.JobLevel)
                .Include(p => p.JobType)
                .ToListAsync();
            if (!positionJobs.Any()) {
                return RedirectToAction("Create", new {
                    companyId = userId
                });
            }
            ViewBag.Count = positionJobs.Count;
            var model = positionJobs.OrderByDescending(m => m.OpenDate).ToList().ToPagedList(6, page.Value);
            return View(model);
        }

        public async Task <IActionResult> LastestPositionJob(long companyId) {
            if (await _context.Company.FindAsync(companyId) == null) {
                return NotFound();
            }
            ViewData["CompanyId"] = companyId;
            return View(await _context.PositionJob
                .Where(p => p.CompanyId == companyId)
                .Include(p => p.EducationLevel)
                .Include(p => p.JobApplication)
                .Include(p => p.JobLevel)
                .Include(p => p.JobType)
                .OrderByDescending(x => x.ModifiedDate)
                .ThenByDescending(x => x.CreatedDate)
                .Take(5)
                .ToListAsync());
        }

        // GET: Employer/PositionJobs/Create
        public async Task < IActionResult > Create(long companyId) {
            if (await _context.Company.FindAsync(companyId) == null) {
                return NotFound();
            }
            ViewData["EducationLevelId"] = new SelectList(await _context.EducationLevel.ToListAsync(), "Id", "Name");
            ViewData["JobLevelId"] = new SelectList(await _context.JobLevel.ToListAsync(), "JobLevelId",
                "JobLevelName");
            ViewData["JobTypeId"] = new SelectList(await _context.JobType.ToListAsync(), "Id", "Name");

            var categories = await _context.CompanyCategory.Where(m => m.CompanyId == companyId).Include(m => m.Category).Select(m => m.Category).ToListAsync();
            ViewData["CategoryId"] = new SelectList(categories, "Id", "Name");
            ViewData["SkillId"] = new SelectList(await _context.Skill.ToListAsync(), "Id", "Name");
            ViewData["LanguageId"] = new SelectList(await _context.Language.ToListAsync(), "Id", "Name");
            ViewData["LocationId"] = new SelectList(await _context.Location.Where(m => m.Type == 2).ToListAsync(), "Id", "Name");
            return View(new PositionJob() {
                CompanyId = companyId, JobStatus = false
            });
        }

        // POST: Employer/PositionJobs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task < IActionResult > Create(PositionJob positionJob, [FromForm] IEnumerable < JobCategory > categoriesSeleted, [FromForm] IEnumerable < JobLanguage > languagesSeleted, [FromForm] IEnumerable < JobSkill > skillsSeleted, [FromForm] IEnumerable < JobLocation > locationsSeleted) {
            if (ModelState.IsValid) {
                if (positionJob.OpenDate != null && positionJob.ClosedDate != null &&
                    positionJob.ClosedDate < positionJob.OpenDate) {
                    ModelState.TryAddModelError("ClosedDate", "ClosedDate is smaller than OpenDate !");
                } else if (positionJob.MaxPay != null && positionJob.MinPay != null && (decimal) positionJob.MaxPay < (decimal) positionJob.MinPay) {
                    ModelState.TryAddModelError("MaxPay", "Max salary is smaller than Min salary !");
                } else {
                    _context.Add(positionJob);

                    foreach(var li in categoriesSeleted) {
                        _context.JobCategory.Add(new JobCategory {
                            PositionJobId = positionJob.Id,
                                CategoryId = li.CategoryId
                        });
                    }

                    foreach(var li in languagesSeleted) {
                        _context.JobLanguage.Add(new JobLanguage {
                            PositionJobId = positionJob.Id,
                                LanguageId = li.LanguageId,
                                Certification = li.Certification,
                                ScoreOrLevel = li.ScoreOrLevel,
                        });

                    }

                    foreach(var li in skillsSeleted) {
                        _context.JobSkill.Add(new JobSkill {
                            PositionJobId = positionJob.Id,
                                KeySkillId = li.KeySkillId,
                                Comment = li.Comment,
                                YearsOfExperienceRequirement = li.YearsOfExperienceRequirement,
                        });

                    }
                    foreach(var li in locationsSeleted) {
                        _context.JobLocation.Add(new JobLocation {
                            PositionJobId = positionJob.Id,
                                LocationId = li.LocationId,
                        });

                    }

                    await _context.SaveChangesAsync();
                    ModelState.AddModelError("Success", "Successful !");
                    return RedirectToAction("Index", new { page=1 });

//                    ViewData["CompanyId"] = positionJob.CompanyId;
                    /*return View("Index", await _context.PositionJob
                        .Where(p => p.CompanyId == positionJob.CompanyId)
                        .Include(p => p.EducationLevel)
                        .Include(p => p.JobApplication)
                        .Include(p => p.JobLevel)
                        .Include(p => p.JobType)
                        .ToPagedListAsync(6, 1));*/

                }
            }
            return BadRequest(ModelState);
            // ViewData["EducationLevelId"] = new SelectList(_context.EducationLevel, "Id", "Name");
            // ViewData["JobLevelId"] = new SelectList(_context.JobLevel, "JobLevelId", "JobLevelName");
            // ViewData["JobTypeId"] = new SelectList(_context.JobType, "Id", "Name");

            // var categories = await _context.CompanyCategory.Where(m => m.CompanyId == positionJob.CompanyId).Include(m => m.Category).Select(m => m.Category).ToListAsync();
            // ViewData["CategoryId"] = new SelectList(categories, "Id", "Name");
            // ViewData["SkillId"] = new SelectList(await _context.Skill.ToListAsync(), "Id", "Name");
            // ViewData["LanguageId"] = new SelectList(await _context.Language.ToListAsync(), "Id", "Name");
            // ViewData["LocationId"] = new SelectList(await _context.Location.Where(m => m.Type == 2).ToListAsync(), "Id", "Name");
            // return View(positionJob);
        }

        // GET: Employer/PositionJobs/Edit/5
        public async Task < IActionResult > Edit(int ? id) {
            if (id == null) {
                return NotFound();
            }

            var positionJob = await _context.PositionJob
                .Include(m => m.JobCategory).ThenInclude(r => r.Category)
                .Include(m => m.JobSkill).ThenInclude(r => r.KeySkill)
                .Include(m => m.JobLanguage).ThenInclude(r => r.Language)
                .Include(m => m.JobLocation).ThenInclude(r => r.Location)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (positionJob == null) {
                return NotFound();
            }
            ViewData["EducationLevelId"] = new SelectList(_context.EducationLevel, "Id", "Name", positionJob.EducationLevelId);
            ViewData["JobLevelId"] = new SelectList(_context.JobLevel, "JobLevelId", "JobLevelName", positionJob.JobLevelId);
            ViewData["JobTypeId"] = new SelectList(_context.JobType, "Id", "Name", positionJob.JobTypeId);

            ViewData["CategoryId"] = await _context.CompanyCategory
                .Where(m => m.CompanyId == positionJob.CompanyId)
                .Include(m => m.Category)
                .Select(m => new SelectListItem {
                    Value = m.CategoryId.ToString(),
                        Text = m.Category.Name,
                        Disabled = positionJob.JobCategory.Any(r => r.CategoryId == m.CategoryId),
                })
                .ToListAsync();

            ViewData["SkillId"] = await _context.Skill
                .Select(m => new SelectListItem {
                    Value = m.Id.ToString(),
                        Text = m.Name,
                        Disabled = positionJob.JobSkill.Any(r => r.KeySkillId == m.Id),
                })
                .ToListAsync();
            ViewData["LanguageId"] = await _context.Language
                .Select(m => new SelectListItem {
                    Value = m.Id.ToString(),
                        Text = m.Name,
                        Disabled = positionJob.JobLanguage.Any(r => r.LanguageId == m.Id),
                })
                .ToListAsync();
            ViewData["LocationId"] = await _context.Location.Where(m => m.Type == 2)
                .Select(m => new SelectListItem {
                    Value = m.Id.ToString(),
                    Text = m.Name,
                    Disabled = positionJob.JobLocation.Any(r => r.LocationId == m.Id),
                }).OrderBy(x => x.Text)
                .ToListAsync();
            ViewData["NationId"] = new SelectList(await _context.Location.Where(x=>x.Type == 1).OrderBy(x=>x.Name).ToListAsync(), "Id", "Name");
            return View(positionJob);
        }

        // POST: Employer/PositionJobs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task < IActionResult > Edit(int id, PositionJob positionJob, [FromForm] IEnumerable < JobCategory > categoriesSeleted, [FromForm] IEnumerable < JobLanguage > languagesSeleted, [FromForm] IEnumerable < JobSkill > skillsSeleted, [FromForm] IEnumerable < JobLocation > locationsSeleted) {
            if (id != positionJob.Id) {
                return NotFound();
            }

            if (ModelState.IsValid) {
                if (positionJob.OpenDate != null && positionJob.ClosedDate != null &&
                    positionJob.ClosedDate < positionJob.OpenDate) {
                    ModelState.TryAddModelError("ClosedDate", "ClosedDate is smaller than OpenDate !");
                } else if (positionJob.MaxPay != null && positionJob.MinPay != null && (decimal) positionJob.MaxPay < (decimal) positionJob.MinPay) {
                    ModelState.TryAddModelError("MaxPay", "Max salary is smaller than Min salary !");
                } else {
                    try {
                        var oldCategories = await _context.JobCategory.Where(m => m.PositionJobId == positionJob.Id).ToListAsync();
                        _context.JobCategory.RemoveRange(oldCategories);
                        _context.SaveChanges();
                        foreach(var item in categoriesSeleted) {
                            _context.JobCategory.Add(new JobCategory {
                                PositionJobId = positionJob.Id,
                                    CategoryId = item.CategoryId,
                            });
                        }

                        var oldLanguages = await _context.JobLanguage.Where(m => m.PositionJobId == positionJob.Id).ToListAsync();
                        _context.JobLanguage.RemoveRange(oldLanguages);
                        _context.SaveChanges();
                        foreach(var item in languagesSeleted) {
                            _context.JobLanguage.Add(new JobLanguage {
                                PositionJobId = positionJob.Id,
                                    LanguageId = item.LanguageId,
                                    Certification = item.Certification,
                                    ScoreOrLevel = item.ScoreOrLevel,
                            });
                        }

                        var oldSkills = await _context.JobSkill.Where(m => m.PositionJobId == positionJob.Id).ToListAsync();
                        _context.JobSkill.RemoveRange(oldSkills);
                        _context.SaveChanges();
                        foreach(var item in skillsSeleted) {
                            _context.JobSkill.Add(new JobSkill {
                                PositionJobId = positionJob.Id,
                                KeySkillId = item.KeySkillId,
                                Comment = item.Comment,
                                YearsOfExperienceRequirement = item.YearsOfExperienceRequirement,
                            });
                        }

                        var oldLocations = await _context.JobLocation.Where(m => m.PositionJobId == positionJob.Id).ToListAsync();
                        _context.JobLocation.RemoveRange(oldLocations);
                        _context.SaveChanges();
                        foreach(var item in locationsSeleted) {
                            _context.JobLocation.Add(new JobLocation {
                                PositionJobId = positionJob.Id,
                                    LocationId = item.LocationId,
                            });
                        }

                        _context.Update(positionJob);
                        _context.SaveChanges();
                        ModelState.AddModelError("Success", "Updated successful !");
                        return Ok(ModelState);
                    } catch (DbUpdateConcurrencyException) {
                        if (!PositionJobExists(positionJob.Id)) {
                            return NotFound();
                        } else {
                            ModelState.AddModelError("Error", "Internal server error ..!");
                        }
                    }
                }
            }
            return BadRequest(ModelState);
            // ViewData["EducationLevelId"] = new SelectList(_context.EducationLevel, "Id", "Name", positionJob.EducationLevelId);
            // ViewData["JobLevelId"] = new SelectList(_context.JobLevel, "JobLevelId", "JobLevelName", positionJob.JobLevelId);
            // ViewData["JobTypeId"] = new SelectList(_context.JobType, "Id", "Name", positionJob.JobTypeId);

            // ViewData["CategoryId"] = await _context.CompanyCategory
            //     .Where(m => m.CompanyId == positionJob.CompanyId)
            //     .Include(m => m.Category)
            //     .Select(m => new SelectListItem
            //     {
            //         Value = m.CategoryId.ToString(),
            //         Text = m.Category.Name,
            //         Disabled = positionJob.JobCategory.Any(r => r.CategoryId == m.CategoryId),
            //     })
            //     .ToListAsync();

            // ViewData["SkillId"] = await _context.Skill
            //     .Select(m => new SelectListItem
            //     {
            //         Value = m.Id.ToString(),
            //         Text = m.Name,
            //         Disabled = positionJob.JobSkill.Any(r => r.KeySkillId == m.Id),
            //     })
            //     .ToListAsync();
            // ViewData["LanguageId"] = await _context.Language
            //     .Select(m => new SelectListItem
            //     {
            //         Value = m.Id.ToString(),
            //         Text = m.Name,
            //         Disabled = positionJob.JobLanguage.Any(r => r.LanguageId == m.Id),
            //     })
            //     .ToListAsync();
            // ViewData["LocationId"] = await _context.Location.Where(m => m.Type == 2)
            //     .Select(m => new SelectListItem
            //     {
            //         Value = m.Id.ToString(),
            //         Text = m.Name,
            //         Disabled = positionJob.JobLocation.Any(r => r.LocationId == m.Id),
            //     })
            //     .ToListAsync();

            // positionJob = await _context.PositionJob
            //     .Include(m => m.JobCategory).ThenInclude(r => r.Category)
            //     .Include(m => m.JobSkill).ThenInclude(r => r.KeySkill)
            //     .Include(m => m.JobLanguage).ThenInclude(r => r.Language)
            //     .Include(m => m.JobLocation).ThenInclude(r => r.Location)
            //     .FirstOrDefaultAsync(m => m.Id == id);
            // return View(positionJob);
        }

        // GET: Employer/PositionJobs/Delete/5
        public async Task < IActionResult > Delete(int ? id) {
            if (id == null) {
                return NotFound();
            }

            var positionJob = await _context.PositionJob
                .Include(p => p.Company)
                .Include(p => p.EducationLevel)
                .Include(p => p.JobLevel)
                .Include(p => p.JobType)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (positionJob == null) {
                return NotFound();
            }

            return View(positionJob);
        }

        // POST: Employer/PositionJobs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task < IActionResult > DeleteConfirmed(int id) {
            var positionJob = await _context.PositionJob.SingleOrDefaultAsync(m => m.Id == id);
            _context.PositionJob.Remove(positionJob);
            await _context.SaveChangesAsync();

            ViewData["CompanyId"] = positionJob.CompanyId;    
            return RedirectToAction("Index");
        }

        private bool PositionJobExists(int id) {
            return _context.PositionJob.Any(e => e.Id == id);
        }
    }
}