using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.Controllers
{
    [Area("Employer")]
    [Authorize(Roles = "Employer")]
    public class CompanyCategoriesController : Controller
    {
        private readonly AseanTalentSearchContext _context;

        public CompanyCategoriesController(AseanTalentSearchContext context)
        {
            _context = context;    
        }

        // GET: Employer/CompanyCategories
        public async Task<IActionResult> Index(long companyId)
        {
            if (await _context.Company.FindAsync(companyId) == null)
            {
                return NotFound();
            }
            ViewData["CompanyId"] = companyId;
            return View(await _context.CompanyCategory
                .Where(m => m.CompanyId == companyId)
                .Include(c => c.Category)
                .OrderBy(m => m.Category.Name)
                .ToListAsync());
        }

        // GET: Employer/CompanyCategories/Create
        public async Task<IActionResult> Create(long companyId)
        {
            if (await _context.Company.FindAsync(companyId) == null)
            {
                return NotFound();
            }
            var categories = await _context.Category
                .Include(m => m.CompanyCategory)
                .OrderBy(m => m.Name)
                .ToListAsync();
            ViewData["CategoryId"] = new SelectList(categories.FindAll(m => !m.CompanyCategory.Any()), "Id", "Name");
            return View(new CompanyCategory() { CompanyId = companyId });
        }

        // POST: Employer/CompanyCategories/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CompanyCategory companyCategory)
        {
            if (ModelState.IsValid)
            {
                if (companyCategory.ActiveDate != null && companyCategory.ClosedDate != null && companyCategory.ClosedDate < companyCategory.ActiveDate)
                {
                    ModelState.TryAddModelError("ClosedDate", "ClosedDate is small than ActiveDate !");
                }
                else
                {
                    _context.Add(companyCategory);
                    await _context.SaveChangesAsync();

                    ModelState.AddModelError("Success", "Created successful !");
                    ViewData["CompanyId"] = companyCategory.CompanyId;
                    return View("Index", await _context.CompanyCategory
                        .Where(m => m.CompanyId == companyCategory.CompanyId)
                        .Include(c => c.Category)
                        .OrderBy(m => m.Category.Name)
                        .ToListAsync());
                }
            }
            return BadRequest(ModelState);
        }

        // GET: Employer/CompanyCategories/Edit/5
        public async Task<IActionResult> Edit(long companyId, int categoryId)
        {
            var companyCategory = await _context.CompanyCategory
                .Include(c => c.Category)
                .SingleOrDefaultAsync(m => m.CompanyId == companyId && m.CategoryId == categoryId);
            if (companyCategory == null)
            {
                return NotFound();
            }
            return View(companyCategory);
        }

        // POST: Employer/CompanyCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(CompanyCategory companyCategory)
        {
            if (ModelState.IsValid)
            {
                if (companyCategory.ActiveDate != null && companyCategory.ClosedDate != null && companyCategory.ClosedDate < companyCategory.ActiveDate)
                {
                    ModelState.TryAddModelError("ClosedDate", "ClosedDate is small than ActiveDate !");
                }
                else
                {
                    try
                    {
                        _context.Update(companyCategory);
                        await _context.SaveChangesAsync();
                        ModelState.AddModelError("Success", "Updated successful !");
                        return Ok(ModelState);
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!CompanyCategoryExists(companyCategory.CompanyId, companyCategory.CategoryId))
                        {
                            return NotFound();
                        }
                        else
                        {
                            ModelState.AddModelError("Error", "Internal server error ..!");
                        }
                    }
                }

            }
            return BadRequest(ModelState);
        }

        // GET: Employer/CompanyCategories/Delete/5
        public async Task<IActionResult> Delete(long companyId, int categoryId)
        {
            var companyCategory = await _context.CompanyCategory
                .Include(c => c.Category)
                .SingleOrDefaultAsync(m => m.CompanyId == companyId && m.CategoryId == categoryId);
            if (companyCategory == null)
            {
                return NotFound();
            }

            return View(companyCategory);
        }

        // POST: Employer/CompanyCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long companyId, int categoryId)
        {
            var companyCategory = await _context.CompanyCategory.SingleOrDefaultAsync(m => m.CompanyId == companyId && m.CategoryId == categoryId);
            _context.CompanyCategory.Remove(companyCategory);
            await _context.SaveChangesAsync();

            ModelState.AddModelError("Success", "Deleted successful !");
            ViewData["CompanyId"] = companyId;
            return View("Index", await _context.CompanyCategory
                .Where(m => m.CompanyId == companyCategory.CompanyId)
                .Include(c => c.Category)
                .OrderBy(m => m.Category.Name)
                .ToListAsync());
        }

        private bool CompanyCategoryExists(long companyId, int categoryId)
        {
            return _context.CompanyCategory.Any(e => e.CompanyId == companyId && e.CategoryId == categoryId);
        }
    }
}
