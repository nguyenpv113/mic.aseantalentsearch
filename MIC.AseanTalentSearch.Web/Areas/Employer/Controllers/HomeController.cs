using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.Areas.Employer.ViewModels;
using MIC.AseanTalentSearch.Web.EF;
using Sakura.AspNetCore;
using MIC.AseanTalentSearch.Web.Models.ManageViewModels;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using MIC.AseanTalentSearch.Web.Controllers;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.Controllers
{
    /// <summary>
    /// Class HomeController.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Area("Employer")]
    [Authorize]
    public class HomeController : BaseController
    {
        /// <summary>
        /// The context
        /// </summary>
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="CompaniesController"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public HomeController(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager, ILoggerFactory loggerFactory)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = loggerFactory.CreateLogger<HomeController>();
        }

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns>Microsoft.AspNetCore.Mvc.IActionResult.</returns>
        public async Task<IActionResult> Index(long? id)
        {
            if (id == null)
            {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                if (userId == null || !User.IsInRole("Employer"))
                {
                    return NotFound();
                }
                id = long.Parse(userId);
            }
            var company = await _context.Company.FindAsync(id);
            if (company == null)
            {
                return RedirectToAction("Create", "Companies");
            }

            ViewData["companyId"] = company.Id;
            return View();
        }
        public async Task<IActionResult> Profile()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            long? id = long.Parse(userId);
            if (id == null)
            {
                return NotFound();
            }
            var company = await _context.Company
                .Include(m => m.CompanyMedia)
                .Include(m => m.CompanyBenefit).ThenInclude(r => r.Benefit)
                .Include(m => m.CompanyCategory).ThenInclude(r => r.Category)
                .SingleOrDefaultAsync(m => m.Id == id);

            if (company == null)
            {
                return NotFound();
            }
            var locations = await _context.Location.Where(m => m.Type == 1 || m.Type == 2).OrderBy(m => m.Name).ThenBy(m => m.EnglishName).Select(m => new
            {
                m.Id,
                m.Name,
                m.EnglishName,
                m.ParentId,
                m.Type
            }).ToListAsync();
            if (company.LocationId != null && GetTypeLocation(company.LocationId.Value) != 1)
            {
                ViewData["NationId"] = new SelectList(locations.Where(m => m.Type == 1).ToList(), "Id", "Name", GetLocationByChildId(company.LocationId.Value));
                ViewData["Locations"] = new SelectList(locations.Where(m => m.Type == 2&&m.ParentId== GetLocationByChildId(company.LocationId.Value)), "Id", "Name", company.LocationId.Value);
            }
            else
            {              
                ViewData["NationId"] = new SelectList(locations.Where(m => m.Type == 1).ToList(), "Id", "Name", company.LocationId);
                ViewData["Locations"] = new SelectList(locations.Where(m => m.Type == 2 && m.ParentId ==company.LocationId.Value), "Id", "Name");
            }






            ViewData["MediaCount"] = await _context.CompanyMedia.Where(m => m.CompanyId == id).CountAsync();
            ViewData["BenefitCount"] = await _context.CompanyBenefit.Where(m => m.CompanyId == id).CountAsync();
            ViewData["CategoryCount"] = await _context.CompanyCategory.Where(m => m.CompanyId == id).CountAsync();
            return View(company);
        }

        public int GetTypeLocation(int Id)
        {
            return _context.Location.Find(Id).Type;
        }

        public int GetLocationByChildId(int childId)
        {
            int LocationResual = _context.Location.Where(m => m.Id == childId && m.Type == 2).FirstOrDefault().ParentId.Value;
            return LocationResual;
        }

        public async Task<JsonResult> GetListProvince(int nationId)
        {
            var listProvince = await _context.Location.Where(m => m.ParentId == nationId).OrderBy(m => m.Name).ToListAsync();
            return Json(listProvince);
        }

        public async Task<JsonResult> GetListDistrict(int provinceId)
        {
            var listDistrict = await _context.Location.Where(m => m.ParentId == provinceId).OrderBy(m => m.Name).ToListAsync();
            return Json(listDistrict);
        }

        public async Task<IActionResult> Recruitment()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            long? companyId = long.Parse(userId);
            if (companyId == null)
            {
                return NotFound();
            }
            if (await _context.Company.FindAsync(companyId) == null)
            {
                return NotFound();
            }
            ViewData["CompanyId"] = companyId;
            ViewData["PositionJobs"] = await _context.PositionJob
                .Where(p => p.CompanyId == companyId)
                .Include(p => p.EducationLevel)
                .Include(p => p.JobLevel)
                .Include(p => p.JobType)
                .OrderBy(x => x.ModifiedDate).ThenBy(x => x.CreatedDate)
                .Take(5)
                .ToListAsync();
            ViewData["EducationLevelId"] = new SelectList(await _context.EducationLevel.OrderBy(x => x.Name).ToListAsync(), "Id", "Name");
            ViewData["JobLevelId"] = new SelectList(await _context.JobLevel.OrderBy(x => x.JobLevelName).ToListAsync(), "JobLevelId", "JobLevelName");
            ViewData["JobTypeId"] = new SelectList(await _context.JobType.OrderBy(m => m.Name).ToListAsync(), "Id", "Name");

            var categories = await _context.CompanyCategory.Where(m => m.CompanyId == companyId).Include(m => m.Category).Select(m => m.Category).ToListAsync();
            ViewData["CategoryId"] = new SelectList(categories.OrderBy(x => x.Name), "Id", "Name");
            ViewData["SkillId"] = new SelectList(await _context.Skill.OrderBy(x => x.Name).ToListAsync(), "Id", "Name");
            ViewData["LanguageId"] = new SelectList(await _context.Language.OrderBy(m => m.Name).ToListAsync(), "Id", "Name");
            ViewData["LocationId"] = new SelectList(await _context.Location.Where(x => x.Type == 2).OrderBy(x => x.Name).ToListAsync(), "Id", "Name");
            ViewData["NationId"] = new SelectList(await _context.Location.Where(x => x.Type == 1).OrderBy(x => x.Name).ToListAsync(), "Id", "Name");

            return View();
        }
        public async Task<IActionResult> CandidateSearch()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            long? companyId = long.Parse(userId);
            if (companyId == null)
            {
                return NotFound();
            }
            if (await _context.Company.FindAsync(companyId) == null)
            {
                return NotFound();
            }
            ViewData["CompanyId"] = companyId;
            var searchModel = new CandidateSearchViewModel();

            var categories = await _context.Category.Include(m => m.CompanyCategory).OrderBy(m => m.Name).ToListAsync();
            searchModel.CategoryFilters = categories.Where(x => x.CandidateCategory.Count > 0)
                .Select(x => new CategoryFilter { Id = x.Id, Name = x.Name, Selected = false });

            var skills = await _context.Skill.Include(m => m.CandidateSkill).OrderBy(m => m.Name).ToListAsync();
            searchModel.SkillFilters = skills.Select(x => new SkillFilter { Id = x.Id, Name = x.Name, Selected = false });

            var joblevels = await _context.JobLevel.Include(m => m.Candidate).OrderBy(m => m.JobLevelName).ToListAsync();
            searchModel.JobLevelFilters = joblevels.Select(x => new JobLevelFilter { Id = x.JobLevelId, Name = x.JobLevelName, Selected = false });


            var languages = await _context.Language.Include(m => m.CandidateLanguage).OrderBy(m => m.Name).ToListAsync();
            searchModel.LanguageFilters = languages.Select(x => new LanguageFilter { Id = x.Id, Name = x.Name, Selected = false });

            var locations = await _context.Location.OrderBy(m => m.Name).ThenBy(m => m.EnglishName).ToListAsync();
            ViewData["NationId"] = new SelectList(locations.FindAll(m => m.Type == 1), "Id", "Name");
            ViewData["ProvinceId"] = new SelectList(locations.FindAll(m => m.Type == 2), "Id", "Name");
            ViewData["DistrictId"] = new SelectList(locations.FindAll(m => m.Type == 3), "Id", "Name");
            ViewData["Candidates"] = await _context.Candidate
                .Where(m => (m.IsPublish ?? false) && (m.ActiveStatus ?? false))
                .Include(m => m.ExpectedJobLevel)
                .Include(m => m.CandidateCategory).ThenInclude(r => r.Category)
                .Include(m => m.CandidateSkill).ThenInclude(r => r.Skill)
                .Include(m => m.CandidateLanguage).ThenInclude(r => r.Language)
                .Include(m => m.CandidateLocation).ThenInclude(r => r.Location)
                .OrderByDescending(m => m.ActiveDate)
                .ToListAsync();
            ViewData["ListCandidate"] = Filter(searchModel, 1).Result;
            return View(searchModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CandidateSearch(CandidateSearchViewModel modelSearch, int? page = 1)
        {
            var query = _context.Candidate
                .Where(m => (m.IsPublish ?? false) && (m.ActiveStatus ?? false))
                .Include(m => m.ExpectedJobLevel)
                .Include(m => m.CandidateCategory).ThenInclude(r => r.Category)
                .Include(m => m.CandidateSkill).ThenInclude(r => r.Skill)
                .Include(m => m.CandidateLanguage).ThenInclude(r => r.Language)
                .Include(m => m.CandidateLocation).ThenInclude(r => r.Location)
                .AsQueryable();
            // Filter by job category
            var categoryIds = modelSearch.CategoryFilters.Where(m => m.Selected).Select(m => m.Id).ToList();
            if (categoryIds.Any())
            {
                query = query.Where(m => categoryIds.Any(r => r == m.ExpectedCategory) || m.CandidateCategory.Any(u => categoryIds.Contains(u.CategoryId)));
            }
            // Filter by skill
            var skillIds = modelSearch.SkillFilters.Where(m => m.Selected).Select(m => m.Id).ToList();
            if (skillIds.Any())
            {
                query = query.Where(m => m.CandidateSkill.Any(u => skillIds.Contains(u.SkillId)));
            }
            // Filter by job level
            var jobLevelIds = modelSearch.JobLevelFilters.Where(m => m.Selected).Select(m => m.Id).ToList();
            if (jobLevelIds.Any())
            {
                query = query.Where(m => jobLevelIds.Any(r => r == m.ExpectedJobLevelId));
            }
            // Filter by langauge
            var languageIds = modelSearch.LanguageFilters.Where(m => m.Selected).Select(m => m.Id).ToList();
            if (languageIds.Any())
            {
                query = query.Where(m => m.CandidateLanguage.Any(u => languageIds.Contains(u.LanguageId)));
            }
            // Filter by salary
            if (modelSearch.MaxPay != null)
            {
                query = query.Where(m => m.ExpectedSalary <= modelSearch.MaxPay);
            }
            if (modelSearch.MinPay != null)
            {
                query = query.Where(m => m.ExpectedSalary >= modelSearch.MinPay);
            }

            // Filter by experience years
            if (modelSearch.MinExperienceYear != null)
            {
                query = query.Where(m => m.YearsOfExperience >= modelSearch.MinExperienceYear);
            }
            if (modelSearch.MinPay != null)
            {
                query = query.Where(m => m.ExpectedSalary >= modelSearch.MinPay);
            }

            // Filter by location
            Nullable<int> locationId = modelSearch.DistrictId ?? modelSearch.ProvinceId ?? modelSearch.NationId ?? null;
            query = query.Where(m => locationId == null || m.CandidateLocation.Any(r => r.LocationId == locationId));



            var candidates = await query.ToListAsync();
            ViewBag.Count = candidates.Count;
            ViewData["ListCandidate"] = Filter(modelSearch, page.Value).Result;
            return View(modelSearch);
        }

        //Get list Candidate by searchModel
        public async Task<IPagedList<Candidate>> Filter(CandidateSearchViewModel model, int? page = 1)
        {
            int pageSize = 21;
            var query = _context.Candidate
                .Where(m => (m.IsPublish ?? false) && (m.ActiveStatus ?? false))
                .Include(m => m.ExpectedJobLevel)
                .Include(m => m.CandidateCategory).ThenInclude(r => r.Category)
                .Include(m => m.CandidateSkill).ThenInclude(r => r.Skill)
                .Include(m => m.CandidateLanguage).ThenInclude(r => r.Language)
                .Include(m => m.CandidateLocation).ThenInclude(r => r.Location)
                .AsQueryable();
            // Filter by job category
            var categoryIds = model.CategoryFilters.Where(m => m.Selected).Select(m => m.Id).ToList();
            if (categoryIds.Any())
            {
                query = query.Where(m => categoryIds.Any(r => r == m.ExpectedCategory) || m.CandidateCategory.Any(u => categoryIds.Contains(u.CategoryId)));
            }
            // Filter by skill
            var skillIds = model.SkillFilters.Where(m => m.Selected).Select(m => m.Id).ToList();
            if (skillIds.Any())
            {
                query = query.Where(m => m.CandidateSkill.Any(u => skillIds.Contains(u.SkillId)));
            }
            // Filter by job level
            var jobLevelIds = model.JobLevelFilters.Where(m => m.Selected).Select(m => m.Id).ToList();
            if (jobLevelIds.Any())
            {
                query = query.Where(m => jobLevelIds.Any(r => r == m.ExpectedJobLevelId));
            }
            // Filter by langauge
            var languageIds = model.LanguageFilters.Where(m => m.Selected).Select(m => m.Id).ToList();
            if (languageIds.Any())
            {
                query = query.Where(m => m.CandidateLanguage.Any(u => languageIds.Contains(u.LanguageId)));
            }
            // Filter by salary
            if (model.MaxPay != null)
            {
                query = query.Where(m => m.ExpectedSalary <= model.MaxPay);
            }
            if (model.MinPay != null)
            {
                query = query.Where(m => m.ExpectedSalary >= model.MinPay);
            }

            // Filter by experience years
            if (model.MinExperienceYear != null)
            {
                query = query.Where(m => m.YearsOfExperience >= model.MinExperienceYear);
            }
            if (model.MinPay != null)
            {
                query = query.Where(m => m.ExpectedSalary >= model.MinPay);
            }

            // Filter by location
            Nullable<int> locationId = model.DistrictId ?? model.ProvinceId ?? model.NationId ?? null;
            query = query.Where(m => locationId == null || m.CandidateLocation.Any(r => r.LocationId == locationId));



            var candidates = await query.ToListAsync();
            ViewBag.Count = candidates.Count;
            var modelresual = candidates.OrderBy(m => m.FullName).ToPagedList(pageSize, page.Value);
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            long? companyId = long.Parse(userId);            
            ViewBag.ListSaved = from savedcandidate in _context.SaveCandidate
                                where savedcandidate.CompanyId == companyId
                                select savedcandidate.CandidateId;
            return modelresual;
        }

        public async Task<IActionResult> CandidateSubmit()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            long? companyId = long.Parse(userId);
            if (companyId == null)
            {
                return NotFound();
            }
            if (await _context.Company.FindAsync(companyId) == null)
            {
                return NotFound();
            }
            // var positionJobs = await _context.PositionJob.Where(m => m.CompanyId == companyId)
            //     .Select(m => m.Id)
            //     .ToListAsync();
            var query = _context.JobApplication
                .Include(m => m.PositionJob)
                .Include(m => m.Candidate).ThenInclude(r => r.ExpectedJobLevel)
                .Include(m => m.Candidate).ThenInclude(r => r.Reference)
                .AsQueryable();
            var jobApplications = await query.Where(m => m.PositionJob.CompanyId == companyId && (m.Candidate.IsPublish == null || m.Candidate.IsPublish.Value))
                .AsNoTracking()
                .ToListAsync();
            ViewData["CompanyId"] = companyId;
            return View(jobApplications);
        }

        public async Task<IActionResult> CandidateSaved(int? page = 1)
        {
            int pageSize = 21;
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            long? companyId = long.Parse(userId);
            if (companyId == null)
            {
                return NotFound();
            }
            if (await _context.Company.FindAsync(companyId) == null)
            {
                return NotFound();
            }

            var model = _context.SaveCandidate.Include(x => x.Candidate)
                .Where(x => x.CompanyId == companyId).OrderByDescending(x => x.CreatedDate).ToPagedList(pageSize, page.Value);
            return View(model);
        }

        public async Task<JsonResult> GetLocationByParentId(int PrId)
        {
            var model = await _context.Location.Where(m => m.ParentId == PrId).ToListAsync();
            return Json(model);
        }

        // GET: /Manage/ChangePassword
        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user != null)
            {
                var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3, "User changed their password successfully.");
                    TempData["message"] = "User changed their password successfully.";
                    return RedirectToAction("Index", "Home", new { Message = ManageMessageId.ChangePasswordSuccess });
                }
                AddErrors(result);
                return View(model);
            }
            return RedirectToAction("ChangePassword", "Home", new { Message = ManageMessageId.Error });
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            AddLoginSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }
        #endregion
    }
}