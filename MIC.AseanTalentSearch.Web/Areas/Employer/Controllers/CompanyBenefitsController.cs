using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.Controllers {
    [Area("Employer")]
    [Authorize(Roles = "Employer")]
    public class CompanyBenefitsController: Controller {
        private readonly AseanTalentSearchContext _context;

        public CompanyBenefitsController(AseanTalentSearchContext context) {
            _context = context;
        }

        // GET: Employer/CompanyBenefits
        public async Task < IActionResult > Index(long companyId) {
            if (await _context.Company.FindAsync(companyId) == null) {
                return NotFound();
            }
            ViewData["CompanyId"] = companyId;
            return View(await _context.CompanyBenefit
                .Where(m => m.CompanyId == companyId)
                .Include(c => c.Benefit)
                .OrderBy(m => m.Benefit.Name)
                .ToListAsync());
        }

        // GET: Employer/CompanyBenefits/Create
        public async Task < IActionResult > Create(long companyId) {
            var company = await _context.Company.Include(m => m.CompanyBenefit).FirstOrDefaultAsync(m => m.Id == companyId);
            if (company == null) {
                return NotFound();
            }
            var benefits = await _context.Benefit
                .Where(m => company.CompanyBenefit.All(r => r.BenefitId != m.Id))
                .OrderBy(m => m.Name).ToListAsync();
            ViewData["BenefitId"] = new SelectList(benefits, "Id", "Name");
            return View(new CompanyBenefit() {
                CompanyId = companyId
            });
        }

        // POST: Employer/CompanyBenefits/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task < IActionResult > Create(CompanyBenefit companyBenefit) {
            if (ModelState.IsValid) {
                if (companyBenefit.ActiveDate != null && companyBenefit.ActiveClosed != null && companyBenefit.ActiveClosed < companyBenefit.ActiveDate) {
                    ModelState.TryAddModelError("ActiveClosed", "ActiveClosed is small than ActiveDate !");
                } else {
                    _context.Add(companyBenefit);
                    await _context.SaveChangesAsync();

                    ModelState.AddModelError("Success", "Created successful !");
                    ViewData["CompanyId"] = companyBenefit.CompanyId;
                    return View("Index", await _context.CompanyBenefit
                        .Where(m => m.CompanyId == companyBenefit.CompanyId)
                        .Include(c => c.Benefit)
                        .OrderBy(m => m.Benefit.Name)
                        .ToListAsync());
                }

            }
            return BadRequest(ModelState);
        }

        // GET: Employer/CompanyBenefits/Edit/5
        public async Task < IActionResult > Edit(long companyId, int benefitId) {
            var companyBenefit = await _context.CompanyBenefit
                .Include(c => c.Benefit)
                .SingleOrDefaultAsync(m => m.CompanyId == companyId && m.BenefitId == benefitId);
            if (companyBenefit == null) {
                return NotFound();
            }
            return View(companyBenefit);
        }

        // POST: Employer/CompanyBenefits/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task < IActionResult > Edit(CompanyBenefit companyBenefit) {
            if (ModelState.IsValid) {
                if (companyBenefit.ActiveDate != null && companyBenefit.ActiveClosed != null && companyBenefit.ActiveClosed < companyBenefit.ActiveDate) {
                    ModelState.TryAddModelError("ActiveClosed", "ActiveClosed is small than ActiveDate !");
                } else {
                    try {
                        _context.Update(companyBenefit);
                        await _context.SaveChangesAsync();
                        ModelState.AddModelError("Success", "Updated successful !");
                        return Ok(ModelState);
                    } catch (DbUpdateConcurrencyException) {
                        if (!CompanyBenefitExists(companyBenefit.CompanyId, companyBenefit.BenefitId)) {
                            return NotFound();
                        } else {
                            ModelState.AddModelError("Error", "Internal server error ..!");
                        }
                    }
                }

            }
            return BadRequest(ModelState);
        }

        // GET: Employer/CompanyBenefits/Delete/5
        public async Task < IActionResult > Delete(long companyId, int benefitId) {
            var companyBenefit = await _context.CompanyBenefit
                .Include(c => c.Benefit)
                .SingleOrDefaultAsync(m => m.CompanyId == companyId && m.BenefitId == benefitId);
            if (companyBenefit == null) {
                return NotFound();
            }

            return View(companyBenefit);
        }

        // POST: Employer/CompanyBenefits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task < IActionResult > DeleteConfirmed(long companyId, int benefitId) {
            var companyBenefit = await _context.CompanyBenefit.SingleOrDefaultAsync(m => m.CompanyId == companyId && m.BenefitId == benefitId);
            _context.CompanyBenefit.Remove(companyBenefit);
            await _context.SaveChangesAsync();

            ModelState.AddModelError("Success", "Deleted successful !");
            ViewData["CompanyId"] = companyId;
            return View("Index", await _context.CompanyBenefit
                .Where(m => m.CompanyId == companyId)
                .Include(c => c.Benefit)
                .OrderBy(m => m.Benefit.Name)
                .ToListAsync());
        }

        private bool CompanyBenefitExists(long companyId, int benefitId) {
            return _context.CompanyBenefit.Any(m => m.CompanyId == companyId && m.BenefitId == benefitId);
        }
    }
}