﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.EntityFrameworkCore;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.ViewComponents
{
    public class ProfileSumaryViewComponent: ViewComponent
    {
        private readonly AseanTalentSearchContext _context;

        public ProfileSumaryViewComponent(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(long companyId)
        {
            ViewData["MediaCount"] = await _context.CompanyMedia.Where(m => m.CompanyId == companyId).CountAsync();
            ViewData["BenefitCount"] = await _context.CompanyBenefit.Where(m => m.CompanyId == companyId).CountAsync();
            ViewData["CategoryCount"] = await _context.CompanyCategory.Where(m => m.CompanyId == companyId).CountAsync();
            return View("~/Areas/Employer/Views/Shared/Components/_ProfileSumary.cshtml");
        }
    }
}
