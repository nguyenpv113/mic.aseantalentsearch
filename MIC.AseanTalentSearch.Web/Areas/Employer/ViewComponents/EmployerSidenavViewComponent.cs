﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.ViewComponents
{
    public class EmployerSidenavViewComponent: ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AseanTalentSearchContext _context;

        public EmployerSidenavViewComponent(UserManager<ApplicationUser> userManager, AseanTalentSearchContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var company = new Company();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user != null)
            {
                company = _context.Company.SingleOrDefault(m => m.Id == user.Id);
                if (company != null)
                {
                    return View("Components/_EmployerSidenav.cshtml", company);
                }
            }
            return View("Components/_EmployerSidenav.cshtml");
        }
    }
}
