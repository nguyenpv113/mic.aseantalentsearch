﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.ViewComponents
{
    public class SuggestCandidateViewComponent: ViewComponent
    {
        private readonly AseanTalentSearchContext _context;

        public SuggestCandidateViewComponent(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(long companyId, int? page =1)
        {
            int pageSize = 20;
            var candidateIds = new List<long>();
//            List<EF.Candidate> candidates = new List<EF.Candidate>();
            var conn = _context.Database.GetDbConnection();
            try
            {
                await conn.OpenAsync();
                using (var command = conn.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter parameter = new SqlParameter("@CompanyId", companyId);
                    command.CommandText = "findCandidate";
                    command.Parameters.Add(parameter);
                    DbDataReader reader = await command.ExecuteReaderAsync();

                    if (reader.HasRows)
                    {
                        while (await reader.ReadAsync())
                        {
                            candidateIds.Add(reader.GetInt64(0));
                        }
                    }
                    reader.Dispose();
                }
            }
            finally
            {
                conn.Close();
            }
//             var positionJobID = new SqlParameter("PositionJobID", 54);

//              var blogs = _context.Set<dynamic<int>>()
//                  .FromSql("EXEC dbo.findCandidate = {0}", 54)
//                  .AsNoTracking()
//                  .ToList();

            // var candidatess = _context.Database.ExecuteSqlCommand("EXECUTE dbo.findCandidate @PositionJobID = {0}", 54);

            var candidates = await _context.Candidate
                .Where(m => candidateIds.Contains(m.Id))
                .Include(m => m.ExpectedJobLevel)
                .Include(m => m.CandidateCategory).ThenInclude(r => r.Category)
                .Include(m => m.CandidateSkill).ThenInclude(r => r.Skill)
                .Include(m => m.CandidateLanguage).ThenInclude(r => r.Language)
                .Include(m => m.CandidateLocation).ThenInclude(r => r.Location)
                .AsNoTracking()
                .ToListAsync();

            var model = candidates.OrderBy(a => a.FullName).ToPagedList(pageSize, page.Value);

//            var candidates = await query.ToListAsync();
            return View("~/Areas/Employer/Views/Shared/Components/_SuggestCandidate.cshtml", model);
        }
    }
}
