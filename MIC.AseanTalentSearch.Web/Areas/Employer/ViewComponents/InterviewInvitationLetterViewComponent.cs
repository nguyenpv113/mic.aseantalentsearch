﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.ViewComponents
{
    public class InterviewInvitationLetterViewComponent: ViewComponent
    {
        private readonly AseanTalentSearchContext _context;

        public InterviewInvitationLetterViewComponent(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(long companyId)
        {
            var letters = await _context.InterviewInvitation
                .Where(m => m.CompanyId == companyId)
                .Include(m => m.Candidate)
                .OrderByDescending(m => m.ActiveDate)
                .ThenByDescending(m => m.ModifiedDate)
                .AsNoTracking()
                .ToListAsync();

            return View("~/Areas/Employer/Views/Shared/Components/_InterviewInvitationLetter.cshtml", letters);
        }
    }
}
