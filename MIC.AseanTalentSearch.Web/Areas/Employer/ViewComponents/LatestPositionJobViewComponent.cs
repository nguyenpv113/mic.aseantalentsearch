﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.EntityFrameworkCore;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.ViewComponents
{
    public class LatestPositionJobViewComponent: ViewComponent
    {
        private readonly AseanTalentSearchContext _context;

        public LatestPositionJobViewComponent(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(long companyId)
        {
            // if(companyId == null) return NotFound();
            return View("~/Areas/Employer/Views/Shared/Components/_LatestPositionJob.cshtml", await _context.PositionJob
                .Where(p => p.CompanyId == companyId)
                .Include(p => p.EducationLevel)
                .Include(p => p.JobApplication)
                .Include(p => p.JobLevel)
                .Include(p => p.JobType)
                .OrderBy(x => x.ModifiedDate)
                .ThenBy(x => x.CreatedDate)
                .Take(5)
                .ToListAsync());
        }
    }
}
