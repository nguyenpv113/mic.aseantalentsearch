﻿using MIC.AseanTalentSearch.Web.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Areas.Employer.Helpers
{
    public class ResultViewSave
    {
        public long CompanyId;
        public IEnumerable<Company> SaveDetail;
        public DateTime? SavedDate;
        public Company Company;
    }
}
