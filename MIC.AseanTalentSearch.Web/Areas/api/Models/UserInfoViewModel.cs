using System.Collections.Generic;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Areas.api.Models
{
    public class UserInfoViewModel
    {
        public long Id { get; set; } 
        public List<Skill> Skill { get; set; }
        public List<Location> Location { get; set; }
        public List<Category> Category { get; set; }
        public List<Benefit> Benefit { get; set; }
        public decimal? ExpectedSalary { get; set; }
        public int? ExpectedJobLevelId { get; set; }
    }
}