﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.Areas.api.Models;
using MIC.AseanTalentSearch.Web.EF;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MIC.AseanTalentSearch.Web.Areas.api.Controllers {
    [Route ("api/[controller]")]
    public class UserInfoController : Controller {
        private readonly AseanTalentSearchContext _context;

        public UserInfoController (AseanTalentSearchContext context) {
            _context = context;
        }
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get () {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        [HttpGet ("{id}")]
        public JsonResult Get (int id) {
            var iscurrentUserRole = _context.AspNetUserRoles.Where (x => x.UserId == id && x.RoleId == 7).FirstOrDefault ();
            if (iscurrentUserRole == null)
                return Json (null);
            var model = _context.Candidate.Include (x => x.CandidateSkill).ThenInclude (c => c.Skill)
                .Include (x => x.CandidateCategory).ThenInclude (c => c.Category)
                .Where (x => x.Id == id && x.IsPublish.Value && x.ActiveStatus.Value)
                .Select (x => new {
                    id = x.Id,
                    Skill = x.CandidateSkill.Select (m => m.Skill),
                    Category = x.CandidateCategory.Select (m => m.Category),
                    Location = x.CandidateLocation.Select (m => m.Location),
                    Benefit = x.CandidateBenefit.Select (m => m.DesiredBenefit),
                    ExpectedSalary = x.ExpectedSalary,
                    YearOfExperience = x.YearsOfExperience,
                    ExpectedJobLevelName = x.ExpectedJobLevel.JobLevelName
                });
            return Json (model);
        }

        // POST api/<controller>
        [HttpPost]
        public void Post ([FromBody] string value) { }

        // PUT api/<controller>/5
        [HttpPut ("{id}")]
        public void Put (int id, [FromBody] string value) { }

        // DELETE api/<controller>/5
        [HttpDelete ("{id}")]
        public void Delete (int id) { }
    }
}