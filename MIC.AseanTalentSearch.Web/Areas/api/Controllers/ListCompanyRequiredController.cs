using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;

namespace MIC.AseanTalentSearch.Web.Areas.api.Controllers {
    [Route ("api/[controller]")]
    public class ListCompanyRequiredController : Controller {
        private readonly AseanTalentSearchContext _context;
        public ListCompanyRequiredController (AseanTalentSearchContext context) {
            _context = context;
        }
        // GET api/<controller>/5
        [HttpGet]
        public JsonResult Get () {            
            var model = _context.PositionJob.Where (x => x.ClosedDate >= DateTime.Now)
                .Select (x => new {
                    id = x.Id,
                        Skill = x.JobSkill.Select (m => m.KeySkill),
                        Category = x.JobCategory.Select (m => m.Category),
                        Location = x.JobLocation.Select (m => m.Location),
                        Benefit = x.Company.CompanyBenefit.Select (m => m.Benefit),
                        Language = x.JobLanguage.Select(m=>m.Language),
                        OpenDate = x.OpenDate,
                        ClosedDate = x.ClosedDate,
                        MinPay = x.MinPay,
                        MaxPay = x.MaxPay,
                        ExperienceRequired = x.YearsOfExperienceRequirement,
                        EducationLevel = x.EducationLevel.Name,
                        JobLevel = x.JobLevel.JobLevelName,
                        JobType = x.JobType.Name
                });
            return Json (model);
        }
    }
}