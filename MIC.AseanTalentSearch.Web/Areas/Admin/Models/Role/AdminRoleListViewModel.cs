﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Models.Role
{
    public class AdminRoleListViewModel
    {
        public long Id { get; set; }
        public string RoleName { get; set; }
        public int NumberOfUsers { get; set; }
    }
}
