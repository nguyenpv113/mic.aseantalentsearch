﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Models.User
{
    public static class ClaimData
    {
        public static List<string> UserClaims { get; set; } = new List<string> { "Instructor Admin", "Instructor Staff"};
    }
}

