﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIC.AseanTalentSearch.Web.Models;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Models.User
{
    public class UserGrid
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public IList<string> ApplicationRoles { get; set; }
    }
}
