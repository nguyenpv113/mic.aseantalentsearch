﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Models.User
{
    public class InstructorGrid
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public int? UniversityId { get; set; }
        public string University { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsStaff { get; set; }
        public IList<string> ApplicationRoles { get; set; }
        public bool IsActive { get; set; }
        
        public IList<long> SelectedRoles { get; set; }
        public List<SelectListItem> UserClaims { get; set; }
    }
}
