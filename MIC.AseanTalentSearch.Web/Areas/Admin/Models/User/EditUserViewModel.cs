﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Models.User
{
    public class EditUserViewModel
    {
        public long Id { get; set; }
        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public MultiSelectList ApplicationRoles { get; set; }
        [Display(Name = "Role")]
        public IList<long> SelectedRoles { get; set; }
        public List<SelectListItem> UserClaims { get; set; }
    }
}
