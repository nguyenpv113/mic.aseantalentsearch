﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIC.AseanTalentSearch.Web.EF;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Models
{
    public class PositionJobPagingModel
    {
        public IPagedList<PositionJob> Result { get; set; }
        public int? ActiveStatus { get; set; }

    }
}
