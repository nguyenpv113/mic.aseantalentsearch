﻿using MIC.AseanTalentSearch.Web.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Models.LocationView
{
    /// <summary>
    /// Using this model to get child of location
    /// </summary>
    public class AdminLocationView
    {
       public Location Location { get; set; }
       public IList<AdminLocationView> Child { get; set; }
    }
}
