﻿using MIC.AseanTalentSearch.Web.Areas.Admin.Models.User;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Models.Instructor
{
    public class InstructorLogic
    {
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public InstructorLogic(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager,
            AseanTalentSearchContext context)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task<List<InstructorGrid>> GetList()
        {
            var data = await _userManager.Users.Select(m => new InstructorGrid
            {
                Id = m.Id,
                UniversityId = GetUniversity(m.Id),
                University = GetUniversityName(m.Id),
                Username = NameofInstructor(m.Id),
                ApplicationRoles = _roleManager.Roles.Where(r => m.Roles.Select(t => t.RoleId).Contains(r.Id))
                .Where(x => x.Name.Contains("Instructor"))
                .Select(n => n.Name).ToList(),
                IsAdmin = GetIsInstructorAdmin(m.Id, "Instructor Admin"),
                IsStaff = GetIsInstructorAdmin(m.Id, "Instructor Staff"),
                IsActive = GetActiveInstructor(m.Id)
            }).ToListAsync();

            return data.Where(m => m.ApplicationRoles.Count > 0 && m.UniversityId != 0 && !string.IsNullOrEmpty(m.Username)).ToList();
        }

        public bool GetActiveInstructor(long UId)
        {
            return _context.Instructor.Find(UId) != null ? _context.Instructor.Find(UId).Status.Value : false;
        }
        public string NameofInstructor(long UId)
        {
            try
            {
                return _context.GeneralProfile.Find(UId).FirstName + " " + _context.GeneralProfile.Find(UId).LastName;
            }
            catch
            {
                return "";
            }
        }

        public int? GetUniversity(long UId)
        {
            try
            {
                return _context.Instructor.Find(UId).UniversityId;
            }
            catch
            {
                return null;
            }
        }

        public string GetUniversityName(long UId)
        {
            try
            {
                var UniversityId = GetUniversity(UId);
                return _context.University.Find(UniversityId).Name;
            }
            catch
            {
                return null;
            }
        }

        public bool GetIsInstructorAdmin(long UId, string Staff)
        {
            try
            {
                return _context.AspNetUserClaims.Where(x => x.UserId == UId && x.ClaimValue.Contains(Staff)).FirstOrDefault() != null ? true : false;
            }
            catch
            {
                return false;
            }
        }

        public bool SetInstructor(long UId, string Staff, bool value)
        {
            try
            {
                var claim = _context.AspNetUserClaims.Where(m => m.UserId == UId && m.ClaimValue.Contains(Staff)).FirstOrDefault();
                if (value)
                {
                    if (claim == null)
                    {
                        claim = new AspNetUserClaims
                        {
                            UserId = UId,
                            ClaimType = Staff,
                            ClaimValue = Staff
                        };
                        _context.Add(claim);
                    }
                }
                else
                {
                    if (claim != null)
                        _context.Remove(claim);
                }
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<int> UpdateInstructor(InstructorGrid Instructor)
        {
            var User = _context.Instructor.Where(m=>m.Id == Instructor.Id).FirstOrDefault();
            SetInstructor(Instructor.Id, "Instructor Admin", Instructor.IsAdmin);
            SetInstructor(Instructor.Id, "Instructor Staff", Instructor.IsStaff);
            if (User == null)
            {
                User = new EF.Instructor
                {
                    Id = Instructor.Id,
                    Status = true
                };
                _context.Instructor.Add(User);
            }
            else
                User.Status = !User.Status;
            return await _context.SaveChangesAsync();
        }

        public async Task<int> UpdateUniversity(University university)
        {
            var u = _context.University.Find(university.Id);
            if (u != null)
                u.ActiveStatus = !u.ActiveStatus;
            return await _context.SaveChangesAsync();
        }
    }
}
