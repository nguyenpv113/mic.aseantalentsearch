using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.EntityFrameworkCore;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class ReligionController : Controller
    {
        private readonly AseanTalentSearchContext _context;
        public ReligionController(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = page ?? 1;
            var model = await _context.Religion.ToListAsync();
            return View(model.ToPagedList(pageSize, pageIndex));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Religion religion)
        {
            if (ModelState.IsValid)
            {
                if (!await _context.Religion.Where(x => x.Name.Equals(religion.Name, StringComparison.OrdinalIgnoreCase)).AnyAsync())
                {
                    _context.Religion.Add(religion);
                    int result = _context.SaveChanges();
                    if (result == 1)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Religion name is exist. Please try another!");
                }
            }

            return View(religion);
        }

        public IActionResult Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var religion = _context.Religion.Where(x => x.Id == id).FirstOrDefault();
            if (religion == null)
            {
                return NotFound();
            }
            return View(religion);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, Religion religion)
        {
            if (id != religion.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (!await _context.Religion.Where(x => x.Id != id && x.Name.Equals(religion.Name, StringComparison.OrdinalIgnoreCase)).AnyAsync())
                {

                    _context.Religion.Update(religion);
                    int result = await _context.SaveChangesAsync();
                    if (result == 1)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Religion name is exist. Please try another!");
                }
            }
            return View(religion);
        }


        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var religion = await _context.Religion.FindAsync(id);
            if (religion == null)
            {
                return NotFound();
            }
            return View(religion);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var religion = await _context.Religion.FindAsync(id);
            if (religion != null)
            {
                try
                {
                    _context.Religion.Remove(religion);
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    ModelState.AddModelError("", "Religion was used. Can not delete!");
                    return View(religion);
                }
            }
            return RedirectToAction("Index");
        }
    }
}