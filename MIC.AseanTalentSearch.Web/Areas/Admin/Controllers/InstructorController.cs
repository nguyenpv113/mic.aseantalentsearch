using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Areas.Admin.Models.Instructor;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.AspNetCore.Identity;
using MIC.AseanTalentSearch.Web.Areas.Admin.Models.User;
using Microsoft.EntityFrameworkCore;
using Sakura.AspNetCore;
using Microsoft.AspNetCore.Authorization;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class InstructorController : Controller
    {
        #region Field
        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        #endregion

        public InstructorController(AseanTalentSearchContext context,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
        }

        public async Task<IActionResult> Index(string search, int? page = 1)
        {
            int pageSize = 10;
            var data = await new InstructorLogic(_userManager, _roleManager, _context).GetList();
            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(x => x.Username.ToUpper().IndexOf(search.ToUpper()) >= 0).ToList();
            }
            return View(data.OrderBy(x => x.Username).ToPagedList(pageSize, page.Value));
        }

        [HttpPost]
        public async Task<JsonResult> Edit(long Id, bool IsAdmin, bool IsStaff)
        {
            InstructorGrid model = new InstructorGrid
            {
                Id = Id,
                IsAdmin = IsAdmin,
                IsStaff = IsStaff
            };
            if (await new InstructorLogic(_userManager, _roleManager, _context).UpdateInstructor(model) > 0)
                return Json("Update successed!");
            else
                return Json("Update Error!");
        }
    }
}