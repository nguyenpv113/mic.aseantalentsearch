using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.EntityFrameworkCore;
using Sakura.AspNetCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using MIC.AseanTalentSearch.Web.EF.Logical;
using MIC.AseanTalentSearch.Web.Models;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class LanguageController : Controller
    {
        private readonly AseanTalentSearchContext _context;
        private readonly IHostingEnvironment _environment;
        private readonly ILocaleStringResource _localeStringResoure;
        public LanguageController(AseanTalentSearchContext context, IHostingEnvironment hostingEnvironment,
            ILocaleStringResource localeStringResource)
        {
            _context = context;
            _environment = hostingEnvironment;
            _localeStringResoure = localeStringResource;
        }

        public async Task<IActionResult> Index(int? page)
        {
            int pageIndex = page ?? 1;

            var model = await _context.Language.ToListAsync();
            return View(model);
        }

        public ActionResult Create()
        {
            ViewBag.Flag = GetSelectNational("");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Language language)
        {
            if (ModelState.IsValid)
            {
                if (!await _context.Language.Where(x => x.Name.Equals(language.Name, StringComparison.OrdinalIgnoreCase)).AnyAsync())
                {
                    _context.Language.Add(language);
                    int result = _context.SaveChanges();
                    if (result == 1)
                    {
                        var lLangSource = ExportResource();
                        foreach (var item in lLangSource)
                        {
                            var ob = new LocaleStringResource
                            {
                                LanguageId = language.Id,
                                ResourceName = item.ResourceName,
                                Page = item.Page,
                                ResourceValue = item.ResourceName.Replace(".", "")
                            };
                            _localeStringResoure.Insert(ob);
                        }
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Language name is exist. Please try another!");
                }
            }

            return View(language);
        }

        public IActionResult Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var language = _context.Language.Where(x => x.Id == id).FirstOrDefault();
            if (language == null)
            {
                return NotFound();
            }
            ViewBag.Culture = GetSelectNational(language.LanguageCulture);
            ViewBag.Flag = GetGlobalization(language.Image);
            return View(language);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, Language language)
        {
            if (id != language.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                var count = CountCurrentLanguage();
                var cL = _context.Language.Where(x => x.Id == id).FirstOrDefault();
                if (count - 1 <= 0 && cL.IsCurrentLanguage)
                {
                    if (!language.IsCurrentLanguage)
                    {
                        ModelState.AddModelError("IsCurrentLanguage", "Please check Current Language");
                        ViewBag.Culture = GetSelectNational(language.LanguageCulture);
                        ViewBag.Flag = GetGlobalization(language.Image);
                        return View(language);
                    }
                }
                _context.Entry(cL).State = EntityState.Detached;
                if (!await _context.Language.Where(x => x.Id != id && x.Name.Equals(language.Name, StringComparison.OrdinalIgnoreCase)).AnyAsync())
                {
                    _context.Language.Update(language);
                    int result = await _context.SaveChangesAsync();
                    if (result == 1)
                    {
                        if (language.IsCurrentLanguage)
                        {
                            var model = _context.Language.ToList();
                            model.Remove(language);
                            foreach (var item in model)
                            {
                                item.IsCurrentLanguage = false;
                                _context.Language.Update(item);
                            }
                            _context.SaveChanges();
                        }
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                    }
                }
                else
                {
                    ModelState.AddModelError("Name", "Language name is exist. Please try another!");
                }
            }
            return View(language);
        }
        
        private int CountCurrentLanguage()
        {
            return _context.Language.Count(x => x.IsCurrentLanguage);
        }
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var language = await _context.Language.FindAsync(id);
            if (language == null)
            {
                return NotFound();
            }
            return View(language);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var language = await _context.Language.FindAsync(id);
            if (language != null)
            {
                try
                {
                    _context.Language.Remove(language);
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    ModelState.AddModelError("", "Language was used. Can not delete!");
                    return View(language);
                }
            }
            return RedirectToAction("Index");
        }

        public List<SelectListItem> GetSelectNational(string selected = null)
        {
            var owners = System.IO.File.ReadAllLines(@"wwwroot\areas\admin\LanguageCulture.txt");

            var lCulture = owners.Select(x => x.Replace("<tr><td data-th=\"Language Culture Name\">", "")).ToList();
            for (int i = 0; i < lCulture.Count; i++)
            {
                var cultule = lCulture[i].Substring(0, lCulture[i].IndexOf("</td><td data-th=\"Display Name\">")).Trim();
                var national = lCulture[i].Replace(cultule + " </td><td data-th=\"Display Name\">", "");
                national = national.Substring(0, national.IndexOf("</td><td data-th=\"Culture Code\">")) + "( " + cultule + " )";
                lCulture[i] = national;
            }


            return lCulture.Select(
                    c => new SelectListItem()
                    {
                        Text = c,
                        Value = c.Substring(c.IndexOf("( "), c.Length - c.IndexOf("( ")).Replace(" )", "").Replace("( ", ""),
                        Selected = c.Substring(c.IndexOf("( "), c.Length - c.IndexOf("( ")).Replace(" )", "").Replace("( ", "") == selected ? true : false
                    }
                ).ToList();
        }

        [HttpPost]
        public JsonResult GetGlobalization()
        {
            var listImg = new List<string>();
            foreach (string file in Directory.EnumerateFiles(Path.Combine(_environment.WebRootPath,
            "areas/admin/images/flags"),
            "*.png",
            SearchOption.AllDirectories)
            )
            {
                listImg.Add(file.Substring(file.LastIndexOf("\\"), file.Length - file.LastIndexOf("\\")).Replace("\\", ""));
            }


            var rs = listImg.Select(
                    c => new SelectListItem()
                    {
                        Value = "areas/admin/images/flags/" + c,
                        Text = c.Substring(c.LastIndexOf("-") + 1)
                    }
                ).ToList();
            return Json(rs);
        }
        public List<SelectListItem> GetGlobalization(string selected = null)
        {
            var listImg = new List<string>();
            foreach (string file in Directory.EnumerateFiles(Path.Combine(_environment.WebRootPath,
            "areas/admin/images/flags"),
            "*.png",
            SearchOption.AllDirectories)
            )
            {
                listImg.Add(file.Substring(file.LastIndexOf("\\"), file.Length - file.LastIndexOf("\\")).Replace("\\", ""));
            }


            var rs = listImg.Select(
                    c => new SelectListItem()
                    {
                        Value = "areas/admin/images/flags/" + c,
                        Text = c.Substring(c.LastIndexOf("-") + 1),
                        Selected = "areas/admin/images/flags/" + c == selected ? true : false
                    }
                ).ToList();
            return rs;
        }

        #region LocaleStringResource
        [HttpGet]
        public JsonResult GetListLocale(int Id)
        {
            var model = _localeStringResoure.GetAllByLanguage(Id);
            return Json(new
            {
                draw = 1,
                recordsTotal = model.Count,
                recordsFiltered = model.Count,
                data = model
            });
        }

        [HttpGet]
        public IActionResult AddResource(int Id)
        {
            var model = new LocaleStringResource();
            model.LanguageId = Id;
            return PartialView("CreateResource", model);
        }

        [HttpPost]
        public JsonResult AddResource(LocaleStringResource model)
        {
            if (ModelState.IsValid)
            {
                if (_localeStringResoure.Insert(model) > 0)
                {
                    var listResource = _localeStringResoure.GetAllByLanguage(model.LanguageId);
                    return Json(new
                    {
                        success = true,
                        resourceId = model.Id,
                        resourceValue = model.ResourceValue
                    });
                }
            }
            return Json(null);
        }


        public IActionResult EditResource(int Id)
        {
            var model = _localeStringResoure.GetLocaleString(Id);
            return PartialView("EditResource", model);
        }

        [HttpPost]
        public JsonResult EditResource(LocaleStringResource model)
        {
            if (_localeStringResoure.Update(model) > 0)
            {
                return Json(new
                {
                    success = true,
                    resourceId = model.Id,
                    resourceValue = model.ResourceValue
                });
            }
            return Json(null);
        }

        public IActionResult DeleteResource(int Id)
        {
            var model = _localeStringResoure.GetLocaleString(Id);
            return PartialView("DeleteResource", model);
        }

        [HttpPost]
        public JsonResult DeleteResource(LocaleStringResource model)
        {
            if (_localeStringResoure.DeleteById(model.Id) > 0)
            {
                return Json(new
                {
                    success = true,
                    resourceId = model.Id,
                });
            }
            return Json(null);
        }

        public List<LocaleStringResource> ExportResource()
        {
            return _localeStringResoure.GetAllByLanguage(_context.Language.FirstOrDefault().Id);
        }

        #endregion
    }
}