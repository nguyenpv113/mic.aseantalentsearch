using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MIC.AseanTalentSearch.Web.EF;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class CandidateController : Controller
    {
        private readonly AseanTalentSearchContext _context;

        public CandidateController(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public IActionResult Index(string search, int? page = 1)
        {
            int pageSize = 10;
            var data = _context.Candidate.ToList();
            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(x => x.FullName?.ToUpper().IndexOf(search.ToUpper()) >= 0).ToList();
            }
            return View(data.OrderBy(x => x.FullName).ToPagedList(pageSize, page.Value));
        }

        [HttpPost]
        public async Task<JsonResult> Edit(long Id, bool ActiveStatus)
        {
            try
            {
                var candidate = _context.Candidate.Where(x => x.Id == Id).FirstOrDefault();
                if (candidate != null)
                {
                    candidate.ActiveStatus = ActiveStatus;
                    await _context.SaveChangesAsync();
                    return Json("Update successed!");
                }
            }
            catch
            {
            }
            return Json("Update Error!");
        }
    }
}