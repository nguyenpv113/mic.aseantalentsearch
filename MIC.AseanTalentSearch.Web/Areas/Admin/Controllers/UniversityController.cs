using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.EntityFrameworkCore;
using Sakura.AspNetCore;
using Microsoft.AspNetCore.Http;
using MIC.AseanTalentSearch.Web.Settings;
using MIC.AseanTalentSearch.Web.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{

    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class UniversityController : Controller
    {
        private readonly AseanTalentSearchContext _context;
        private readonly PathUpLoad _pathUpLoad;
        private readonly IImageService _imageService;
        private IHostingEnvironment _env;

        public UniversityController(AseanTalentSearchContext context, IImageService imageService, IHostingEnvironment env, IOptions<PathUpLoad> pathUpload)
        {
            _context = context;
            _imageService = imageService;
            _env = env;
            _pathUpLoad = pathUpload.Value;
        }


        public IActionResult FindProvinceByCountryId(int? id)
        {
            List<SelectListItem> states = new List<SelectListItem>();
            if (id != null)
            {
                foreach (var item in _context.Location.Where(m => m.ParentId == id))
                {
                    states.Add(new SelectListItem { Text = item.Name, Value = item.Id.ToString() });
                }
            }
            return Json(states.OrderBy(x=>x.Text));
        }

        [HttpGet]
        public IActionResult Index(int? page, string search)
        {
            search = search ?? "";
            int pageIndex = page ?? 1;
            var rs = _context.University.Include(x => x.Location)
                .Where(x=>x.Name.Contains(search))
                .OrderByDescending(x => x.ActiveStatus).ThenBy(x => x.Name);
             var model = rs.ToPagedList(10, pageIndex);

            foreach (var item in model)
            {
                if (item.Location.ParentId != null)
                {
                    item.Location = _context.Location.Find(item.Location.ParentId.Value);
                }
            }

            return View(model);
        }

        public ActionResult Create()
        {
            var location = _context.Location.OrderBy(x => x.Name).Where(m => m.Type == 1).ToList();
            ViewBag.Locations = new SelectList(location, "Id", "Name");
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(University university, IFormFile logo, int province)
        {
            List<Location> location = null;
            if (ModelState.IsValid)
            {
                if (province == 0)
                {
                    ModelState.AddModelError("", "Please select nation and province");
                    location = _context.Location.Where(m => m.Type == 1).ToList();
                    ViewBag.Locations = new SelectList(location, "Id", "Name");
                    return View(university);
                }

                if (!await _context.University.Where(x => x.Name.Equals(university.Name)).AnyAsync())
                {

                    AspNetUsers currentUser = _context.AspNetUsers.Where(x => x.UserName.Equals(User.Identity.Name)).FirstOrDefault();
                    university.CreatedBy = currentUser.Id;
                    university.CreatedDate = DateTime.Now;
                    string imagePath = String.Empty;

                    university.LocationId = province;

                    if (university.ActiveStatus.Value)
                    {
                        university.ActiveBy = currentUser.Id;
                        university.ActiveDate = DateTime.Now;
                    }

                    //validate and upload logo
                    if (logo != null)
                    {
                        if (_imageService.Validate(logo))
                        {
                            try
                            {
                                imagePath = await _imageService.SaveAsync(logo, _pathUpLoad.FrontImage);
                                university.Logo = _pathUpLoad.FrontImage + imagePath;
                            }
                            catch (Exception e)
                            {
                                ModelState.AddModelError("", "Sorry an error occurred saving to the database, please try again");
                                location = _context.Location.Where(m => m.Type == 1).ToList();
                                ViewBag.Locations = new SelectList(location, "Id", "Name");
                                return View(university);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "The logo file must be gif, png, jpeg or jpg and less than 2MB in size");
                            location = _context.Location.Where(m => m.Type == 1).ToList();
                            ViewBag.Locations = new SelectList(location, "Id", "Name");
                            return View(university);
                        }


                    }

                    try
                    {
                        await _context.University.AddAsync(university);
                        _context.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    catch
                    {
                        ModelState.AddModelError("", "Sorry an error occurred saving to the database, please try again");
                    }


                }
                else
                {
                    ModelState.AddModelError("", "University name is exist. Please try another!");
                }
            }
            location = _context.Location.Where(m => m.Type == 1).ToList();
            ViewBag.Locations = new SelectList(location, "Id", "Name");
            return View(university);
        }


        public IActionResult Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var university = _context.University.Include(m => m.Location).Where(x => x.Id == id).FirstOrDefault();
            if (university == null)
            {
                return NotFound();
            }

            int? currentLocationId = university.LocationId;
            int nationIdSelected = 0;
            int provinceIdSelected = 0;


            if (currentLocationId != null)
            {
                nationIdSelected = university.Location.ParentId ?? 0;
                provinceIdSelected = currentLocationId ?? 0;
            }

            var nationLocations = _context.Location.OrderBy(x => x.Name).Where(m => m.Type == 1).ToList();
            ViewBag.NationLocations = new SelectList(nationLocations, "Id", "Name", nationIdSelected);

            var provinceLocations = _context.Location.OrderBy(x=>x.Name).Where(m => m.ParentId == nationIdSelected).ToList();
            ViewBag.ProvinceLocations = new SelectList(provinceLocations, "Id", "Name", provinceIdSelected);



            return View(university);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, University university, IFormFile logo, int province)
        {
            if (id != university.Id)
            {
                return NotFound();
            }

            ///for location select option
            int? currentLocationId = university.LocationId;
            int nationIdSelected = 0;
            int provinceIdSelected = 0;


            if (currentLocationId != null)
            {
                nationIdSelected = university.Location.ParentId ?? 0;
                provinceIdSelected = currentLocationId ?? 0;
            }

            List<Location> nationLocations = _context.Location.Where(m => m.Type == 1).ToList();
            ViewBag.NationLocations = new SelectList(nationLocations, "Id", "Name", nationIdSelected);

            List<Location> provinceLocations = _context.Location.Where(m => m.ParentId == nationIdSelected).ToList();
            ViewBag.ProvinceLocations = new SelectList(provinceLocations, "Id", "Name", provinceIdSelected);

            ///





            if (ModelState.IsValid)
            {
                if (province == 0)
                {
                    ModelState.AddModelError("", "Please select nation and province");

                    nationLocations = _context.Location.Where(m => m.Type == 1).ToList();
                    ViewBag.NationLocations = new SelectList(nationLocations, "Id", "Name", nationIdSelected);

                    provinceLocations = _context.Location.Where(m => m.ParentId == nationIdSelected).ToList();
                    ViewBag.ProvinceLocations = new SelectList(provinceLocations, "Id", "Name", provinceIdSelected);
                    return View(university);
                }
                if (!await _context.University.Where(x => x.Id != id && x.Name.Equals(university.Name)).AnyAsync())
                {
                    AspNetUsers currentUser = _context.AspNetUsers.Where(x => x.UserName.Equals(User.Identity.Name)).FirstOrDefault();
                    university.ModifiedBy = currentUser.Id;
                    university.ModifiedDate = DateTime.Now;
                    university.LocationId = province;

                    //check update logo
                    if (logo != null)
                    {
                        string imagePath = string.Empty;
                        if (_imageService.Validate(logo))
                        {
                            try
                            {
                                //upload new logo
                                imagePath = await _imageService.SaveAsync(logo, _pathUpLoad.FrontImage);

                                //delete old logo
                                try
                                {
                                    System.IO.File.Delete(_env.WebRootPath + university.Logo);
                                }
                                catch
                                {

                                }



                                //set new logo
                                university.Logo = _pathUpLoad.FrontImage + imagePath;


                            }
                            catch (Exception e)
                            {
                                ModelState.AddModelError("", "Sorry an error occurred saving to the database, please try again");
                                nationLocations = _context.Location.Where(m => m.Type == 1).ToList();
                                ViewBag.NationLocations = new SelectList(nationLocations, "Id", "Name", nationIdSelected);

                                provinceLocations = _context.Location.Where(m => m.ParentId == nationIdSelected).ToList();
                                ViewBag.ProvinceLocations = new SelectList(provinceLocations, "Id", "Name", provinceIdSelected);
                                return View(university);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "The logo file must be gif, png, jpeg or jpg and less than 2MB in size");
                            nationLocations = _context.Location.Where(m => m.Type == 1).ToList();
                            ViewBag.NationLocations = new SelectList(nationLocations, "Id", "Name", nationIdSelected);

                            provinceLocations = _context.Location.Where(m => m.ParentId == nationIdSelected).ToList();
                            ViewBag.ProvinceLocations = new SelectList(provinceLocations, "Id", "Name", provinceIdSelected);
                            return View(university);
                        }


                    }
                    try
                    {
                        _context.University.Update(university);
                        await _context.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    catch
                    {
                        ModelState.AddModelError("", "Sorry an error occurred saving to the database, please try again");
                        //error
                    }
                }
                else
                {
                    ModelState.AddModelError("", "University name is exist. Please try another!");
                }
            }
            nationLocations = _context.Location.Where(m => m.Type == 1).ToList();
            ViewBag.NationLocations = new SelectList(nationLocations, "Id", "Name", nationIdSelected);

            provinceLocations = _context.Location.Where(m => m.ParentId == nationIdSelected).ToList();
            ViewBag.ProvinceLocations = new SelectList(provinceLocations, "Id", "Name", provinceIdSelected);
            return View(university);
        }

        [HttpPost]
        public async Task<JsonResult> Update(int Id, bool IsActive)
        {
            University model = new University
            {
                Id = Id,
                ActiveStatus = IsActive
            };
            var u = _context.University.Find(Id);
            if (u != null)
                u.ActiveStatus = !u.ActiveStatus;
            if (await _context.SaveChangesAsync() > 0)
                return Json("Update successed!");
            else
                return Json("Update Error!");
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var university = await _context.University.FindAsync(id);
            if (university == null)
            {
                return NotFound();
            }
            return View(university);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var university = await _context.University.FindAsync(id);
            if (university != null)
            {
                try
                {
                    _context.University.Remove(university);
                    await _context.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", "University was used. Can not delete!");
                    return View(university);
                }


            }
            return RedirectToAction("Index");
        }

    }
}