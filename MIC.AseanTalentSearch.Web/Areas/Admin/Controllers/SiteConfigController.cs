using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.EntityFrameworkCore;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{

    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class SiteConfigController : Controller
    {

        private readonly AseanTalentSearchContext _context;

        public SiteConfigController(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = page ?? 1;

            var model = await _context.SiteConfig.ToListAsync();
            return View(model.ToPagedList(pageSize, pageIndex));
        }


        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(SiteConfig config)
        {

            if (ModelState.IsValid)
            {
                if (!await _context.SiteConfig.Where(x => x.Key.Equals(config.Key, StringComparison.OrdinalIgnoreCase)).AnyAsync())
                {
                    try
                    {
                        _context.SiteConfig.Add(config);
                        await _context.SaveChangesAsync();
                        return RedirectToAction("Index");

                    }
                    catch
                    {
                        ModelState.AddModelError("", "Sorry an error occurred saving to the database, please try again");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Key is exist. Please try another!");
                }
            }
            return View(config);
        }




        public async Task<IActionResult> Edit(string key)
        {
            if (key == null)
            {
                return NotFound();
            }

            var config = await _context.SiteConfig.FindAsync(key);
            if (config == null)
            {
                return NotFound();
            }
            return View(config);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string key, SiteConfig config)
        {
            if (key != config.Key)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.SiteConfig.Update(config);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Sorry an error occurred saving to the database, please try again");
                }
            }
            return View(config);
        }
    }
}