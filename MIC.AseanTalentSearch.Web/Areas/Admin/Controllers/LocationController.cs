using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.Areas.Admin.Models.LocationView;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class LocationController : Controller
    {
        enum LocationType
        {
            Nation = 1,
            Province = 2,
            District = 3
        }

        private readonly AseanTalentSearchContext _context;
        public LocationController(AseanTalentSearchContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            //var model = await _context.Location.OrderBy(x=>x.Name).ToListAsync();
            //return View(model);
            List<AdminLocationView> model = new List<AdminLocationView>();
            var nation = await _context.Location.OrderBy(x => x.Name).Where(x => x.Type == (int)LocationType.Nation).ToListAsync();
            foreach (var locationNation in nation)
            {
                AdminLocationView adLocationNation = new AdminLocationView();
                adLocationNation.Location = locationNation;
                var province = await _context.Location.OrderBy(x => x.Name).Where(x => (x.ParentId == locationNation.Id) && x.Type == (int)LocationType.Province).ToListAsync();

                List<AdminLocationView> listProvince = new List<AdminLocationView>();
                foreach (var locationProvince in province)
                {
                    AdminLocationView adLocationProvince = new AdminLocationView();
                    adLocationProvince.Location = locationProvince;
                    var distric = await _context.Location.OrderBy(x => x.Name).Where(x => (x.ParentId == locationProvince.Id) && x.Type == (int)LocationType.District).ToListAsync();

                    List<AdminLocationView> listDistric = new List<AdminLocationView>();
                    foreach (var locationDistric in distric)
                    {
                        AdminLocationView adLocationDistric = new AdminLocationView();
                        adLocationDistric.Location = locationDistric;
                        listDistric.Add(adLocationDistric);
                    }

                    adLocationProvince.Child = listDistric;
                    listProvince.Add(adLocationProvince);
                }

                adLocationNation.Child = listProvince;
                model.Add(adLocationNation);
            }
            // var province;
            // var district;

            return View(model);
        }




        public IActionResult AddLocation(int? id)
        {
            if (id != null)
            {
                var model = _context.Location.Find(id);
                // ViewData["parentLocation"] = model;
                return PartialView(model);
            }
            return PartialView(null);
        }
        [HttpPost]
        public async Task<ActionResult> AddSave(int pId, Location location)
        {
            //nation
            if (pId == 0)
            {
                location.ParentId = null;
                location.Type = 1;
            }
            else
            {
                //province or  district
                location.ParentId = pId;
                location.Type = _context.Location.Find(pId).Type + 1;
            }

            if (ModelState.IsValid)
            {
                if (!await _context.Location.Where(x => x.Name.Equals(location.Name, StringComparison.OrdinalIgnoreCase) && x.ParentId == location.ParentId).AnyAsync())
                {
                    _context.Location.Add(location);

                    try
                    {
                        _context.SaveChanges();
                        return Json(new { success = true });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e.Message);
                    }

                }
                else
                {
                    ModelState.AddModelError("", "Location name is exist. Please try another!");
                }
            }


            return PartialView("AddLocation", _context.Location.Find(pId));
        }




        public IActionResult EditForm(int? id)
        {
            var model = _context.Location.Find(id);

            if (model.Type == 2)
            {
                var parentLocations = _context.Location.Where(m => m.Type == 1).OrderBy(x=>x.Name).ToList();

                ViewBag.ParentLocations = new SelectList(parentLocations, "Id", "Name", model.ParentId);
            }
            else if (model.Type == 3)
            {
                var n = _context.Location.Include(m => m.Parent).SingleOrDefault(m => m.Id == model.ParentId).Parent.Id;
                //var province = _context.Location.Find(model.ParentId);
                //var nation = _context.Location.Find(province.ParentId);

                var parentLocations = _context.Location.Where(m => m.Type == 2&&m.ParentId==n).OrderBy(x => x.Name).ToList();

                ViewBag.ParentLocations = new SelectList(parentLocations, "Id", "Name", model.ParentId);
            }

           

            return PartialView(model);
        }

        [HttpPost]
        public async Task<ActionResult> EditSave(int id, int? parentIdEdit, Location location)
        {

            if (id != location.Id)
            {
                return NotFound();
            }

            if (location.Type > 1 && (parentIdEdit == null || parentIdEdit == 0))
            {
                if (location.Type == 2)
                {
                    ModelState.AddModelError("validateParent", "Please select Nation location.Try again!");
                }
                else if (location.Type == 3)
                {
                    ModelState.AddModelError("validateParent", "Please select Province location.Try again!");
                }              
            }
            else
            {
                if (ModelState.IsValid)
                {
                    if (!await _context.Location.Where(x => x.Id != id && x.Name.Equals(location.Name, StringComparison.OrdinalIgnoreCase) && x.ParentId == location.ParentId).AnyAsync())
                    {
                        if (parentIdEdit != null && parentIdEdit > 0)
                        {
                            location.ParentId = parentIdEdit;
                        }

                        _context.Location.Update(location);
                        int result = await _context.SaveChangesAsync();
                        if (result == 1)
                        {
                            return Json(new { success = true, dataId = location.Id });
                        }
                        else
                        {
                            //error
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Location name is exist. Please try another!");
                    }
                }
            }
            var parentLocations = _context.Location.Where(m => m.Type == 1).OrderBy(x => x.Name).ToList();

            ViewBag.ParentLocations = new SelectList(parentLocations, "Id", "Name", location.ParentId);

            return PartialView("EditForm", location);
        }


        public IActionResult DeleteLocation(int? id)
        {

            return PartialView(_context.Location.Find(id));
        }

        public async Task<ActionResult> DeleteConfirmed(int? id)
        {
            var location = await _context.Location.FindAsync(id);
            if (location != null)
            {
                try
                {
                    _context.Location.Remove(location);
                    await _context.SaveChangesAsync();
                    return Json(new { success = true, dataId = location.Id });
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", "Location " + location.Name + " was used. You can't delete");
                    return PartialView("DeleteLocation", _context.Location.Find(id));
                }

            }
            return Json(new { success = false, dataId = location.Id });
        }


        //public ActionResult Create()
        //{
        //    return View();
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create(Location location)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        if (!await _context.Location.Where(x => x.Name.Equals(location.Name, StringComparison.OrdinalIgnoreCase) && x.ParentId == location.ParentId).AnyAsync())
        //        {
        //            _context.Location.Add(location);
        //            int result = _context.SaveChanges();
        //            if (result == 1)
        //            {
        //                return RedirectToAction("Index");
        //            }
        //            else
        //            {
        //                //error
        //            }
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", "Location name is exist. Please try another!");
        //        }
        //    }

        //    return View(location);
        //}

        //public IActionResult Edit(long? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }
        //    var location = _context.Location.Where(x => x.Id == id).FirstOrDefault();
        //    if (location == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(location);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(long id, Location location)
        //{
        //    if (id != location.Id)
        //    {
        //        return NotFound();
        //    }
        //    if (ModelState.IsValid)
        //    {
        //        if (!await _context.Location.Where(x => x.Id != id && x.Name.Equals(location.Name, StringComparison.OrdinalIgnoreCase) && x.ParentId == location.ParentId).AnyAsync())
        //        {

        //            _context.Location.Update(location);
        //            int result = await _context.SaveChangesAsync();
        //            if (result == 1)
        //            {
        //                return RedirectToAction("Index");
        //            }
        //            else
        //            {
        //                //error
        //            }
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", "Location name is exist. Please try another!");
        //        }
        //    }
        //    return View(location);
        //}

        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }
        //    var location = await _context.Location.FindAsync(id);
        //    if (location == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(location);
        //}

        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var location = await _context.Location.FindAsync(id);
        //    if (location != null)
        //    {
        //        _context.Location.Remove(location);
        //        await _context.SaveChangesAsync();

        //    }
        //    return RedirectToAction("Index");
        //}
    }
}