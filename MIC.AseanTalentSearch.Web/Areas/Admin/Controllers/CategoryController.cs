using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MIC.AseanTalentSearch.Web.EF;
using Sakura.AspNetCore;
using MIC.AseanTalentSearch.Web.Areas.Admin.Models.CategoryView;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class CategoryController : Controller
    {
        private readonly AseanTalentSearchContext _context;

        public CategoryController(AseanTalentSearchContext context)
        {
            _context = context;
        }
        public IActionResult Index(int? page)
        {
            int pageIndex = page ?? 1;

            var categoryViews = new List<AdminCategoryView>();
            var categoryParent = _context.Category.Where(x => x.ParentId == null).OrderBy(x => x.Name);
            foreach (var category in categoryParent)
            {
                AdminCategoryView adCategory = new AdminCategoryView();
                adCategory.Category = category;
                adCategory.Child = _context.Category.Where(x => x.ParentId == category.Id).OrderBy(x => x.Name).ToList();
                categoryViews.Add(adCategory);
            }


            return View(categoryViews);
        }


        public ActionResult Create(int? id)
        {
            Category parentCategory = null;
            if (id != null)
            {
                parentCategory = _context.Category.Find(id);
            }

            var parentCategories = _context.Category.Where(m => m.ParentId == null).OrderBy(x => x.Name).ToList();
            int pId = 0;
            if (parentCategory != null)
            {
                pId = parentCategory.Id;
            }
            ViewBag.ParentCategories = new SelectList(parentCategories, "Id", "Name", pId);

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Category category)
        {
            if (ModelState.IsValid)
            {
                if (!await _context.Category.Where(x => x.Name.Equals(category.Name, StringComparison.OrdinalIgnoreCase)).AnyAsync())
                {
                    if (category.ParentId != null && category.ParentId < 1)
                    {
                        category.ParentId = null;
                    }
                    try
                    {
                        _context.Category.Add(category);
                        int result = _context.SaveChanges();

                        return RedirectToAction("Index");
                    }
                    catch
                    {
                        ModelState.AddModelError("", "Sorry an error occurred saving to the database, please try again");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Category name is exist. Please try another!");
                }
            }
            return View(category);
        }
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            var model = _context.Category.Find(id);
            if (model == null)
            {
                return NotFound();
            }
            var parentCategories = _context.Category.Where(m => m.ParentId == null && m.Id != model.Id).OrderBy(x => x.Name).ToList();
            int? pId = 0;
            if (model != null && model.ParentId != null)
            {
                pId = model.ParentId;
            }
            ViewBag.ParentCategories = new SelectList(parentCategories, "Id", "Name", pId);
            return View(model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, Category category)
        {
            if (id != category.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (!await _context.Category.Where(x => x.Id != id && x.Name.Equals(category.Name, StringComparison.OrdinalIgnoreCase)).AnyAsync())
                {
                    if (category.ParentId != null && category.ParentId < 1)
                    {
                        category.ParentId = null;
                    }
                    try
                    {
                        _context.Category.Update(category);
                        _context.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    catch
                    {
                        ModelState.AddModelError("", "Sorry an error occurred saving to the database, please try again");
                    }                
                }
                else
                {
                    ModelState.AddModelError("", "Category name is exist. Please try another!");
                }
            }
            return View(category);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var category = await _context.Category.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var category = await _context.Category.FindAsync(id);
            if (category != null)
            {
                try
                {
                    _context.Category.Remove(category);
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    ModelState.AddModelError("", "Category was used. Can not delete!");
                    return View(category);
                }


            }
            return RedirectToAction("Index");
        }
    }
}