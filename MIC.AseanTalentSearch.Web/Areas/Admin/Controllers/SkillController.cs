using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.EntityFrameworkCore;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class SkillController : Controller
    {

        private readonly AseanTalentSearchContext _context;
        public SkillController(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(int? page)
        {
            int pageIndex = page ?? 1;
            var model = await _context.Skill.ToListAsync();
            return View(model);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Skill skill)
        {
            if (ModelState.IsValid)
            {
                if (!await _context.Skill.Where(x => x.Name.Equals(skill.Name, StringComparison.OrdinalIgnoreCase)).AnyAsync())
                {

                    skill.CreatedDate = DateTime.Now;
                    _context.Skill.Add(skill);
                    int result = _context.SaveChanges();
                    if (result == 1)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Skill name is exist. Please try another!");
                }
            }

            return View(skill);
        }


        public IActionResult Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var skill = _context.Skill.Where(x => x.Id == id).FirstOrDefault();
            if (skill == null)
            {
                return NotFound();
            }
            return View(skill);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, Skill skill)
        {
            if (id != skill.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (!await _context.Skill.Where(x => x.Id != id && x.Name.Equals(skill.Name, StringComparison.OrdinalIgnoreCase)).AnyAsync())
                {
                    skill.ModifiedDate = DateTime.Now;
                    _context.Skill.Update(skill);
                    int result = await _context.SaveChangesAsync();
                    if (result == 1)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Skill name is exist. Please try another!");
                }
            }
            return View(skill);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var skill = await _context.Skill.FindAsync(id);
            if (skill == null)
            {
                return NotFound();
            }
            return View(skill);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var skill = await _context.Skill.FindAsync(id);
            if (skill != null)
            {
                try
                {
                    _context.Skill.Remove(skill);
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    ModelState.AddModelError("", "Skill was used. Can not delete!");
                    return View(skill);
                }
            }
            return RedirectToAction("Index");
        }
    }
}