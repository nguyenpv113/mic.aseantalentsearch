﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class JobLevelController : Controller
    {

        private readonly AseanTalentSearchContext _context;
        public JobLevelController(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = page ?? 1;
            var model = await _context.JobLevel.ToListAsync();
            return View(model.ToPagedList(pageSize, pageIndex));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(JobLevel jobLevel)
        {
            if (ModelState.IsValid)
            {
                if (!await _context.JobLevel.Where(x => x.JobLevelName.Equals(jobLevel.JobLevelName)).AnyAsync())
                {
                    jobLevel.CreatedDate = DateTime.Now;
                    _context.JobLevel.Add(jobLevel);
                    int result = _context.SaveChanges();
                    if (result == 1)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                    }
                }
                else
                {
                    ModelState.AddModelError("", "JobLevel name is exist. Please try another!");
                }
            }

            return View(jobLevel);
        }


        public IActionResult Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var jobLevel = _context.JobLevel.Where(x => x.JobLevelId == id).FirstOrDefault();
            if (jobLevel == null)
            {
                return NotFound();
            }
            return View(jobLevel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long jobLevelId, JobLevel jobLevel)
        {
            if (jobLevelId != jobLevel.JobLevelId)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (!await _context.JobLevel.Where(x => x.JobLevelId != jobLevelId && x.JobLevelName.Equals(jobLevel.JobLevelName)).AnyAsync())
                {
                    jobLevel.ModifiedDate = DateTime.Now;
                    _context.JobLevel.Update(jobLevel);
                    int result = await _context.SaveChangesAsync();
                    if (result == 1)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                    }
                }
                else
                {
                    ModelState.AddModelError("", "JobLevel name is exist. Please try another!");
                }
            }
            return View(jobLevel);
        }


        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var jobLevel = await _context.JobLevel.FindAsync(id);
            if (jobLevel == null)
            {
                return NotFound();
            }
            return View(jobLevel);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int jobLevelId)
        {
            var jobLevel = await _context.JobLevel.FindAsync(jobLevelId);
            if (jobLevel != null)
            {
                try
                {
                    _context.JobLevel.Remove(jobLevel);
                    await _context.SaveChangesAsync();
                }
                catch
                {

                    ModelState.AddModelError("", "JobLevel was used. Can not delete!");
                    return View(jobLevel);
                }
            }
            return RedirectToAction("Index");
        }

    }
}