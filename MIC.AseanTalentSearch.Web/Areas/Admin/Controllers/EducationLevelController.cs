using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class EducationLevelController : Controller
    {
        private readonly AseanTalentSearchContext _context;

        public EducationLevelController(AseanTalentSearchContext context)
        {
            _context = context;
        }

        // GET: Admin/EducationLevel
        public async Task<IActionResult> Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = page ?? 1;
            return View(await _context.EducationLevel.ToPagedListAsync(pageSize, pageIndex));
        }

        // GET: Admin/EducationLevel/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var educationLevel = await _context.EducationLevel
                .SingleOrDefaultAsync(m => m.Id == id);
            if (educationLevel == null)
            {
                return NotFound();
            }

            return View(educationLevel);
        }

        // GET: Admin/EducationLevel/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/EducationLevel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(EducationLevel educationLevel)
        {
            if (ModelState.IsValid)
            {
                if (!await _context.EducationLevel.Where(x => x.Name.Equals(educationLevel.Name)).AnyAsync())
                {
                    _context.EducationLevel.Add(educationLevel);
                    int result = _context.SaveChanges();
                    if (result == 1)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Education level name is exist, please try another!");
                }
            }
            return View(educationLevel);
        }

        // GET: Admin/EducationLevel/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var educationLevel = await _context.EducationLevel.SingleOrDefaultAsync(m => m.Id == id);
            if (educationLevel == null)
            {
                return NotFound();
            }
            return View(educationLevel);
        }

        // POST: Admin/EducationLevel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, EducationLevel educationLevel)
        {
            if (id != educationLevel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (!await _context.EducationLevel.Where(x => x.Name.Equals(educationLevel.Name)).AnyAsync())
                {
                    _context.EducationLevel.Update(educationLevel);
                    int result = await _context.SaveChangesAsync();
                    if (result == 1)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //Error
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Education level name is exist, please try another!");
                }
            }
            return View(educationLevel);
        }

        // GET: Admin/EducationLevel/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var educationLevel = await _context.EducationLevel.FindAsync(id);
            if (educationLevel == null)
            {
                return NotFound();
            }
            return View(educationLevel);
        }

        // POST: Admin/EducationLevel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var educationLevel = await _context.EducationLevel.FindAsync(id);
            if (educationLevel != null)
            {
                try
                {
                    _context.EducationLevel.Remove(educationLevel);
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    ModelState.AddModelError("", "EducationLevel was used. Can not delete!");
                    return View(educationLevel);
                }
              
            }
            return RedirectToAction("Index");
        }

    }
}