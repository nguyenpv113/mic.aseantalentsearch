using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.Areas.Admin.Models;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class RoleController : Controller
    {
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly AseanTalentSearchContext _context;

        public RoleController(RoleManager<ApplicationRole> roleManager, AseanTalentSearchContext context)
        {
            _roleManager = roleManager;
            _context = context;
        }

        public async Task<IActionResult> Index(int?page)
        {
            int pageSize = 10;
            int pageIndex = page ?? 1;

            var model =await _context.AspNetRoles.ToListAsync();
            return View(model.ToPagedList(pageSize, pageIndex));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(AspNetRoles role)
        {
            ApplicationRole roleToCreate = new ApplicationRole {Name = role.Name};
            if (ModelState.IsValid)
            {
                if (!await _roleManager.RoleExistsAsync(role.Name))
                {
                    IdentityResult roleResult = await _roleManager.CreateAsync(roleToCreate);
                    if (roleResult.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        foreach (IdentityError error in roleResult.Errors)
                        {
                            ModelState.AddModelError("", $"Error code: {error.Code}, Description: {error.Description}");
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Role is exist. Please try another!");
                }
            }
            return View(role);
        }

        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationRole = await _roleManager.FindByIdAsync(id.ToString());
            if (applicationRole == null)
            {
                return NotFound();
            }
            AspNetRoles role = new AspNetRoles
            {
                Name = applicationRole.Name
            };
            return View(role);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, AspNetRoles role)
        {
            if (id != role.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    
                    if (!await _roleManager.RoleExistsAsync(role.Name))
                    {
                        ApplicationRole roleToUpdate = await _roleManager.FindByIdAsync(id.ToString());
                        roleToUpdate.Name = role.Name;

                        IdentityResult roleResult = await _roleManager.UpdateAsync(roleToUpdate);

                        if (roleResult.Succeeded)
                        {
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            foreach (IdentityError error in roleResult.Errors)
                            {
                                ModelState.AddModelError("", $"Error code: {error.Code}, Description: {error.Description}");
                            }
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Role is exist. Please try another!");
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RoleExists(role.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View(role);
        }

        // GET: Admin/Companies/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ApplicationRole applicationRole = await _roleManager.FindByIdAsync(id.ToString());
            if (applicationRole == null)
            {
                return NotFound();
            }

            return View(applicationRole);
        }

        // POST: Admin/Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            ApplicationRole applicationRole = await _roleManager.FindByIdAsync(id.ToString());
            if (applicationRole != null)
            {
                IdentityResult roleRuslt = _roleManager.DeleteAsync(applicationRole).Result;
            }
            return RedirectToAction("Index");
        }

        private bool RoleExists(long id)
        {
            return _context.AspNetRoles.Any(e => e.Id == id);
        }
    }
}