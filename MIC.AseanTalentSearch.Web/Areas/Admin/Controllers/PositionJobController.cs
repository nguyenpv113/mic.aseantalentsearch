using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.Areas.Admin.Models;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Routing;
using ReflectionIT.Mvc.Paging;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class PositionJobController : Controller
    {
        private readonly AseanTalentSearchContext _context;

        public PositionJobController(AseanTalentSearchContext context)
        {
            _context = context;
        }

        // GET: Admin/PositionJob
        public async Task<IActionResult> Index(int? page, int filter=1)
        {

            List<SelectListItem> activelist = new List<SelectListItem>();
            activelist.Add(new SelectListItem
            {
                Text = "Not Activated",
                Value = "1"
            });
            activelist.Add(new SelectListItem
            {
                Text = "Activated",
                Value = "2"
            });
            activelist.Add(new SelectListItem
            {
                Text = "All",
                Value = "3"
            });

            var selectList = new SelectList(activelist, "Value", "Text", filter);
            ViewBag.ActiveList = selectList;


            int pageSize = 10;
            int pageIndex = page ?? 1;


            //var model = _context.PositionJob.AsNoTracking()
            //    .Include(p => p.Company)
            //    .Include(p => p.EducationLevel)
            //    .Include(p => p.JobLevel)
            //    .Include(p => p.JobType)
            //    .AsQueryable();
            //if (filter != null)
            //{
            //    if (filter == 1)
            //    {
            //        model = model
            //            .Where(m => m.JobStatus == false || m.JobStatus == null);
            //    }
            //    if (filter == 2)
            //    {
            //        model = model
            //          .Where(m => m.JobStatus == true);
            //    }
            //}
            //else
            //{
            //    model = model
            //        .Where(m => m.JobStatus == false || m.JobStatus == null);
            //}

            //var data = await PagingList<PositionJob>.CreateAsync(model, pageSize,pageIndex, "CreatedDate", "CreatedDate");
            //data.RouteValue = new RouteValueDictionary
            //{{"Filter",filter}};
            var model = new List<PositionJob>();
            if (filter == 1)
            {
                model =await _context.PositionJob.Where(x => x.JobStatus == null || x.JobStatus == false).ToListAsync();
            }
            else if (filter == 2)
            {
                model = await _context.PositionJob.Where(x => x.JobStatus == true).ToListAsync();
            }
            else
            {
                model = await _context.PositionJob.OrderBy(m => m.JobStatus).ToListAsync();
            }




            return View(model.ToPagedList(pageSize, pageIndex));
        }
        // GET: Admin/PositionJob/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            //if (id == null)
            //{
            //    return NotFound();
            //}

            //var positionJob = await _context.PositionJob
            //    .Include(p => p.Company)
            //    .Include(p => p.EducationLevel)
            //    .Include(p => p.JobLevel)
            //    .Include(p => p.JobType)
            //    .SingleOrDefaultAsync(m => m.Id == id);
            //if (positionJob == null)
            //{
            //    return NotFound();
            //}

            //return View(positionJob);
            return View();
        }

        // GET: Admin/PositionJob/Create
        public IActionResult Create()
        {
            ViewData["CompanyId"] = new SelectList(_context.Company, "Id", "Address");
            ViewData["EducationLevelId"] = new SelectList(_context.EducationLevel, "Id", "Name");
            ViewData["JobLevelId"] = new SelectList(_context.JobLevel, "JobLevelId", "JobLevelName");
            ViewData["JobTypeId"] = new SelectList(_context.JobType, "Id", "Name");
            return View();
        }

        // POST: Admin/PositionJob/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CompanyId,JobTypeId,PositionTitle,Code,CreatedDate,OpenDate,ClosedDate,CreatedBy,EducationLevelId,JobLevelId,PayStatus,PayStatusView,MaxPay,MinPay,Currency,JobDescription,YearsOfExperienceRequirement,SkillRequirement,Responsibilities,JobStatus,TravelRequired,TopNumber,Quantity,ModifiedBy,ModifiedDate")] PositionJob positionJob)
        {
            if (ModelState.IsValid)
            {
                _context.Add(positionJob);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["CompanyId"] = new SelectList(_context.Company, "Id", "Address", positionJob.CompanyId);
            ViewData["EducationLevelId"] = new SelectList(_context.EducationLevel, "Id", "Name", positionJob.EducationLevelId);
            ViewData["JobLevelId"] = new SelectList(_context.JobLevel, "JobLevelId", "JobLevelName", positionJob.JobLevelId);
            ViewData["JobTypeId"] = new SelectList(_context.JobType, "Id", "Name", positionJob.JobTypeId);
            return View(positionJob);
        }

        // GET: Admin/PositionJob/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aseanTalentSearchContext = _context.PositionJob.Include(p => p.Company).Include(p => p.EducationLevel).Include(p => p.JobLevel).Include(p => p.JobType).FirstOrDefault(m => m.Id == id);

            if (aseanTalentSearchContext == null)
            {
                return NotFound();
            }

            return View(aseanTalentSearchContext);
        }

        // POST: Admin/PositionJob/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, PositionJob positionJob, bool jobStatus)
        {
            positionJob = _context.PositionJob.Include(p => p.Company).Include(p => p.EducationLevel).Include(p => p.JobLevel).Include(p => p.JobType).FirstOrDefault(m => m.Id == id);
            if (id != positionJob.Id)
            {
                return NotFound();
            }
            if (await _context.PositionJob
                .Where(x => x.Id == id)
                .AnyAsync())
            {
                AspNetUsers currentUser = _context.AspNetUsers.Where(x => x.UserName.Equals(User.Identity.Name)).FirstOrDefault();
                positionJob.ModifiedBy = currentUser.Id;
                positionJob.ModifiedDate = DateTime.Now;
                positionJob.JobStatus = jobStatus;
                _context.PositionJob.Update(positionJob);

                int result = _context.SaveChanges();
                if (result > 0)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    //Error
                }
            }
            else
            {
            }
            return View(positionJob);
        }

        // GET: Admin/PositionJob/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var positionJob = await _context.PositionJob
                .Include(p => p.Company)
                .Include(p => p.EducationLevel)
                .Include(p => p.JobLevel)
                .Include(p => p.JobType)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (positionJob == null)
            {
                return NotFound();
            }

            return View(positionJob);
        }

        // POST: Admin/PositionJob/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var positionJob = await _context.PositionJob.SingleOrDefaultAsync(m => m.Id == id);
            if (positionJob != null)
            {
                try
                {
                    _context.PositionJob.Remove(positionJob);
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    ModelState.AddModelError("", "PositionJob was used. Can not delete!");
                    return View(positionJob);
                }
            }
           
            return RedirectToAction("Index");
        }

        private bool PositionJobExists(int id)
        {
            return _context.PositionJob.Any(e => e.Id == id);
        }
        [HttpGet]
        public async Task<JsonResult> ChangeStatus(int JobId)
        {
            var Job = _context.PositionJob.Find(JobId);
            Job.JobStatus = !Job.JobStatus;
            if (await _context.SaveChangesAsync() > 0)
            {
                return Json(Job);
            }
            else
                return Json(null);
        }
    }
}
