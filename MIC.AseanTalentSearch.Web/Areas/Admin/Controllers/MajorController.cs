using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class MajorController : Controller
    {
        private readonly AseanTalentSearchContext _context;
        public MajorController(AseanTalentSearchContext context)
        {
            _context = context;
        }
        
        public async Task<IActionResult> Index(int? universityId, int? educationId, int? page = 1)
        {
            var aseanTalentSearchContext = _context.Major.Include(m => m.EducationLevel).Include(m => m.University).AsQueryable();
            var majorView = new MajorViewModel { universityId = universityId, educationlevelId = educationId };
            majorView.UniversityList = new SelectList(_context.University, "Id", "Name");
            majorView.EducationLevelList = new SelectList(_context.EducationLevel, "Id", "Name");


            if (majorView.universityId != null)
            {
                if (majorView.educationlevelId != null)
                {
                    aseanTalentSearchContext =
                        aseanTalentSearchContext.Where(m => m.UniversityId == majorView.universityId &&
                                                            m.EducationLevelId == majorView.educationlevelId);
                }
                else
                {
                    aseanTalentSearchContext =
                        aseanTalentSearchContext.Where(m => m.UniversityId == majorView.universityId);
                }
            }
            //ViewData["Major"] = new List<Major>(_context.Major.ToList());
            majorView.Major = await aseanTalentSearchContext.ToListAsync();
            var model = majorView.Major;
            ViewData["Major"] = majorView;
            return View(model);
        }

        // GET: Admin/Major/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var major = await _context.Major
                .Include(m => m.EducationLevel)
                .Include(m => m.University)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (major == null)
            {
                return NotFound();
            }

            return View(major);
        }

        // GET: Admin/Major/Create
        public ActionResult Create(int? universityId, int? educationlevelId)
        {
            ViewData["EducationLevelId"] = new SelectList(_context.EducationLevel, "Id", "Name",universityId);
            ViewData["UniversityId"] = new SelectList(_context.University, "Id", "Name",educationlevelId);
            return View();
        }

        // POST: Admin/Major/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Major major)
        {
            if (ModelState.IsValid)
            {
                if (!await _context.Major.Where(x => x.Name.Equals(major.Name)).AnyAsync())
                {
                    AspNetUsers currentUser = _context.AspNetUsers.Where(x => x.UserName.Equals(User.Identity.Name))
                        .FirstOrDefault();

                    //University university = _context.University.Find(_universityId);
                    //major.UniversityId = university.Id;
                    //EducationLevel educationLevel = _context.EducationLevel.Find(_educationlevelId);
                    //major.EducationLevelId = educationLevel.Id;

                    major.CreatedDate = DateTime.Now;
                    major.CreatedBy = currentUser.Id;

                    await _context.Major.AddAsync(major);
                    int result = await _context.SaveChangesAsync();
                    if (result == 1)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //Error
                    }
                    _context.Major.Add(major);
                }
                else
                {
                    ModelState.AddModelError("", "Major name is exist. Please try another!");
                }
                _context.Add(major);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewData["EducationLevelId"] =
                new SelectList(_context.EducationLevel, "Id", "Name", major.EducationLevelId);
            ViewData["UniversityId"] = new SelectList(_context.University, "Id", "Name", major.UniversityId);
            return View(major);
        }


        // GET: Admin/Major/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var major = await _context.Major.FindAsync(id);
            if (major == null)
            {
                return NotFound();
            }
            ViewData["EducationLevelId"] = new SelectList(_context.EducationLevel, "Id", "Name", major.EducationLevelId);
            ViewData["UniversityId"] = new SelectList(_context.University, "Id", "Name", major.UniversityId);
            return View(major);
        }

        // POST: Admin/Major/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Major major)
        {
            if (id != major.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (!await _context.Major.Where(x => x.Id != major.Id && x.Name.Equals(major.Name)).AnyAsync())
                {
                    AspNetUsers currentUser = _context.AspNetUsers.Where(x => x.UserName.Equals(User.Identity.Name))
                        .FirstOrDefault();
                    major.ModifiedBy = currentUser.Id;
                    major.ModifiedDate = DateTime.Now;
                    _context.Major.Update(major);
                    int result = await _context.SaveChangesAsync();
                    if (result == 1)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //Error
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Major name is exist. Please try another!");
                }
            }
            ViewData["EducationLevelId"] = new SelectList(_context.EducationLevel, "Id", "Name", major.EducationLevelId);
            ViewData["UniversityId"] = new SelectList(_context.University, "Id", "Name", major.UniversityId);
            return View(major);
        }

        // GET: Admin/Major/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var major = await _context.Major.FindAsync(id);
            if (major == null)
            {
                return NotFound();
            }

            return View(major);
        }

        // POST: Admin/Major/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var major = await _context.Major.FindAsync(id);
            if (major != null)
            {
                try
                {
                    _context.Major.Remove(major);
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    ModelState.AddModelError("", "Major was used. Can not delete!");
                    return View(major);
                }
            }
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Search()
        {
            return RedirectToAction("Index");
        }
    }
}
