using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MIC.AseanTalentSearch.Web.EF.Logical;
using Sakura.AspNetCore;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Services;
using MIC.AseanTalentSearch.Web.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class StartupController : Controller
    {
        private readonly INewsManager _newsManager;
        private readonly IImageService _imageService;
        private readonly PathUpLoad _pathUpLoad;
        private IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> _userManager;

        public StartupController(INewsManager newsManager, IImageService imageService,
            IHostingEnvironment env, IOptions<PathUpLoad> pathUpload, UserManager<ApplicationUser> userManager)
        {
            _newsManager = newsManager;
            _imageService = imageService;
            _env = env;
            _pathUpLoad = pathUpload.Value;
            _userManager = userManager;
        }

        public IActionResult Index(int? page)
        {
            int pageIndex = page ?? 1;
            var model = _newsManager.GetAllNews();
            model = model.Where(x => !x.Type).ToList();
            return View(model);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
                return NotFound();
            var model = _newsManager.GetNewsById(id.Value, false);
            if (model == null)
                return NotFound();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(News model, IFormFile formFile)
        {
            if (ModelState.IsValid)
            {
                if (formFile != null)
                {
                    string imagePath = string.Empty;
                    if (_imageService.Validate(formFile))
                    {
                        try
                        {
                            //upload new logo
                            imagePath = _imageService.SaveAsync(formFile, _pathUpLoad.FrontImage).Result;

                            //delete old logo
                            try
                            {
                                System.IO.File.Delete(_env.WebRootPath + model.Picture);
                            }
                            catch
                            {

                            }
                            //set new logo
                            model.Picture = _pathUpLoad.FrontImage + imagePath;
                        }
                        catch (Exception e)
                        {
                            ModelState.AddModelError("", "Sorry an error occurred saving to the database, please try again");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "The logo file must be gif, png, jpeg or jpg and less than 2MB in size");
                    }
                }
                model.ModifiedDate = DateTime.Now;
                model.ModifiedBy = _userManager.FindByNameAsync(User.Identity.Name).Result.Id;
                if (_newsManager.UpdateNews(model) > 0)
                    return RedirectToAction("Index");
                ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
                return View(model);
            }
            ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
            return View(model);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(News model, IFormFile formFile)
        {
            if (ModelState.IsValid)
            {
                if (formFile != null)
                {
                    string imagePath = string.Empty;
                    if (_imageService.Validate(formFile))
                    {
                        try
                        {
                            //upload new logo
                            imagePath = _imageService.SaveAsync(formFile, _pathUpLoad.FrontImage).Result;

                            //delete old logo
                            try
                            {
                                System.IO.File.Delete(_env.WebRootPath + model.Picture);
                            }
                            catch
                            {

                            }
                            //set new logo
                            model.Picture = _pathUpLoad.FrontImage + imagePath;
                        }
                        catch (Exception e)
                        {
                            ModelState.AddModelError("", "Sorry an error occurred saving to the database, please try again");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "The logo file must be gif, png, jpeg or jpg and less than 2MB in size");
                    }
                }
                model.Type = false;
                model.CreatedBy = _userManager.FindByNameAsync(User.Identity.Name).Result.Id;
                model.ModifiedBy = _userManager.FindByNameAsync(User.Identity.Name).Result.Id;
                if (_newsManager.InsertNews(model) != null)
                    return RedirectToAction("Index");
                ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
                return View(model);
            }
            ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
            return View(model);
        }

        public IActionResult Delete(int? Id)
        {
            if (Id == null)
            {
                return NotFound();
            }
            var language = _newsManager.GetNewsById(Id.Value, false);
            if (language == null)
            {
                return NotFound();
            }
            return View(language);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int NewsId)
        {
            if (_newsManager.DeleteNewsById(NewsId) > 0)
                return RedirectToAction("Index");
            ModelState.AddModelError("", "Can't delete this Startup");
            return View();
        }

        [HttpPost]
        public JsonResult ChangeStatus(int Id)
        {
            try
            {
                var _new = _newsManager.GetNewsById(Id, false);
                _new.Status = !_new.Status;

                if (_newsManager.UpdateNews(_new) > 0)
                {
                    return Json("Update successed!");
                }
            }
            catch
            {
            }
            return Json("Update Error!");
        }
    }
}