using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.IO.Compression;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class LayoutController : Controller
    {

        private readonly AseanTalentSearchContext _context;

        private IHostingEnvironment _env;
        string themeFolder;
        string tempFolder = "Temp";
        string tempNameUpload = "temptheme.zip";



        public LayoutController(AseanTalentSearchContext context, IHostingEnvironment env)
        {
            _context = context;
            _env = env;
            themeFolder = Path.Combine(_env.WebRootPath, "Themes");
        }
        public async Task<IActionResult> Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = page ?? 1;

            var model = await _context.Layout.ToListAsync();
            return View(model.ToPagedList(pageSize, pageIndex));
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Layout layout, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    layout.Title = layout.Title.Trim();
                    // check title them contain space with split(" ") method length;
                    if (layout.Title.Split(' ').Length == 1)
                    {
                        if (!await _context.Layout.Where(x => x.Title.Equals(layout.Title, StringComparison.OrdinalIgnoreCase)).AnyAsync())
                        {
                            if (validate(file))
                            {
                                //copyfile upload to temp folder
                                string fullPahtTempFolder = Path.Combine(themeFolder, tempFolder);
                                if (!Directory.Exists(fullPahtTempFolder))
                                {
                                    Directory.CreateDirectory(fullPahtTempFolder);
                                }
                                string zipTemp = Path.Combine(fullPahtTempFolder, tempNameUpload);
                                using (var fileStream = new FileStream(zipTemp, FileMode.Create))
                                {
                                    await file.CopyToAsync(fileStream);
                                    fileStream.Flush();
                                }
                                //extra file zip end delete zip file
                                extra(zipTemp, fullPahtTempFolder);
                                System.IO.File.Delete(zipTemp);
                                //check number of folder (theme format...)
                                string[] folderInZip = System.IO.Directory.GetDirectories(fullPahtTempFolder);
                                if (folderInZip.Length == 1)
                                {
                                    //Move theme folder to Themes with new name
                                    Directory.Move(folderInZip[0], Path.Combine(themeFolder, layout.Title));

                                    //Delete Temp Folder
                                    Directory.Delete(fullPahtTempFolder, true);

                                    //insert to database
                                    _context.Layout.Add(layout);
                                    int result = _context.SaveChanges();
                                    if (result == 1)
                                    {
                                        return RedirectToAction("Index");
                                    }
                                    else
                                    {
                                        //error
                                    }
                                }
                                else
                                {
                                    Directory.Delete(fullPahtTempFolder, true);
                                    ModelState.AddModelError("", "Upload one theme only. Current you upload " + folderInZip.Length + " themes");
                                }
                            }
                            else
                            {
                                ModelState.AddModelError("", "The file must be ZIP and less than 100MB in size");
                            }


                        }
                        else
                        {
                            ModelState.AddModelError("", "Theme name is exist. Please try another!");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Theme name can not contain space");
                    }



                }
                else
                {
                    ModelState.AddModelError("", "Theme is null. Please choose file(zip) to upload new theme!");
                }

            }

            return View(layout);
        }


        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var layout = _context.Layout.Where(x => x.Id == id).FirstOrDefault();
            if (layout == null)
            {
                return NotFound();
            }
            return View(layout);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Layout layout, IFormFile file)
        {
            if (id != layout.Id)
            {
                return NotFound();
            }
            Layout oldLayout = _context.Layout.Find(id);

            _context.Entry(oldLayout).State = EntityState.Detached;

            if (ModelState.IsValid)
            {
                layout.Title = layout.Title.Trim();
                if (!await _context.Layout.Where(x => x.Id != id && x.Title.Equals(layout.Title, StringComparison.OrdinalIgnoreCase)).AnyAsync())
                {
                    if (layout.Title.Split(' ').Length == 1)
                    {
                        //up new them
                        if (file != null)
                        {
                            if (validate(file))
                            {
                                //copyfile upload to temp folder
                                string fullPahtTempFolder = Path.Combine(themeFolder, tempFolder);
                                if (!Directory.Exists(fullPahtTempFolder))
                                {
                                    Directory.CreateDirectory(fullPahtTempFolder);
                                }
                                string zipTemp = Path.Combine(fullPahtTempFolder, tempNameUpload);
                                using (var fileStream = new FileStream(zipTemp, FileMode.Create))
                                {
                                    await file.CopyToAsync(fileStream);
                                    fileStream.Flush();
                                }
                                //extra file zip end delete zip file
                                extra(zipTemp, fullPahtTempFolder);
                                System.IO.File.Delete(zipTemp);
                                //check number of folder (theme format...)
                                string[] folderInZip = System.IO.Directory.GetDirectories(fullPahtTempFolder);
                                if (folderInZip.Length == 1)
                                {
                                    //delete old folder
                                    Directory.Delete(Path.Combine(themeFolder, oldLayout.Title), true);

                                    //Move theme folder to Themes with new name
                                    Directory.Move(folderInZip[0], Path.Combine(themeFolder, layout.Title));

                                    //Delete Temp Folder
                                    Directory.Delete(fullPahtTempFolder, true);

                                    //update to database
                                    _context.Layout.Update(layout);
                                    int result = _context.SaveChanges();
                                    if (result == 1)
                                    {
                                        return RedirectToAction("Index");
                                    }
                                    else
                                    {
                                        //error
                                    }
                                }
                                else
                                {
                                    Directory.Delete(fullPahtTempFolder, true);
                                    ModelState.AddModelError("", "Upload one theme only. Current you upload " + folderInZip.Length + " themes");
                                }
                            }
                            else
                            {
                                ModelState.AddModelError("", "The file must be ZIP and less than 100MB in size");
                            }



                        }
                        else
                        {
                            //rename theme
                            if (!oldLayout.Title.Equals(layout.Title, StringComparison.OrdinalIgnoreCase))
                            {
                                string oldFolder = Path.Combine(themeFolder, oldLayout.Title);
                                string newFolder = Path.Combine(themeFolder, layout.Title);
                                //Move theme folder to Themes with new name
                                Directory.Move(oldFolder, newFolder);

                                //Delete Temp Folder
                                // Directory.Delete(oldFolder, true);

                                

                                _context.Layout.Update(layout);
                                int result = await _context.SaveChangesAsync();
                                if (result == 1)
                                {
                                    return RedirectToAction("Index");
                                }
                                else
                                {
                                    //error
                                }
                            }
                            else
                            {
                                return RedirectToAction("Index");
                            }
                           

                        }


                    }
                    else
                    {
                        ModelState.AddModelError("", "Theme name can not contain space");
                    }

                }
                else
                {
                    ModelState.AddModelError("", "Theme name is exist. Please try another!");
                }
            }
            return View(layout);
        }



        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var layout = await _context.Layout.FindAsync(id);
            if (layout == null)
            {
                return NotFound();
            }
            return View(layout);
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var layout = await _context.Layout.FindAsync(id);
            if (layout != null)
            {
                try
                {                  
                    //delete old folder
                    Directory.Delete(Path.Combine(themeFolder, layout.Title), true);

                    _context.Layout.Remove(layout);
                    await _context.SaveChangesAsync();
                }
                catch(Exception e)
                {
                    ModelState.AddModelError("", "Theme was used. Can not delete!");
                    return View(layout);

                }
              

            }
            return RedirectToAction("Index");
        }

        private bool validate(IFormFile file)
        {
            // ZipArchive

            string fileExtentison = System.IO.Path.GetExtension(file.FileName).ToLower();
            string[] allowedFileType = { ".zip" };
            if (file.Length > 0 && file.Length < 104857600 && allowedFileType.Contains(fileExtentison))
            {
                return true;
            }
            return false;
        }
        private void extra(string source, string des)
        {
            using (ZipArchive archive = ZipFile.Open(source, ZipArchiveMode.Read))
            {
                foreach (ZipArchiveEntry file in archive.Entries)
                {
                    string completeFileName = Path.Combine(des, file.FullName);
                    if (file.Name == "")
                    {// Assuming Empty for Directory
                     // Debug.WriteLine("_//__" + completeFileName);
                        Directory.CreateDirectory(Path.GetDirectoryName(completeFileName));
                        continue;
                    }
                    file.ExtractToFile(completeFileName, true);
                }
            }
        }
    }
}