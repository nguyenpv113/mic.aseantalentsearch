using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.EntityFrameworkCore;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class JobTypeController : Controller
    {
        private readonly AseanTalentSearchContext _context;
        public JobTypeController(AseanTalentSearchContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = page ?? 1;
            var model = await _context.JobType.ToListAsync();
            return View(model.ToPagedList(pageSize, pageIndex));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(JobType jobType)
        {
            if (ModelState.IsValid)
            {
                if (!await _context.JobType.Where(x => x.Name.Equals(jobType.Name, StringComparison.OrdinalIgnoreCase)).AnyAsync())
                {
                    _context.JobType.Add(jobType);
                    int result = _context.SaveChanges();
                    if (result == 1)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                    }
                }
                else
                {
                    ModelState.AddModelError("", "JobType name is exist. Please try another!");
                }
            }

            return View(jobType);
        }


        public IActionResult Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var jobType = _context.JobType.Where(x => x.Id == id).FirstOrDefault();
            if (jobType == null)
            {
                return NotFound();
            }
            return View(jobType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, JobType jobType)
        {
            if (id != jobType.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (!await _context.JobType.Where(x => x.Id != id && x.Name.Equals(jobType.Name,StringComparison.OrdinalIgnoreCase)).AnyAsync())
                {
                   
                    _context.JobType.Update(jobType);
                    int result = await _context.SaveChangesAsync();
                    if (result == 1)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                    }
                }
                else
                {
                    ModelState.AddModelError("", "JobType name is exist. Please try another!");
                }
            }
            return View(jobType);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var jobType = await _context.JobType.FindAsync(id);
            if (jobType == null)
            {
                return NotFound();
            }
            return View(jobType);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var jobType = await _context.JobType.FindAsync(id);
            if (jobType != null)
            {
                try
                {
                    _context.JobType.Remove(jobType);
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    ModelState.AddModelError("", "JobType was used. Can not delete!");
                    return View(jobType);
                }
            }
            return RedirectToAction("Index");
        }
    }
}