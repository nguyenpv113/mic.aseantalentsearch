using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.AspNetCore.Authorization;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class CompanyController : Controller
    {
        private readonly AseanTalentSearchContext _context;

        public CompanyController(AseanTalentSearchContext context)
        {
            _context = context;
        }

        // GET: Admin/Company
        public IActionResult Index(int ?page)
        {
            int pageIndex = page ?? 1;

            var model = _context.Company.OrderByDescending(x=>x.ActiveStatus).ThenBy(x => x.Name);

            return View(model);
        }

        // GET: Admin/Company/Details/5
        //public async Task<IActionResult> Details(long? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var company = await _context.Company
        //        .Include(c => c.IdNavigation)
        //        .Include(c => c.Location)
        //        .SingleOrDefaultAsync(m => m.Id == id);
        //    if (company == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(company);
        //}

        // GET: Admin/Company/Create
        //public ActionResult Create()
        //{
        //    //ViewData["Id"] = new SelectList(_context.AspNetUsers, "Id", "Id");
        //    ViewData["LocationId"] = new SelectList(_context.Location, "Id", "Name");
        //    return View();
        //}

        // POST: Admin/Company/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create(Company company)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        if (!await _context.Company.Where(x => x.Name.Equals(company.Name)).AnyAsync())
        //        {
        //            AspNetUsers currentUsers = _context.AspNetUsers.Where(x => x.UserName.Equals(User.Identity.Name))
        //                .FirstOrDefault();
        //            company.CreatedDate = DateTime.Now;
        //            company.ModifiedBy = currentUsers.Id;
        //            company.ModifiedDate = DateTime.Now;
        //            if (company.ActiveStatus!=null&&company.ActiveStatus==true)
        //            {
        //                company.ActiveDate = DateTime.Now;
        //                company.ActiveBy = currentUsers.Id;
        //            }
        //            await _context.Company.AddAsync(company);
        //            int result = await _context.SaveChangesAsync();
        //            if (result == 1)
        //            {
        //                return RedirectToAction("Index");
        //            }
        //            else
        //            {
        //                //error
        //            }
        //        }
        //    }
        //    else
        //    {
        //        ModelState.AddModelError("","The Company name is exist. Please try another!");
        //    }
        //    return View(company);
        //    //_context.Add(company);
        //    //await _context.SaveChangesAsync();
        //    //return RedirectToAction("Index");
        //}

        // GET: Admin/Company/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var company = await _context.Company.SingleOrDefaultAsync(m => m.Id == id);
            if (company == null)
            {
                return NotFound();
            }
            //ViewData["Id"] = new SelectList(_context.AspNetUsers, "Id", "Id", company.Id);
            //ViewData["LocationId"] = new SelectList(_context.Location, "Id", "Name", company.LocationId);
            return View(company);
        }

        // POST: Admin/Company/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(long id, [Bind("Id,Name,Website,Address,Email,LocationId,Taxcode,Phone,ContactPhone,ContactPerson,ContactPersonGender,Owner,CompanySize,Logo,Introduction,Facebook,Instagram,LinkedIn,Twitter,AttractivePolicies,ActiveStatus,ActiveDate,CreatedDate,ActiveBy,ModifiedBy,ModifiedDate")] Company company)
        //{
        //    if (id != company.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(company);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!CompanyExists(company.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction("Index");
        //    }
        //    ViewData["Id"] = new SelectList(_context.AspNetUsers, "Id", "Id", company.Id);
        //    ViewData["LocationId"] = new SelectList(_context.Location, "Id", "Name", company.LocationId);
        //    return View(company);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,ActiveStatus")] Company company)
        {
            if (id != company.Id)
            {
                return NotFound();
            }

            try
            {
                var companyUpdate = _context.Company.Find(id);
                companyUpdate.ActiveStatus = company.ActiveStatus;
                await _context.SaveChangesAsync();            
                return RedirectToAction("Index");
            }
            catch
            {

            }
            return View(company);
        }


        // GET: Admin/Company/Delete/5
        //public async Task<IActionResult> Delete(long? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var company = await _context.Company
        //        .Include(c => c.IdNavigation)
        //        .Include(c => c.Location)
        //        .SingleOrDefaultAsync(m => m.Id == id);
        //    if (company == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(company);
        //}

        //// POST: Admin/Company/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(long id)
        //{
        //    var company = await _context.Company.SingleOrDefaultAsync(m => m.Id == id);
        //    _context.Company.Remove(company);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        private bool CompanyExists(long id)
        {
            return _context.Company.Any(e => e.Id == id);
        }
    }
}
