using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.AspNetCore.Authorization;
using Sakura.AspNetCore;

namespace MIC.AseanTalentSearch.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class BenefitController : Controller
    {
        private readonly AseanTalentSearchContext _context;

        public BenefitController(AseanTalentSearchContext context)
        {
            _context = context;
        }

        // GET: Admin/Benefit
        public async Task<IActionResult> Index(int? page)
        {
            int pageIndex = page ?? 1;
            var model = await _context.Benefit.ToListAsync();
            return View(model);
        }

        // GET: Admin/Benefit/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var benefit = await _context.Benefit
                .SingleOrDefaultAsync(m => m.Id == id);
            if (benefit == null)
            {
                return NotFound();
            }

            return View(benefit);
        }

        // GET: Admin/Benefit/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Benefit/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Benefit benefit)
        {
            if (ModelState.IsValid)
            {

                if (!await _context.Benefit.Where(x => x.Name.Equals(benefit.Name, StringComparison.OrdinalIgnoreCase)).AnyAsync())
                {
                    _context.Benefit.Add(benefit);
                    int result = _context.SaveChanges();
                    if (result == 1)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Benefit level name is exist, please try another!");
                }
            }
            return View(benefit);
        }

        // GET: Admin/Benefit/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var benefit = await _context.Benefit.SingleOrDefaultAsync(m => m.Id == id);
            if (benefit == null)
            {
                return NotFound();
            }
            return View(benefit);
        }

        // POST: Admin/Benefit/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, Benefit benefit)
        {
            if (id != benefit.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (!await _context.Benefit.Where(x => x.Id != id && x.Name.Equals(benefit.Name)).AnyAsync())
                {
                    _context.Benefit.Update(benefit);
                    int result = await _context.SaveChangesAsync();
                    if (result == 1)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //Error
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Benefit name is exist. Please try another!");
                }
            }
            return View(benefit);
        }

        // GET: Admin/Benefit/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var benefit = await _context.Benefit.FindAsync(id);
            if (benefit == null)
            {
                return NotFound();
            }

            return View(benefit);
        }

        // POST: Admin/Benefit/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var benefit = await _context.Benefit.FindAsync(id);
            if (benefit != null)
            {
                try
                {
                    _context.Benefit.Remove(benefit);
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    ModelState.AddModelError("", "Benefit was used. Can not delete!");
                    return View(benefit);
                }

            }
            return RedirectToAction("Index");
        }

        private bool BenefitExists(int id)
        {
            return _context.Benefit.Any(e => e.Id == id);
        }
    }
}
