﻿using MIC.AseanTalentSearch.Web.Areas.Instructor.Models;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MIC.AseanTalentSearch.Web.Areas.Instructor.ViewComponents
{
    public class SlidebarViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInmanager;

        private AseanTalentSearchContext _context;
        public SlidebarViewComponent(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInmanager = signInManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            InstructorSlideBarViewModel slideModel = new InstructorSlideBarViewModel();
            slideModel.IsAdmin = false; 

           var Currentuser = await _userManager.FindByNameAsync(User.Identity.Name);
            var lClams = await _userManager.GetClaimsAsync(Currentuser);
            foreach(var item in lClams)
            {
                if (item.Value == "Instructor Admin")
                {
                    slideModel.IsAdmin = true;
                }
            }

            var generalProfile = _context.GeneralProfile.SingleOrDefault(m => m.Id == Currentuser.Id);

            slideModel.FullName = Currentuser.UserName;
            slideModel.Image = generalProfile?.Image;
           

            return View(slideModel);
        }
    }
}
