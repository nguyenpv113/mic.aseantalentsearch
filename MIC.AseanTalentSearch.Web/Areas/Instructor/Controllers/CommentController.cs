using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.Areas.Instructor.Models;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Areas.Instructor.Controllers
{
    [Area("Instructor")]
    public class CommentController : Controller
    {
        private readonly AseanTalentSearchContext _context;

        public CommentController(AseanTalentSearchContext context)
        {
            _context = context;    
        }

        // GET: Instructor/Comment
        public async Task<IActionResult> Index()
        {
            var aseanTalentSearchContext = _context.CandidateComment.Include(c => c.Candidate).Include(c => c.UserComment);
            return View(await aseanTalentSearchContext.ToListAsync());
        }

        // GET: Instructor/Comment/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidateComment = await _context.CandidateComment
                .Include(c => c.Candidate)
                .Include(c => c.UserComment)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (candidateComment == null)
            {
                return NotFound();
            }

            return View(candidateComment);
        }

        // GET: Instructor/Comment/Create
        public IActionResult Create()
        {
            ViewData["CandidateId"] = new SelectList(_context.Candidate, "Id", "Id");
            ViewData["UserCommentId"] = new SelectList(_context.AspNetUsers, "Id", "Id");
            return View();
        }

        // POST: Instructor/Comment/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UserCommentId,CandidateId,Comment,StarRating,CreatedDate,ModifiedDate")] CandidateComment candidateComment)
        {
            if (ModelState.IsValid)
            {
                _context.Add(candidateComment);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["CandidateId"] = new SelectList(_context.Candidate, "Id", "Id", candidateComment.CandidateId);
            ViewData["UserCommentId"] = new SelectList(_context.AspNetUsers, "Id", "Id", candidateComment.UserCommentId);
            return View(candidateComment);
        }

        // GET: Instructor/Comment/Edit/5
        public async Task<IActionResult> Edit(int? candidateId)
        {
            CommentViewModel comment = new CommentViewModel();

            if (candidateId == null)
            {
                return NotFound();
            }

            var candidateComment = await _context.CandidateComment.Include(m => m.Candidate)
                .FirstOrDefaultAsync(m => m.CandidateId == candidateId);
            var userComment =
                await _context.AspNetUsers.Include(m => m.GeneralProfile)
                    .Include(m => m.Instructor)
                    .SingleOrDefaultAsync(m => m.UserName == User.Identity.Name);

            // Populate data
            comment.CandidateId = (candidateComment?.CandidateId??candidateId);
            comment.Comment = (candidateComment?.Comment??"");
            comment.UserCommentId = userComment?.Id;          
            return View(comment);
        }

        // POST: Instructor/Comment/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("UserCommentId,CandidateId,Comment")]CommentViewModel model)
        {
            var a = await _context.Benefit.ToListAsync();
            //if (id != candidateComment.Id)
            //{
            //    return NotFound();
            //}

            //if (ModelState.IsValid)
            //{
            //    try
            //    {
            //        _context.Update(candidateComment);
            //        await _context.SaveChangesAsync();
            //    }
            //    catch (DbUpdateConcurrencyException)
            //    {
            //        if (!CandidateCommentExists(candidateComment.Id))
            //        {
            //            return NotFound();
            //        }
            //        else
            //        {
            //            throw;
            //        }
            //    }
            //    return RedirectToAction("Index");
            //}
            //ViewData["CandidateId"] = new SelectList(_context.Candidate, "Id", "Id", candidateComment.CandidateId);
            //ViewData["UserCommentId"] = new SelectList(_context.AspNetUsers, "Id", "Id", candidateComment.UserCommentId);
            return View(model);
        }

        // GET: Instructor/Comment/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidateComment = await _context.CandidateComment
                .Include(c => c.Candidate)
                .Include(c => c.UserComment)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (candidateComment == null)
            {
                return NotFound();
            }

            return View(candidateComment);
        }

        // POST: Instructor/Comment/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var candidateComment = await _context.CandidateComment.SingleOrDefaultAsync(m => m.Id == id);
            _context.CandidateComment.Remove(candidateComment);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool CandidateCommentExists(int id)
        {
            return _context.CandidateComment.Any(e => e.Id == id);
        }
    }
}
