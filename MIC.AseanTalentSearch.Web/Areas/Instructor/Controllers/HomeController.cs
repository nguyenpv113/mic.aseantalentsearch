﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MIC.AseanTalentSearch.Web.Areas.Instructor.Models;
using MIC.AseanTalentSearch.Web.EF;
using Microsoft.AspNetCore.Identity;
using MIC.AseanTalentSearch.Web.Models;
using Sakura.AspNetCore;
using MIC.AseanTalentSearch.Web.Services;
using MIC.AseanTalentSearch.Web.Settings;
using Microsoft.Extensions.Options;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MIC.AseanTalentSearch.Web.Areas.Instructor.Controllers
{
    [Area("Instructor")]
    [Authorize(Roles = "Instructor")]
    public class HomeController : Controller
    {

        private readonly AseanTalentSearchContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly PathUpLoad _pathUpLoad;
        private readonly IImageService _imageService;

        public HomeController(AseanTalentSearchContext context, UserManager<ApplicationUser> userManager, IOptions<PathUpLoad> pathUpload, IImageService imageService)
        {
            _userManager = userManager;
            _context = context;
            _imageService = imageService;
            _pathUpLoad = pathUpload.Value;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return RedirectToAction("Update");
        }

        public async Task<IActionResult> Update()
        {
            Models.ProfileViewModel model = new Models.ProfileViewModel();
            var user =
                await _context.AspNetUsers.Include(m => m.GeneralProfile)
                    .Include(m => m.Instructor)
                    .SingleOrDefaultAsync(m => m.UserName == User.Identity.Name);

            model.FirstName = user.GeneralProfile?.FirstName;
            model.LastName = user.GeneralProfile?.LastName;
            model.UniversityId = user.Instructor?.UniversityId;
            model.University = new SelectList(_context.University, "Id", "Name");
            model.Department = user.Instructor?.Department;
            model.Position = user.Instructor?.Position;

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Update(Models.ProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user =
                    await _context.AspNetUsers.Include(m => m.GeneralProfile)
                        .Include(m => m.Instructor)
                        .SingleOrDefaultAsync(m => m.UserName == User.Identity.Name);
                var profile = (user.GeneralProfile ?? new GeneralProfile() { Id = user.Id });
                var instructor = (user.Instructor ?? new EF.Instructor() { Id = user.Id });


                if (!model.FirstName.Equals(profile.FirstName) || !model.LastName.Equals(profile.LastName)
                    || model.UniversityId != instructor.UniversityId || !model.Department.Equals(instructor.Department)
                    || !model.Position.Equals(instructor.Position)
                  )
                {
                    profile.FirstName = model.FirstName;
                    profile.LastName = model.LastName;

                    instructor.Name = model.FirstName + " " + model.LastName;
                    instructor.HighestQualification = false;

                    instructor.Status = false;
                    instructor.UniversityId = (int)model.UniversityId;
                    instructor.Department = model.Department;
                    instructor.Position = model.Position;

                    user.GeneralProfile = profile;
                    user.Instructor = instructor;
                }

                try
                {
                    _context.SaveChanges();
                    TempData["message"] = "Update success!";
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", "Sorry an error has occurred saving to the database, please try again");
                }
            }

            model.University = new SelectList(_context.University, "Id", "Name");
            return View(model);
        }
        
        
        public IActionResult ListStudent()
        {
            var student = new List<Candidate>();
            try
            {
                var instructor = _context.AspNetUsers.Include(m => m.GeneralProfile)
                        .Include(m => m.Instructor)
                        .SingleOrDefault(m => m.UserName == User.Identity.Name);
                var universityId = instructor.Instructor?.UniversityId;
                var rs = instructor.Instructor?.Status ?? false;
                if (rs)
                {
                    //var qr = from candidate in _context.Candidate
                    //         join educationHistory in _context.EducationHistory on candidate.Id equals educationHistory.CandidateId
                    //         join major in _context.Major on educationHistory.MajorId equals major.Id
                    //         join university in _context.University on major.UniversityId equals university.Id
                    //         where major.UniversityId == universityId
                    //         select candidate;
                    //student = qr.ToList();
                    student =
                        _context.Candidate.Include(m => m.IdNavigation)
                            .ThenInclude(m => m.AspNetUserRoles)
                            .ThenInclude(m => m.Role)
                            .Include(m => m.Layout)
                            .Include(m => m.EducationHistory)
                            .ThenInclude(m => m.Major)
                            .ThenInclude(m => m.University)
                            .Where(
                                m =>
                                    m.EducationHistory.Any(u => u.Major.Id == u.MajorId && u.Major.UniversityId == universityId) &&
                                    m.IdNavigation.AspNetUserRoles.Any(r => r.Role.Name == "Candidate"))
                            .ToList();

                }
                foreach (var item in student)
                {
                    List<EducationHistory> listM = new List<EducationHistory>();
                    var countList = item.EducationHistory.Count;
                    if (countList > 0)
                    {
                        foreach (var major in item.EducationHistory)
                        {
                            if (major.Major.UniversityId != universityId)
                                listM.Add(major);
                        }

                        foreach (var major in listM)
                        {
                            item.EducationHistory.Remove(major);
                        }
                    }

                }
                return View(student);
            }
            catch
            {
                return View(null);
            }
        }

        
        public async Task<IActionResult> CommentCandidate(long? candidateId)
        {
            CommentViewModel comment = new CommentViewModel();


            if (candidateId == null)
            {
                return NotFound();
            }

            var instructor =
               await _context.AspNetUsers.Include(m => m.GeneralProfile)
                   .Include(m => m.Instructor)
                   .SingleOrDefaultAsync(m => m.UserName == User.Identity.Name);
            int universityId = instructor.Instructor.UniversityId;
            var student =
                await _context.Candidate.Include(m => m.IdNavigation)
                    .ThenInclude(m => m.AspNetUserRoles)
                    .ThenInclude(m => m.Role)
                    .Include(m => m.Layout)
                    .Include(m => m.EducationHistory)
                    .ThenInclude(m => m.Major)
                    .ThenInclude(m => m.University)
                    .Where(m => m.EducationHistory.Any(u => u.Major.UniversityId == universityId))
                    .SingleOrDefaultAsync(
                        m =>
                            m.Id == candidateId &&
                            m.IdNavigation.AspNetUserRoles.Any(r => r.Role.Name == "Candidate"));

            if (student == null)
            {
                return NotFound();
            }

            var commentToUpdate =
                _context.CandidateComment.SingleOrDefault(
                    c => c.CandidateId == student.Id && c.UserCommentId == instructor.Id);

            ViewData["SudentName"] = student.FullName;

            comment.UserCommentId = instructor.Id;
            comment.CandidateId = student.Id;
            comment.Comment = commentToUpdate?.Comment ?? "";
            return View(comment);
        }

        [HttpPost]
        public IActionResult CommentCandidate(CommentViewModel model)
        {

            if (ModelState.IsValid)
            {
                var instructor =
                    _context.AspNetUsers.Include(m => m.GeneralProfile)
                        .Include(m => m.Instructor)
                        .SingleOrDefault(m => m.UserName == User.Identity.Name);
                int universityId = instructor.Instructor.UniversityId;
                var student =
                    _context.Candidate.Include(m => m.IdNavigation)
                        .ThenInclude(m => m.AspNetUserRoles)
                        .ThenInclude(m => m.Role)
                        .Include(m => m.Layout)
                        .Include(m => m.EducationHistory)
                        .ThenInclude(m => m.Major)
                        .ThenInclude(m => m.University)
                        .Where(m => m.EducationHistory.Any(u => u.Major.UniversityId == universityId))
                        .SingleOrDefault(
                            m =>
                                m.Id == model.CandidateId &&
                                m.IdNavigation.AspNetUserRoles.Any(r => r.Role.Name == "Candidate"));

                if (student == null)
                {
                    return NotFound();
                }

                ViewData["SudentName"] = student.FullName;
                model.UserCommentId = instructor.Id;
                model.CandidateId = student.Id;

                var commentToUpdate =
                    _context.CandidateComment.SingleOrDefault(
                        c => c.CandidateId == student.Id && c.UserCommentId == instructor.Id);

                if (commentToUpdate == null)
                {
                    commentToUpdate = new CandidateComment
                    {
                        UserCommentId = instructor.Id,
                        CandidateId = student.Id,
                        Comment = model.Comment,
                        UniversityId = universityId
                    };
                    _context.CandidateComment.Add(commentToUpdate);
                    _context.SaveChanges();
                    TempData["message"] = "Comment success!";
                    return RedirectToAction("ListStudent");
                }
                else
                {
                    commentToUpdate.Comment = model.Comment;
                    commentToUpdate.UniversityId = universityId;
                    _context.CandidateComment.Update(commentToUpdate);
                    _context.SaveChanges();
                    TempData["message"] = "Comment success!";
                    return RedirectToAction("ListStudent");
                }
            }
            return View(model);
        }

        public async Task<IActionResult> ListInstructor(int? page)
        {
            int pageSize = 10;
            int pageIndex = page ?? 1;
            var CurrentUser = await _context.AspNetUsers.Include(m => m.GeneralProfile)
                .Include(m => m.Instructor)
                .SingleOrDefaultAsync(m => m.UserName == User.Identity.Name);
            var model = new InstructorManager(_context).GetListInstructorUniversityId(CurrentUser.Instructor.UniversityId);
            var user = await _userManager.FindByIdAsync(CurrentUser.Id.ToString());
            var lClams = await _userManager.GetClaimsAsync(user);
            foreach (var item in lClams)
            {
                if (item.Value == "Instructor Admin")
                {
                    //dùng datatable để phân trang nên không cần phân trang ở admin
                    //return View(model.ToPagedList(pageSize, pageIndex));
                    return View(model.ToList());

                }
            }
            return RedirectToAction("Update");
        }

        [HttpGet]
        public async Task<IActionResult> ActiveInstructor(long id)
        {
            var Instructor = await new InstructorManager(_context).GetInstrustor(id);
            return View(Instructor);
        }

        [HttpPost]
        public async Task<IActionResult> ActiveInstructor(EF.Instructor user)
        {
            await new InstructorManager(_context).UpdateStatusInstructor(user);
            return RedirectToAction("ListInstructor");

        }


        //upload avatar instructor
        [HttpPost]
        public async Task<IActionResult> UploadAvatar()
        {
            string imagePath = String.Empty;
            var file = Request.Form.Files[0];
            if (file != null)
            {
                if (_imageService.Validate(file))
                {
                    try
                    {
                        imagePath = _pathUpLoad.FrontImage + await _imageService.SaveAsync(file, _pathUpLoad.FrontImage);
                    }
                    catch (Exception e)
                    {
                        TempData["error"] = "Sorry an error occurred saving to the database, please try again";
                        ModelState.AddModelError("",
                            "Sorry an error occurred saving to the database, please try again");
                    }
                }
                else
                {
                    TempData["error"] = "The file must be gif, png, jpeg or jpg and less than 2MB in size";
                    ModelState.AddModelError("", "The file must be gif, png, jpeg or jpg and less than 2MB in size");
                }
            }
            else
            {
                TempData["error"] = "Please choose a file";
                ModelState.AddModelError("", "Please choose a file");
            }

            if (ModelState.IsValid)
            {
                var user = _context.AspNetUsers.Include(m => m.Candidate).Include(m => m.GeneralProfile).SingleOrDefault(m => m.UserName == User.Identity.Name);
                if (user != null)
                {
                    var profile = (user.GeneralProfile ?? new GeneralProfile() { Id = user.Id });

                    profile.Image = imagePath;
                    profile.FirstName = profile.FirstName ?? string.Empty;
                    profile.LastName = profile.LastName ?? string.Empty;

                    // user.Candidate.Image = imagePath;
                    //user.GeneralProfile.Image = imagePath;

                    user.GeneralProfile = profile;
                    _context.Update(user);

                    try
                    {
                        await _context.SaveChangesAsync();
                        TempData["message"] = "Success!";
                    }
                    catch (Exception e)
                    {
                        TempData["error"] = "Sorry an error occurred saving to the database, please try again";
                        ModelState.AddModelError("",
                            "Sorry an error occurred saving to the database, please try again");
                    }
                }
            }
            return Json(new { success = true });
        }
    }
}
