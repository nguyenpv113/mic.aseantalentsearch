﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Areas.Instructor.Models
{
    public class CommentViewModel
    {
        public long? CandidateId { get; set; }
        public long? UserCommentId { get; set; }
        public string Comment { get; set; }
    }
}
