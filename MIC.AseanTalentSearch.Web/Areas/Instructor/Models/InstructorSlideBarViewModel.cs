﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MIC.AseanTalentSearch.Web.Areas.Instructor.Models
{
    public class InstructorSlideBarViewModel
    {
        public string FullName { get; set; }

        public string Image { get; set; }

        public bool IsAdmin { get; set; }

    }
}
