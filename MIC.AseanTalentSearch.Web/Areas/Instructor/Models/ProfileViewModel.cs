﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MIC.AseanTalentSearch.Web.Areas.Instructor.Models
{
    public class ProfileViewModel
    {
        [Display(Name = "First name")]
        [Required]
        public string FirstName { get; set; }
        [Display(Name = "Last name")]
        [Required]
        public string LastName { get; set; }
        [Display(Name = "University")]
        [Required]
        public long? UniversityId { get; set; }

        [Display(Name = "Department")]
        [Required]
        public string Department { get; set; }
        [Display(Name = "	Position")]
        [Required]
        public string Position { get; set; }
        public SelectList University { get; set; }
    }
}
