﻿using MIC.AseanTalentSearch.Web.EF;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MIC.AseanTalentSearch.Web.Areas.Instructor.Models
{
    public class InstructorManager
    {
        private readonly AseanTalentSearchContext _context;
        public InstructorManager(AseanTalentSearchContext context)
        {
            _context = context;
        }
        public IQueryable<EF.Instructor> GetListInstructorUniversityId(int Uid)
        {
            //return _context.Instructor.Where(m => m.UniversityId == Uid && (m.Status == false || m.Status == null)).Include(m => m.University);
            return _context.Instructor.Where(m => m.UniversityId == Uid).Include(m => m.University);
        }

        public async Task<EF.Instructor> GetInstrustor(long id)
        {
            return await _context.Instructor.Include(m => m.University).Where(m => m.Id == id).FirstAsync();
        }

        public async Task<int> UpdateStatusInstructor(EF.Instructor instructor)
        {
            var newinstructor = await GetInstrustor(instructor.Id);
            newinstructor.Department = instructor.Department;
            newinstructor.Status = instructor.Status;
            return await _context.SaveChangesAsync();
        }
    }
}
