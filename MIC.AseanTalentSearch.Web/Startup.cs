﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MIC.AseanTalentSearch.Web.Data;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;
using MIC.AseanTalentSearch.Web.Services;
using MIC.AseanTalentSearch.Web.Settings;
using ReflectionIT.Mvc.Paging;
using Sakura.AspNetCore.Mvc;

using Microsoft.AspNetCore.Mvc;
using MIC.AseanTalentSearch.Web.EF.Logical;

namespace MIC.AseanTalentSearch.Web
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
               options.UseSqlServer(Configuration.GetConnectionString("AspnetConnection")));

            services.AddDbContext<AseanTalentSearchContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("AseanTalentSearchConnection")));

            services.AddIdentity<ApplicationUser, ApplicationRole>(config =>
            {
                config.SignIn.RequireConfirmedEmail = true;
            })
             .AddEntityFrameworkStores<ApplicationDbContext, long>()
             .AddDefaultTokenProviders();

            services.Configure<SmtpConfig>(Configuration.GetSection("Smtp"));
            services.Configure<PathUpLoad>(Configuration.GetSection("PathUpLoad"));
            services.AddMvc();
            //services.AddMvc(options=>
            //{
            //    options.SslPort = 44398;
            //    options.Filters.Add(new RequireHttpsAttribute());
            //});
            services.AddSession();
            //Add paging service
            services.AddPaging();
            services.AddMemoryCache();            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
            services.AddScoped<ConfigTable>();
            services.AddScoped<IImageService, ImageService>();
            services.AddScoped<INewsManager, NewsManager>();
            services.AddTransient<MultipleLanguage>();
            services.AddTransient<ILocaleStringResource, LocaleStringResourceManager>();

            services.AddBootstrapPagerGenerator(options =>
            {
                // Use default pager options.
                options.ConfigureDefault();
            });

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = false;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;

                // Cookie settings
                options.Cookies.ApplicationCookie.CookieName = "YouAppCookieName";
                options.Cookies.ApplicationCookie.ExpireTimeSpan = TimeSpan.FromDays(150);
                options.Cookies.ApplicationCookie.LoginPath = "/Account/LogIn";
                options.Cookies.ApplicationCookie.LogoutPath = "/Account/LogOff";
                options.Cookies.ApplicationCookie.AccessDeniedPath = "/Account/AccessDenied";
                options.Cookies.ApplicationCookie.AutomaticAuthenticate = true;
                options.Cookies.ApplicationCookie.AuthenticationScheme = Microsoft.AspNetCore.Authentication.Cookies.CookieAuthenticationDefaults.AuthenticationScheme;
                options.Cookies.ApplicationCookie.ReturnUrlParameter = Microsoft.AspNetCore.Authentication.Cookies.CookieAuthenticationDefaults.ReturnUrlParameter;

                // User settings
                options.User.RequireUniqueEmail = true;
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("InstructorStaff", policy => policy.RequireClaim("Instructor Staff"));
                options.AddPolicy("InstructorAdmin", policy => policy.RequireClaim("Instructor Admin"));
            });
            services.AddMvcCore(options =>
            {
            }).AddJsonFormatters();
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseSession();

            app.UseIdentity();



           
            // Add external authentication middleware below. To configure them please see https://go.microsoft.com/fwlink/?LinkID=532715
            app.UseFacebookAuthentication(new FacebookOptions()
            {
                //AppId = Configuration["SocialNetworkLogin:FacebookAppId"],
                //AppSecret = Configuration["SocialNetworkLogin:FacebookAppSecret"]
                AppId = "457411877956560",
                AppSecret = "233027df49676743aca0a46e39b36cbf"

            });
            app.UseGoogleAuthentication(new GoogleOptions()
            {
                ClientId = "630399389969-luuctem1anotb6r3l7em527n4h72vu36.apps.googleusercontent.com",
                ClientSecret = "bHh3IuDtFW5yHJbnBw-bj1mO"
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute("AdminHome",
                    "Admin",
                 defaults: new { area = "Admin", controller = "PositionJob", action = "Index" });

                routes.MapRoute(name: "areaRoute",
                    template: "{area:exists}/{controller=Home}/{action=Index}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            //DbInitializer.SeedRoles(app.ApplicationServices).Wait();
        }
    }
}
