﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using MIC.AseanTalentSearch.Web.EF;
using MIC.AseanTalentSearch.Web.Models;

namespace MIC.AseanTalentSearch.Web.Data
{
    public static class DbInitializer
    {
        public static async Task SeedRoles(IServiceProvider serviceProvider)
        {
            using (var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                var roleManager = serviceProvider.GetRequiredService<RoleManager<ApplicationRole>>();
                var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();

                context.Database.EnsureCreated();

                //// Look for any students.
                //if (context.Roles.Any())
                //{
                //    return;   
                //}

                if (!await roleManager.RoleExistsAsync("Administrator"))
                {
                    ApplicationRole role = new ApplicationRole {Name = "Administrator"};

                    IdentityResult roleResult = await roleManager.CreateAsync(role);

                    if (roleResult.Succeeded)
                    {
                        //Here we create a Admin super user who will maintain the website                  
                        var user = new ApplicationUser
                        {
                            UserName = "administrator@gmail.com",
                            Email = "nguyenpvdtu@gmail.com",
                            EmailConfirmed = true
                        };

                        string password = "Abc123@@@";

                        IdentityResult userResult = await userManager.CreateAsync(user, password);
                        if (userResult.Succeeded)
                        {
                            await userManager.AddToRoleAsync(user, "Administrator");
                        }
                    }
                }

                if (!await roleManager.RoleExistsAsync("Employer"))
                {
                    ApplicationRole role = new ApplicationRole { Name = "Employer" };

                    IdentityResult roleResult = await roleManager.CreateAsync(role);

                    if (roleResult.Succeeded)
                    {
                        //Here we create a Admin super user who will maintain the website                  
                        var user = new ApplicationUser
                        {
                            UserName = "employer@gmail.com",
                            Email = "vnvipper@gmail.com",
                            EmailConfirmed = true
                        };

                        string password = "Abc123@@@";

                        IdentityResult userResult = await userManager.CreateAsync(user, password);
                        if (userResult.Succeeded)
                        {
                            await userManager.AddToRoleAsync(user, "Employer");
                        }
                    }
                }

                if (!await roleManager.RoleExistsAsync("Candidate"))
                {
                    ApplicationRole role = new ApplicationRole { Name = "Candidate" };

                    IdentityResult roleResult = await roleManager.CreateAsync(role);

                    if (roleResult.Succeeded)
                    {
                        //Here we create a Admin super user who will maintain the website                  
                        var user = new ApplicationUser
                        {
                            UserName = "candidate@gmail.com",
                            Email = "voduyhung58@gmail.com",
                            EmailConfirmed = true
                        };

                        string password = "Abc123@@@";

                        IdentityResult userResult = await userManager.CreateAsync(user, password);
                        if (userResult.Succeeded)
                        {
                            await userManager.AddToRoleAsync(user, "Candidate");
                        }
                    }
                }
                if (!await roleManager.RoleExistsAsync("Instructor"))
                {
                    ApplicationRole role = new ApplicationRole { Name = "Instructor" };

                    IdentityResult roleResult = await roleManager.CreateAsync(role);                 
                }
            }
        }
    }
}
