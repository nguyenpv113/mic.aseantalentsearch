﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MIC.AseanTalentSearch.Web.EF;

namespace MIC.AseanTalentSearch.Web.Data
{
    public class ConfigTable
    {
        private readonly AseanTalentSearchContext _context;

        public ConfigTable(AseanTalentSearchContext context)
        {
            _context = context;
        }

        public string GetValue(string key)
        {
            var value = _context.SiteConfig.SingleOrDefault(m => m.Key.Equals(key, StringComparison.OrdinalIgnoreCase))
                               ?.Value ?? String.Empty;
            return value;
        }

        public void SetValue(string key, string value)
        {
            var config =
                _context.SiteConfig.SingleOrDefault(m => m.Key.Equals(key, StringComparison.OrdinalIgnoreCase));

            if (config == null)
            {
                config = new SiteConfig
                {
                    Key = key,
                    Value = value
                };
                _context.Add(config);
            }
            else
            {
                config.Value = value;
                _context.SiteConfig.Update(config);
            }
            
            _context.SaveChanges();
        }
    }
}
