﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MIC.AseanTalentSearch.Web.Data.Migrations
{
    public partial class ChangeIdIdentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropIndex(
            //    name: "IX_AspNetUserRoles_UserId",
            //    table: "AspNetUserRoles");

            //migrationBuilder.DropIndex(
            //    name: "RoleNameIndex",
            //    table: "AspNetRoles");

            //migrationBuilder.AlterColumn<long>(
            //    name: "Id",
            //    table: "AspNetUsers",
            //    nullable: false,
            //    oldClrType: typeof(string))
            //    .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            //migrationBuilder.AlterColumn<long>(
            //    name: "UserId",
            //    table: "AspNetUserTokens",
            //    nullable: false,
            //    oldClrType: typeof(string));

            //migrationBuilder.AlterColumn<long>(
            //    name: "RoleId",
            //    table: "AspNetUserRoles",
            //    nullable: false,
            //    oldClrType: typeof(string));

            //migrationBuilder.AlterColumn<long>(
            //    name: "UserId",
            //    table: "AspNetUserRoles",
            //    nullable: false,
            //    oldClrType: typeof(string));

            //migrationBuilder.AlterColumn<long>(
            //    name: "UserId",
            //    table: "AspNetUserLogins",
            //    nullable: false,
            //    oldClrType: typeof(string));

            //migrationBuilder.AlterColumn<long>(
            //    name: "UserId",
            //    table: "AspNetUserClaims",
            //    nullable: false,
            //    oldClrType: typeof(string));

            //migrationBuilder.AlterColumn<long>(
            //    name: "RoleId",
            //    table: "AspNetRoleClaims",
            //    nullable: false,
            //    oldClrType: typeof(string));

            //migrationBuilder.AlterColumn<long>(
            //    name: "Id",
            //    table: "AspNetRoles",
            //    nullable: false,
            //    oldClrType: typeof(string))
            //    .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            //migrationBuilder.CreateIndex(
            //    name: "RoleNameIndex",
            //    table: "AspNetRoles",
            //    column: "NormalizedName",
            //    unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles");

            migrationBuilder.AlterColumn<string>(
                name: "Id",
                table: "AspNetUsers",
                nullable: false,
                oldClrType: typeof(long))
                .OldAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "AspNetUserTokens",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<string>(
                name: "RoleId",
                table: "AspNetUserRoles",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "AspNetUserRoles",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "AspNetUserLogins",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "AspNetUserClaims",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<string>(
                name: "RoleId",
                table: "AspNetRoleClaims",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<string>(
                name: "Id",
                table: "AspNetRoles",
                nullable: false,
                oldClrType: typeof(long))
                .OldAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_UserId",
                table: "AspNetUserRoles",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName");
        }
    }
}
